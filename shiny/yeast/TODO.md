## 202210

* fix broken data: okazaki_smith12, pola_reijns15, 
* minimally add DOIs for dataSets, 
* add links to legend plots,
* adapt plot height in renderPlot via settings,

### mid-term
* formalize settings changes: getDataSettings/setDataSettings via
data.frame, use same data.frame for display,
* left yaxis: extra plot in layout, use this for mouse-over?
* mouse-over for all: split plotData into separate calls to
renderPlot,
* mobile shiny and allow swiping to browse,
