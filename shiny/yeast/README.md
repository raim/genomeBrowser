
# The `genomeBrowser` as a `shiny` App

## Requirements

To run `genomeBrowser` as a `shiny` app on
your local computer you need:

* `git`,
* git Large File Storage: `[git-lfs](https://git-lfs.github.com/)`,
* `R` and the `shiny` package.


## Download, Edit Paths \& Run Locally

Download the `genomeBrowser` source code and the
`genomeData` git repositories via `git`. You will
need to install `[git-lfs](https://git-lfs.github.com/)`,
version 1.0.1 or higher. The latter needs about 10 GB
of storage.

```
git clone git@gitlab.com:raim/genomeBrowser.git
git clone git@gitlab.com:raim/genomeData.git
``` 

**Then you need to adapt the local paths** to boths repos in file
`genomeBrowser/shiny/yeast/loadGenomeData.R`.

To run it locally on your machine:

```
cd genomeBrowser/shiny/yeast
R --vanilla < startLocal.R 
```

Then wait for 10-20 sec until all data is loaded,
and open the displayed URL in your browser:
(http://127.0.0.1:4975). 

Now click the "update" or scroll/zoom buttons
to get lost in yeast. 


## Running on a Server

Set an IP address and port in file `genomeBrowser/shiny/yeast/startApp.R`,
change to the directory and start the script:

```
cd genomeBrowser/shiny/yeast
R --vanilla < startApp.R &> runshiny.log
```


