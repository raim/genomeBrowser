# 20221013

## shiny genomeBrowser

* learn how to create interactive figures instead of png,
* integrate circos-based browser for whole genome view: https://gallery.shinyapps.io/genome_browser/ and https://github.com/rstudio/shiny-gallery/tree/master/genome-browser

# 201810

* create new multi-species project for gI scan
* get more recent version of parseHomHits from gIScanner, and
provide a script to convert diverse hits to genomeBrowser data
* start to move to R library in new folder R
