# The vsRGB / uRGB

A library-free scripting framework for parsing and plotting genome
data, and generate new genome projects: the very simple R Genome
Browser, or u'R Gene Bro.

It's speciality is the easy integration of diverse data sets, incl.
structural and expression information, to quickly inspect genomic
regions of interest in their context, and to produce publication
quality figures.

Example figures produced by vsRGB/uRGB can be found in
[Machne, Murray & Stadler 2017](http://www.nature.com/articles/s41598-017-12401-8).

This repo currently also serves as my dump for all kind of genome,
sequence, and feature-handling utilities used to set up genome data
from, eg., NCBI genbank files. 

Established R code is out-sourced to the R library
[segmenTools](https://github.com/raim/segmenTools), which is also
heavily used herein. Otherwise, the genome browser interface code is
quite free of any R library dependencies.

## Requirements

To use the genomeBrowser you need the data. These are generated
by the scripts in data projects in directory [data](data/). Also
see local README.md files there. The generation can be quite tedious
and can require many operating system specific tools. Please
email raim at tbi.univie.ac.at to obtain the required RData files
directly. Or perhaps https://gitlab.com/raim/genomeData/ is functional
by the time you read this.

## Usage

See individual README.md file for data projects in directory [data](data/),
eg. [data/pcc6803/README.md](data/pcc6803/README.md)

## Generate your own Project

Please reproduce the steps in
[data/pcc6803/generateGenomeBase.sh](data/pcc6803/generateGenomeBase.sh)
for your own genome. Ask raim at tbi.univie.ac.at for help.

### Details 

Data must be prepared as [ID].RData files residing in a location that
must be indicated as "data.path" (see src/example\_pcc6803.R).  The
data can then be loaded via their [ID]. The R scripts in data/pcc6803/
show how to create [ID].RData from a variety of raw data for the
*Synechocystis* sp.  PCC 6803 genome (available soon also at
https://gitlab.com/raim/genomeData/).

* Reserved Names:

    * annotation.RData: 
      contains data from the gff3 annotation of the genome release

    * sequence.RData: 
      contains the nucleotide sequence for the genome release

* [ID].RData file Content:
The data files contain only one R list object, `odat', which
comprises of the data [ID], the data, and plot instructions
(plot function, styles and relative height). 

While not strictly required, the most important file for a new genome
project would be annotation.RData, containing information from an
official genome annotation (genes, chromosomes, etc).  The perl
scripts [src/genbank2gff.pl](src/genbank2gff.pl) and
[src/genbank2fasta.pl](src/genbank2fasta.pl) convert genbank files to
tab-delimited format (cf. `gff` format) and fasta files, which can be
loaded into R with the genomeBrowser function `gff2tab`; an ideal
start for your new genome project.

See eg. [data/pcc6803/annotation.R](data/pcc6803/annotation.R) for an
example how to parse an NCBI-derived tab-deliminted file, re-annotate
IDs by more common gene names (here from cyanobase), map to external
data for informative coloring, etc., and then generate the required
`annotation.RData` file.

* The `odat` Object

The `odat' object in single .RData track files is a simple list
comprising the following list items, which carry both the data
to be plotted and plot type and style instructions:

* `ID`: ID of the data set, as a convention the ID should reflect the .RData
track file name: `<odat$ID>.RData`
* `description`: a short description of the data
* `data`: the data set, 
** genomic track data: a matrix, with coordinates in the first three 
columns 'chr', 'start', 'end', and data in variable addtional columns.
If values for each chromosome position are present the coordinates can be
omitted and the `settings` item
* `settings`: a list indicating plot type and settings. The required and 
allowed entries depend on the plot type (first item):
** `settings$type` : the plot type
*** genomic track data: 'plotHeat' (heatmap) or 'plotBlocks' (x-y plots)
*** genomic feature/segment data: `plotFeatures` (segments as arrows
** plot type-specific `settings` items: TODO

