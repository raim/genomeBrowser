#!/usr/bin/env bash

## command line script to call plotFeature.R for the yeast genome

genes=$1
selection=$2
## TODO: defaults if not passed

## TODO: allow this as arguments?
range=1500
settings=$GENBRO/data/selections.R
genome=$GENDAT/yeast

cmd="$GENBRO/src/plotFeature.R -i $genome -n $genes -r $range  -s $selection -S $settings  -f png -v"
echo $cmd
$cmd 
