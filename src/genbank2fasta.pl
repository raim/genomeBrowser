#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <2008-11-21 14:34:35 raim>
# $Id: genbank2fasta.pl,v 1.2 2013/11/15 15:57:35 raim Exp $

# EXTRACT ID and SEQUENCE from a GenBank file
# and write a fasta file (without newline!)

## TODO:
#generate outfile from infile
#generate fasta header (id) from genbank info (ACCESSION, VERSION, DEFINITION)

use strict;
use Getopt::Long;


my $outfile = "";
my $id = "";

# get command line parameters
usage() unless GetOptions("id=s"  => \$id,
			  "h"     => \&usage); 
    
sub usage
{
    printf STDERR "\n\nExtracts ID and sequence from a GenBank file";
    printf STDERR "\nand writes a fasta-like (w/o newline!) outfile\n\n";

    
    printf STDERR "\tUsage: genbank2fasta.pl [options] <FILE>\n\n";
    
    printf STDERR "<FILE>:\tGenBanke file\n";
    printf STDERR "-id\talternative ID\n\n";
    exit;
}

my $inseq = 0;
my $seq = "";

while(<>)
{
    chomp();
    $id = $1 if /^LOCUS\s+(\w+)\s+\d/ && $id eq "";    
    if ( /^ORIGIN/ )
    {
	$inseq = 1;
	next;
    }    
    next unless $inseq;

    $seq .= $_;    
}

# replace numbers and spaces
$seq =~ s/[\s0-9]//g;
# upper case
$seq = uc $seq;
# replace trailing '/' (from ncbi genbank)
$seq =~ s/\/\///;

printf STDOUT ">".$id."\n";
printf STDOUT $seq;
printf STDOUT "\n";


__END__
