### README ###

### To the run the program, type "perl WS_scheme_average.pl seq.txt output.txt" in command. 
### The "seq.txt" contains your sequences in fasta format

## SUPPLEMENTAL SCRIPT FROM
## Accurate prediction of nucleosome rotational positioning in yeast and 
## human genomes based on sequence-dependent DNA anisotropy
## Feng Cui, Linlin Chen, Peter R. LoVerso, Victor B. Zhurkin

## obtained from Feng Cui, downloaded on 20180218 from
## https://people.rit.edu/fxcsbi/WS_scheme/
## NOTE: no new lines in FASTA sequence allowed!

### END of README ###

#! perl -w

open(INPUT, "<$ARGV[0]");
open(OUTPUT, ">$ARGV[1]");

$window_147 = 147;
$window_146 = 146;

@seq_WW = ();
@seq_array_WW = ();
@seq_SS = ();
@seq_array_SS = ();

@seq_WW_major = ();
@seq_array_WW_major = ();

@seq_SS_minor = ();
@seq_array_SS_minor = ();


@final_WW_minor_score = ();
@final_WW_major_score = ();
@final_SS_minor_score = ();
@final_SS_major_score = ();

$nseq = 1;

@minor_position_147 = (	[qw(5 6 7)],
			[qw(15 16 17)],
			[qw(26 27 28)],
			[qw(37 38 39)],
			[qw(47 48 49)],
			[qw(57 58 59)],
			[qw(67 68 69)],
			[qw(78 79 80)],
			[qw(88 89 90)],
			[qw(98 99 100)],
			[qw(108 109 110)],
			[qw(119 120 121)],
			[qw(130 131 132)],
			[qw(140 141 142)],
		);

@major_position_147 = (	[qw(10 11 12)],
			[qw(21 22 23)],
			[qw(32 33 34)],
			[qw(42 43 44)],
			[qw(52 53 54)],
			[qw(62 63 64)],
			[qw(83 84 85)],
			[qw(93 94 95)],
			[qw(103 104 105)],
			[qw(113 114 115)],
			[qw(124 125 126)],
			[qw(135 136 137)],
		);

@minor_position_146 = (	[qw(5 6 7)],
			[qw(15 16 17)],
			[qw(26 27 28)],
			[qw(37 38 39)],
			[qw(47 48 49)],
			[qw(57 58 59)],
			[qw(67 68 69)],
			[qw(77 78 79)],
			[qw(87 88 89)],
			[qw(97 98 99)],
			[qw(107 108 109)],
			[qw(118 119 120)],
			[qw(129 130 131)],
			[qw(139 140 141)],
		);

@major_position_146 = (	[qw(10 11 12)],
			[qw(21 22 23)],
			[qw(32 33 34)],
			[qw(42 43 44)],
			[qw(52 53 54)],
			[qw(62 63 64)],
			[qw(82 83 84)],
			[qw(92 93 94)],
			[qw(102 103 104)],
			[qw(112 113 114)],
			[qw(123 124 125)],
			[qw(134 135 136)],
		);

while (<INPUT>) {
	if (m/^>/) {
		$name = $';
		chomp($name);

		$seq = <INPUT>;
		chomp($seq);

#		$nseq = $nseq + 1;
	
		$length = length($seq);
		$x = 0;
		while ($x <= $length - $window_147) {

			$total_WW = 0;		### WW in minor sites
			$total_WW_major = 0;	### WW in major sites
			$total_SS = 0;		### SS in major sites
			$total_SS_minor = 0;	### SS in minor sites


			$total_WW_146_1 = 0;		### WW in minor sites (146bp template 1)
			$total_WW_major_146_1 = 0;	### WW in major sites (146bp template 1)
			$total_SS_146_1 = 0;		### SS in major sites (146bp template 1)
			$total_SS_minor_146_1 = 0;	### SS in minor sites (146bp template 1)

			$total_WW_146_2 = 0;		### WW in minor sites (146bp template 2)
			$total_WW_major_146_2 = 0;	### WW in major sites (146bp template 2)
			$total_SS_146_2 = 0;		### SS in major sites (146bp template 2)
			$total_SS_minor_146_2 = 0;	### SS in minor sites (146bp template 2)

			$sum_147 = 0;			### total score (147 template)
			$sum_146_1 = 0;			### total score (146 template 1)
			$sum_146_2 = 0;			### total score (146 template 2)

			$subseq = substr($seq, $x, $window_147);		### 147-bp template
			$subseq_146_1 = substr($seq, $x, $window_146);		### 146-bp template 1
			$subseq_146_2 = substr($seq, $x+1, $window_146); 	### 146-bp template 2 (shift by 1bp)


#######################################################################################################################			
# minor positions -- 147bp template
			for ($i=0; $i<=$#minor_position_147; $i++) {
				$position = $minor_position_147[$i][0];
				$tetramer = substr($subseq, $position-1, 4);


				if ($tetramer =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
					$total_WW = $total_WW + 3;
				}
				elsif ($tetramer =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
					$total_WW = $total_WW + 2;
				}
				elsif ($tetramer =~ /AA|AT|TA|TT/) {
					$total_WW = $total_WW + 1;
				}

### Penalty -- SSSS = 3; SSS = 2; SS = 1;

				if ($tetramer =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
					$total_SS_minor = $total_SS_minor + 3;
				}
				elsif ($tetramer =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
					$total_SS_minor = $total_SS_minor + 2;
				}
				elsif ($tetramer =~ /GG|GC|CC|CG/){
					$total_SS_minor = $total_SS_minor + 1;
				}
			}



			for ($i=0; $i<=$#minor_position_146; $i++) {
				$position_146 = $minor_position_146[$i][0];
				$tetramer_146_1 = substr($subseq_146_1, $position_146-1, 4);
				$tetramer_146_2 = substr($subseq_146_2, $position_146-1, 4);

# minor positions -- 146bp template (1)

				if ($tetramer_146_1 =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
					$total_WW_146_1 = $total_WW_146_1 + 3;
				}
				elsif ($tetramer_146_1 =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
					$total_WW_146_1 = $total_WW_146_1 + 2;
				}
				elsif ($tetramer_146_1 =~ /AA|AT|TA|TT/) {
					$total_WW_146_1 = $total_WW_146_1 + 1;
				}

### Penalty -- SSSS = 3; SSS = 2; SS = 1;

				if ($tetramer_146_1 =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
					$total_SS_minor_146_1 = $total_SS_minor_146_1 + 3;
				}
				elsif ($tetramer_146_1 =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
					$total_SS_minor_146_1 = $total_SS_minor_146_1 + 2;
				}
				elsif ($tetramer_146_1 =~ /GG|GC|CC|CG/){
					$total_SS_minor_146_1 = $total_SS_minor_146_1 + 1;
				}

# minor positions -- 146bp template (2)

				if ($tetramer_146_2 =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
					$total_WW_146_2 = $total_WW_146_2 + 3;
				}
				elsif ($tetramer_146_2 =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
					$total_WW_146_2 = $total_WW_146_2 + 2;
				}
				elsif ($tetramer_146_2 =~ /AA|AT|TA|TT/) {
					$total_WW_146_2 = $total_WW_146_2 + 1;
				}

### Penalty -- SSSS = 3; SSS = 2; SS = 1;

				if ($tetramer_146_2 =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
					$total_SS_minor_146_2 = $total_SS_minor_146_2 + 3;
				}
				elsif ($tetramer_146_2 =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
					$total_SS_minor_146_2 = $total_SS_minor_146_2 + 2;
				}
				elsif ($tetramer_146_2 =~ /GG|GC|CC|CG/){
					$total_SS_minor_146_2 = $total_SS_minor_146_2 + 1;
				}
			}


#######################################################################################################################
# major positions -- 147 bp template

			for ($i=0; $i<=$#major_position_147; $i++) {

					$position = $major_position_146[$i][0];
					$tetramer = substr($subseq, $position-1, 4);	
					$tetramer = substr($subseq, $position-1, 4);			

					if ($tetramer =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
						$total_SS = $total_SS + 3;
					}
					elsif ($tetramer =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
						$total_SS = $total_SS + 2;
					}
					elsif ($tetramer =~ /GG|GC|CC|CG/) {
						$total_SS = $total_SS + 1;
					}

### Penalty -- WWWW = 3; WWW =2; WW =1;

					if ($tetramer =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
						$total_WW_major = $total_WW_major + 3;
					}
					elsif ($tetramer =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
						$total_WW_major = $total_WW_major + 2;	
					}
					elsif ($tetramer =~ /AA|AT|TT|TA/) {
						$total_WW_major = $total_WW_major + 1;
					}

			}

			for ($i=0; $i<=$#major_position_146; $i++) {

					$position_146 = $major_position_146[$i][0];
					$tetramer_146_1 = substr($subseq_146_1, $position_146-1, 4);	
					$tetramer_146_2 = substr($subseq_146_2, $position_146-1, 4);	

# major positions -- 146bp template (1)		

					if ($tetramer_146_1 =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
						$total_SS_146_1 = $total_SS_146_1 + 3;
					}
					elsif ($tetramer_146_1 =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
						$total_SS_146_1 = $total_SS_146_1 + 2;
					}
					elsif ($tetramer_146_1 =~ /GG|GC|CC|CG/) {
						$total_SS_146_1 = $total_SS_146_1 + 1;
					}

### Penalty -- WWWW = 3; WWW =2; WW =1;

					if ($tetramer_146_1 =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
						$total_WW_major_146_1 = $total_WW_major_146_1 + 3;
					}
					elsif ($tetramer_146_1 =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
						$total_WW_major_146_1 = $total_WW_major_146_1 + 2;	
					}
					elsif ($tetramer_146_1 =~ /AA|AT|TT|TA/) {
						$total_WW_major_146_1 = $total_WW_major_146_1 + 1;
					}

# major positions -- 146bp template (2)		

					if ($tetramer_146_2 =~ /GGGG|GGGC|GGCG|GGCC|GCGG|GCGC|GCCG|GCCC|CGGG|CGGC|CGCG|CGCC|CCGG|CCGC|CCCG|CCCC/) {
						$total_SS_146_2 = $total_SS_146_2 + 3;
					}
					elsif ($tetramer_146_2 =~ /GGG|GGC|GCG|GCC|CGG|CGC|CCG|CCC/) {
						$total_SS_146_2 = $total_SS_146_2 + 2;
					}
					elsif ($tetramer_146_2 =~ /GG|GC|CC|CG/) {
						$total_SS_146_2 = $total_SS_146_2 + 1;
					}

### Penalty -- WWWW = 3; WWW =2; WW =1;

					if ($tetramer_146_2 =~ /AAAA|AAAT|AATA|AATT|ATAA|ATAT|ATTA|ATTT|TAAA|TAAT|TATA|TATT|TTAA|TTAT|TTTA|TTTT/) {
						$total_WW_major_146_2 = $total_WW_major_146_2 + 3;
					}
					elsif ($tetramer_146_2 =~ /AAA|AAT|ATA|ATT|TAA|TAT|TTA|TTT/) {
						$total_WW_major_146_2 = $total_WW_major_146_2 + 2;	
					}
					elsif ($tetramer_146_2 =~ /AA|AT|TT|TA/) {
						$total_WW_major_146_2 = $total_WW_major_146_2 + 1;
					}

			}

			$sum_147 = $total_WW + $total_SS - $total_WW_major - $total_SS_minor;
			$sum_146_1 = $total_WW_146_1 + $total_SS_146_1 - $total_WW_major_146_1 - $total_SS_minor_146_1;
			$sum_146_2 = $total_WW_146_2 + $total_SS_146_2 - $total_WW_major_146_2 - $total_SS_minor_146_2;

			if ($sum_147 >= $sum_146_1 && $sum_147 >= $sum_146_2) {

				push (@seq_WW, $total_WW);
				push (@seq_SS, $total_SS);
	
				push (@seq_WW_major, $total_WW_major);
				push (@seq_SS_minor, $total_SS_minor);
			}
			elsif ($sum_146_1 >= $sum_147 && $sum_146_1 >= $sum_146_2) {
				push (@seq_WW, $total_WW_146_1);
				push (@seq_SS, $total_SS_146_1);
	
				push (@seq_WW_major, $total_WW_major_146_1);
				push (@seq_SS_minor, $total_SS_minor_146_1);
			}
			elsif ($sum_146_2 >= $sum_147 && $sum_146_2 >= $sum_146_1) {
				push (@seq_WW, $total_WW_146_2);
				push (@seq_SS, $total_SS_146_2);
	
				push (@seq_WW_major, $total_WW_major_146_2);
				push (@seq_SS_minor, $total_SS_minor_146_2);
			}
			
			$x = $x + 1;
		}
	push(@seq_array_WW, [@seq_WW]);
	push(@seq_array_SS, [@seq_SS]);

	push(@seq_array_WW_major, [@seq_WW_major]);
	push(@seq_array_SS_minor, [@seq_SS_minor]);

	@seq_WW = ();
	@seq_SS = ();

	@seq_WW_major = ();
	@seq_SS_minor = ();
	}
}

#print "\nhallo:\t".@seq_array_WW."\n";	
### WW score in minor sites

	for ($m=0; $m<=$#{$seq_array_WW[0]}; $m++) {
		$sum = 0;
		$ave = 0;

		for ($n=0; $n<=$#seq_array_WW; $n++) {
			$sum = $sum + $seq_array_WW[$n][$m];
		}
		$ave = $sum / $nseq;

		push(@final_WW_minor_score, $ave);
	}

### SS score in major sites

	for ($m=0; $m<=$#{$seq_array_SS[0]}; $m++) {

		$sum = 0;
		$ave = 0;

		for ($n=0; $n<=$#seq_array_SS; $n++) {
			$sum = $sum + $seq_array_SS[$n][$m];
		}
		$ave = $sum / $nseq;

		push(@final_SS_major_score, $ave);
	}

### WW score in major sites

	for ($m=0; $m<=$#{$seq_array_WW_major[0]}; $m++) {

		$sum = 0;
		$ave = 0;

		for ($n=0; $n<=$#seq_array_WW_major; $n++) {
			$sum = $sum + $seq_array_WW_major[$n][$m];
		}
		$ave = $sum / $nseq;

		push(@final_WW_major_score, $ave);
	}


### SS score in minor sites

	for ($m=0; $m<=$#{$seq_array_SS_minor[0]}; $m++) {

		$sum = 0;
		$ave = 0;

		for ($n=0; $n<=$#seq_array_SS_minor; $n++) {
			$sum = $sum + $seq_array_SS_minor[$n][$m];
		}
		$ave = $sum / $nseq;

		push(@final_SS_minor_score, $ave);
	}

$start = 74;

print OUTPUT "Position\tScore\n";
#print @final_WW_minor_score."hallo\n";
for ($m=0; $m<@final_WW_minor_score; $m++) {

	$total = $final_WW_minor_score[$m] + $final_SS_major_score[$m] - $final_WW_major_score[$m] - $final_SS_minor_score[$m];
	print OUTPUT "$start\t$total\n";

	$start = $start + 1;
}


close (OUTPUT);
close (INPUT);
