#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <2009-07-13 12:47:01 raim>
# $Id: getColumn.pl,v 1.4 2009/07/13 03:46:49 raim Exp $

use strict;
use Getopt::Long;
use IO::Handle;
use File::Basename;

$|=1; # flush print to stdout

# !!! TODO : adapt for genome.csv, chrom and coord to GREP SINGLE FEATURES
# !!! TODO : avoid that first file has to be master
# !!! TODO : optionally write xmgrace file, with same colors as in R
# !!! TODO : write all values to separate files:
# !!! TODO :  one value, each cluster AND one cluster, all values (mean+std)

# !!! TODO : allow simple arithmetic between columns (+,-,*,/)
# !!! TODO : allow binning and smoothing, and integral


my $column   = "";
my $file     = "";
my $header   = 0;
my $firstpos = "";# start row value in columns
my $lastpos  = "";#  stop row value in columns
my $chrom    = 0;   
my $sep      = "\t";   

# get command line parameters
usage() unless GetOptions("v=s" => \$column,
			  "ch=i"=> \$chrom,
			  "up=i"=> \$firstpos,
			  "do=i"=> \$lastpos,
			  "s=s" => \$sep,
			  "l"   => \$header,
			  "h"   => \&usage);

sub usage
{    
    printf STDERR "\n";
    printf STDERR "Extracts columns from one or more files and writes these to stdout.\nWhen used with several files, numeric reference columns\nwhich must be equal in each line of the files,\nmust exist and be passed as the first value to option -v\n";
    printf STDERR "\n";
    printf STDERR "\tUsage: @{[basename($0)]} [options] <FILE(S)>  -v 'columname1,columnname2'\n";
    printf STDERR "\n";
    
    printf STDERR "\tFILE(S):\ttab-delimited data file, current: $file\n";
    printf STDERR "     \t\tcan also be the first letters of several files!\n";
    printf STDERR "\t-v:s\tcomma-separated list of column headers, current: $column\n";
    printf STDERR "     \t\tthe first value will be used as a reference value, and only printed once, if several files are parsed\n";
    printf STDERR "\t-up=i\tupstream threshold for reference value (first value passed to -v)\n";
    printf STDERR "\t-do=i\tdownstream threshold for reference value (first value passed to -v)\n";
    printf STDERR "\n";
    printf STDERR "\t-s:s\tfield delimiter, current: \"$sep\"\n";
    printf STDERR "\t-l\tprint header line\n";
    printf STDERR "\n";
    exit;
}

# set limits, if given
my $upfilter = 0;
$upfilter = 1 if $firstpos != "";
my $dofilter = 0;
$dofilter = 1 if $lastpos != "";



# GET (LIST OF) FILE(S)
$file = $ARGV[0];
$sep =~ s/"//g;

if ( $file eq "" )
{
    print "\n\n\tERROR\tno files to parse\n";
    usage();
}

# parse and expand file list for wildcards *

my $fils;
my @list;
# one file only
if ( -e $file )
{
   $fils    = $file;
   $list[0] = $file;
}
# several files    
else
{
    $fils = $file."*";
    @list = glob($fils);
}

my @name;
for ( my $i=0; $i<@list; $i++ )
{
    print STDERR $list[$i]."\tregistered as\t";
    if ( $list[$i] =~ /$file(.*)\.[a-z]/ )
    {
	print STDERR "\t".$1;
	$name[$i] = $1;
    }
    else
    {
	print STDERR "\tall";
	$name[$i] = "";
    }
    print STDERR "\n";
}

# remove ""
$column =~ s/"//g;
my @head = split /,/, $column; #/
# get operators
my %adds;
my $len = @head;
for ( my $i=0; $i<$len; $i++ )
{
    my @vals = split /\+/, $head[$i];
    # append new values
    $head[$i] = $vals[0];
    for ( my $j=1; $j<@vals; $j++ )
    {
	$adds{$vals[0]}[$j-1] = $vals[$j]; 
    }
}

my @coln; # main columns
my %cola; # columns to add

# init to -1
for ( my $i=0; $i<@head; $i++ )
{
    $coln[$i] = -1;
    next unless exists $adds{$head[$i]};
    for ( my $j=0; $j<@{$adds{$head[$i]}}; $j++ )
    {
	$cola{$head[$i]}[$j] = -1;
    }
}


my @fp;
my @ok;

# open files and check headers
for ( my $i=0; $i<@list; $i++ )
{
    # open files
    $fp[$i] = new IO::Handle;
    open($fp[$i], "<$list[$i]") or die "\n\tERROR\tcan'topen infile $_\n";
    my $fx = $fp[$i];
    my @vals = split /$sep/, <$fx>;
    chomp(@vals);
    
    # find columns in first file header
    if ( $i == 0 )
    {
	for ( my $i=0; $i<@vals; $i++ )
	{
	    for ( my $j=0; $j<@head; $j++ )
	    {
		$coln[$j] = $i if $head[$j] eq $vals[$i];
		next unless exists $adds{$head[$j]};
		for ( my $k=0; $k<@{$adds{$head[$j]}}; $k++ )
		{
#		    print  "\tHALLO column $i $adds{$head[$j]}[$k]\n"
#			if $adds{$head[$j]}[$k] eq $vals[$i];
		    $cola{$head[$j]}[$k] =  $i
			if $adds{$head[$j]}[$k] eq $vals[$i];
		}
	    }
	}
	print $vals[$coln[0]] if $header;
    }
    
    $ok[$i] = 1;

    for ( my $j=0; $j<@head; $j++ )
    {
	if ( $vals[$coln[$j]] ne $head[$j] )
	{
	    print STDERR
		"\n\tWARNING\t$list[$i]\t$vals[$coln[$j]] ne $head[$j]\n";
	    $ok[$i] = 0; 
	}
	else
	{
	    if ( $header && ( $coln[$j] != -1 && $j != 0 ) )
	    { 
		print "\t".$vals[$coln[$j]];
		print ".".$name[$i] if $name[$i] ne "";
		
	    }
	}
    }
}
print "\n" if $header;

my %values;

my $fx = $fp[0];
my $start = 0;
print STDERR "ONLY from $firstpos\n" if $upfilter;
print STDERR "ONLY to $lastpos\n" if $dofilter;
while(<$fx>)
{
    # FIRST FILE, used for first column
    my @vals = split /$sep/;
    chomp(@vals);

    # skip upstream positions, move filepointer
    if ( $upfilter & $vals[$coln[0]] < $firstpos )    
    {
	for ( my $i=1; $i<@list; $i++ )
	{
	    my $fy = $fp[$i];
	    <$fy>;
	}
	next;
    }

    # skip downstream positions
    last if $vals[$coln[0]] > $lastpos & $dofilter;

    print $vals[$coln[0]];
    my $pos = $vals[$coln[0]];

    for ( my $j=1; $j<@head; $j++ )
    {
	$values{$pos}[0] = $vals[$coln[$j]] unless $coln[$j] == -1;
	if ( exists $adds{$head[$j]} )
	{
	    for ( my $k=0; $k<@{$adds{$head[$j]}}; $k++ )
	    {	    
		$values{$pos}[0] +=
		    $vals[$cola{$head[$j]}[$k]]
		    unless $cola{$head[$j]}[$k] == -1;	
	    }
	}
	print "\t".$values{$pos}[0] unless $coln[$j] == -1;
    }
    
    # OTHER FILES	
    for ( my $i=1; $i<@list; $i++ )
    {
	next unless $ok[$i];
	
	my $fy = $fp[$i];
	my @vals = split /$sep/, <$fy>;
	chomp(@vals);
	
	for ( my $j=1; $j<@head; $j++ )
	{
	    $values{$pos}[$i] = $vals[$coln[$j]] unless $coln[$j] == -1;
	    if ( exists $adds{$head[$j]} )
	    {
		for ( my $k=0; $k<@{$adds{$head[$j]}}; $k++ )
		{	    
		    $values{$pos}[$i] +=
			$vals[$cola{$head[$j]}[$k]]
			unless $cola{$head[$j]}[$k] == -1;	
		}
	    }
	    print "\t".$values{$pos}[$i] unless $coln[$j] == -1;
	}
    }
    print "\n";    
}

# ARITHMETIC OPERATIONS

# BINNING, SMOOTHING

__END__
