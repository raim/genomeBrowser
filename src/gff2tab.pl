#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <2013-07-30 17:54:38 raim>
# $Id: gff2tab.pl,v 1.4 2013/07/30 15:55:38 raim Exp $

use strict;
use Getopt::Long;
use File::Basename;

# extracts coordinates for all features from an SGD gff3 file
# also counts the number of introns and 5'UTR introns for each feature,
# and writes intron positions using the parent ID and the intron number
# used from preprocess.sh

# !!! TODO : separate yeast SGD specific stuff (introns and 5'UTR introns)
# into separate script

## requires a GFF3 file and a chromosome index file:
## ID<tab>name<tab>length  ## <- the header
## <chromosome number><tab><chromosome name><tab><length>

my $outfile = "";
my $infile  = ""; # SGF GFF3 genome annotation
my $chromFile = ""; # chromosome index file
my $restrict= "all";
# get command line parameters
usage() unless GetOptions("i=s"   => \$infile,
			  "c=s"   => \$chromFile,
			  "o=s"   => \$outfile,
			  "r=s"   => \$restrict,
			  "h"     => \&usage);

sub usage
{
    printf STDERR "\n\n Reads a genome GFF3 file and extracts all \n";
    printf STDERR " annotated features and their coordinates\n\n";
    
    printf STDERR "\tUsage: @{[basename($0)]}\n\n";

    printf STDERR "-i=s\tgenome GFF3 file (current: $infile)\n";
    printf STDERR "-c=s\tchromosome mapping file (current: $chromFile)\n";
    printf STDERR "-o=s\talternative outfile name (current: $outfile)\n\n";
    exit;
}

# read in chromosome mapping

open(IDX, "<$chromFile") or die "can't read chromosome mapping file $chromFile\n";

my %chr;
my @chrID = ();
my @chrName = ();
my @chrLength =();
# parse header
my @val = split '\t', <IDX>;
chomp(@val);
my $lengthCol ="";
my $idCol     = "";
my $nameCol   = "";
for( my $i=0; $i<@val; $i++ )
{
    $idCol = $i if $val[$i] eq "#ID";
    $nameCol = $i if $val[$i] eq "name";
    $lengthCol = $i if $val[$i] eq "length";
}
my $cnt = 0;
while(<IDX>)
{
    my @val = split '\t';
    chomp(@val);

    $chrID[$cnt] = $val[$idCol];
    $chrName[$cnt] = $val[$nameCol];
    $chrLength[$cnt] = $val[$lengthCol];
    $chr{$val[$nameCol]} = $val[$idCol];
}


# read GFF3 genome annotation

open(gff3, "<$infile") or die "can't read GFF3 file $infile\n";
print "reading GFF3 file $infile\n";

my %name  = ();
my %type  = ();
my %chrom = ();
my %start = ();
my %end   = ();
my %strand= ();
my %coding= ();
my %rnacoding= ();
my %intron= ();
my %utr5intron= ();
my %atgintron = ();
my %go = ();
my %ygd = ();

while(<gff3>)
{

    $_ =~ s/\r//g; ## rm ^M which strangely occured in saccharomyces_cerevisiae_R64-1-1_20110208.gff

    # skip non feature rows
    next unless (/^chr/ || /^2-micron/);
    my @val = split '\t';
    # skip non gff3 format conforming lines
    next unless 9 == @val;
    
    # skip all ID-less features    
    my @attrib = split ';', $val[8]; 
    my $id = "";
    my $name ;
    # FEATURES WITH IDs
    if ( $attrib[0] =~ /^ID=(.*)/ )
    {
	$id = $1;
    }
    # INTRONS
    elsif ( $val[2] eq "intron" )
    {
	# INCLUDE INTRONS AS FEATURES GENEID.intron.NUM
	if ( $attrib[0] =~ /^Parent=(.*)/ )
	{
	    # count and name introns
	    $id = $1.".intron.".($intron{$1}+1);
	    $intron{$1}++;
	}	
    }
    # 5'UTR INTRONS
    elsif ( $val[2] eq "five_prime_UTR_intron" )
    {
	if ( $attrib[0] =~ /^Parent=(.*)/ )
	{
	    # count and name introns
	    $id = $1.".intron.5utr.".($intron{$1}+1);
	    $intron{$1}++;
	    $utr5intron{$1}++;
	    # 8c) Check START CODON == ATG
	    # Genes which don't have an ATG as a start codon have a 5'
	    # or a start codon intron, SGD features thus include the
	    # 5'UTR and should not be used in subsequent averaging
	}
    }
    # CODING SEQUENCES
    elsif ( $val[2] eq "CDS" )
    {
	if ( $attrib[0] =~ /^Parent=(.*)/ )
	{
	    # count and name introns
	    $id = $1.".coding.".($coding{$1}+1);
	    $coding{$1}++;
	}
    }
    # RNA CODING SEQUENCES
    elsif ( $val[2] eq "noncoding_exon" )
    {
	if ( $attrib[0] =~ /^Parent=(.*)/ )
	{
	    # count and name RNA exons
	    $id = $1.".rna.coding.".($rnacoding{$1}+1);
	    $rnacoding{$1}++;
	}
    }
    my $dubious = 0;
    foreach ( @attrib )
    {
	# gene names
	$name{$id} = $1 if /^gene=(.*)/;
	# handle dubious and coding
	$dubious = 1 if /orf_classification=Dubious/;
	# GO classification
	$go{$id} = $1 if /^Ontology_term=(.*)/;
	# YGD ID
	$ygd{$id} = $1 if /^dbxref=SGD:(S\d+)/;
    }
    
    unless ( $id eq "" )
    {
	$chrom{$id} = $val[0];
	# convert chromosome to number
	$chrom{$id} = $chr{$chrom{$id}};
	
 	$type{$id}  = $val[2];
	$type{$id}  = "dubious" if $dubious && $type{$id} eq "gene";
	$start{$id} = $val[3];
	$end{$id}   = $val[4];
	$strand{$id}= $val[6];

	#print $id."\t".$ygd{$id}."\n" if !exists $ygd{$id};
	#print $id."\t".$go{$id}."\n";# if exists $ygd{$id};
	
    }    
}

#$outfile = $infile =~ s/.gff/.tab/gr if $outfile == ""; 
print "writing coordinates and introns to $outfile\n";

open(outf,  ">$outfile") or die "can't write outfile $outfile\n";
# print header
print outf "ID\tparent\tname\ttype\tchr\tstart\tend\tstrand\tintrons\tUTR.intron\tGO\tSGD\n";

my $id = "";
foreach my $id ( sort {$type{$a} cmp $type{$b}} keys %type )
{
    print STDOUT $id."\n";
    # only print requested types
    next unless $restrict eq "all" || $restrict eq $type{$id};
    # only print CDS for features with several
    my $parent;
    if ( $type{$id} eq "CDS" )
    {
	$parent = $id;
	$parent =~ s/\.coding\.\d+//g;	
    }
    elsif ( $type{$id} eq "noncoding_exon" )
    {
	$parent = $id;
	$parent =~ s/\.rna.coding\.\d+//g;	
	$type{$id} = $type{$parent}.".exon";
    }
    elsif ( $type{$id} eq "intron" )
    {
	$parent = $id;
	$parent =~ s/\.intron\.\d+//g;
	$type{$id} = "tRNA.intron" if $type{$parent} eq "tRNA";
    }
    elsif ( $type{$id} eq "five_prime_UTR_intron" )
    {
	$parent = $id;
	$parent =~ s/\.intron.5utr\.\d+//g;	
    }
    
    my $int = 0;
    $int = $intron{$id} if exists $intron{$id};
    print outf
	$id."\t".$parent."\t".$name{$id}."\t".$type{$id}.
	"\t".$chrom{$id}."\t".$start{$id}."\t".$end{$id}."\t".$strand{$id}.
	"\t".$intron{$id}."\t".$utr5intron{$id}.
	"\t".$go{$id}."\t".$ygd{$id}."\n";
    
# ?? FIND START CODON INTRONS

    # CHECK INTRONS
#    if ( $id =~ /(.*)\.intron\.\d+/ )
#    {
#	my $dist = 0;
#	$dist = $start{$id} - $start{$1}; # positive if insight CDS
#	$dist = ($end{$1} - $end{$id})
#	    if $strand{$id} eq "-";
#	
#	if ( $dist == 0 )
#	{
#	    print "WARNING: intron $id in $1 - $name{$1}\n";
#	    print "gene\tintron\n";
#	    print $strand{$1}."\t".$strand{$id}."\n";
#	    print $start{$1}."\t".$start{$id}."\t:$dist\n";
#	    print $end{$1}  ."\t".$end{$id}  ."\t:$dist\n";
#	}
#    }

}

__END__
