#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <2012-06-19 16:08:23 raim>
# $Id: downloadData.pl,v 1.13 2016/04/01 10:20:52 raim Exp $

# uses an input table with URLs and target file
# to establish whether a given file has been downloaded
# and potentially moved to another filename
# and downloads files not yet present

## TODO: move gunzip out, give target file name in downloaddata.csv
## OR repair .gz handling if URL contains a code isntead of "."
## as in smith10 data (whitehouse and smith, bed files via geo)

use strict;
use LWP::Simple;
use Getopt::Long;
use File::Basename;
use URI::Escape;

my $outDir = "";
my $logFile= "";

usage() unless GetOptions("o=s" => \$outDir,
			  "l=s" => \$logFile,
			  "h"   => \&usage);

unless ( -e $outDir )
{
    print STDERR "\n\tERROR\t$outDir\tDOWNLOAD directory does not exist\n";
    usage();
}


sub usage
{
    printf STDERR "\n\nReads a specific table giving URLs for data-download";
    printf STDERR "\n and data pre-processing steps. Downloads all files";
    printf STDERR "\n which are not yet existing in the passed directory\n\n";
    
    printf STDERR "USAGE: @{[basename($0)]} <FILE> -o <PATH> -l <LOGFILE>\n\n";
    
    printf STDERR "\t<FILE>\tdownload table!, current: $ARGV[0]\n";
    printf STDERR "\t-o:s\tdirectory for downloaded and processed files, current: $outDir\n";
    printf STDERR "\t-l:s\tdownload log file, current: $logFile\n\n";
    exit;
}



# open log file
if ( $logFile eq "" )
{
    $logFile = $outDir."/download.log";
    print STDERR "WARNING\tUsing default logfile name: $logFile\n";
}
open(LOGF, ">$logFile") or die "ERROR\tcan't write LOGFILE $logFile\n";


my $line = <>;
$line =~ s/"//g;
my @val = split '\t', $line;
my $commandCol = "";
my $targetCol = "";
my $urlCol = "";

for ( my $i=0; $i<@val; $i++ )
{
    $urlCol = $i if $val[$i] eq "URL";
    $targetCol = $i if $val[$i] eq "target";
    $commandCol = $i if $val[$i] eq "command";
}

my $cnt = 0;
while(<>)
{
    $_ =~ s/"//g;
    my @val = split '\t';
    chomp(@val);

    # skip defunct URLs

    next if $val[0] =~ /^#/;

    # GET URL AND FILENAMES
    my $url = $val[$urlCol];
    # rm whitespaces
    $url =~ s/^\s+//;
    $url =~ s/\s+$//;

    # get basename and decode URI 
    my $file = uri_unescape(basename($url));
    chomp($file);

    my $target = "";
    if ( $val[$targetCol] eq "" )
    {
	$target = $outDir."/".$file;
    }
    else
    {
	$target = $outDir."/".$val[$targetCol];
    }
    $cnt++;

    # account for unpacked files
    my $unzipped = $target;
    $unzipped =~ s/\.gz$//g;

    my $dos2unix = 0;
    $dos2unix = 1 if $val[$commandCol] eq "dos2unix";
    
    print LOGF "FILE #$cnt\t$target\n";

    # is the target file already downloaded?
    if ( -e $target || -e $unzipped )
    {
	print LOGF "\tfile exists in directory $outDir\n\n";
    }
    else
    {

	print "FILE #$cnt\t$target DOES NOT EXIST - DOWNLOAD NOW\n";    
	print LOGF "\tDOWNLOADING from URL: $val[2]\n";
	# download
	my @results = `wget \"$url\" 2>&1`;
	
	print LOGF "\tSTART WGET MESSAGE:\n";
	my $savedName = $file;
	foreach ( @results )
	{
	    print LOGF "\t".$_;
	    if ( /`(.*)'\ssaved/ )
	    {
		$savedName = $1;
	    }
	}
	print LOGF "\tEND WGET MESSAGE.\n";
	if ( $file ne $savedName )
	{
	    print LOGF "\tSAVED AS $savedName\n";
	    $file = $savedName;
	}

	unless ( -e $file )
	{
	    print STDERR "ERROR\t$file could not be downloaded!\n";
	    print LOGF "ERROR\t$file could not be downloaded!\n";
	    next;
	}

	#if ( $gzipped )
	#{
	#    print LOGF "\tGUNZIPPING $file\n";
	#    my @results = `gunzip -f $file 2>&1`;
	#    foreach ( @results )
	#    {
	#	print LOGF "ERROR ?\t".$_;
	#    }
	#    $file =~ s/\.gz$//g;
	#}

	if ( $dos2unix )
	{
	    print LOGF "\tCONVERT TO UNIX FORMAT\n";
	    my @results = `dos2unix $file && dos2unix $file 2>&1`;
	    foreach ( @results )
	    {
		print LOGF "dos2unix message:\t".$_;
	    }
	}
	
	# move file to new name
	if ( $file ne $target )
	{
	    print "\tMOVING $file TO $target  \n";
	    print LOGF "\tMOVING $file TO $target  \n";
	    my @results = `mv \"$file\" \"$target\" 2>&1`;
	    foreach ( @results )
	    {
		print STDERR "mv message\t".$_;
		print LOGF "mv message\t".$_;
	    }
	}
	if ( -e $target )
	{
	    print STDERR "FILE #$cnt\t$target download SUCCESS!\n";
	}
	else
	{
	    print LOGF "ERROR\t$target\tdownload FAILED - ".
		"check logfile $logFile!\n"; 
	    print STDERR "ERROR\t$target\tdownload FAILED - ".
		"check logfile $logFile!\n"; 
	}
    }

    print LOGF "FINISHED $target\n";
}
close(LOGF);

__END__
