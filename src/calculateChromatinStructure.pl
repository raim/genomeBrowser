#!/usr/bin/perl
# -*-Perl-*-

use strict;
use Getopt::Long;
use IO::Handle;
use File::Basename;

$|=1; # flush print to stdout


my $seqIndex  = ""; # index file for chromosome sequence files
                    # TODO : seqIndex should indicate circular chromosomes!
my $fastaFile = ""; # overrides fasta search via seqIndex
my $outDir    = ""; # output directory
my $outFile   = ""; # basename for output files
my $motifFile = ""; # input file defining motifs to be scanned
my $doTracts = 0;   # calculate poly(N) tract lengths
my $dinucFile = ""; # input file defining dinucleotide parameters
my $trinucFile= ""; # input file defining trinucleotide parameters
my $pwmFile   = ""; # input file defining position-weith-matrices (PWMs)
my $pwmCutoff = ".8"; # threshold for PWM score (fraction of maximal value)
my $pwmBoolean= 1;  # whether to plot the PWM score or just 1 for scores>threshold
my $nt2st     = 0;  # add nucleotides also to structure file
my $backGround= ""; # NOT USED YET: list of background frequencies A,C,G,T - will otherwise
                    # be calculated from local sequence context

# get command line parameters
usage() unless GetOptions("o=s"  => \$outDir,
			  "fasta=s" => \$fastaFile,
			  "tri=s"=> \$trinucFile,
			  "din=s"=> \$dinucFile,
			  "mot=s"=> \$motifFile,
			  "t"    => \$doTracts,
			  "pwm=s"=> \$pwmFile,
			  "cut=s"=> \$pwmCutoff,
			  "bool" => \$pwmBoolean,
			  "out=s"=> \$outFile,
			  "nt2st"=> \$nt2st,
			  "h"    => \&usage);
$seqIndex = $ARGV[0];

if ( ! -e $outDir ) {
    printf STDERR "Output directory does not exist:\t$outDir\n";
    exit
}

sub usage
{
    printf STDERR "\n\nReads DNA sequence files (fasta without line breaks),";
    printf STDERR "\n and writes out structure file(s) for each sequence,";
    printf STDERR "\n sequence motifs (motifs.csv), PWMs (pwmscore.csv) and structural parameters (structures.csv)";
    printf STDERR "\n\n";
    
    printf STDERR "\tUsage: @{[basename($0)]}  [options] <FILE> -o <PATH> \n\n";
    
    printf STDERR "<FILE>\tsequence index file, current: $seqIndex'\n";
    printf STDERR "\tNOTE: the sequence index file defines the file names\n";
    printf STDERR "\t      of the DNA sequence files!\n";
    printf STDERR "-fasta:s\tmulti-sequence fasta file, alternative to separate fasta files defined by <FILE>, current $fastaFile\n";
   printf STDERR "-tri:s\tfile providing trinucleotide parameters, current $trinucFile\n";
    printf STDERR "-din:s\tfile providing dinucleotide parameters, current $dinucFile\n";
    printf STDERR "-mot:s\tfile providing motif patterns, current $motifFile\n";
    printf STDERR "-pwm:s\tfile providing motif position weight matrices, current $pwmFile\n";
    printf STDERR "-cut:s\tPWM cutoff (fraction of maximal score), current $pwmCutoff\n";
    printf STDERR "-o:s\toutput directory, current: $outDir\n";
    printf STDERR "-nt2st\t: add nucleotides also to structure.csv, current$nt2st\n";
    printf STDERR "-out:s\tbasename for output files, current: $outFile\n\n";
    exit;
}

# REGISTER SEQUENCE FILES 


unless ( open(IDX, "<$seqIndex") )
{
    printf STDERR "\n\tERROR\t$seqIndex\tNO INDEX FILE FOR SEQUENCES FOUND!\n";
    usage();
}

my %chr;
my @chrID = ();
my @chrName = ();
my @chrLength =();
# parse header
my @val = split '\t', <IDX>;
chomp(@val);
my $lengthCol ="";
my $idCol     = "";
my $nameCol   = "";
for( my $i=0; $i<@val; $i++ )
{
    $idCol = $i if $val[$i] eq "#ID";
    $nameCol = $i if $val[$i] eq "name";
    $lengthCol = $i if $val[$i] eq "length";
}
my $cnt = 0;
while(<IDX>)
{
    my @val = split '\t';
    chomp(@val);

    $chrID[$cnt] = $val[$idCol];
    $chrName[$cnt] = $val[$nameCol];
    $chrLength[$cnt] = $val[$lengthCol];
    $chr{$val[$nameCol]} = $val[$idCol];
    $cnt++;
}

# set sequence file names
my @filelist = ();

if ( $fastaFile ne "" ) {
    $filelist[0] = $fastaFile;
    unless ( -e $filelist[0] )
    {
	printf STDERR
	    "\n\tERROR\t$filelist[0]\tsequence file not found!";
	usage();
    }
    print "REGISTERED $filelist[0] of ".@chrID." files\n";
} else {

    my $chromDir = dirname($seqIndex);
    
    foreach ( my $i=0; $i<@chrID; $i++ )
    {
	$filelist[$i] = $chromDir."/".$chrName[$i].".fasta";
	unless ( -e $filelist[$i] )
	{
	    printf STDERR
		"\n\tERROR\t$filelist[$i]\tsequence file not found!";
	    usage();
	}
	print "REGISTERED $filelist[$i] of ".@chrID." files\n";
    }
}



# !!! TODO : read parameters and motifs from parameter file !!

# SEQUENCE PARAMETERS

# TRINUCLEOTIDE PARAMETER FILE

my @trinucleotide = ();
my @triname = ();

unless ( open(TRI, "<$trinucFile") )
{
	printf STDERR
	    "\n\tWARNING\t$trinucFile\ttrinuc. param. file not found!\n";
	#usage();
}


my $line = <TRI>;
# remove ""
$line =~ s/"//g; 
# trim white spaces
$line =~ s/\s+$//g;
$line =~ s/^\s+//g;

my @val = split '\t', $line;
chomp(@val);
my $idCol     = "";
my @trinCol = ();
my @trin = ();
my $cnt = 0;
for( my $i=0; $i<@val; $i++ )
{
	$idCol = $i if $val[$i] eq "ID";
	if ( $val[$i] =~ /^([AGTC][AGTC][AGTC])$/ )
	{
	    $trinCol[$cnt] = $i;
	    $trin[$cnt] = $1;
	    #print "found $trin[$cnt] in column $trinCol[$cnt]\n";
	    $cnt++;
	}
}
$cnt = 0;
while(<TRI>)
{
	my $line = $_;
	# remove ""
	$line =~ s/"//g;
	# trim white spaces
	$line =~ s/\s+$//g;
	$line =~ s/^\s+//g;
	
	my @val = split '\t', $line;
	chomp(@val);
	
	# trim white spaces
	foreach(@val)
	{
	    $_ =~ s/\s+$//g;
	    $_ =~ s/^\s+//g;
	}
	
	
	$triname[$cnt] = $val[$idCol];
	#print "$triname[$cnt]\n";
	for ( my $i=0; $i<@trin; $i++ )
	{
	    unless ( $val[$trinCol[$i]] eq "" )
	    {
		$trinucleotide[$cnt]{$trin[$i]} = $val[$trinCol[$i]];
		#print "\t$trin[$i] : $val[$trinCol[$i]]";
	    }
	}
	#print "\n";
	$cnt++;
}




# DINUCLEOTIDE PARAMETER FILE

my @dinucleotide  = ();
my @diname = ();

unless ( open(DIN, "<$dinucFile") )
{
	printf STDERR
	    "\n\tWARNING\t$dinucFile\tdinuc. param. file not found!\n";
	#usage();
}


my $line = <DIN>;
# remove ""
$line =~ s/"//g; 
# trim white spaces
$line =~ s/\s+$//g;
$line =~ s/^\s+//g;

my @val = split '\t', $line;
chomp(@val);
my $idCol     = "";
my @dinCol = ();
my @din = ();
my $cnt = 0;
for( my $i=0; $i<@val; $i++ )
{
	$idCol = $i if $val[$i] eq "ID";
	if ( $val[$i] =~ /^([AGTC][AGTC])$/ )
	{
	    $dinCol[$cnt] = $i;
	    $din[$cnt] = $1;
	    #print "found $din[$cnt] in column $dinCol[$cnt]\n";
	    $cnt++;
	}
}
$cnt = 0;
while(<DIN>)
{
	my $line = $_;
	# remove ""
	$line =~ s/"//g;
	# trim white spaces
	$line =~ s/\s+$//g;
	$line =~ s/^\s+//g;
	
	my @val = split '\t', $line;
	chomp(@val);
	
	# trim white spaces
	foreach(@val)
	{
	    $_ =~ s/\s+$//g;
	    $_ =~ s/^\s+//g;
	}
	
	
	$diname[$cnt] = $val[$idCol];
	#print "$diname[$cnt]\n";
	for ( my $i=0; $i<@din; $i++ )
	{
	    $dinucleotide[$cnt]{$din[$i]} = $val[$dinCol[$i]];
	    #print "\t$din[$i] : $val[$dinCol[$i]]";
	}
	#print "\n";
	$cnt++;
}


    

# MOTIF SEQUENCE PATTERN FILE
my @motif = ();
my @motname = ();

unless ( open(MOT, "<$motifFile") )
{
    printf STDERR
	"\n\tWARNING\t$motifFile\tmotif file not found!\n";
    #usage();
}


my $line = <MOT>;
$line =~ s/"//g;
my @val = split '\t', $line;
chomp(@val);
my $idCol  = "";
my $motCol = "";
for( my $i=0; $i<@val; $i++ )
{
    $idCol = $i if $val[$i] eq "ID";
    $motCol= $i if $val[$i] eq "SEQUENCE PATTERN";
}
$cnt = 0;
while(<MOT>)
{
    my $line = $_;
    # remove ""
    $line =~ s/"//g;
    # trim white spaces
    $line =~ s/\s+$//g;
    $line =~ s/^\s+//g;
    
    my @val = split '\t', $line;
    chomp(@val);
    
    # trim white spaces
    foreach(@val)
    {
	$_ =~ s/\s+$//g;
	$_ =~ s/^\s+//g;
    }	
    
    $motname[$cnt] = $val[$idCol];
    $motif[$cnt]   = $val[$motCol];
# WARNING: when changing regex make sure the length calculation
#          for this motif still works!
    $cnt++;
}


# POSITION WEIGHT MATRIX FILE
unless ( open(PWM, "<$pwmFile") )
{
    printf STDERR
	"\n\tWARNING\t$pwmFile\tposition weight matrix file not found!\n";
    #usage();
}

my %matrix = ();
my %pwmLength = ();
my $pwmId = "";
my $foundPWM = 0;
# NOTE : own format, e.g
#MOTIF\tname
#A\t0.3\t0.1\t...
#C\t0.2\t0.1\t...
#G\t0.1\t0.1\t...
#T\t0.4\t0.7\t...
while(<PWM>)
{
    my $line = $_;
    # remove ""
    $line =~ s/"//g;
    
    my @val = split '\t', $line;
    chomp(@val);
    # trim white spaces
    foreach(@val)
    {
	$_ =~ s/\s+$//g;
	$_ =~ s/^\s+//g;
    }	

    # skip empty rows
    next if $val[0] eq "";

    # parse matrix header
    if ( $val[0] eq "MOTIF" )
    {
	$pwmId = $val[1]; # motif ID
	$foundPWM++;
	next;
    }

    # parse matrix, first value in row must be [ACGT]
    if ( $val[0] =~ /^[AGCT]$/ )
    {
	$pwmLength{$pwmId} = @val - 1;
	for ( my $i=1; $i<=$pwmLength{$pwmId}; $i++ )
	{
	    ##       ID     NUCL     POS
	    $matrix{$pwmId}{$val[0]}[$i-1] = $val[$i];
	}	
    }
}

## calculate maximal score
my %pwmMaxScore = ();
my $nucs = "ACGT";
foreach my $pwmId ( keys %matrix )
{
    my $tot = 0;
    # go through all positions
    for ( my $pos=0; $pos<$pwmLength{$pwmId}; $pos++ )
    {
	my @max = ();
	# go through nucleotides
	for ( my $nt=0; $nt<length($nucs); $nt++ )
	{
	    # store maximal value
	    my $Nt = substr($nucs,$nt,1);
	    $max[$pos] = $matrix{$pwmId}{$Nt}[$pos]
		if ( !defined($max[$pos]) ||
		     $matrix{$pwmId}{$Nt}[$pos] > $max[$pos] );
	}
	# calculate sum
	$tot += $max[$pos];
    }
    $pwmMaxScore{$pwmId} = $tot;
    #print "MATRIX\t$pwmId\tMAX:$tot\n";
}
#exit;

# NOT USED YET
# OPEN optional PWM cutoff file
#unless ( open(CUT, "<$pwmCutoff") )
#{
#    #printf STDERR
#	#"\n\tWARNING\t$pwmFile\tPWM cutoff file not found!\n";
#    #usage();
#}
#
#my %pwmCut = ();
#
#while(<CUT>)
#{
#    #my $line = $_;
#    # remove ""
#    #$line =~ s/"//g;
#}



# poly(dN) tracts
my @tract = ();
    
$tract[0] = "A";
$tract[1] = "T";
$tract[2] = "G";
$tract[3] = "C";

my @tractname;
$tractname[0] = "polyA";
$tractname[1] = "polyT";
$tractname[2] = "polyG";
$tractname[3] = "polyC";

my @tract_r = ();
$tract_r[0] = "T";
$tract_r[1] = "A";
$tract_r[2] = "C";
$tract_r[3] = "G";

my @tractname_r;
$tractname_r[0] = "polyA.r";
$tractname_r[1] = "polyT.r";
$tractname_r[2] = "polyG.r";
$tractname_r[3] = "polyC.r";

# CHECK PARSED PARAMETERS AND DATA
print "TRINUCLEOTIDE PARAMETERS:\t";
foreach ( @triname ) { print $_,", "; }
print "\n";
print "DINUCLEOTIDE PARAMETERS:\t";
foreach ( @diname ) { print $_,", "; }
print "\n";
print "MOTIF PATTERNS:         \t";
foreach ( @motname ) { print $_,", "; }
if ( $doTracts ) {foreach ( @tractname ) { print $_,", "; } }
print "\n";
print "PWM SCORES:             \t";
foreach ( sort {$pwmLength{$a} <=> $pwmLength{$b}} keys %pwmLength ) { print $_,", "; }
print "\n";



# SET OUTPUT FILES

my $strf = new IO::Handle;
my $motf = new IO::Handle;
my $pwmf = new IO::Handle;

$outFile .= "." if $outFile ne "";

# STRUCTURE FILE

if ( @diname || @triname )
{
    my $strucfile = $outDir."/".$outFile."structures.csv";
    open($strf, ">$strucfile") or die "can't open structure file: $strucfile\n";
    print  "writing structure file $strucfile\n";

    $strf->print("chr\tcoor");
    $strf->print("\tNt") if ( $nt2st ) ;# plot nucleotide ALSO in motif file
    for ( my $j=0; $j<@dinucleotide; $j++ )
    {
	$strf->print("\t".$diname[$j]);
#	$strf->print("\t".$diname[$j].".r");
    }
    for ( my $j=0; $j<@trinucleotide; $j++ )
    {
	$strf->print("\t".$triname[$j]);
    }
    $strf->print("\n");
    
}

# MOTIF FILE

if ( @motname )
{
    my $motfile =  $outDir."/".$outFile."motifs.csv";
    open($motf, ">$motfile") or die "can't open motif file: $motfile\n";
    print  "writing motif file $motfile\n";

    $motf->print("chr\tcoor\tNt"); # plot nucleotide in motif file
    for ( my $j=0; $j<@motif; $j++ )
    {
	$motf->print("\t".$motname[$j]);
	$motf->print("\t".$motname[$j].".r");
    }
    if ( $doTracts )
    {
	for ( my $j=0; $j<@tract; $j++ )
	{
	    $motf->print("\t".$tractname[$j]);
	}
	for ( my $j=0; $j<@tract; $j++ )
	{
	    $motf->print("\t".$tractname_r[$j]);
	}
    }
    $motf->print("\n");
}

# PWM FILE

if ( $foundPWM > 0 )
{
    my $pwmfile =  $outDir."/".$outFile."pwmscores.".$pwmCutoff.".csv";
    open($pwmf, ">$pwmfile") or die "can't open PWM file: $pwmfile\n";
    print  "writing PWM file $pwmfile , found $foundPWM motifs\n";

    $pwmf->print("chr\tcoor");
    foreach ( sort  {$pwmLength{$a} <=> $pwmLength{$b}} keys %pwmLength )
    {
	$pwmf->print("\t".$_);
	$pwmf->print("\t".$_.".r");
    }
    $pwmf->print("\n");
    
}


# PARSE SEQUENCE FILES AND CALCULATE VALUES

my $cnt = 1;
for  ( my $idx=0;$idx<@filelist; $idx++ )
{
    my $filename = $filelist[$idx];
    # open fasta file
    open(fasta, "<$filename") or die "can't open sequence file: $filename\n";

    my $cntError = 0;
    while(<fasta>)
    {
	# check header and jump to sequence
	# TODO: different header for chromosome files!!
	if ( /^>\s?(.*[^\s])/ ) 
	{
	    # which chromosome?
	    my $chrom = $1;

	    # CHECK CHROMOSOME !

	    unless ( $chrom =~ /^$chrName[$idx]/ )
	    {
		print STDERR "FATAL\tWRONG CHROMOSOME\t$chrom ne $chrName[$idx]\n";
		exit;
	    }

	    
#	    $chrom =~ s/chr//g;
	    my $seq=<fasta>;
	    print "\twriting chromosome $chrom ";


	    # CHECK IF WE HAVE A SEQUENCE
	    if ( $seq =~/^[ATGC]/)
	    {
		$cnt++;
		chomp($seq);
		my $length = length($seq);
		
		for( my $i=0; $i<$length; $i++)
		{
		    # FIRST: current nucleotide
		    my $nuc = substr($seq,$i,1);

		    
		    $strf->print($chrID[$idx]."\t".($i+1))
			if @diname || @triname;
		    $strf->print("\t".$nuc) if $nt2st;
		    
		    
		    # DINUCLEOTIDE params
		    my $di = substr($seq,$i,2); # in forward dir.
		    # catch last positions
		    $di = "xx" if $i >= $length-2;
		    $di = "xx" if $i >= $length-1;  

		    for ( my $j=0; $j<@dinucleotide; $j++ )
		    {
			# forward strand
			$strf->print("\t");
			if ( exists $dinucleotide[$j]{$di} )
			{
			    $strf->print($dinucleotide[$j]{$di});
			}
			else # errors for nt. wildcards and last pos.
			{
			    unless ( $i >= $length-2 )
			    {
				print "ERROR di# $j, $i:$di, ";
				$cntError++;
			    }
			}
		    }
		    
		    # TRINUCLEOTIDE params
		    my $tri = substr($seq,$i-1,3);
		    # catch first and last positions
#		    $tri = "xxx" if $i < 1 || $i >= $length-2;
		    $tri = "xxx" if $i < 1 || $i >= $length-1; # BETTER?
		    # reverse complement
		    my $triR = reverse $tri;
		    $triR =~ tr/ATGC/TACG/;

		    for ( my $j=0; $j<@trinucleotide; $j++ )
		    {
			$strf->print("\t");
			if ( exists $trinucleotide[$j]{$tri} )
			{
			    $strf->print($trinucleotide[$j]{$tri});
			}
			elsif( exists $trinucleotide[$j]{$triR} )
			{
			    $strf->print($trinucleotide[$j]{$triR});
			}
			else # errors for nt. wildcards and 1st/last pos.
			{
			    unless ( $i < 2 || $i >= $length-2 )
			    {
				print "ERRORR tri# $j, $i:$tri, ";
				$cntError++;
			    }
			}
		    }
		    
		    $strf->print("\n") if @diname || @triname;

		    # MOTIFS

		    $motf->print($chrID[$idx]."\t".($i+1)."\t".$nuc) if @motname;
		    # TODO : get motif lengths before and
		    # sort by decreases length, then use
		    # subst only once on big sequence and reuse
		    # for smaller sequences : general : also do tri/di params
		    # within this loop!
		    for ( my $j=0; $j<@motif; $j++ )
		    {			
			 
			# remove option brackets to get tru motif length
			my $molen = length($motif[$j]);
			$molen -= (length($1)-1) while $motif[$j] =~ /(\[\w+\])/g ;

			# extract sequence with same length as motif
			my $mot = substr($seq,$i,$molen);

                        # get reverse complement in other direction
			my $motR = substr($seq,($i-($molen-1)),$molen);
			my $motR = reverse $motR;
			$motR =~ tr/ATGC/TACG/;			

			# match ?
			$motf->print("\t");
			$motf->print("1") if $mot  =~ /$motif[$j]/;

			# reverse  match ?
			$motf->print("\t");   
			$motf->print("1") if $motR =~ /$motif[$j]/;
		    }

		    # TRACTS
		    if ( $doTracts )
		    {
			for ( my $j=0; $j<@tract; $j++ )
			{
			    my $tractlength = 1;
			    # for the first nucleotide in a potential poly(N) tract ...
			    if ( $tract[$j] eq $nuc &&
				 substr($seq,$i-1,1) ne $nuc )			     
			    {
				for ( my $k=1; $k<10; $k++ )
				{
				    # ... count tract length
				    if ( $nuc eq substr($seq,$i+$k,1) )
				    {
					$tractlength++;
				    }
				    else
				    {
					$k=10;last;
				    }
				}
			    }
			    $motf->print("\t");
			    $motf->print($tractlength) if $tractlength > 1;
			}
			# REVERSE TRACTS
			for ( my $j=0; $j<@tract_r; $j++ )
			{
			    my $tractlength = 1;
			    # for the first nucleotide in a potential poly(N) tract ...
			    if ( $tract_r[$j] eq $nuc &&
				 substr($seq,$i+1,1) ne $nuc )
			    {
				for ( my $k=1; $k<10; $k++ )
				{
				    # ... count tract length				
				    if ( $nuc eq substr($seq,$i-$k,1) )
				    {
					$tractlength++;
				}
				    else
				    {
					$k=10;last;
				    }
				}
			    }
			    $motf->print("\t");
			    $motf->print($tractlength) if $tractlength > 1;
			}
		    }
		    
		    $motf->print("\n") if @motname;

		    
		    # PWMs
		    # TODO : use neighborhood to calculate background nt.
		    # frequencies, if not provided on commandline
		    # set neighborhood size on commandline, use whole chrom.
		    # or +/- 50 as defaults
		    $pwmf->print($chrID[$idx]."\t".($i+1)) if %matrix;
		    foreach my $id ( sort  {$pwmLength{$a} <=> $pwmLength{$b}}
				     keys %pwmLength )
		    {
			# extract sequence with same length as motif
			my $mot = substr($seq,$i,$pwmLength{$id});

                        # get reverse complement in other direction
			my $motR = substr($seq,($i-($pwmLength{$id}-1)),$pwmLength{$id});
			my $motR = reverse $motR;
			$motR =~ tr/ATGC/TACG/;

			# forward score
			$pwmf->print("\t");
			my $score = getPWMScore($matrix{$id}, $mot);
			if ( $score <= $pwmCutoff*$pwmMaxScore{$id} )
			{
			    $score = "";
			}
			else
			{
			    $score /= $pwmMaxScore{$id};
			    $score = 1 if $pwmBoolean;
			}
			$pwmf->print($score)
			    if $length >= $i + $pwmLength{$id};

			# reverse score
			$pwmf->print("\t");   
			my $score = getPWMScore($matrix{$id}, $motR);
			if ( $score <= $pwmCutoff*$pwmMaxScore{$id} )
			{
			    $score = "";
			}
			else
			{
			    $score /= $pwmMaxScore{$id};
			    $score = 1 if $pwmBoolean;
			}
			$pwmf->print($score)
			    if ($i+1) >= $pwmLength{$id};
		    }
		    $pwmf->print("\n") if %matrix;		    
		}
		
	    }
	    else
	    {
		print  " ERROR: NO SEQUENCE FOUND IN $filename\n";
	    }
	}
	else
	{
	    print  " ERROR: NO HEADER FOUND IN $filename\n";
	}
	
    }
    if ( $cntError > 0 )
    {
	print  " ERROR: $cntError di- or trinucleotides not calculated";
    }
    else
    {
	print  " success.";
    }
    print  "\n";
    
}
close($strf);
close($motf);
close($pwmf);


# TODO : implement background model and score filter!
sub getPWMScore
{
    my %matrix = %{$_[0]};
    my $seq    = $_[1];
    my $cutoff = $_[2];
	
    my $score;

    my @nt = split "", $seq;
    for ( my $i=0; $i<@nt; $i++ )
    {
	$score += $matrix{$nt[$i]}[$i];
    }
    
    return $score;
}
