#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <2009-10-14 20:51:36 raim>
# $Id: mergeFeatures.pl,v 1.15 2009/11/03 09:38:07 raim Exp $

use strict;
use FileHandle;
use Getopt::Long;
use File::Basename;

my $outfile = "features.csv";
my $files   = "featureData/featurefiles";
my $reqvals = "";
my $path = ".";

# TODO : convert yeast feature IDs to upper or lower case before
# comparison

# get command line parameters
usage() unless GetOptions("i=s"   => \$files,
			  "p=s"   => \$path,
			  "o=s"   => \$outfile,
			  "f=s"   => \$reqvals,
			  "h"     => \&usage);

sub usage
{
    printf STDERR "\n\n Reads several .csv files with values for single \n";
    printf STDERR " features (e.g. proteins) and merges them into one file\n\n";
    
    printf STDERR "\tUsage: @{[basename($0)]} -i <DATAFILE> [options]\n\n";

    printf STDERR "-i=s\tDATAFILE, containing paths and column data\n";
    printf STDERR "-p=s\tpath where files in DATAFILE can be found\n";
    printf STDERR " to be merged, current: $files\n";
    printf STDERR "-o=s\talternative outfile name, current: $outfile\n";
    printf STDERR "-f=s\tcomma-separated list of requested files (ID), ".
	"current: $reqvals\n";
    printf STDERR "\n";
    exit;
}

my $errorfile = $outfile.".log";
unless ( open(ERRF, ">$errorfile") )
{
    print "\n\nERROR:\tcan't open log file $errorfile\n\n";
    usage();
    
}

unless ( open(dataf, "<$files") ) 
{
    print "\n\nERROR\tNO DATAFILE PROVIDED at $files\n\n";
    usage();
}

my @fileid = (); # id for infiles
my @infile = (); # infiles
my @headln = (); # number of header lines in infile
my @duplic = (); # how to handle duplicates
my @child  = (); # should the values also be printed for children
                 # features in input file? (introns, CDS)
my @columh = (); # columns of interest in infile, as HEADER IDs
my @column = (); # columns of interest in infile, as NUMBERS
my @numval = (); # column value should be numeric
my @header = (); # new headers in outfile
my @nodata = (); # strings to print if no data is available

# The following data will go in an additional annotation file:
my @biotyp = (); # biological data classification, e.g. ChIP
my @dattyp = (); # data classification, e.g. number, log2ratio, etc.
my @source = (); # data source IDs 
my @descrp = (); # files with further data description to be attached to annotation file

# READ LIST OF INPUT FILES

# default take all files
my $all = 1;    

# activate filter
my @filter = split(/,/, $reqvals);

if ( @filter > 0 )
{
    $all = 0;
    print "Requested files\n\t";
}
for ( my $i=0;$i<@filter; $i++ )
{
    print "$filter[$i] ";
}
print "\n";

my $cnt = -1;
my $id;
my %take =();
my @fid;
while(<dataf>)
{
    my @values = split '\='; 
    #skip comments and empty lines
    next if @values != 2;

    chomp(@values);

    # remove whitespace
    $values[0] =~ s/^\s+//;    
    $values[0] =~ s/\s+$//;
    $values[1] =~ s/^\s+//;    
    $values[1] =~ s/\s+$//;
    
    # register ID
    if ( $values[0] eq "ID" )
    {
	$id = $values[1];
	for ( my $i=0;$i<@filter; $i++ )
	{
	    $take{$id} = 1 if $id eq $filter[$i];	    
	}
    }

    if ( $all || ( $take{$id} || $id eq "main" ) )
    {
	# start with infile
	if ( $values[0] eq "infile" )
	{
	    $cnt++ ;
	    $infile[$cnt]  = $values[1];
	    # remember file id
	    $fileid[$cnt]  = $id;
	    print "$id\tregistering file #$cnt\t$infile[$cnt]\n";
	}
	
	next if $cnt < 0;
	
	# how many header rows?
	$headln[$cnt] = $values[1] if $values[0] eq "headln";
	# inherit values for children ?
	$child[$cnt]  = $values[1] if $values[0] eq "child";
	# how to handle duplicates?
	$duplic[$cnt] = $values[1] if $values[0] eq "duplic";

	# requested column numbers ?
	if ( $values[0] eq "column" )
	{
	    #print $values[1]."\t columns\tat $cnt\n";
	    @{$column[$cnt]} = split ',', $values[1]; 
	}
	# requested column header IDs
	if ( $values[0] eq "oldhead" )
	{
	    #print $values[1]."\t old header\tat $cnt\n";
	    @{$columh[$cnt]} = split ',', $values[1]; 
	}
	# new header names - if specified 
	@{$header[$cnt]} = split ',', $values[1] if $values[0] eq "header"; 
	    
	# should it be a numerical value?
	@{$numval[$cnt]} = split ',', $values[1] if $values[0] eq "type"; 

	# string for unavailable/empty data
	@{$nodata[$cnt]} = split ',', $values[1] if $values[0] eq "nodata";

	# META DATA	# biotyp e?
	@{$dattyp[$cnt]} = split ',', $values[1] if $values[0] eq "dattyp";
	@{$biotyp[$cnt]} = split ',', $values[1] if $values[0] eq "biotyp";
	$source[$cnt] = $values[1] if $values[0] eq "source.id";
	$descrp[$cnt] = $values[1] if $values[0] eq "descrpt";

    }
}

# EXPAND COLNUM, NUMVAL AND NODATA ARRAYS 
for ( my $cnt=0; $cnt<@infile; $cnt++ )
{
    print ERRF "CHECKING $infile[$cnt]\n";

    if ( (defined $column[$cnt] && defined $columh[$cnt]) &&
	 (scalar(@{$column[$cnt]}) != scalar(@{$columh[$cnt]}) ) )
    {
	print ERRF "ERROR\t".scalar(@{$column[$cnt]})."!=".scalar(@{$columh[$cnt]}).
	    "\tCOLUMN CONTROL FAILED: "."different number of columns for ".$infile[$cnt]."\n"; 
	print      "ERROR\t".scalar(@{$column[$cnt]})."!=".scalar(@{$columh[$cnt]}).
	    "\tCOLUMN CONTROL FAILED: "."different number of columns for ".$infile[$cnt]."\n";
	exit;	
    }

    
    my $colnum = 0;
    # number of columns
    if ( defined $columh[$cnt] ) # use column header IDs preferentially
    {
	$colnum = scalar(@{$columh[$cnt]});	
    }
    elsif ( defined $column[$cnt] ) # else use column numbers
    {
	$colnum = scalar(@{$column[$cnt]});
    }    
    # check if number of input columns equals number of requested columns
    if ( scalar(@{$header[$cnt]}) != $colnum )
    {
	print  "ERROR:\tdifferent number of columns and headers ".
	    "in $infile[$cnt]\n\t$colnum != ".scalar(@{$header[$cnt]})."\n";
	print ERRF "ERROR:\tdifferent number of columns and headers ".
	    "in $infile[$cnt]\n\t$colnum != ".scalar(@{$header[$cnt]})."\n";
	exit;
    }

    # expand NUMERIC FLAG to all columns, if not given
    # default value 0 (need not be numeric)
    $numval[$cnt][0] = 0 unless ( defined $numval[$cnt] );
    if ( scalar(@{$numval[$cnt]}) == 1 && $colnum > 1 )
    {
	print ERRF "Message\t$colnum columns expanded for NUMERIC flag\n";
	for ( my $i=1; $i<$colnum; $i++ )
	{
	    $numval[$cnt][$i] = $numval[$cnt][0];
	}	
    }
    
    # expand NODATA STRING to all columns, if not given
    # default value 0 (need not be numeric)
    $nodata[$cnt][0] = "" unless ( defined $nodata[$cnt] );
    if ( scalar(@{$nodata[$cnt]}) == 1 && $colnum > 1 )
    {
	print ERRF "Message\t$colnum columns expanded for NODATA string\n";
	for ( my $i=1; $i<$colnum; $i++ )
	{
	    $nodata[$cnt][$i] = $nodata[$cnt][0];
	}	
    }
    
    # expand DATA TYPE STRING to all columns, if not given
    # default value 0 (need not be numeric)
    $biotyp[$cnt][0] = "" unless ( defined $biotyp[$cnt] );
    if ( scalar(@{$biotyp[$cnt]}) == 1 && $colnum > 1 )
    {
	print ERRF "Message\t$colnum columns expanded for DATA TYP string\n";
	for ( my $i=1; $i<$colnum; $i++ )
	{
	    $biotyp[$cnt][$i] = $biotyp[$cnt][0];
	}	
    }  
    $dattyp[$cnt][0] = "" unless ( defined $dattyp[$cnt] );
    if ( scalar(@{$dattyp[$cnt]}) == 1 && $colnum > 1 )
    {
	print ERRF "Message\t$colnum columns expanded for DATA TYP string\n";
	for ( my $i=1; $i<$colnum; $i++ )
	{
	    $dattyp[$cnt][$i] = $dattyp[$cnt][0];
	}	
    }  
    
}


my %value;
my %total; # for "mean" handling of duplicate
my $valcnt = 0;
my %parent;

# PARSE INPUT FILES
for ( my $cnt=0; $cnt<@infile; $cnt++)
{
    print      "Reading $fileid[$cnt] : $infile[$cnt]\t";
    print ERRF "Reading $fileid[$cnt] : $infile[$cnt]\t";
    
   
    if ( open(val, "<$path/$infile[$cnt]") )
    {
	print      "ok\n";
	print ERRF "ok\n";
    }
    else
    {
	print      "ERROR\tcouldn't find input file \"$infile[$cnt]\"\n";
	print ERRF "ERROR\tcouldn't find input file \"$infile[$cnt]\"\n";
	exit;
    }
    # PARSE HEADER LINES
    my $foundhead = 0;
    if ( $headln[$cnt] > 0 && defined $headln[$cnt] )
    {
	# skip header lines
	# count-down header lines until LAST
	print ERRF "COUNT DOWN\t$headln[$cnt]\tHEADER LINES\n"
	    if  ($headln[$cnt] - 1) > 1 && defined $headln[$cnt];
	while ( ($headln[$cnt] - 1) > 0 && defined $headln[$cnt] )
	{
	    $headln[$cnt]--;
	    <val>;
	}
	
	# get column numbers
	my $line = <val>;
	my @vals = split /\t/, $line;
	chomp(@vals);

	# use column IDs if defined
	if ( defined $columh[$cnt] )
	{
	    for ( my $i=0; $i<@vals; $i++ )
	    {
		for ( my $j=0; $j<@{$columh[$cnt]}; $j++ )
		{
		    if ( $vals[$i] eq $columh[$cnt][$j] )
		    {
			print ERRF
			    "\t".$columh[$cnt][$j]." -> ".$i."\tIS".
			    "\t".$header[$cnt][$j];
			# print column number if it was also given
			print ERRF " -> ".$column[$cnt][$j] if
			    defined $column[$cnt][$j]; 
			print ERRF "\n";

			# COLUMN CONTROL - if both ID and number were given
			if ( defined $column[$cnt][$j] &&
			     $column[$cnt][$j] != $i )
			{
			    print ERRF "ERROR\t$column[$cnt][$j] != $i\t".
				"COLUMN CONTROL FAILED: ID $columh[$cnt][$j] ".
				"found in wrong column\n";
			    print      "ERROR\t$column[$cnt][$j] != $i\t".
				"COLUMN CONTROL FAILED: ID $columh[$cnt][$j] ".
				"found in wrong column\n";
			    exit;
			}
			$column[$cnt][$j] = $i;
		    }
		}
	    }
	}
	# use column numbers
	elsif ( defined $column[$cnt] )
	{
	    print ERRF "\tUSING COLUMN NUMBERS\t";
	    for (my $j=0; $j<@{$column[$cnt]}; $j++)
	    {
		print ERRF $vals[$column[$cnt][$j]]."->".$header[$cnt][$j].",";
	    }
	    print ERRF "\n";
	    # TODO : check if requested column numbers are available
	}
    }
    # no header, use column numbers 
    elsif ( defined $column[$cnt] )
    {
	print ERRF "WARNING\tNO header line present, trusting COLUMN NUMBERS\n";
	for (my $j=0; $j<@{$column[$cnt]}; $j++)
	{
	    print ERRF "\t".$column[$cnt][$j]."->".$header[$cnt][$j].",";
	}
	print ERRF "\n";
	# TODO : check during parsing if requested column numbers are available
    }
    else
    {
	print ERRF "ERROR\tNEITHER COLUMN NUMBERS NOR ID DEFINED in $files ".
	    " for file \"$infile[$cnt]\"\n";
	print      "ERROR\tNEITHER COLUMN NUMBERS NOR ID DEFINED in $files ".
	    " for file \"$infile[$cnt]\"\n";
	exit;
    }

    # CHECK IF HEADER COLUMNS WERE FOUND for headers were only ID was supplied!!
    for ( my $j=0; $j<@{$header[$cnt]}; $j++ )
    {
	if ( !defined($column[$cnt][$j]) )
	{
	    print ERRF "ERROR\tcolumn ".
		$j.": ".$columh[$cnt][$j]." -> ".$header[$cnt][$j].
		" NOT FOUND in file ".$cnt.": ".$infile[$cnt]."\n";
	    print      "ERROR\tcolumn ".
		$j.": ".$columh[$cnt][$j]." -> ".$header[$cnt][$j].
		" NOT FOUND in file ".$cnt.": ".$infile[$cnt]."\n";
	    exit();
	}	
    }
    
    print ERRF "PARSING DATA from file".$infile[$cnt]."\n"; 

    # count and handle duplicates
    my %duplicates = ();
    my %updateType = ();
    my $duplperfile= 0;
    
    # default action: skip subsequent duplicates!
    $duplic[$cnt] = "skip" unless $duplic[$cnt] ne "";

    # NOW PARSE DATA
    while(<val>)
    {
	
	my @values = split /\t/, $_;

	# preprocess values
	for ( my $i=0; $i<@values; $i++ )
	{
	    # substitute ""
	    $values[$i] =~ s/"//g;
	    # chomp
	    chomp($values[$i]);
	    # remove whitespace
	    $values[$i] =~ s/^\s+//;    
	    $values[$i] =~ s/\s+$//;	    
	}
	chomp(@values);
	
	my $id = $values[0];
	# only in coordinate file "main"
	$parent{$id} = $values[1]
	    if $fileid[$cnt] eq "main" && $values[1] ne "";
	
	# PREPROCESS IDs
	# substitute ""
	$id =~ s/"//g;
	# chomp
	chomp($id);
	# remove whitespace
	$id =~ s/^\s+//;    
	$id =~ s/\s+$//;
	
	
	# further preprocess only subsequent input files' IDs
	if ( $fileid[$cnt] ne "main" && ($id =~ /^Y/ && !($id =~ /delta/)) )
	{
	    # trailing index to uppercase
	   # unless ( $id =~ /delta/ )
	    {
		$id =~ s/a$/A/;
		$id =~ s/b$/B/;
		$id =~ s/c$/C/;
		$id =~ s/d$/D/;
		# uppercase strand index
		$id =~ s/w-([A-Z])$/W-$1/;
		$id =~ s/c-([A-Z])$/C-$1/;
		# add trailing -, e.g. for WA, W-A
		$id =~ s/W([A-Z])$/W-$1/;
		$id =~ s/C([A-Z])$/C-$1/;
	    }
	    # repeats, transposons etc. are often given in uppercase
	    # while in official SGD they are in lowercase
	    
	}
	
	# SKIP empty IDs
	next if $id eq "";
	# SKIP "NA" IDs
	next if $id eq "NA";

	my @update = updateFeature($id);
	my $ID = $update[0];
	

	# SKIP features not in reference input file
	next if $update[1] eq "deleted";
	next if $update[1] eq "unknown";

	# SKIP features not in reference input file
	if ( !(exists $value{$ID}) && $fileid[$cnt] ne "main" )
	{
	    print ERRF
		"WARNING\t".$ID." (".$id."/".$values[0].")".
		" from file $infile[$cnt] NOT IN REFERENCE file $infile[0]";
	    print ERRF "\twas ".$update[1] if $update[1] ne "none";
	    print ERRF "\n";
	    next;
	}
	
	# HANDLE DUPLICATES !!

	# check if ID already exists
	if ( exists $duplicates{$ID} )
	{
	    print ERRF "WARNING\t".$ID." (".$id."/".$values[0].")";
	    print ERRF " is DUPLICATE #".$duplicates{$ID}.
		" from ".$infile[$cnt];
	    
	    print ERRF "\n\tCURRENT was ".$update[1].","
		if $update[1] ne "none";
	    print ERRF "\n\tPREVIOUS was ".$updateType{$ID}.","
		if $updateType{$ID} ne "none";

	    print ERRF "\n\tHANDLE: ".$duplic[$cnt]."\n";
	    
	    $duplperfile++;

	}
	# remember this ID and it's update type
	$duplicates{$ID}++;
	my $oldUpdateType = $updateType{$ID}; 
	$updateType{$ID} = $update[1];
	
	# ALL OK: go through requested columns
	for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
	{
	    # store total for "mean" handle of duplicates
	    $total{$ID}[$cnt][$vals]++ if $value{$ID}[$cnt][$vals] ne ""; 

	    # MAIN : STORE DATA ON FIRST OCCURENCE
	    if ( $duplicates{$ID} == 1 )
	    {
		$value{$ID}[$cnt][$vals] = $values[$column[$cnt][$vals]];
	    }
	    # handle duplicates
	    else
	    {
		print ERRF "\tVALUE\t".
		    $header[$cnt][$vals]."\tprevious: ".
		    $value{$ID}[$cnt][$vals]."\tcurrent: ".
		    $values[$column[$cnt][$vals]];
		
		if ( $duplic[$cnt] eq "merge" ) # join values, separated by ','
		{
		    $value{$ID}[$cnt][$vals] .= ","
			if ( $value{$ID}[$cnt][$vals] ne "" );
		    $value{$ID}[$cnt][$vals] .= $values[$column[$cnt][$vals]];
		}
		# take add or mean (mean taken below)
		elsif ( $duplic[$cnt] eq "add" ||
		        $duplic[$cnt] eq "mean" ) 
		{
		    $value{$ID}[$cnt][$vals] += $values[$column[$cnt][$vals]];
		}
		elsif ( $duplic[$cnt] eq "higher" ) # take higher value
		{		    
		    $value{$ID}[$cnt][$vals] = $values[$column[$cnt][$vals]]
			if ( $values[$column[$cnt][$vals]] >
			     $value{$ID}[$cnt][$vals] );
		}
		elsif ( $duplic[$cnt] eq "lower" ) # take lower value
		{		    
		    $value{$ID}[$cnt][$vals] = $values[$column[$cnt][$vals]]
			if ( $values[$column[$cnt][$vals]] <
			     $value{$ID}[$cnt][$vals] );
		}
		elsif ( $duplic[$cnt] eq "original" )# take original from merged
		{		    
		    $value{$ID}[$cnt][$vals] = $values[$column[$cnt][$vals]]
			if ( $updateType{$ID} eq "none" &&
			     $oldUpdateType =~ /^merged/ );
		}
		elsif ( $duplic[$cnt] eq "eliminate" ) # eliminate value
		{		    
		    $value{$ID}[$cnt][$vals] = "";
		}
		print ERRF "\tnew: $value{$ID}[$cnt][$vals]\n";

		# DEFAULT:
		# do nothing default duplicate handle types
		# i.e. just TAKE FIRST OCCURING VALUE!
	    }
	}
    }

    if ( $duplperfile>0 )
    {
	print    "\tWARNING\t".$infile[$cnt]." had ".$duplperfile.
	    " duplicates, HANDLE was: \"".$duplic[$cnt]."\"\n";
	print ERRF "WARNING\t".$infile[$cnt]." had ".$duplperfile.
	    " duplicates, HANDLE was: \"".$duplic[$cnt]."\"\n";
    }
}


open(OUTF, ">$outfile") or die "can't open outfile $outfile\n";

# print header
print OUTF "ID";	

for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	print OUTF "\t".$header[$cnt][$vals];
    }
}
print OUTF "\n";




foreach my $id ( sort {$a cmp $b} keys %value )
{
    # skip empty keys
    next if $id eq "";

    print OUTF $id;

    for ( my $cnt = 0; $cnt<@infile; $cnt++ )
    {
	my $ID = $id;
	# map ID to parent's id if requested
	$ID = $parent{$id} if $child[$cnt] && exists $parent{$id};

	# print data if available as numeric value
	for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
	{
	    if ( exists $value{$ID}[$cnt][$vals] )
	    {
		# should it be a numeric value?
		if ( $numval[$cnt][$vals] )
		{
		    if ( $value{$ID}[$cnt][$vals] =~ /^-?\d/ )
		    {
			# duplicate handle "mean" - TODO : test!
			if ( $duplic[$cnt]  eq "mean" )
			{
			    $value{$ID}[$cnt][$vals] =
				$value{$ID}[$cnt][$vals]/
				$total{$ID}[$cnt][$vals];
			}
			print OUTF "\t".$value{$ID}[$cnt][$vals];
		    }
		    else
		    {
                        # if not numeric print nodata key
			print OUTF "\t".$nodata[$cnt][$vals];
		    }
		}
		else
		{
		    print OUTF "\t".$value{$ID}[$cnt][$vals];
		}
	    }
	    # if n.a. print nodata key
	    else
	    {
		print OUTF "\t".$nodata[$cnt][$vals];		
	    }
	}
    }
    print OUTF "\n";
}

close(OUTF);
print      "MERGED ALL DATA into file\t$outfile\n";
print ERRF "MERGED ALL DATA into file\t$outfile\n";

# PRINT META DATA (ANNOTATION) FILE
$outfile = $outfile.".type.csv";
open(OUTF, ">$outfile") or die "can't open outfile $outfile\n";

# print header
print OUTF "ID";	

for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	print OUTF "\t".$header[$cnt][$vals];
    }
}
print OUTF "\n";

# print data type
print OUTF "EXP";
for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	print OUTF "\t".$biotyp[$cnt][$vals];
    }
}
print OUTF "\n";
# print data type
print OUTF "TYP";
for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	print OUTF "\t".$dattyp[$cnt][$vals];
    }
}
print OUTF "\n";
# data numeric?
print OUTF "NUM";
for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	print OUTF "\t".$numval[$cnt][$vals];
    }
}
print OUTF "\n";
# print data source
print OUTF "SOURCE";
for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    for (my $vals = 0; $vals<@{$column[$cnt]}; $vals++)
    {
	my $src = "";
	$src = $source[$cnt] if defined($source[$cnt]);
	print OUTF "\t".$src;
    }
}
print OUTF "\n";

## attach additional data description
print OUTF "DESCRIPTION";
for (my $cnt = 0; $cnt<@infile; $cnt++)
{
    my %descrtext = ();
    if ( defined($descrp[$cnt]) )
    {
	open(DF, "<$descrp[$cnt]") or
	    die "can't open specified data description file: $descrp[$cnt]\n";
	
	print      "ADDING ANNOTATION FILE\t$descrp[$cnt]\n";
	print ERRF "ADDING ANNOTATION FILE\t$descrp[$cnt]\n";

	while(<DF>)
	{
	    my @val = split '\t';
	    chomp(@val);
	    # column 0 should be equivalent to header in main data
	    # and column 3 should be a description
	    $descrtext{$val[0]} = $val[2];
	}
    }
    my @heads = @{$header[$cnt]};
    for (my $vals = 0; $vals<@heads; $vals++)
    {
	print OUTF "\t";
	if ( defined($descrtext{$heads[$vals]}) )
	{
	    print OUTF ($descrtext{$heads[$vals]});
	}
	else
	{
	    print OUTF ($heads[$vals]); # just repeat header
	}
    }
}    
print OUTF "\n";


close(OUTF);

print      "STORED DATA ANNOTATION in file\t$outfile\n";
print ERRF "STORED DATA ANNOTATION in file\t$outfile\n";


close(ERRF);
print "CHECK LOGFILE for errors\t$errorfile \n";

# to be expanded by ALIASes!!
sub updateFeature
{
    my $id = $_[0];
    my $newId = $id;

    # MERGED ORFs
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003504
    # 2007-12-11: ORFs merged
    $newId = "YGR271C-A" if $id eq "YGR272C";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000117700
    $newId = "YER109C" if $id eq "YER108C";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001683
    # 1998-11-10: ORFs merged
    $newId = "YKL201C" if $id eq "YKL200C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000082
    $newId = "YAR042W" if $id eq "YAR044W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000509
    $newId = "YCL004W" if $id eq "YCL003W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000651
    $newId = "YCR057C" if $id eq "YCR055C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000676
    $newId = "YCR081W" if $id eq "YCR080W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000279
    $newId = "YBR074W" if $id eq "YBR075W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000304
    $newId = "YBR098W" if $id eq "YBR100W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000518
    $newId = "YCL014W" if $id eq "YCL012W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000565
    $newId = "YCL061C" if $id eq "YCL060C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000567
    $newId = "YCL063W" if $id eq "YCL062W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000626
    $newId = "YCR030C" if $id eq "YCR029C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000654
    $newId = "YCR057C" if $id eq "YCR058C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000658
    $newId = "YCR061W" if $id eq "YCR062W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002882
    $newId = "YDR475C" if $id eq "YDR474C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001888
    $newId = "YFL007W" if $id eq "YFL006W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002882
    $newId = "YDR475C" if $id eq "YDR474C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001851
    $newId = "YFL042C" if $id eq "YFL043C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003014
    $newId = "YGL045W" if $id eq "YGL046W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007609
    $newId = "YJL012C" if $id eq "YJL012C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003554
    $newId = "YJL016W" if $id eq "YJL017W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003555
    $newId = "YJL019W" if $id eq "YJL018W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003558
    $newId = "YJL020C" if $id eq "YJL021C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028807
    $newId = "YKL157W" if $id eq "YKL158W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001682
    $newId = "YKL198C" if $id eq "YKL199C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028570
    $newId = "YLR313C" if $id eq "YLR312C-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000115154
    $newId = "YLR390W-A" if $id eq "YLR391W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000082441
    $newId = "YML034W" if $id eq "YML033W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000005614
    $newId = "YOR087W" if $id eq "YOR088W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000005766
    $newId = "YOR239W" if $id eq "YOR240W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000006294
    $newId = "YPR089W" if $id eq "YPR090W";
    #http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000664
    # NOTE: YCR068W-A never was official SGD annotation
    $newId = "YCR068W" if $id eq "YCR068W-A";
    #http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000006907
    $newId = "YERCTy1-2" if $id eq "YERCsigma4";
    #http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007028
    $newId = "YJLWTy4-1" if $id eq "YJLWdelta2";

    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id MERGED into $newId.\n";
	return ($newId, "merged from: ".$id);
    }
  

    # DELETED ORFs, KNOWN    
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000519
    $newId = "deleted" if $id eq "YCL013W";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000531
    $newId = "deleted" if $id eq "YCL026C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000624
    $newId = "deleted" if $id eq "YCR029C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000700
    $newId = "deleted" if $id eq "YCR103C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000120182
    $newId = "deleted" if $id eq "YAR043C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000120184
    $newId = "deleted" if $id eq "YAR052C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000120186
    $newId = "deleted" if $id eq "YAR074C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000512
    $newId = "deleted" if $id eq "YCL006C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000526
    $newId = "deleted" if $id eq "YCL021W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000558
    $newId = "deleted" if $id eq "YCL053C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000652
    $newId = "deleted" if $id eq "YCR056W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000666
    $newId = "deleted" if $id eq "YCR070W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000119982
    $newId = "deleted" if $id eq "YCR074C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001920
    $newId = "deleted" if $id eq "YFR024C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000700
    $newId = "deleted" if $id eq "YCR103C";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007427
    $newId = "deleted" if $id eq "YER186W-A";
    $newId = "deleted" if $id eq "YER187W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007614
    $newId = "deleted" if $id eq "YKL003W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000006918
    $newId = "deleted" if $id eq "YERWdelta18";
     
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=L000003336
    # The following putative ORFs were originally included in the annotation of Chromosome III, but were later withdrawn: YCLX01W, YCLX02C, YCLX03C, YCLX04W, YCLX05C, YCLX06C, YCLX07W, YCLX09W, YCLX10C, YCLX11W, YCLX12W, YCRX01W, YCRX02C, YCRX03C, YCRX04W, YCRX05W, YCRX06W, YCRX07W, YCRX08W, YCRX09C, YCRX10W, YCRX11W, YCRX12W, YCRX14W, YCRX15W, YCRX16C, YCRX17W, YCRX18C, YCRX19W, YCRX20C, and YCRX21C.
    $newId = "deleted" if $id eq "YCLX01W";
    $newId = "deleted" if $id eq "YCLX02C";
    $newId = "deleted" if $id eq "YCLX03C";
    $newId = "deleted" if $id eq "YCLX04W";
    $newId = "deleted" if $id eq "YCLX05C";
    $newId = "deleted" if $id eq "YCLX06C";
    $newId = "deleted" if $id eq "YCLX07W";
    $newId = "deleted" if $id eq "YCLX09W";
    $newId = "deleted" if $id eq "YCLX10C";
    $newId = "deleted" if $id eq "YCLX11W";
    $newId = "deleted" if $id eq "YCLX12W";
    $newId = "deleted" if $id eq "YCRX01W";
    $newId = "deleted" if $id eq "YCRX02C";
    $newId = "deleted" if $id eq "YCRX03C";
    $newId = "deleted" if $id eq "YCRX04W";
    $newId = "deleted" if $id eq "YCRX05W";
    $newId = "deleted" if $id eq "YCRX06W";
    $newId = "deleted" if $id eq "YCRX07W";
    $newId = "deleted" if $id eq "YCRX08W";
    $newId = "deleted" if $id eq "YCRX09C";
    $newId = "deleted" if $id eq "YCRX10W";
    $newId = "deleted" if $id eq "YCRX11W";
    $newId = "deleted" if $id eq "YCRX12W";
    $newId = "deleted" if $id eq "YCRX14W";
    $newId = "deleted" if $id eq "YCRX15W";
    $newId = "deleted" if $id eq "YCRX16C";
    $newId = "deleted" if $id eq "YCRX17W";
    $newId = "deleted" if $id eq "YCRX18C";
    $newId = "deleted" if $id eq "YCRX19W";
    $newId = "deleted" if $id eq "YCRX20C";
    $newId = "deleted" if $id eq "YCRX21C";

    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id DELETED.\n";
	return ($id, "deleted");
    }

    # DELETED ORFs, UNKNOWN
    # only YER087C-A OR YER087C-B exist
    $newId = "unknown" if $id eq "YER087C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YIL162C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YPR041C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YER144W";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YAR037W";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YAR040C";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YHR214C
    # only YHR214C-A to YHR214C-E exist
    $newId = "unknown" if $id eq "YHR214C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YMR325C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YPR012C";
    # not annotated at SGD
    $newId = "unknown" if $id eq "YDR542C";

    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id UNKNOWN.\n";
	return ($id, "unknown");
    }

    # NAME CHANGES, MISSING SUFFIX
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003855
    $newId = "YJR094W-A" if $id eq "YJR094W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002127
    $newId = "YER019C-A" if $id eq "YER019C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002101
    $newId = "YKL006C-A" if $id eq "YKL006C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000000718
    $newId = "YCR073W-A" if $id eq "YCR073W";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YAL034W
    $newId = "YAL034W-A" if $id eq "YAL034W";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002957
    $newId = "YER007C-A" if $id eq "YER007C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002959
    $newId = "YER066C-A" if $id eq "YER066C";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YHR049C
    $newId = "YHR049C-A" if $id eq "YHR049C";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YOR304C
    $newId = "YOR304C-A" if $id eq "YOR304C";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YBR084C
    $newId = "YBR084C-A" if $id eq "YBR084C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002158
    $newId = "YBR162W-A" if $id eq "YBR162W";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YCR087C
    $newId = "YCR087C-A" if $id eq "YCR087C";
    
    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$newId SUFFIX added.\n";
	return ($newId, "suffix added to: ".$id);
    }

    # NAME CHANGES
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000005413
    $newId = "YOL052C-A" if $id eq "YOL053C";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001859
    $newId = "YFL034C-B" if $id eq "YFL035C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002965
    $newId = "YFL017W-A" if $id eq "YFL018W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002954
    $newId = "YEL059C-A" if $id eq "YEL059C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003770
    $newId = "YJR010C-A" if $id eq "YJR010C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004329
    $newId = "YLR337C" if $id eq "YLR337W";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002137
    $newId = "YAL034C-B" if $id eq "YAL035C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028517
    $newId = "YBL039W-B" if $id eq "YBL039W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002138
    $newId = "YAL042C-A" if $id eq "YAL043C-A";
    #http://db.yeastgenome.org/cgi-bin/locus.pl?locus=YAL058C
    $newId = "YAL056C-A" if $id eq "YAL058C";
    $newId = "YAL056C-A" if $id eq "YAL058C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002140
    $newId = "YAL064C-A" if $id eq "YAL065C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002148
    $newId = "YBL100W-A" if $id eq "YBL101W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002149
    $newId = "YBL100W-B" if $id eq "YBL101W-B";
    # http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028598
    $newId = "YBL100W-C" if $id eq "YBL101W-C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002157
    $newId = "YBR089C-A" if $id eq "YBR090C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007548
    $newId = "YCL026C-B" if $id eq "YCL027C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028740
    $newId = "YDR524W-C" if $id eq "YDR524W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001859
    $newId = "YFL034C-B" if $id eq "YFL035C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000002956
    $newId = "YEL075W-A" if $id eq "YEL076W";
    $newId = "YEL075W-A" if $id eq "YEL076W-C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001859
    $newId = "YFL034C-B" if $id eq "YFL035C-A;";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000006436
    $newId = "YFL034C-A" if $id eq "YFL035C-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003536
    $newId = "YIL014C-A" if $id eq "YIL015C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007241
    $newId = "YIR020W-A" if $id eq "YIR020W-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000003742
    $newId = "YJL205C" if $id eq "YJL206C-A";
    $newId = "YJL205C" if $id eq "YJL205C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000001956
    $newId = "YKL096W-A" if $id eq "YKL097W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028677
    $newId = "YLR157W-D" if $id eq "YLR157W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028678
    $newId = "YLR157W-E" if $id eq "YLR157W-C";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004471
    $newId = "YML009W-B" if $id eq "YML010W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004469
    $newId = "YML009C-A" if $id eq "YML010W-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004469
    $newId = "YML009C-A" if $id eq "YML010C-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004474
    $newId = "YML012C-A" if $id eq "YML013C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028809
    $newId = "YML031C-A" if $id eq "YML032C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004499
    $newId = "YML034C-A" if $id eq "YML035C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004512
    $newId = "YML047W-A" if $id eq "YML048W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004522
    $newId = "YML057C-A" if $id eq "YML058C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004561
    $newId = "YML094C-A" if $id eq "YML095C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004569
    $newId = "YML101C-A" if $id eq "YML102C-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004586
    $newId = "YML116W-A" if $id eq "YML117W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000004768
    $newId = "YMR158W-B" if $id eq "YMR158W-A";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007249
    $newId = "YMR158C-A" if $id eq "YMR158C-B";
    #http://db.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000028699
    $newId = "YNL097C-B" if $id eq "YNL097C-A";

    # Retired Names and Aliases
    $newId = "YHL021C" if $id eq "FMP12";
    $newId = "YKR016W" if $id eq "FMP13";
    $newId = "YPL099C" if $id eq "FMP14";
    $newId = "YHR198C" if $id eq "FMP22";
    $newId = "YJR080C" if $id eq "FMP26";
    $newId = "YER080W" if $id eq "FMP29";
    
    $newId = "YOR286W" if $id eq "FMP31";
    $newId = "YHR199C" if $id eq "FMP34";
    $newId = "YIL157C" if $id eq "FMP35";
    $newId = "YDR493W" if $id eq "FMP36";
    $newId = "YOR205C" if $id eq "FMP38";
    $newId = "YMR157C" if $id eq "FMP39";
    $newId = "YBR262C" if $id eq "FMP51";
    $newId = "YLR274W" if $id eq "CDC46";
    $newId = "YBR202W" if $id eq "CDC47";
    $newId = "YPR019W" if $id eq "CDC54";
    $newId = "YDR359C" if $id eq "VID21";
#    $newId ""          if $id eq "39356";
    $newId = "YJR122W" if $id eq "CAF17";
    $newId = "YNR065C" if $id eq "YSN1";
    $newId = "YML013W" if $id eq "SEL1";
    $newId = "YBR194W" if $id eq "SOY1";
    $newId = "YJL092W" if $id eq "HPR5";
    $newId = "YJR006W" if $id eq "HYS2";
    $newId = "YDL013W" if $id eq "HEX3";
    $newId = "YNR017W" if $id eq "MAS6";
    $newId = "YMR022W" if $id eq "QRI8";
    $newId = "YBR287W" if $id eq "ZSP1";

    $newId = "YCL026C-A" if $id eq "YCLX08C";
    $newId = "YCR073W-A" if $id eq "YCRX13W";
    $newId = "YOL052C-A" if $id eq "YOL053C-A";
    $newId = "YFL010W-A" if $id eq "YFL011W-A";
    $newId = "YHR039C-A" if $id eq "YHR039C-B";
    $newId = "YHR079C-A" if $id eq "YHR079C-B";
    $newId = "YLR390W-A" if $id eq "YLR391W-A";
    $newId = "YIL017C" if $id eq "YIL017W";
    $newId = "YIL071C" if $id eq "YIL071W";
    $newId = "YCR097W" if $id eq "YCR097W-B";
    $newId = "YOR343W-A" if $id eq "YOR343C-A";
    $newId = "YOR343W-B" if $id eq "YOR343C-B";
    $newId = "YDR210C-C" if $id eq "YDR210W-C";
    $newId = "YDR210C-D" if $id eq "YDR210W-D";
    
    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id UPDATED to $newId.\n";
	return($newId, "updated from: ".$id);
    }

    # PSEUDO-UPDATES:

    ## INTEIN : not physically mapped and thus not in feature main file,
    ## but converted here for future versions
    $newId = "VDE" if $id eq "YDL184W-A";
    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id UPDATED to $newId.\n";
	return($newId, "updated from: ".$id);
    }
    
    # ALIASES - strand change!
    # NOT CONVERTED, since all where assigned a different strand
    # with re-annotation,the wrong strand might have been used for array design!
    $newId = "YBLWsigma1"  if $id eq "YBLCsigma1";
    $newId = "YBLCtau1"    if $id eq "YBLWtau1";
    $newId = "YBRWdelta18" if $id eq "YBRCdelta18";
    $newId = "YDRWsigma2"  if $id eq "YDRCsigma2";
    $newId = "YDRCsigma4"  if $id eq "YDRWsigma4";
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007012
    $newId = "YILWdelta2"  if $id eq "YILCdelta2";
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007032
    $newId = "YJRCdelta15" if $id eq "YJLWdelta15";
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007033
    $newId = "YJRCdelta16" if $id eq "YJLWdelta16";
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007034
    $newId = "YJRCdelta19" if $id eq "YJLWdelta19";
    # http://www.yeastgenome.org/cgi-bin/locusHistory.pl?dbid=S000007075
    $newId = "YLRWdelta24" if $id eq "YLRCdelta24";
    
    if ( $newId ne $id )
    {
	print ERRF "WARNING\t$id NOT UPDATED to $newId , ".
	    "because of strand switch!\n";
	return($newId, "none");
    }

    
    return ($newId, "none");    
}

__END__
CLN3 MAK16 CYS3 ADE1 PHO11 ILS1 MCM2 RAD16 SUP45 MET8 HMLALPHA1 MATALPHA1 HMRA1 CDC9 CDC2 TRP1 SIR4 XRS2 CAN1 CUP5 FCY2 MET6 RAD3 YPT1 SMC1 HIS2 HXK1 ADH4 CUP2 TRP5 GCD2 PFK1 SPO11 ARD1 CUP1-1 FUR1 ERG9 SUC2 HIS5 BCY1 LYS1 TPK1 ARG3 CYR1 CYC1 ECM17 URA1 APE2 ELM1 VPS1 SIR1 CDC25 LEU3 HMG1 NDC1 MCM1 PFK2 ADE4 DAL82 KEX2 RPC31 TOP2 LYS9 HXT11 TOP1 DED1 PRO2 RAD17 GAL4 TPK2 PEP4 ERG10 HTS1 RPC40 COX1 COB COX2 
