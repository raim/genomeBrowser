#!/usr/bin/env perl
#	$Id: wig2coors.pl,v 1.3 2015/01/05 11:21:26 raim Exp $

## downloaded 20150105 from http://genomewiki.ucsc.edu/images/9/9d/FixStepToBedGraph_pl.txt
## and modified to write tataProject genome data files (1-based)
## REPLACES wig2coordinates.pl

## original comments
#	fixStepToBedGraph.pl - read fixedStep and variableStep wiggle input data,
#		output four column bedGraph format data
#
#	used thusly:
#	zcat fixedStepData.gz | ./fixStepToBedGraph.pl \
#		| gzip > bedGraph.gz


use warnings;
use strict;
use Scalar::Util qw(looks_like_number);

my $position = 0;
my $chr = "";
my $step = 1;
my $span = 1;

## yeast chromosome index
my @r2a = ("I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII","XIII","XIV","XV","XVI","M","2-micron");

print STDERR "usage: wig2coors.pl\n";
print STDERR "\trun in a pipeline like this:\n";
print STDERR "usage: ",
"zcat fixedStepData.gz | fixStepToBedGraph.pl | gzip > bedGraph.gz",
	"\n";
print STDERR "reading input data from stdin ...\n";

while (my $dataValue = <>)
{
    chomp $dataValue;
    if ( ($dataValue =~ m/^track / || $dataValue =~ m/^browser /) || 
	 $dataValue =~ m/^#/ ) {
        print STDERR "Skipping track line\n";
        next;
    }
    if ( $dataValue =~ m/^fixedStep / || $dataValue =~ m/^variableStep / ) {
        $position = 1;
        $chr = "";
        $step = 1;
        $span = 1;
        my @a = split('\s', $dataValue);
        for (my $i = 1; $i < scalar(@a); ++$i) {
            my ($ident, $value) = split('=',$a[$i]);
            if ($ident =~ m/chrom/) { $chr = $value; }
            elsif ($ident =~ m/start/) { $position = $value; }
            elsif ($ident =~ m/step/) { $step = $value; }
            elsif ($ident =~ m/span/) { $span = $value; }
            else {
            print STDERR "ERROR: unrecognized fixedStep line: $dataValue\n";
            exit 255;
            }
	    $chr =~ s/chr//g;
	    if ( !looks_like_number($chr) ) {
		($chr) = grep { $r2a[$_] eq $chr } 0..$#r2a;	
		$chr+=1;
	    }
        }
      } else {
          my @b = split('\s+', $dataValue);
          if (scalar(@b)>1) {
              $position = $b[0];
              $dataValue = $b[1];
          }
	  if ( $span > 1 ) {
	      printf "%s\t%d\t%d\t%g\n", $chr, $position, $position+$span-1, $dataValue;
	  } else {
	      printf "%s\t%d\t%g\n", $chr, $position, $dataValue;
	  }

          $position = $position + $step;
      }
}
