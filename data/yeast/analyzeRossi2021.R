
## Promoter classes by Rossi et al. 2021

yeast.path <- Sys.getenv("YEASTDAT")
fig.path <- file.path(yeast.path, "figures")
out.path <- file.path(yeast.path, "processedData")

feature.file <- file.path(yeast.path,
                          "feature_R64-1-1_20110208_v20241119.csv")
cluster.file <- file.path(yeast.path, 'parameters',
                          'clusters.tsv')

## genomeBrowser feature table
genes <-read.delim(feature.file)
genes <- genes[genes$type=='gene',]

## overwrite relaxed CL.segment mapping by Jaccard filter
genes$CL.segment[which(genes$J.segment < .5)] <- 'na'


cls.type <- 'CL_rdx' # 'CL.segment' # 
cls.info <- read.delim(cluster.file, row.names=1)
cls.info <- cls.info[cls.info$column%in%c(cls.type,'na'),]

## ADD HOC/ab.n AND LOC/cd.n to major
##if ( cls.type=='CL_rdx' ) {
##    cls.info$group[cls.info$name%in% c('hoc', 'loc')] <- 'major'
##}

cls.ids <- rownames(cls.info)
cls.map <- setNames(cls.info$name, rownames(cls.info))
cls.srt <- cls.info$name
cls.srt.major <-  cls.info$name[cls.info$group%in%c('major','coding')]
cls.nms <- setNames(cls.info$name, cls.srt)
cls.col <- setNames(cls.info$color, cls.srt)

cls <- cls.map[genes[,cls.type]]

ovl <- clusterCluster(genes$CL_promoter, cls, cl2.srt=cls.srt.major)
ovl <- sortOverlaps(ovl, p.min=1e-3)

plotdev(file.path(out.path, "rossi21_classes_major"), type='png',
        width=4, height=3)
par(mai=c(.5,1.2,.5,.5), mgp=c(1.3,.3,0), tcl=-.15)
plotOverlaps(ovl, p.min=1e-10, p.txt=1e-5, show.total=TRUE, xlab=NA, ylab=NA,
             axis1.col=cls.col[cls.srt.major])
dev.off()

ovl <- clusterCluster(genes$CL_promoter, cls, cl2.srt=cls.srt)
ovl <- sortOverlaps(ovl, p.min=1e-3)

plotdev(file.path(out.path, "rossi21_classes"), type='png',
        width=8, height=3)
par(mai=c(.5,1.2,.5,.5), mgp=c(1.3,.3,0), tcl=-.15)
plotOverlaps(ovl, p.min=1e-10, p.txt=1e-5, show.total=TRUE, xlab=NA, ylab=NA,
             axis1.col=cls.col)
dev.off()

ovl <- clusterCluster(genes$CL_fragile, cls, cl2.srt=cls.srt)
ovl <- sortOverlaps(ovl, p.min=1e-3)

plotdev(file.path(out.path, "rossi21_fragile"), type='png',
        width=8, height=2)
par(mai=c(.5,.6,.5,.5), mgp=c(1.3,.3,0), tcl=-.15)
plotOverlaps(ovl, p.min=1e-10, p.txt=1e-5, show.total=TRUE, xlab=NA, ylab=NA,
             axis1.col=cls.col)
dev.off()

ovl <- clusterCluster(genes$CL_fragile, cls, cl2.srt=cls.srt.major)
ovl <- sortOverlaps(ovl, p.min=1e-3)

plotdev(file.path(out.path, "rossi21_fragile_major"), type='png',
        width=3.5, height=2)
par(mai=c(.5,.6,.5,.5), mgp=c(1.3,.3,0), tcl=-.15)
plotOverlaps(ovl, p.min=1e-10, p.txt=1e-5, show.total=TRUE, xlab=NA, ylab=NA,
             axis1.col=cls.col[cls.srt.major])
dev.off()
