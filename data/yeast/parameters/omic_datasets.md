# Methods for the Omics Dataset Collection

collected from Methods and Supporting Information of the indicated publications.

## slavov11b

* Platform: custom Agilent Yeast oligo microarrays 8 × 15k

* From the supporting material/Appendix PDF:

```
3 Measuring mRNA

To measure mRNA levels, we used 2ml of the culture sampled at each
time point and vacuum filtered the cells followed by immediate
freezing in liquid nitrogen and then in a freezer at −80 o C. RNA for
microarray anal- ysis was extracted by the acid–phenol–chloroform
method. RNA was amplified and labeled using the Agilent low RNA input
fluorescent linear amplification kit (P/N 5184-3523; Agilent
Technologies, Palo Alto, CA). This method involves initial synthesis
of cDNA by using a poly(T) primer attached to a T7 promoter. Labeled
cRNA is subsequently synthesized using T7 RNA polymerase and either
Cy3 or Cy5 UTP. Each Cy5-labeled experimental cRNA sample was mixed
with the Cy3-labeled reference cRNA and hybridized for 17h at 65 o C
to custom Agilent Yeast oligo microarrays 8 × 15k having 8 microarrays
per glass slide. Microarrays were washed, scanned with an Agilent DNA
microarray scanner (Agilent Technologies), and the resulting TIF files
processed using Agilent Fea- ture Extraction Software version
7.5. Resulting microarray intensity data were submitted to the PUMA
Database for archiving.

[...]

In our batch culture, genes that peak in LOC generally have higher
mean expression levels relative to the reference, a glucose-limited,
asynchronous culture growing exponentially at growth rate µ = 0.25h −1
.
```

* From Figure 3 caption:

"The blue bars correspond to the same data but centered to a zero mean
for each gene."

### Note

The data was downloaded from website at Princeton and the file name
`ymc_eth_raw_centered_ben.cdt` indicates that this the data centered
to a mean of zero. 

### Open Questions

* Were the raw data just ratios of Cy5/Cy3 fluorescence or additionally
logged?
* Was the reference data always the same from Brauer et al. 2008, or
was this freshly done for each experiment?
* How was the centering was done, merely by subtracting the mean of
each gene, or also scaled to standard deviation?

## nocetti16

* Platform: Illumina NEBNext Ultra, Illumina sequencer unspecified

* From the Methods section:

```
RNA from each time point was extracted by using the hot phenol
method, and libraries were generated with the NEBNext Ultra
RNA library preparation kit for Illumina (catalog no. E7530L) as
per manufacturer’s instructions (Schmitt et al. 1990). Data were
mapped and processed using Bowtie, SAMtools, and custom
Perl scripts.
```

* From GEO

```
Data processing: 	
alignment - bowtie2
nucleosome peak calling - iNPS_V1.1.2
RPKM values were generated using in house created software. RPKM = (ReadsWithinORF) / ( ORFLength/1000 x TotalNumReads/1,000,000 )
```

## chin12

* Platform: Affymetrix,  GeneChip Yeast Genome 2.0 Array

* From GEO:

```
Characteristics 	strain: CEN.PK113-7D
phase angle: 356.87 degrees

Treatment protocol 	For cells with a ~2 h dissolved oxygen period, 32 samples (the pellet from 0.5 ml with the supernatant removed) were removed every 4 min, immediately frozen in liquid nitrogen, and store at -80 C.

Growth protocol 	CEN.PK113-7D (MATa), grown overnight in 20 ml YPD at 275 rpm and 30C, was inoculated into the fermentor. The fermentor maintained the pH, agitation speed, aeration, and temperature at 4.0, 750 rpm, 150 ml/min, and 30 oC, respectively. Cells were grown in either the B. Braun Biotech Biolab CP (Aylesbury, Buckinghamshire, UK) at 650 ml or the New Brunswick Scientific Bioflo 110 Fermentor/Bioreactor (Edison, NJ) at 850 ml volume. Continuous flow was started after 24-30 h by pumping in fermenter media (19.25 g/l D(+)glucose monohydrate, 5 g/l (NH4)2SO4, 2 g/l KH2PO4, 0.5 g/l MgSO4, 1 g/l yeast extract, 0.2 ml/l antifoam A, 0.1 g/l CaCl2, 0.02 g/l FeSO4, 0.01 g/l ZnSO4, 0.005 g/l CuSO4, 0.001 g/l MnCl2, and 1 ml/l 75% H2SO4) at a dilution rate of 0.086/h. Media with excess cells was pumped out of the fermentor to maintain a constant fermentor volume. The media was maintained at pH 4.0 with 2.5 N NaOH. The dilution rate was adjusted to 0.06-0.086/h to maintain the period.
Extracted molecule 	total RNA

Extraction protocol 	RNA was purified from the cells and DNA was digested with DNase 1 using the RiboPureTM Yeast kit (Ambion, Austin, TX) according to the manufacturer’s instructions, except the cells were lysed in a minibead beater (BioSpec Products, Bartlesville, OK) rather than by vortexing. The isolated RNA was analyzed for concentration and purity with the NanoDrop ND-1000 spectrophotometer (Thermo Fisher Scientific, Wilmington, DE) and on an Agilent 2100 Bioanalyzer (Agilent Technologies, Palo Alto, CA).
Label 	Biotin

Label protocol 	GeneChip Expression 3'-Amplification Reagents One-Cycle cDNA Synthesis Kit (Affymetrix, Santa Clara, CA) and oligo(dT) primers containing T7 RNA polymerase promoter was used to synthesize double-stranded cDNA from RNA adjusted to 1 mg/ml for each sample. Then, biotinylated cRNA was synthesized using the GeneChip IVT labeling kit from Affymetrix. After framentation to 35-200 bases according to the Affymetrix protocol, 5 micrograms was hybridized to Affymetrix GeneChip Yeast Genome 2.0 Array (Santa Clara, CA) for 16 h at 45°C. The arrays were washed and stained with streptavidin-phycoerythrin using an Affymetrix Fluidics Station 450.
  	
Hybridization protocol 	Standard Affymetrix protocol
Scan protocol 	GeneChips were scanned using an Affymetrix GeneArray scanner according to the manufacturer’s instructions.
Description 	B584_yeast-R21
Data processing 	MicroArray Suite 6.1 and GCOS were used to quantify and analyze results. The submitted files show raw and unnormalized data adjusted for hybridization efficiency using biotinylated Escherichia coli standards.
```

```
Treatment protocol Cells with a ~4 h period (49 samples total) were collected every 5 min for one cycle with a small overlap into the next cycle.
```

## li06 & tu05

* Platform: Affymetrix, GeneChip Yeast Genome 2.0 Array

The data was taken from Dataset S1 of @Machne2012, and normalized
according to this paper. In short: the raw microarray fluorescence
data were normalized to the 'least oscillating set' (LOS)
[@Machne2012] by LOESS regression and using the medians of all genes
with a reported oscillation p-value>0.6 as reference.

### li06

* From Supporting Methods:

```
Sampling. For expression measurements, samples (0.5 ml) were taken every 4 min through an interval of 200 min. The cells
were centrifuged, the supernatant was decanted, and the pellet was placed in a dry-ice acetone bath. The time from removal of
the sample to freezing was less than 1 min. Samples were stored at -80°C.

Total RNA Preparation. For RNA isolation, the pellet was resuspended in 0.5 ml of RNA Later (cat. no. 7020; Ambion,
Austin, TX ) containing 1% 2-mercaptoethanol. Cells were lysed by beating in a minibead beater (BioSpec Products,
Bartlesville, OK) for 3 min with 0.5 ml of acid-washed glass beads (cat. no. G-8772; Sigma, St. Louis, MO). After the cell
lysate was removed, the beads were washed three times with 0.5 ml of RLT buffer (Qiagen, Valencia, CA) containing 1%
2-mercaptoethanol by bead beater (1 min each wash). The cell lysate and washes were pooled. An equal volume of 70%
ethanol was added, and RNA was purified with RNA easy columns (Qiagen) according to the manufacturer. DNA was
digested on the columns according to the instructions. RNA was eluted in a total of 0.1 ml of RNase-free water. The final
RNA samples were analyzed on an Agilent 2100 Bioanalyzer (Agilent Technologies, Palo Alto, CA). Typical total RNA
yields were 20-40 µg with absorbance 260/280 ratios of 1.8-2.2.

Control for differences in RNA yield was accomplished using Affymetrix (Santa Clara, CA) poly(A) standards (cat. no.
900433) added to cell pellets at the beginning of RNA isolation. To normalize RNA yields between different samples, 14 µl
of 1:500 B. subtilis lys, phe, thr, and dap poly(A) standards was added to every 0.5 ml pellet of cells resuspended in 500 µl
of RNA Later before cell lysis and RNA purification to achieve a reasonable signal on the microarray.
```

## kuang14

* Platform: Illumina TruSeq RNA Sample Prep Kit, Illumina HiSeq 2000.

* From Methods:

```
RNA-seq.

RNA was extracted across 16 time points of one cycle at the indicated time points, and RNA-seq was performed. With this strategy, the shortest interval between two OX-phase time points is <5 min, and across the entire cycle the samples were taken at intentionally uneven intervals. 1 ml BY5765 cells from the cycle (about 15 OD/ml) were collected and flash frozen. The pellet was resuspended in 200 μl RNA stat-60 (Tel-Test) and disrupted in a Mini-Beadbeater-16 (Biospec), broken up three times with 100 μl glass beads in a 1.5-ml conical screw-cap tube (USA Scientific, 1415-8700, Ocala, FL). Supernatant was collected into a new microcentrifuge tube, and 800 μl RNA stat-60 was added to repeat bead-beating one more time. The supernatants were combined and incubated at room temperature for 10 min. 200 μl chloroform was then added, and the tube was vortexed for 15 s and left at room temperature for 10 min. The tubes were centrifuged at 4 °C, 16,000 r.c.f. for 15 min, and the supernatant was transferred to a new tube. 500 μl isopropanol was added and mixed by inversion of the tube several times. The mixture was incubated at room temperature for 10 min and centrifuged at 4 °C, 16,000 r.c.f. for 15 min. The pellet was washed with 70% ethanol twice, air dried and resuspended in 100 μl H2O. Library construction and sequencing were performed with the HiSeq platform supervised by the UTSW Microarray Core Facility. Briefly, samples were run on an Agilent 2100 Bioanalyzer to ensure that high-quality RNA was used. 4 μg total RNA was then prepared with the TruSeq RNA Sample Prep Kit (Illumina). mRNA was purified and fragmented before cDNA synthesis. cDNA was then end-repaired and A-tailed. After adaptor ligation, samples were PCR amplified and purified with AmpureXP beads (Agencourt, A63880), then validated again on the Agilent 2100 Bioanalyzer. Before being run on an Illumina HiSeq 2000, samples were quantified by qPCR. Primers within the adaptor sequence were: P1, 5′-AAT GAT ACG GCG ACC ACC GA-3′ and P2, 5′-CAA GCA GAA GAC GGC ATA CGA-3′.

Differential gene expression analysis was performed with TopHat and Cuffdiff according to a standard protocol36. Briefly, the TopHat parameters used were: –bowtie1 -i 40–genome-read-mismatches 4–no-coverage-search -G Saccharomyces_cerevisiae_Ensembl_EF2/Saccharomyces_cerevisiae/Ensembl/EF2/Annotation/Archives/archive-2012-03-09-08-22-49/Genes/genes.gtf Saccharomyces_cerevisiae_Ensembl_EF2/Saccharomyces_cerevisiae/Ensembl/EF2/Sequence/BowtieIndex/genome.  k-means clustering and heat maps were produced with R. Three previously defined superclusters of genes were obtained from a previous study12. A subset of genes from those previously identified as noncycling showed elevated expression in the OX phase by hierarchical clustering with the dChiP package37. Previously defined and newly identified OX-phase genes were combined. FPKM values were imported into R, and the list was intersected with the three superclusters. The values of each supercluster were then subject to k-means clustering with the function 'kmeans'.
```

* From Figure 1 caption:

```
The blue-yellow gradient represents RNA signals normalized to read depth with Cuffdiff and centered to a mean of zero across the 16 time points.
```

### Note

FPKM are Fragments Per Kilobase Million.  The data in the
Supplementary Table S1 appears to be the mean-centered version.

## wang15

* Platform: "comparable to" Illumina TruSeq Stranded mRNA Sample
  Preparation Kit, Illumina HiSeq 2500

* From Methods:

```
Continuous Culture Conditions
Yeast cultures were grown as previously described (Tu et al., 2005). Samples
were collected over two metabolic cycles. For the low-glucose condition, sam-
ples were taken every 36 min for 14.5 hr. For the high glucose condition sam-
ples were taken every 13 min for 4.25 hr.

Library Preparation
RNA-seq libraries were prepared as described in detail previously (Takahashi
et al., 2015).

Bioinformatic Analysis of Metabolic Cycles under Low and Higher
Glucose
TopHat v2.0.10 was used as the mapping program (Trapnell et al., 2009); the
unmapped reads and the reads with mapping quality score less than 10 were
filtered out after mapping (Table S1). The mapped and filtered reads were used
to calculate the RPKM values with HOMER (Heinz et al., 2010). Mapping the
reads to the less well-annotated CEN.PK genome (Nijkamp et al., 2012; Otero
et al., 2010) or normalizing the RPKM between samples did not change the ma-
jor findings of this study (Figure S7).
```

* From the referenced Takashi2015
(url(https://www.sciencedirect.com/science/article/pii/S0076687914000603)):

Trizol -> Ribo-Zero Gold kit for Human/Mouse/Rat (Illumina) -> `The
protocol is based on the Illumina workflow and is comparable to the
Illumina® TruSeq® Stranded mRNA Sample Preparation Kit which has been
used as a reference.` [antisense fix: actinomycinD; strand-specific:
Uracil DNA Glycosylase]

## oneill20

* Platform: Ultimate 3000 RSLC nano System,

* From Methods:

```
Sample:pooled sample ratio (SPS) was calculated for each time point and
normalised so that the median SPS = 1. Sampling times were calculated from
dissolved oxygen (DO 2 ) measurements and converted into radians where 2π
radians is equal to the offset time where the correlation coefﬁcient of a serial
autocorrelation was at its maximum. Linear interpolation was used on each dataset
to calculate the mean SPS across all dilution rates every 0.9 radians. The fold-
change between the maximum and minimum SPS detected in the time course for
each protein identiﬁed in all dilution rates was calculated (FC).
```

## orlando08 & bristow14

* Platform: Affymetrix Yeast 2.0
* Synchronization: elutriation, 
    - P GALL -CDC20 cdc20Δ:  centrifugal elutriation,
	- rad53-1: centrifugal elutriation,
	- cdc8ts: centrifugal elutriation,

* From Methods in @Orlando2008:

```
Strains and cell synchronization

Wild-type and cyclin-mutant strains of S. cerevisiae are derivatives of BF264-15Dau, and they were constructed by standard yeast methods. The clb1,2,3,4,5,6 GAL1–CLB1 mutant strain, along with its growth conditions and synchrony procedures, was described previously10,11.

Strains and cell synchronization

Yeast strains were grown in rich YEP medium (1% yeast extract, 2% peptone, 0.012% adenine, 0.006% uracil) containing 2% galactose. At 45 min before elutriation, dextrose was added to YEP 2% galactose medium to terminate CLB1 expression from the GAL1 promoter. After elutriation, wild-type and clb1,2,3,4,5,6 GAL1–CLB1 cells were grown in rich YEP 2% dextrose, 1 M sorbitol at 30 °C at a density of 107 ml-1. Sorbitol was added to stabilize cells with elongated buds. Aliquots of 50 ml (cell density 107 ml-1) were harvested every 8 min for 4 h. Budding index was determined microscopically by counting at least 200 cells for each time point.

RNA isolation and microarray analysis. Total RNA was isolated at intervals
(every 16 min for a total of 15 time points) as described previously 10 . mRNA was
amplified and fluorescently labelled by GeneChip One-Cycle Target Labelling
(Affymetrix). Hybridization to Yeast 2.0 oligonucleotide arrays (Affymetrix)
and image collection were performed at the Duke Microarray Core Facility
(http://microarray.genome.duke.edu/) in accordance with standard Affymetrix
protocols.
```

* From Results in @Bristow2014:

```
A G1-synchronized population of P GALL -CDC20 cdc20Δ cells was collected by centrifugal elutriation, and suspended in dextrose-containing growth medium at time 0. Aliquots of cells were collected at 20-min intervals for 300 or 360 min (two experimental replicates). 

A G1-synchronized population of cdc20;P GALL -CDC20;cdc8ts;rad53-1 cells was collected by centrifugal elutriation, and suspended in dextrose-containing growth medium at time 0 min.

A G1-synchronized population of cdc20;P GALL -CDC20;cdc8ts cells was collected and analyzed as described above for cdc20;P GALL -CDC20;cdc8ts;rad53-1 cells (one experimental replicate).
```

* From Methods in @Bristow2014:

```
WT and all mutant strains of S. cerevisiae are derivatives of BF264-15Dau and were constructed by standard yeast methods. A description of all yeast strains and plasmids used in this study are outlined in Additional file 1: Table S4. Standard growth conditions were used. Cells were synchronized as previously described [5 @Orlando2008],[6 @SimmonsKovacs2012].

RNA isolation and microarray analysis
For all global mRNA level studies, total RNA was isolated
at time intervals by methods described previously [3].
RNA was purified and concentrated using the RNAeasy
MinElute Cleanup Kit (QIAGEN). mRNA amplification
and fluorescent labeling was done using either the Gene-
Chip One Cycle Labeling (Affymetrix) or the Ambion
MessageAmp Premier kit (Ambion Biosystems). Labeled
cDNA was hybridized to Yeast 2.0 Expression arrays
(Affymetrix) and image collection was carried out by the
Duke Microarray Core Facility [48] using standard Affy-
metrix protocols.
```

## simmonskovacs12

* Platform:
* Synchronization: cdc28-4: centrifugal elutriation, 
* From Methods:

```
Yeast cultures were grown in YEP medium (1% yeast extract, 2% peptone, 0.012% adenine, 0.006% uracil supplemented with 2% sugar: dextrose, sucrose or galactose). For GAL1p-SIC1Δ3P experiments, cells were grown in YEP containing 2% sucrose and 0.1% dextrose prior to centrifugal elutriation or were grown in YEP containing 2% sucrose and arrested in 20–50 ng/mL α factor pheromone. The resulting population was resuspended in YEP medium containing 2% galactose. Identical protocols were carried out for control experiments in cycling cells. For experiments carried out at the cdc28-4 restrictive temperature, cells were grown at 25°C in YEP medium containing 2% dextrose prior to synchronization by centrifugal elutriation. The resulting population was resuspended in YEP media containing 2% dextrose prewarmed to 37°C. Samples were taken every 20 min for 5 or more hours. For CLB1 shut-off experiments, B-cyclin mutant cells were grown in YEP medium containing 2% galactose prior to centrifugal elutriation. The resulting population was resuspended in YEP medium containing 2% dextrose and 1M sorbitol.
```

## cho17

* Platform: Affymetrix Yeast 2.0
* Synchronization: alpha factor arrest (180 min).

* From Methods:

```
Yeast strains and cell culture synchronization

The cdc14–3 and cdc15–2 strains are derivatives of S. cerevisiae BF264–15D (ade1 his2 leu2–3,112 trp1–1a). Strain A1268 (W303 cdc14–3 PDS1-HA-LEU2::pds1) was provided by Angelika Amon47 and outcrossed with BF264–15D 5 times. Yeast cultures were grown in standard YEP medium (1% yeast extract, 2% peptone, 0.012% adenine, 0.006% uracil supplemented with 2% sugar). For synchronization by α-factor, cultures were grown in YEP-galactose medium at 25°C and incubated with 50 ng/ml α-factor for 180 minutes. Synchronized cultures were then resuspended in YEP-dextrose medium at 37°C. Aliquots were taken at each time point for subsequent analysis.

RNA extraction and microarray assay

Total RNA was isolated by standard acid phenol protocol and cleaned up by RNA Clean and Concentrator (Zymo Research) if necessary. Samples were submitted to Duke Microarray Facility for labeling, hybridization, and image collection. mRNA was amplified and labeled by Ambion MessageAmp Premier kit (Ambion Biosystems) and hybridized to Yeast Genome 2.0 Array (Affymetrix).
Normalization of microarray data

Previously published data sets used in this study are GEO: GSE8799, GEO: GSE32974, and GEO: GSE49650. All CEL files analyzed in this study were normalized together using the dChip method from the Affy package in Bioconductor as described previously.28
```

## brauer08

* Platform:  custom Agilent Yeast oligo microarrays 8 × 15k

* From Methods:

```
RNA for microarray analysis was extracted from culture filtrate by the acid-
phenol method. The poly(A)⫹ fraction was purified from total RNA by
oligo(dT) resin, as provided by Oligotex midi kit (QIAGEN, Valencia, CA).
One to 2 mug of poly(A) ⫹ RNA was labeled by direct incorporation of Cy5-
(experimental) or Cy3-dCTP (reference) by using reverse transcription. The
labeled cDNA was mixed and hybridized overnight to cDNA arrays contain-
ing all known and predicted S. cerevisiae open reading frames. Microarrays
were washed, scanned with a GenePix 4000B laser scanner (Axon Instru-
ments, Foster City, CA), and analyzed using GenePix Prosoftware. Resulting
microarray data were submitted to the Stanford Microarray Database for
analysis.
Results for each gene and time point were expressed as the log 2 of the
sample signal divided by the signal in the reference channel. Spots flagged as
“bad” or “missing” during gridding were discarded, as were those with a
red:green pixel regression correlation of ⬍0.6. Batch experiment 1 had an
additional filter applied. The expression ratio for each spot was calculated in
terms of the SD of noise present on an array. Spots with a signal of ⬍2
standard deviations of noise (2 “noise units”) were discarded.
```

## slavov11a

* Platform: custom Agilent Yeast oligo microarrays 8 × 15k

* From Methods:

```
Measuring mRNA

To measure RNA levels we sampled 10–30 ml of steady-state cul-
ture and vacuum filtered the cells, which were immediately frozen
in liquid nitrogen and stored in a freezer at −80°C. RNA for mi-
croarray analysis was extracted by the acid–phenol–chloroform
method. RNA was amplified and labeled using the Agilent low
RNA input fluorescent linear amplification kit (P/N 5184–3523,
Agilent Technologies, Palo Alto, CA). This method involves initial
synthesis of cDNA by using a poly(T) primer attached to a T7 pro-
moter. Labeled cRNA is subsequently synthesized using T7 RNA
polymerase and either Cy3 or Cy5 UTP. Each Cy5-labeled experi-
mental cRNA sample was mixed with the Cy3-labeled reference
cRNA and hybridized for 17 h at 65°C to custom Agilent Yeast
oligo microarrays 8 × 15 K having 8 microarrays per glass slide.
Microarrays were washed and scanned with an Agilent DNA mi-
croarray scanner (Agilent Technologies), and the resulting TIF
files were processed using Agilent Feature Extraction Software,
version 7.5. Resulting microarray intensity data were submitted to
the PUMA Database for archiving.
```

* From Figure 9/10 caption:

```
(B) The left panels show the log 2 expression levels relative to the
reference and the right panels show zero-transformed data to enhance
the visibility of growth rate trends.
```

## xia22 - transcripts

* Platform: Illumina TruSeq v2, NextSeq 500

* From Methods: 


```
mRNA sequencing. Total RNA was extracted and puriﬁed using a Qiagen
RNeasy Mini Kit, according to the user manual, with a DNase step
included (Qiagen, Hilden, Germany). RNA integrity was veriﬁed using a
2100 Bioanalyzer (Agilent Technologies, Santa Clara, USA), and RNA
concentration was determined using a NanoDrop 2000 (Thermo Scientiﬁc,
Wilmington, USA).  To prepare RNA for sequencing, the Illumina TruSeq
sample preparation kit v2 was used with poly-A selection. cDNA
libraries were then loaded onto a high- output ﬂow cell and sequenced
on a NextSeq 500 platform (Illumina Inc., San Diego) with paired-end 2
× 75 nt length reads.  The raw data of reads generated by NextSeq 500
were processed using TopHat version 2.1.1 42 to map paired-end reads
to the CEN.PK113-7D reference genome
(http://cenpk.tudelft.nl/cgi-bin/gbrowse/cenpk/). Eight to 15 million
reads were mapped to the reference genome with an average map rate of
95%. Cufﬂinks version 2.1.1 43 was then used to calculate the FPKM
values for each sample.  Mapped read counts were generated from SAM
ﬁles using bedtools version 2.26.0 44 . Differential expression
analysis was performed with the Bioconductor R package DESeq2 45 .

Absolute mRNA quantiﬁcation. 
To quantify RNA-Seq read counts, 18 mRNAs with FPKM values ranging
from 3.4 × 10 1 –1.4 × 10 4 were selected, covering 80% of the dynamic
range of mRNA expression under reference conditions (D = 0.1 h −1 ).
The absolute concentrations of these 18 mRNAs were then measured using
the QuantiGene assay (Affymetrix, Santa Clara, CA, United
States). Further details of this measurement can be found in our
previous publication 26 . A positive linear correlation with a Pearson
R value of 0.8 was achieved among these 18 selected mRNA absolute
concentrations and their corresponding FPKM values (Supple- mentary
Table 2). The same correlation was then applied to all remaining mRNAs
identiﬁed by RNA sequencing to quantify their respective absolute mRNA
levels.  Absolute values of mRNA under other dilution rates were then
calculated based on the fold-change obtained from differential
expression analysis relative to the reference chemostat (D = 0.1 h −1
). Assuming that the weight of yeast cells does not change under
different speciﬁc growth rates, a cell weight of 13 pg measured under
reference conditions (D = 0.1 h −1 ) was applied to all other
chemostat conditions.  The same assumption of a constant cell weight
was applied for proteome absolute quantiﬁcation. The calculated
absolute mRNA concentrations were subsequently presented in the unit
of [molecules/cell].
```

### Notes

The authors assume constant cell weight over all growth rates. This is
likely wrong, since cell volume increases at higher growth rates.
They measured 13 pg per cell at growth rate 0.1/h.


## xia22 - proteins

Platform: Ultimate 3000 RSLC nano (Nano-LC/MS/MS), iBAQ


```
Nano-LC/MS/MS analysis for protein quantiﬁcation. Two micrograms of
peptides (for phosphoenriched samples, the entire sample) were
injected into an Ultimate 3000 RSLC nano system (Dionex, Sunnyvale,
CA, United States) using a C18 cartridge trap column in a backﬂush
conﬁguration and an in-house-packed (3 µm C18 particles, Dr Maisch,
Ammerbuch, Germany) analytical 50 cm × 75 µm emitter column (New
Objective, Woburn, MA, United States). Peptides were separated at 200
nl/min (for phosphopeptides: 250 nl/min) with a 5–40% B 240 and 480
min gradient for spiked and heavy standard samples, respectively. For
phos- phopeptides, a 90 min two-step separation gradient was used,
consisting of 5–115% B for 60 min and 15–330% B for 30 min. Buffer B
was 80% acetonitrile + 0.1% formic acid, and buffer A was 0.1% formic
acid in water. Eluted peptides were sprayed onto a quadrupole-orbitrap
Q Exactive Plus (Thermo Fisher Scientiﬁc, Waltham, MA, United States)
tandem mass spectrometer (MS) using a nanoe- lectrospray ionization
source and a spray voltage of 2.5 kV (liquid junction con-
nection). The MS instrument was operated with a top-10 data-dependent
MS/MS acquisition strategy. One 350–1400 m/z MS scan (at a resolution
setting of 70,000 at 200 m/z) was followed by MS/MS (R = 17,500 at 200
m/z) of the ten most intense ions using higher-energy collisional
dissociation fragmentation (normalized collision energies of 26 and 27
for regular and phosphopeptides, respectively). For total proteome
analysis, the MS and MS/MS ion target and injection time values were 3
× 10 6 (50 ms) and 5 × 10 4 (50 ms), respectively. For
phosphopeptides, the MS and MS/MS ion target and injection time values
were 1 × 10 6 (60 ms) and 2 × 10 4 (60 ms), respectively. The dynamic
exclusion time was limited to 45 s, 70 s and 110 s for the
phosphopeptide, spiked samples and heavy standard, respectively.  Only
charge states +2 to +6 were subjected to MS/MS, and for
phosphopeptides, the ﬁxed ﬁrst mass was set to 95 m/z. The heavy
standard was analyzed with three technical replicates, and all other
samples were analyzed with a single technical replicate.

Mass spectrometric raw data analysis and proteome quantiﬁcation. Raw
data were identiﬁed and quantiﬁed with the MaxQuant 1.4.0.8 software
package 47 . For heavy-spiked samples, the labeling state
(multiplicity) was set to 2, and Lys8 was deﬁned as the heavy
label. Methionine oxidation, asparagine/glutamine deamida- tion and
protein N-terminal acetylation were set as variable modiﬁcations, and
cysteine carbamidomethylation was deﬁned as a ﬁxed modiﬁcation. For
phospho- analysis, serine/threonine phosphorylation was used as an
additional variable modiﬁcation. A search was performed against the
UniProt (www.uniprot.org) S.  cerevisiae S288C reference proteome
database (version from July 2016) using the Lys-C/P (trypsin/P for
phosphoproteomics) digestion rule. Only protein identiﬁ- cations with
a minimum of 1 peptide of 7 amino acids long were accepted, and
transfer of peptide identiﬁcations between runs was enabled. The
peptide-spectrum match and protein FDR were kept below 1% using a
target-decoy approach with reversed sequences as decoys.  In
heavy-spiked samples, normalized H/L ratios (by shifting the median
peptide log H/L ratio to zero) were used in all downstream
quantitative analyses to account for any H/L 1:1 mixing
deviations. Protein H/L values themselves were derived by using the
median of a protein’s peptide H/L ratios and required at least one
peptide ratio measurement for reporting quantitative values. Signal
integration of missing label channels was enabled. For enriched
phosphoproteome samples, an in-house- written R script based on median
phosphopeptide intensity was used to normalize the phosphopeptide
intensities.  The heavy spike-in standard used for deriving the copy
numbers was quantiﬁed using the iBAQ method as described by ref. 48
. Essentially, UPS2 protein intensities were divided by the number of
theoretically observable peptides, log-transformed and plotted against
log-transformed known protein amounts of the UPS2 proteins.  This
regression was then applied to derive all other protein absolute
quantities using each protein’s iBAQ intensity. The relative ratios of
individual proteins to total protein were then converted to protein
concentration in the cell by multiplying the total protein content in
the cell for each condition. The total protein content per cell under
each condition was measured using the modiﬁed Lowery method.
```

### Notes

As for their transcriptome data, the authors assume constant cell
weight over all growth rates. This is likely wrong, since cell volume
increases at higher growth rates.  They measured 13 pg per cell at
growth rate 0.1/h.

## paulo16


* Platform: Orbitrap Fusion Lumos, SPS-MS3
* Units: unitless TMT intensity values

* From Methods:

TODO: summarize and understand

* From Methods in @Paulo2015:

```
LC-MS/MS
Our mass spectrometry data were collected using an Orbitrap
Fusion mass spectrometer (Thermo Fisher Scientific, San Jose,
CA) coupled to a Proxeon EASY-nLC II LC pump (Thermo Fisher
Scientific). Peptides were fractionated on a 75-μm inner diameter
microcapillary column packed with ∼0.5 cm of Magic C4 resin (5 μm,
100 Å, Michrom Bioresources) followed by ∼35 cm of GP-18 resin
(1.8 μm, 200 Å; Sepax, Newark, DE). For each analysis, we loaded
∼1 μg onto the column.
Peptides were separated using a 3-h gradient of 6–26% acetoni-
trile in 0.125% formic acid at a flow rate of ∼350 nl/min. Each analy-
sis used the multi-notch MS 3 -based TMT method (McAlister et al.,
2014) on an Orbitrap Fusion mass spectrometer. The scan sequence
began with an MS 1 spectrum (Orbitrap analysis; resolution 120,000;
mass range 400−1400 m/z; automatic gain control [AGC] target
2 × 10 5 ; maximum injection time 100 ms). Precursors for MS 2 /MS 3
analysis were selected using the TopSpeed parameter of 2 s. MS 2
analysis consisted of collision-induced dissociation (quadrupole ion-
trap analysis; AGC 4 × 10 3 ; normalized collision energy (NCE) 35;
maximum injection time 150 ms). Following acquisition of each MS 2
spectrum, we collected an MS 3 spectrum using our recently de-
scribed method in which 10 MS 2 fragment ions were captured in the
MS 3 precursor population using isolation waveforms with multiple
frequency notches (McAlister et al., 2014). MS 3 precursors were
fragmented by high-energy collision-induced dissociation and ana-
lyzed using the Orbitrap (NCE 55; AGC 5 × 10 4 ; maximum injection
time 150 ms, resolution was 60,000 at 400 Th).

[..]

Proteins were quantified by summing reporter ion counts across
all matching PSMs using in-house software, as described previously
(McAlister et al., 2012, 2014). Briefly, a 0.003 Th window around the
theoretical m/z of each reporter ion (126: 126.127 Th; 127N: 127.124
Th; 127C: 127.131 Th; 128N: 128.128 Th; 128C: 128.134 Th; 129N:
129.131 Th; 129C: 129.138 Th; 130N: 130.135 Th; 130C: 130.141
Th) was scanned for ions, and the maximum intensity nearest the
theoretical m/z was used. PSMs with poor quality, MS 3 spectra with
more than six TMT reporter ion channels missing, MS 3 spectra with
TMT reporter summed S/N ratio < 100, quantitation isolation speci-
ficity of <0.7, or no MS 3 spectra were excluded (McAlister et al.,
2012). Protein quantification values were exported for further analy-
sis in Excel or JMP. Each reporter ion channel was summed across all
quantified proteins and normalized assuming equal protein loading
of all nine samples
```


### Notes

The authors appear to never state any units for their "quantification"
and seem to present and analyze the log2(sample/ref.) with the glucose
culture as the reference.


## hackett16

* Platform: Agilent 6538 Q-TOF and 6550 Q-TOF (Nano-LC/MS/MS),
  15N-labeled reference sample
* From Materials and Methods:

```
To quantify the relative levels of proteins across
25 chemostats, each experimental sample was
paired to an internal 15 N-labeled reference sam-
ple and analyzed in triplicate.

Protein extraction: Yeast from 300 ml of cul-
ture volume were collected by rapid filtration
onto a 0.8 mm cellulose acetate filter (CA089025,
Sterlitech, Kent, WA). The filter containing the
yeast cake was folded and placed into a 50 ml
conical vial, which was immediately frozen by
immersion in liquid nitrogen, followed by stor-
age at -80°C. Complete yeast particle disruption
was accomplished by placing each cryofrozen
yeast-laden filter into a 50 ml canister of a Retsch
CryoMill (Haan, Germany) precooled on dry ice and 
pulverizing the total material into a fine
powder by cryogenic ball milling at -196°C. Yeast
protein was extracted from the powder by resus-
pension in an 80°C solution of 4% SDS, 100 mM
Tris pH 8, 1 mM DTT, and Halt Protease and
Phosphatase Inhibitors (ThermoFisher Scientific,
MA), followed by boiling for 20 min with periodic
agitation. Insoluble material, including filter ma-
terial, was removed from the samples by centrif-
ugation at 24,000 g for 20 min. The concentration
of extracted protein was determined using the
Pierce BCA Protein Assay Kit (ThermoFisher, MA).
Experimental samples were mixed 1:1 with an
equal protein content of 15 N-labeled reference
protein solution. The protein reference was yeast
cultured in phosphate-limited chemostats (N =
2, mixed together) growing at a dilution rate of
0.05 hours −1 in which ( 15 NH 4 ) 2 SO 4 was the sole
nitrogen-source.

[...]

nano-LC-MS/MS three times: once
using an Agilent 6538 Q-TOF platform and twice
using an Agilent 6550 Q-TOF platform. Both Q-TOF
platforms were equipped with Agilent 1260 nano-
flow HPLC systems and an Agilent ChipCube LC/
ion source interface.

[...]

For each peptide, i, and condition,
c, the log 2 -ratio of the experimental to reference
peptide ion current integrated areas was treated as
the peptide relative abundance, log2(IC_light/IC_heavy).

Protein quantification: To infer the relative
abundance of a protein from its corresponding
peptides, we need to aggregate multiple noisy esti-
mates into a single consensus. The relative abun-
dance of each peptide was taken as the geometric
mean of technical replicates. To downweight noi-
sier peptides, each peptide’s variance was esti-
mated as the mean squared error over technical
replicates. The relative abundance (and variance) of
a protein in each condition was found by weighting
the peptide relative abundances (x) according to
their inverse variance:

```

## jackson20 - TODO

## gasch17 - TODO
