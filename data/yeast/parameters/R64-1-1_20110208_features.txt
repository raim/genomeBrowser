# TYPE # MAIN : COORDINATE FILE, FIRST FILE IS BASEFILE
ID     = main
infile = feature_R64-1-1_20110208.csv
oldhead= parent,name,type,chr,start,end,strand,introns,UTR.intron,GO,SGD,description
column = 1,2,3,4,5,6,7,8,9,10,11,12
type   = 0,0,0,1,1,1,0,1,1,0,0,0
header = parent,name,type,chr,start,end,strand,introns,utr.introns,GO,SGD,description
headln = 1
nodata = ,,,,,,,0,0,,,
source.id=yeastGenome
biotyp = main

# SGD RELEASE R64-2-1 NAMES
ID     = R64-2-1
infile = feature_R64-2-1_20150113.csv
oldhead= name
column = 2
type   = 0
header = name.R64.2.1
headln = 1
source.id=yeastGenome.R64-2-1
biotyp = IDs


# SGD XREF UNIPROT
ID     = uniprot
infile = processedData/uniprot.dat
column = 1
type   = 0
header = UniProt
headln = 0
source.id=yeastXref
biotyp = IDs

# AFFYMETRIX PROBE IDs
ID     = affy
infile = processedData/affyprobes.csv
column = 1,2
oldhead= Yeast_2,YG_S98
type   = 0
header = Yeast_2,YG_S98
headln = 1
duplic = merge
source.id=yeast2.annot,yeast2.bestmatch
biotyp = IDs

# TYPE # DOWNLOADED EXPERIMENTAL DATA

# FILE # ID <-> COLUMN MAPPING : columns modified by a given gene
ID     = modifier
infile = featureData/modifiers.dat
column = 2
type   = 0
header = modifies
headln = 0
duplic = merge
source.id=doAll.sh
biotyp = column.map

# FILE # REDOX CLUSTERS : Machne et al. 2012
ID     = machne12
infile = processedData/machne12_timecourses.csv
column = 3,57,99
oldhead= "Overlap Clustering","li06_clusters","tu05_clusters"
type   = 0
header = CL_rdx,CL_li06,CL_tu05
headln = 1
source.id=./parameters/downloaddata.csv

# FILE # YMC CLUSTERS : Kuang et al. 2014
ID     = kuang14cls
infile = processedData/kuang14_cls.csv
column = 1
type   = 0
header = CL_kuang14
headln = 0
source.id=./parameters/downloaddata.csv

# FILE # YMC CLUSTERS : Kuang et al. 2014
ID     = kuang14rnah
infile = processedData/kuang14_rnahcluster.csv
column = 1
type   = 0
header = CL_kuang14rnah
headln = 0
source.id=./parameters/downloaddata.csv

# FILE # YMC CLUSTERS : Kuang et al. 2014
ID     = kuang14rnam
infile = processedData/kuang14_oxclusters.csv
column = 1
type   = 0
header = CL_kuang14rnam
headln = 0
source.id=./parameters/downloaddata.csv

# FILE # GENOME STRUCTURE : Duan et al. 2010, tRNA localization classes
ID     = duan10
infile = originalData/trna_positions_and_clusters.txt
column = 5
oldhead= cluster
type   = 0
header = tRNA.class
headln = 1
source.id=./parameters/downloaddata.csv


# FILE # LOC. AT NUCLEAR PORE : Casolari et al. 2004
ID     = casolari04
infile = processedData/casolari04_glucose.csv
column = 3,7,11,15,19,23,27
oldhead= "Bound by Cse1","Bound by Mlp1?","Bound by Nup100?","Bound by Nup116?","Bound by Nup60?","Bound by Xpo1?","Bound by Prp20?cluster"
type   = 0
header = Cse1_glu,Mlp1_glu,Nup100_glu,Nup116_glu,Nup60_glu,Xpo1_glu,Prp20_glu
headln = 1
source.id=./parameters/downloaddata.csv

# FILE # LOC. AT NUCLEAR PORE : Casolari et al. 2004
ID     = casolari04
infile = processedData/casolari04_galactose.csv
column = 1,5,9,13,17,21,25
oldhead= "Bound by Cse1?","Bound by Nup2?","Bound by Mlp1?","Bound by Mlp2?","Bound by Nic96?","Bound by Nup116?","Bound by Xpo1?"
type   = 0
header = "Cse1_gal","Nup2_gal","Mlp1_gal","Mlp2_gal","Nic96_gal","Nup116_gal","Xpo1_gal"
headln = 1
source.id=./parameters/downloaddata.csv

# FILE # EVOLUTION : Esser et al. 2004 archaeal vs. eubacterial origin of ORFs
ID     = esser04
infile = processedData/esser04result.csv
column = 2,3
type   = 1
header = eubacterial,archaeal
headln = 0
source.id=./scripts/mapEsser04Data.R

# FILE # EVOLUTION : Cotton & McInerny 2010 archaeal vs. eubacterial origin of ORFs
ID     = cotton10
infile = processedData/cotton10.csv
column = 1
type   = 0
header = origins
headln = 0
source.id=./scripts/processCotton10.pl

# FILE # EVOLUTION : Bogumil et al. 2012 archaeal vs. eubacterial, chaperons
ID     = bogumil12
infile = processedData/bogumil12.csv
oldhead= BestEvalue(Criterion1),BestScore(Criterion2),CLASSIFICATIONpaup(criterion3),CLASSIFICATIONmrp(criterion4),CLASSesser50(criterion5)
column = 3,5,10,12,17
header = EvoCrit1,EvoCrit2,EvoCrit3,EvoCrit4,EvoCrit5
type   = 0
headln = 1
source.id=./scripts/preprocess.sh

# FILE # EVOLUTION : Bogumil et al. 2012 archaeal vs. eubacterial, chaperons
ID     = bogumil12
infile = processedData/bogumil12/modules.list
column = 1,2
header = ChaperonFam,isChaperon
type   = 0
headln = 1
source.id=./scripts/preprocess.sh

# FILE # Lee et al., k-means clusters
ID     = nfrcl
infile = featureData/lee_clusters.csv
oldhead= CL.lee
column = 1
type   = 0
#child  = 1
nodata = na
header = CL.lee
headln = 1
dattyp = class
biotyp = promotor.class
source.id=lee07.clusters

# FILE # Huisinga and Pugh 2004, SAGA vs. TFIID
ID     = saga
infile = processedData/huisinga04s2.csv
oldhead= CL.pugh
column = 1
type   = 0
#child  = 1
nodata = na
header = CL.pugh
headln = 1
dattyp = class
biotyp = promotor.class
source.id=huisinga04.clusters

# FILE # Hartley and Madhani 2009, chrIII promotors affected in Sth1-degr. (RSC)
ID     = hartley09
infile = processedData/hartley09.csv
column = 1,2,3
type   = 0
#child  = 1
oldhead= STH1.chrIII,ABF1.chrIII,REB1.chrIII
header = STH1.chrIII,ABF1.chrIII,REB1.chrIII
headln = 1
nodata = na
dattyp = class
biotyp = promotor.class
source.id=hartley09

# FILE # Zhang et al. Science 2011
ID     = zhang11
infile = originalData/KG5.kgg
column = 1
type   = 0
header = CL.zhang
headln = 0
nodata = na
dattyp = class
biotyp = promotor.class
source.id=zhang11

# FILE # Ng et al. 2002 - RSC TARGETS - p-values
# log2 ratios are in feature.chip.csv !!
ID     = ng02.pval
infile = processedData/ng02.csv
headln = 1
type   = 1
oldhead= Rsc1,Rsc2,Rsc3,Rsc8,Sth1,Combined P value
column = 1,3,5,7,9,13
header = Rsc1.ng02.p,Rsc2.ng02.p,Rsc3.ng02.p,Rsc8.ng02.p,Sth1.ng02.p,Rsc.p.ng02
dattyp = log2ratio
biotyp = ChIP
source.id=ng02.genes

# FILE # ANGUS-HILL et al. 2001 - RSC3/RSC30 transcriptomes
# log2 ratios are in feature.chip.csv !!
ID     = angushill01
infile = processedData/angushill01.csv
headln = 1
type   = 1
column = 1,2
header = rsc3.2,rsc30Delta
dattyp = log2ratio
biotyp = ChIP
source.id=angushill01

# FILE # DAMELIN et al. 2001 - RSC9 LOCATIONS
ID     = damelin02
infile = processedData/damelin02.dat
headln = 1
type   = 1
column = 1,2,3,4
header = Rsc9_control,Rsc9_Rapamycin,Rsc9_H2O2,Rsc9_alphaFactor
dattyp = medianPercentile
biotyp = ChIP
source.id=damelin02

# FILE # YEN et al. 2012 - REMODELLER TARGETS
ID     = yen12
infile = originalData/yen12_genes.txt
headln = 3
type   = 1
column = 1,2,3,4,5,6,7,8
oldhead= Arp5,Ino80,Ioc3,Ioc4,Isw1,Isw2,Rsc8,Snf2
header = Arp5_yen12,Ino80_yen12,Ioc3_yen12,Ioc4_yen12,Isw1_yen12,Isw2_yen12,Rsc8_yen12,Snf2_yen12
dattyp = logic
biotyp = ChIP-exo
source.id=yen12_genes

# FILE # Tirosh and Barkai 2008, OPN/DPN genes
ID     = tirosh08
infile = processedData/tirosh08.dat
column = 1
type   = 0
#child  = 1
header = CL.tir
headln = 0
nodata = na
dattyp = class
biotyp = promotor.class
source.id=tirosh08


# FILE # Choi and Kim 2008, cis- and trans- variability
ID     = choi08
infile = processedData/choi08.csv
column = 2,1
oldhead= civ,trv
type   = 1
header = CIV,TRV
headln = 1
dattyp = variability
biotyp = promotor.class
source.id=choi08

# FILE # Ioshikes et al. 2006 - NPS-based clusters
ID     = ioshikes06
infile = processedData/ioshikes06.csv
column = 1
type   = 0
header = CL.iosh
headln = 0
dattyp = class
biotyp = nucleosome.class
source.id=ioshikes06

# FILE # Vaillant et al. 2010 - bistable/crystalline nucleosome configuration
ID     = vaillant10
infile = processedData/vaillant10.csv
column = 1
type   = 0
header = CL.vail
nodata = na
headln = 0
dattyp = class
biotyp = CDS.class
source.id=vaillant10

# FILE # Tirosh et al. 2010 - midcoding region affected by Isw1
ID     = tirosh10
infile = processedData/tirosh10.csv
column = 1
type   = 0
oldhead  ISW1.midcodingType
header = ISW1.midcodingType
nodata = na
headln = 1
dattyp = class
biotyp = CDS.class
source.id=tirosh10

# FILE # Choi and Kim 2008 - chromatin-regulated
ID     = choi08
infile = originalData/cre.txt
column = 1
type   = 1
oldhead= CRE
header = CRE
headln = 1
dattyp = score
biotyp = chromatin.regulation.score
source.id=choi08

# FILE # Brauer et al. 2008 - growth-rate
ID     = brauer08
infile = processedData/brauer08.csv 
column = 1
type   = 1
oldhead= Growth Rate Slope
header = growthRate
headln = 1
dattyp = score
biotyp = growthrate.score
source.id=brauer08

# FILE # Brauer et al. 2008 - growth-rate CLASSES
ID     = brauer08.2
infile = processedData/brauer08.csv 
oldhead= Growth Rate Response @1.5SD,ESR,Unresponsive?
type   = 0
header = GRR,ESR,no.GRR
headln = 1
dattyp = class
biotyp = growthrate.classes
source.id=brauer08

# FILE # Koerber et al. 2009 TF-nucleosome interactions
ID     = koerber09
infile = processedData/koerber09.csv
oldhead= Bdf1,Vps72,Rap1,Reb1,Rpo21,Srm1
type   = 0
#child  = 1
header = Bdf1.kb09,Vps72.kb09,Rap1.kb09,Reb1.kb09,Rpo21.kb09,Srm1.kb09
headln = 1
dattyp = class
biotyp = promotor.class
source.id=koerber09

# FILE # Holstege et al. 1998, RNA half-life
ID     = hR1
infile = processedData/orf_transcriptome.csv
oldhead= ExpressionLevel,HalfLife,TranscriptionalFreq
column = 1,2,3
type   = 1,1,1
header = RNA.level,RNA.t05.H,transc.freq
headln = 1
biotyp = RNA.cycle
source.id=holstege98

# FILE # Pelechano et al. 2010, trancription rates
ID     = hR1
infile = processedData/pelechano10_transcription.csv
oldhead= transcription.rate_n/min
column = 1
header = transcription.rate_n/min
headln = 1
biotyp = RNA.cycle
source.id=pelechano10

# FILE # Wang et al., RNA half-life, 2 experiments
ID     = hR2
infile = processedData/halflifeTable.csv
#oldhead= Decay constant (m),"Halflife (t1/2_ min)",decay constant (m),"Halflife (t1/2_ min)".1
column = 4,7,12,15
type   = 1,1, 1, 1
header = decay.W1,RNA.t05.W1,decay.W2,RNA.t05.W2
headln = 1
dattyp = degradation.rates.and.halflives
biotyp = RNA.cycle
source.id=wang02

# FILE # Geisberg et al. 2014, RNA half-life, 
ID     = hR3
infile = processedData/geisberg14_s1_halflives.csv
column = 1
type   = 1
header = RNA.t05.G
headln = 1
dattyp = degradation.rates.and.halflives
biotyp = RNA.cycle
source.id=geisberg14


# FILE # Ghaemmaghami et al., protein abundance
ID     = aP
infile = processedData/ghaemmaghami03.csv
oldhead= "Protein Molecules/Cell"
column = 1
type   = 1
header = protein.level
headln = 1
duplic = original
dattyp = concentration
biotyp = protein.cycle
source.id=ghaemmaghami03

# FILE # Belle et al. 2006, protein half-life
ID     = hP
infile = originalData/SuppDataSet.txt
oldhead= Corrected Half Life
column = 3
type   = 1
header = protein.t05
headln = 6
biotyp = protein.cycle
source.id=belle06

# FILE # Christiano et al. 2014, protein degradation rate, minutes
ID     = degP
infile = processedData/christiano14.csv
column = 1,2
type   = 1,1
header = protein.Deg.C,protein.Deg.C.R
headln = 1
biotyp = protein.cycle
source.id=christiano14

# FILE # Arava et al. 2003, ribosome densities
ID     = rdns_arava03
infile = processedData/arava03_ribosomedensity.csv
oldhead= number.of.ribosomes
column = 1
header = number.of.ribosomes_arava03
headln = 1
biotyp = protein.cycle
source.id=arava03

# FILE # Riba et al. 2019, ribosome densities
ID     = rdns_riba19
infile = processedData/riba19_ribosomedensity.csv
oldhead= number.of.ribosomes
column = 1
header = number.of.ribosomes_riba19
headln = 1
biotyp = protein.cycle
source.id=riba19

# FILE # Man et al., protein translational efficiencies
ID     = tai
infile = processedData/man07.csv
oldhead= S. cerevisiae
column = 1
type   = 1
header = tAI
headln = 1
biotyp = protein.cycle
source.id=man07

# FILE # Newman et al. 2006 - expression noise
ID     = newman06noise
infile = processedData/newman06noise.csv
column = 2,4
oldhead= DM YEPD,DM SD
headln = 1
type   = 1
header = noise.YEPD,noise.SD
biotyp = protein.cycle
source.id=newman06.prot

# FILE # Newman et al. 2006 - expression noise
ID     = newman06noise
infile = processedData/newman06change.csv
column = 1,2
oldhead= GFP Values,mRNA Values
headln = 1
type   = 1
header = protein.MvF,RNA.MvF
biotyp = protein.cycle
source.id=newman06.var

# FILE # Ringner and Korgh 2005, 5'UTR MFEs
ID     = mfe
#infile = originalData/10.1371_journal.pcbi.0010072.sd001.tds
infile = originalData/journal.pcbi.0010072.sd001
oldhead= Minimum free energy (5'-UTR 50 nt),Free energy of ensemble (5'-UTR 50 nt),Z-score (5'-UTR 50 nt),Minimum free energy (3'-UTR 50 nt),Free energy of ensemble (3'-UTR 50 nt)
column = 1,2,11,9,10
type   = 1,1,1,1,1
header = 5UTR.mfe,5UTR.efe,5UTR.mfe.z,3UTR.mfe,3UTR.efe, 
headln = 1
biotyp = RNA.structure
source.id=ringner05.UTR

# FILE # Cvijovic et al. 2007 - uORF prediction
ID     = uORF.cvi
infile = processedData/cvijovic_uORFs.csv
oldhead= uORFs
type   = 1
header = uORF.cvi
headln = 1
dattyp = number
biotyp = RNA.structure
source.id=cvijovic07.s2

# FILE # Nagalakshmi et al. 2008 - uORF prediction
ID     = uORF
infile = processedData/nagalakshmi08.uORFs.csv
oldhead= uORF.length
type   = 1
header = uORF.length
headln = 1
dattyp = motif.length
biotyp = RNA.structure
source.id=nagalakshmi08.tables


# FILE # PEST motifs # CALCULATED DATA
ID     = pest
infile = featureData/pestmotifs.csv
oldhead= PEST.num,PEST.score,PEST.length
column = 1,2,3
type   = 1,1,1
header = PEST,PEST.score,PEST.length
nodata = 0,0,0
headln = 1
biotyp = protein.motif
source.id=doAll.sh

# PROTEIN PROPERTIES, SGD
ID     = prot.prop
infile = processedData/protein_properties.tab
column = 2,3,4,5,8,29,30,31
type   = 1
header = MW,pI,cAI,p.length,codon.bias,fop,gravy,arom
headln = 0
biotyp = protein.structure
source.id=proteinProps

# PROTEIN PROPERTIES, AMINO ACID CONTENT
ID     = prot.aa
infile = coding/AA.freq.dat
type   = 1
oldhead= A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y
header = pr.A,pr.C,pr.D,pr.E,pr.F,pr.G,pr.H,pr.I,pr.K,pr.L,pr.M,pr.N,pr.P,pr.Q,pr.R,pr.S,pr.T,pr.V,pr.W,pr.Y
headln = 1
biotyp = protein.structure
source.id=transcriptome.sh

# DNA/RNA PROPERTIES, transcript nucleotide content
ID     = rna.nt
infile = coding/Nt.freq.dat
type   = 1
oldhead= A,C,G,T,A.1,A.2,A.3,C.1,C.2,C.3,G.1,G.2,G.3,T.1,T.2,T.3
header= tr.A,tr.C,tr.G,tr.T,A.1,A.2,A.3,C.1,C.2,C.3,G.1,G.2,G.3,T.1,T.2,T.3
headln = 1
biotyp = RNA.codonpositions
source.id=transcriptome.sh


# FILE # 5' FREQUENCIES -350..0
ID     = str350
infile = featureData/motifs.all.-350..0.tab
oldhead= A.Nt,G.Nt,C.Nt,T.Nt,AA,AC,AG,AT,CA,CC,CG,CT,GA,GC,GG,GT,TA,TC,TG,TT,YY,RR,RY,YR
type   =  1
header = A.Nt.atg,G.Nt.atg,C.Nt.atg,T.Nt.atg,AA.atg,AC.atg,AG.atg,AT.atg,CA.atg,CC.atg,CG.atg,CT.atg,GA.atg,GC.atg,GG.atg,GT.atg,TA.atg,TC.atg,TG.atg,TT.atg,YY.atg,RR.atg,RY.atg,YR.atg
headln = 1
nodata = 0
biotyp = nucleicacid.content
source.id=motifs.csv

# FILE # 3' FREQUENCIES 0..350
ID     = str350.3p
infile = featureData/motifs.all.3p.0..350.tab
oldhead= A.Nt,G.Nt,C.Nt,T.Nt,AA,AC,AG,AT,CA,CC,CG,CT,GA,GC,GG,GT,TA,TC,TG,TT,YY,RR,RY,YR
type   =  1
header = A.Nt.stop,G.Nt.stop,C.Nt.stop,T.Nt.stop,AA.stop,AC.stop,AG.stop,AT.stop,CA.stop,CC.stop,CG.stop,CT.stop,GA.stop,GC.stop,GG.stop,GT.stop,TA.stop,TC.stop,TG.stop,TT.stop,YY.stop,RR.stop,RY.stop,YR.stop
headln = 1
nodata = 0
biotyp = nucleicacid.content.3p
source.id=motifs.csv

## FILE # 5' FREQUENCIES -401..201
#ID     = str401
#infile = featureData/motifs.all.-401..-201.tab
#oldhead= A.Nt,G.Nt,C.Nt,T.Nt,AA,AA.r,AC,AC.r,AG,AG.r,AT,AT.r,CA,CA.r,CC,CC.r,CG,CG.r,CT,CT.r,GA,GA.r,GC,GC.r,GG,GG.r,GT,GT.r,TA,TA.r,TC,TC.r,TG,TG.r,TT,TT.r,YY,YY.r,RR,RR.r,RY,RY.r,YR,YR.r
#type   =  1
#header = A.Nt.atg.401..201,G.Nt.atg.401..201,C.Nt.atg.401..201,T.Nt.atg.401..201,AA.atg.401..201,AA.atg.401..201.r,AC.atg.401..201,AC.atg.401..201.r,AG.atg.401..201,AG.atg.401..201.r,AT.atg.401..201,AT.atg.401..201.r,CA.atg.401..201,CA.atg.401..201.r,CC.atg.401..201,CC.atg.401..201.r,CG.atg.401..201,CG.atg.401..201.r,CT.atg.401..201,CT.atg.401..201.r,GA.atg.401..201,GA.atg.401..201.r,GC.atg.401..201,GC.atg.401..201.r,GG.atg.401..201,GG.atg.401..201.r,GT.atg.401..201,GT.atg.401..201.r,TA.atg.401..201,TA.atg.401..201.r,TC.atg.401..201,TC.atg.401..201.r,TG.atg.401..201,TG.atg.401..201.r,TT.atg.401..201,TT.atg.401..201.r,YY.atg.401..201,YY.atg.401..201.r,RR.atg.401..201,RR.atg.401..201.r,RY.atg.401..201,RY.atg.401..201.r,YR.atg.401..201,YR.atg.401..201.r
#headln = 1
#nodata = 0
#biotyp = nucleicacid.content
#source.id=motifs.csv




# FILE # CODING SEQUENCE NUCLEOSOME STATISTICS
ID     = cds.nucl
infile = featureData/coding.nucleosomes.CDS.start.0..end.0.tot
oldhead= H2A.Z,Nucl.Lee,Nucl.WT,Nucl.isw2,Nucl.remod,Isw2,Nucl.norm,Nucl.heat,Nucl.pred,H3.p,H3.m,H4.p,H4.m,H2A.Z.p,H2A.Z.m,H3.YPD,H4.YPD,H3.H2O2,H3K9acvsH3.YPD,H3K14acvsH3.YPD,H3K14acvsWCE.YPD,H3K14acvsH3.H2O2,H4acvsH3.YPD,H4acvsH3.H2O2,H3K4me1vsH3.YPD,H3K4me2vsH3.YPD,H3K4me3vsH3.YPD,H3K36me3vsH3.YPD,H3K79me3vsH3.YPD,ESA1.YPD,GCN5.YPD,GCN4.AA,IgG.YPD,noAB.YPD
type   = 1
header = H2A.Z,Nucl.Lee,Nucl.WT,Nucl.isw2,Nucl.remod,Isw2,Nucl.norm,Nucl.heat,Nucl.pred,H3.p,H3.m,H4.p,H4.m,H2A.Z.p,H2A.Z.m,H3.YPD,H4.YPD,H3.H2O2,H3K9acvsH3.YPD,H3K14acvsH3.YPD,H3K14acvsWCE.YPD,H3K14acvsH3.H2O2,H4acvsH3.YPD,H4acvsH3.H2O2,H3K4me1vsH3.YPD,H3K4me2vsH3.YPD,H3K4me3vsH3.YPD,H3K36me3vsH3.YPD,H3K79me3vsH3.YPD,ESA1.YPD,GCN5.YPD,GCN4.AA,IgG.YPD,noAB.YPD
headln = 1
source.id=./scripts/featuremotifs.sh

# FILE # CODING SEQUENCE CONSERVATION
ID     = cds.cons
infile = featureData/coding.cons.CDS.start.0..end.0.tot
oldhead= CONS
type   = 1
header = CDS.CONS
headln = 1
source.id=./scripts/featuremotifs.sh

ID     = cds.nt
infile = featureData/coding.motifs.CDS.start.0..end.0.tot
oldhead= AA,AA.r,AC,AC.r,AG,AG.r,AT,CA,CA.r,CC,CC.r,CG,GA,GA.r,GC,TA
type   = 1
header = cds.AA,cds.TT,cds.AC,cds.GT,cds.AG,cds.CT,cds.AT,cds.CA,cds.TG,cds.CC,cds.GG,cds.CG,cds.GA,cds.TC,cds.GC,cds.TA
headln = 1
source.id=./scripts/featuremotifs.sh


#ID     = cds
#infile = featureData/coding.CDS.start..end.tot
#oldhead= AA,AA.r,AC,AC.r,AG,AG.r,AT,AT.r,CA,CA.r,CC,CC.r,CG,CG.r,GA,GA.r,GC,GC.r,TA,TA.r,RNA.p.tot,RNA.m.tot,RNApl.p,RNApl.m,CONS,A,T,G,C,Nucl.remod,Isw2,PolII,Nucl.Fuzz,Nucl.Well,TS.p,TS.m
#type   = 1
#header = AA,AA.r,AC,AC.r,AG,AG.r,AT,AT.r,CA,CA.r,CC,CC.r,CG,CG.r,GA,GA.r,GC,GC.r,TA,TA.r,RNA.p,RNA.m,RNApl.p,RNApl.m,CONS,A,T,G,C,cds.Nucl.remod,cds.Isw2,cds.PolII,cds.Nucl.Fuzz,cds.Nucl.Well,cds.TS.p,cds.TS.m
#headln = 1
#source.id=./scripts/featuremotifs.sh

# FILE # PROMOTOR STATISTICS
#ID     = prom.stat
#infile = featureData/promotor.all.-250..-50.tab
#oldhead= A,T,G,C,AA,AA.r,AC,AC.r,AG,AG.r,AT,AT.r,CA,CA.r,CC,CC.r,CG,CG.r,GA,GA.r,GC,GC.r,TA,TA.r,B,PP,L1,PT,Nucl.Lee,Nucl.WT,Nucl.isw2,Nucl.norm,Nucl.heat,TS.p,TS.m,CONS
#type   = 1
#header = pr.A,pr.T,pr.G,pr.C,pr.AA,pr.AA.r,pr.AC,pr.AC.r,pr.AG,pr.AG.r,pr.AT,pr.AT.r,pr.CA,pr.CA.r,pr.CC,pr.CC.r,pr.CG,pr.CG.r,pr.GA,pr.GA.r,pr.GC,pr.GC.r,pr.TA,pr.TA.r,pr.B,pr.PP,pr.L1,pr.PT,pr.Nucl.Lee,pr.Nucl.WT,pr.Nucl.isw2,pr.Nucl.norm,pr.Nucl.heat,pr.TS.p,pr.TS.m,pr.CONS
#headln = 1
#source.id=./scripts/featuremotifs.sh


# FILE # DOWNSTREAM STATISTICS
#ID     = ds.stat
#infile = featureData/downstream.all.3p.0..200.tab
#oldhead= AA,AA.r,AC,AC.r,AG,AG.r,AT,AT.r,CA,CA.r,CC,CC.r,CG,CG.r,GA,GA.r,GC,GC.r,TA,TA.r,CONS,A,T,G,C,Isw2,PolII,Nucl.Fuzz,Nucl.Well,TS.p,TS.m
#type   = 1
#header = ds.AA,ds.AA.r,ds.AC,ds.AC.r,ds.AG,ds.AG.r,ds.AT,ds.AT.r,ds.CA,ds.CA.r,ds.CC,ds.CC.r,ds.CG,ds.CG.r,ds.GA,ds.GA.r,ds.GC,ds.GC.r,ds.TA,ds.TA.r,ds.CONS,ds.A,ds.T,ds.G,ds.C,ds.Isw2,ds.PolII,ds.Nucl.Fuzz,ds.Nucl.Well,ds.TS.p,ds.TS.m
#headln = 1
#source.id=./scripts/featuremotifs.sh


## FILE # 5' STRUCTURES 250..50
#ID     = str250
#infile = featureData/structures.all.-250..-50.log
#oldhead= B,PP,L1,PT
#type   = 1
#header = B.atg.250..50,PP.atg.250..50,L1.atg.250..50,PT.atg.250..50
#headln = 1
#nodata = 0
#source.id=./scripts/featuremotifs.sh

# FILE # 5' STRUCTURES 300..0 at TSS
#ID     = str250tss
#infile = featureData/structures.all.at.TSS.c.all.-250..-50.log
#oldhead= B,PP,L1,PT
#type   = 1
#header = B.tss.300..0,PP.tss.300..0,L1.tss.300..0,PT.tss.300..0
#headln = 1
#nodata = 0
#source.id=./scripts/featuremotifs.sh



# STATS

# FILE # UPSTREAM CONSERVATION
#ID     = us.cons
#infile = featureData/upstream.cons.all.3p.-700..-300.tab
#oldhead= CONS
#type   = 1
#header = us.CONS
#headln = 1
#source.id=./scripts/featuremotifs.sh


# ENZYME FUNCTION, BASED ON SBML MODEL SUBSYSTEM ANNOTATION
ID     = enzymes
infile = featureData/metabolicgenes_nocomp.csv
type   = 1
oldhead= Sphingolipid Metabolism,Phospholipid Metabolism,Arabinose Metabolism,Glycoprotein Metabolism,Complex Alcohol Metabolism,Threonine and Lysine Metabolism,Fructose and Mannose Metabolism,Folate Metabolism,Taurine Metabolism,Methane Metabolism,Fatty Acid  Biosynthesis,Methionine Metabolism,Citric Acid Cycle,Histidine Metabolism,Xylose Metabolism,Thiamine Metabolism,Pyruvate Metabolism,NAD Biosynthesis,Phospholipid Biosynthesis,Nitrogen Metabolism,Riboflavin Metabolism,Glutamate metabolism,Glycerolipid Metabolism,Pantothenate and CoA Biosynthesis,Fatty Acid Degradation,Pyridoxine Metabolism,NOTASSIGNED,Glycolysis/Gluconeogenesis,Galactose metabolism,Other Amino Acid Metabolism,Glycine and Serine Metabolism,Cysteine Metabolism,Oxidative Phosphorylation,Transport. Vacuolar,Pentose Phosphate Pathway,Other,Starch and Sucrose Metabolism,Transport. Mitochondrial,Alanine and Aspartate Metabolism,tRNA charging,Anaplerotic reactions,Sterol Metabolism,Glutamine Metabolism,Fatty Acid Metabolism,Transport. Extracellular,Asparagine metabolism,Quinone Biosynthesis,Anaplerotic Reactions,Nucleotide Salvage Pathway,Arginine and Proline Metabolism,Alternate Carbon Metabolism,Porphyrin and Chlorophyll Metabolism,Transport. Peroxisomal,Purine and Pyrimidine Biosynthesis,Valine. Leucine. and Isoleucine Metabolism,Tyrosine. Tryptophan. and Phenylalanine Metabolism
header= Sphingolipid Metabolism,Phospholipid Metabolism,Arabinose Metabolism,Glycoprotein Metabolism,Complex Alcohol Metabolism,Threonine and Lysine Metabolism,Fructose and Mannose Metabolism,Folate Metabolism,Taurine Metabolism,Methane Metabolism,Fatty Acid  Biosynthesis,Methionine Metabolism,Citric Acid Cycle,Histidine Metabolism,Xylose Metabolism,Thiamine Metabolism,Pyruvate Metabolism,NAD Biosynthesis,Phospholipid Biosynthesis,Nitrogen Metabolism,Riboflavin Metabolism,Glutamate metabolism,Glycerolipid Metabolism,Pantothenate and CoA Biosynthesis,Fatty Acid Degradation,Pyridoxine Metabolism,NOTASSIGNED,Glycolysis/Gluconeogenesis,Galactose metabolism,Other Amino Acid Metabolism,Glycine and Serine Metabolism,Cysteine Metabolism,Oxidative Phosphorylation,Transport. Vacuolar,Pentose Phosphate Pathway,Other,Starch and Sucrose Metabolism,Transport. Mitochondrial,Alanine and Aspartate Metabolism,tRNA charging,Anaplerotic reactions,Sterol Metabolism,Glutamine Metabolism,Fatty Acid Metabolism,Transport. Extracellular,Asparagine metabolism,Quinone Biosynthesis,Anaplerotic Reactions,Nucleotide Salvage Pathway,Arginine and Proline Metabolism,Alternate Carbon Metabolism,Porphyrin and Chlorophyll Metabolism,Transport. Peroxisomal,Purine and Pyrimidine Biosynthesis,Valine. Leucine. and Isoleucine Metabolism,Tyrosine. Tryptophan. and Phenylalanine Metabolism
headln = 1
nodata = 0
biotyp = metabolism
source.id=herrgard08.nocomp


# FILE # Whitehouse et al. 2007 - Isw2 targets
ID     = isw2cl
infile = processedData/whitehouse07cluster.csv
column = 1,2
oldhead= CL.rmd,CL.rmd3
type   = 1
header = CL.rmd,CL.rmd3
nodata = na
headln = 1
biotyp = promotor.class
source.id=whitehouse07.dyads


# FILE # Perocchi et al. 2007, confirmed antisense transcripts
ID     = antisense
infile = processedData/perocchi07_antisense.csv
column = 1
headln = 0
type   = 1
header = asRNA
datayp = antisense.transcriptome
source.id=perocchi07.segments

# FILE # van Dijk et al. 2011, XUTs
ID     = xut
infile = processedData/vandijk11_classes.dat
column = 1
headln = 1
type   = 0
header = CL.vandijk
nodata = na
datayp = noncoding
biotyp = promotor.class
source.id=vandijk11.classes




# TYPE # DATA CALCULATED FROM GENOME DATA

# FILE # TSS, consenus of Miura, Zhang and Perocchi data
ID     = tss.all
infile = featureData/closest.TSS.all.c.range.csv
oldhead= TSS.all.c.main,TSS.all.c.num,TSS.all.c.tot,TSS.all.c.us,TSS.all.c.ds
column = 1,6,7,8,9
type   = 1,1,1,1,1
header = TSS.all.c.5p,TSS.all.c.num,TSS.all.c.tot,TSS.all.c.us,TSS.all.c.ds
headln = 1
biotyp = TSS
source.id=./scripts/closestsite.sh

# FILE # TSS, segments, Perocchi, unprocessed transcription start sites
ID     = tss.seg
infile = featureData/closest.TSS.P_TSS.M_TSS.Z.range.csv
oldhead= TSS.P.main
column = 1
type   = 1
header = TSS.P
headln = 1
biotyp = TSS
source.id=./scripts/closestsite.sh

# FILE # TES, segments, Perocchi, unprocessed transcription end sites
ID     = tss.seg
infile = featureData/closest.TES.P.3p.range.csv
oldhead= TES.P.main.3p
column = 2
type   = 1
header = TES.P
headln = 1
biotyp = TES
source.id=./scripts/closestsite.sh


# FILE # TSS, consenus of Miura and Zhang
#ID     = tss.mz
#infile = featureData/closest.TSS.MZ.c.range.csv
#oldhead= TSS.MZ.c.main,TSS.MZ.c.num,TSS.MZ.c.tot,TSS.MZ.c.us,TSS.MZ.c.ds
#column = 1,6,7,8,9
#type   = 1,1,1,1,1
#header = TSS.MZ.c.5p,TSS.MZ.c.num,TSS.MZ.c.tot,TSS.MZ.c.us,TSS.MZ.c.ds
#headln = 1
#biotyp = TSS
#source.id=./scripts/closestsite.sh



# FILE # DYADS, Whitehouse et al., Lee et al., Shivaswamy et al.
ID     = dy5
infile = featureData/dyad.Dyads.norm_Dyads.heat_Dyads.WT_Dyads.isw2_Dyads.HMM.range.csv
oldhead=Dyads.norm.prox,Dyads.norm.num,Dyads.norm.tot,Dyads.heat.prox,Dyads.heat.num,Dyads.heat.tot,Dyads.WT.prox,Dyads.WT.num,Dyads.WT.tot,Dyads.isw2.prox,Dyads.isw2.num,Dyads.isw2.tot,Dyads.HMM.prox,Dyads.HMM.num,Dyads.HMM.tot
column = 3,6,7,13,16,17,23,26,27,33,36,37,43,46,47
type   = 1,1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
header = Dyad.S.norm.5p,Dyad.S.norm.num.5p,Dyad.S.norm.tot.5p,Dyad.S.heat.5p,Dyad.S.heat.num.5p,Dyad.S.heat.tot.5p,Dyad.W.wt.5p,Dyad.W.wt.num.5p,Dyad.W.wt.tot.5p,Dyad.W.isw2.5p,Dyad.W.isw2.num.5p,Dyad.W.isw2.tot.5p,Dyad.L.5p,Dyad.L.num.5p,Dyad.L.tot.5p
headln = 1
biotyp = dyads
source.id=./scripts/closestsite.sh

# FILE # DYADS, 3' end Whitehouse et al., Lee et al., Shivaswamy et al.
ID     = dy3
infile = featureData/dyad.Dyads.norm_Dyads.heat_Dyads.WT_Dyads.isw2_Dyads.HMM.3p.range.csv
oldhead= Dyads.norm.prox,Dyads.norm.num,Dyads.norm.tot,Dyads.heat.prox,Dyads.heat.num,Dyads.heat.tot,Dyads.WT.prox,Dyads.WT.num,Dyads.WT.tot,Dyads.isw2.prox,Dyads.isw2.num,Dyads.isw2.tot,Dyads.HMM.prox,Dyads.HMM.num,Dyads.HMM.tot
column = 3,6,7,13,16,17,23,26,27,33,36,37,43,46,47
type   = 1,1,1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
header = Dyad.S.norm.3p,Dyad.S.norm.num.3p,Dyad.S.norm.tot.3p,Dyad.S.heat.3p,Dyad.S.heat.num.3p,Dyad.S.heat.tot.3p,Dyad.W.wt.3p,Dyad.W.wt.num.3p,Dyad.W.wt.tot.3p,Dyad.W.isw2.3p,Dyad.W.isw2.num.3p,Dyad.W.isw2.tot.3p,Dyad.L.3p,Dyad.L.num.3p,Dyad.L.tot.3p
headln = 1
biotyp = dyads
source.id=./scripts/closestsite.sh



# FILE # NUCLEOSOME REMODELLING STATISTICS
#ID     = us.remod
#infile = featureData/remodelling.all.-400..-50.tab
#oldhead= Isw2,Nucl.remod,Nucl.Fuzz,Nucl.Well
#type   = 1
#header = us.Isw2,us.Nucl.remod,us.Nucl.Fuzz,us.Nucl.Well
#headln = 1
#
#ID     = ds.remod
#infile = featureData/remodelling.all.-50..300.tab
#oldhead= Isw2,Nucl.remod,Nucl.Fuzz,Nucl.Well
#type   = 1
#header = ds.Isw2,ds.Nucl.remod,ds.Nucl.Fuzz,ds.Nucl.Well
#headln = 1



# TYPE # REDOX DATA

# FILE # Li vs. Tu
#ID     = tuli
#infile = processedData/tuli.snr.csv
#oldhead= Tu_basic_angle,Tu_basic_amplitude,Tu_basic_expression,Tu_basic_snr,Tu_basic_snr_pvals,Li_basic_angle,Li_basic_amplitude,Li_basic_expression,Li_basic_snr,Li_basic_snr_pvals,cluster_numbers_5
#header = Tu_basic_angle,Tu_basic_amplitude,Tu_basic_expression,Tu_basic_snr,Tu_basic_snr_pvals,Li_basic_angle,Li_basic_amplitude,Li_basic_expression,Li_basic_snr,Li_basic_snr_pvals,CL
#type   = 1
#headln = 1

# FILE # Li vs. Tu
ID     = tuli
infile = processedData/tuli.snr.csv
oldhead= cluster_numbers_5
header = CL
type   = 0
headln = 1
nodata = na
biotyp = redox.class
source.id=tuliData

# FILE # LiTu vs Lee
#ID     = rdxchr
#infile = workingData/rdx.chr.csv
#oldhead= CL.rdxchr
#header = CL.rdxchr
#nodata = na
#type   = 0
#headln = 1

## OWN, NOT PUBLIC DATA

# FILE # Li vs. Tu
ID     = elp3
infile = experiments/elp3/elp3.dat
oldhead=GCN5-ELP3 double average log(2),GCN5del average log(2),ELP3del
header = GCN5.ELP3.ko,GCN5.ko,ELP3.ko
type   = 1,1,1
headln = 1
biotyp = ko.transcriptome
source.id=elp3data



