
OCOR <- list()

for ( i in seq_along(ODAT) ) {

    id <- names(ODAT)[i]

    cat(paste('calculating correlations:', id, '\n'))
     
    DAT  <- ODAT[[i]]$data

    ## correlation matrix
    ## TODO: full correlation matrices!
    OCOR[[id]] <- getCorm(data=DAT, method=METHOD)
}
