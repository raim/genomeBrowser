
## extract ribosome densities from Riba et al. 2019 - oligo(dT)-based dataset
## also see processArava2003.R

in.path <- Sys.getenv("YEASTDAT")
rdnsr <- read.delim(file.path(in.path,"originalData","riba19_ribodense_oligodt.tsv"))

## density is ribosomes per codon
## to convert to ribosomes per mRNA we multiply by ORF length/3
rdnsr$X.Ribosomes <- rdnsr$real_density*rdnsr$orf_length/3

out <- rdnsr[,c("ensembl_gene_id","X.Ribosomes")]
colnames(out) <- c("ID","number.of.ribosomes")
write.table(out, file.path(in.path,"processedData","riba19_ribosomedensity.csv"),
            sep="\t",quote=FALSE,col.names=TRUE, row.names=FALSE)
