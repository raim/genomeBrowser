library(segmenTools) # coor2index
library(colorRamps)
DEBUG <- TRUE # FALSE # 
if ( DEBUG ) {
    library(Rcpp)
    source("~/programs/growthphases/pkg/R/dpseg.R")
    sourceCpp("~/programs/growthphases/pkg/src/dpseg.cpp")
} else {
    library(dpseg)
}

### PIECEWISE LINEAR SEGMENTATION  of total signal of yeast RNA-seq data
## using dpseg started 20181219

## TODO: FIX cut segments at y<0 and use addLm to re-calculate slopes etc.
##       or add this to dpseg as a util?


## DPSEG PARAMETERS
use.tot <- FALSE # TRUE # use genomeData total track or raw data
use.raw <- FALSE # TRUE # # apply ash trafo if FALSE
rm.zero <- FALSE # TRUE #  set zero to -2 to segment 0-ranges
maxl <- 5e3 # 10e3 # 7e3 # 5e3 # 
P <- .1
jumps <- TRUE
minl <- 5 # 100 #
maxsl <- 0.01 # maximal absolute slope for coloring!
## NOTE: negative option is only available in zeros branch of dpseg
negative <- FALSE # ALLOW NEGATIVE y BORDERS?
    

use.primseg <- FALSE # TRUE # 
plot.primseg <- TRUE # FALSE # 
plot.reference <- FALSE # primseg genomeBrowser plot
if ( use.primseg ) maxl <- 5e3 # 10e3 # 7e3 # Inf # NOTE: allow full length segments?
did <- ifelse(use.primseg, "dpseg_primseg", "dpseg")
did <- paste(did, as.character(maxl), as.character(P), "negative", negative, sep="_")
datid <- paste0("osciSeq_", did)
parid <- paste0("minl", minl, "_P",P, "_maxl", maxl)

## paths
data.path <- Sys.getenv("YEASTSEQ")
out.path <- file.path(Sys.getenv("GENDAT"), "yeast")
bro.path <- file.path(Sys.getenv("GENBRO"), "data", "yeast")
work.path <- file.path(Sys.getenv("YEASTSEGRES"), "dpseg")
dir.create(work.path, recursive=TRUE)
fig.path <- file.path(work.path, did)
dir.create(fig.path, recursive=TRUE)
if ( use.primseg ) {
    prim.path <- file.path(fig.path,"primsegs")
    dir.create(prim.path, recursive=TRUE)
}

if ( use.tot ) {
    ## NOTE: cut at a max value, see osciSeq_tot.R
    ## load total signal
    load(file.path(out.path,  "osciSeq_tot.RData"))
    tot <- c(odat$data, odat$data_rev)
} else {
    tot.file <- file.path(work.path,  "osciSeq_tot_raw.RData")
    if ( file.exists(tot.file) )
        load(tot.file)
    else {
        load(file.path(data.path,"genomeData",
                       "Sacchromyces.cerevisiae.clean.S288C.RData"))
        
        frw <- dat[,grep(".p$",colnames(dat))]
        rev <- dat[,grep(".m$",colnames(dat))]
        rm(dat)
        
        ## total sum of read-counts takes long!
        totf <- apply(frw,1,sum)
        totr <- apply(rev,1,sum)
        tot <- c(totf,totr)
        rm(totf,totr)
        gc()
        save(tot, file=tot.file)
    }
}

if ( rm.zero ) tot[tot==0] <- -3.62686 ## ash(x) = 2

## load annotation to get chrS
load(file.path(out.path,  "annotation.RData"))
annot <- odat$data
cf <- odat$data[odat$data[,"type"]=="chromosome",c("chr","start","end")]
cf <- cf[order(cf[,"chr"]),]
## return cumulative chromosome lengths, prepend 0 so chromsome number
## can be directly used to calculate offset
chrS <- c(0,cumsum(cf[,"end"]))

## segment each chromosome

if ( use.primseg ) {
    source(file.path(bro.path,"osciSeq_dpseg_primseg.R"))
} else {
    times <- rep(NA, length(chrS))
    segments <- NULL
    for ( i in 2:length(chrS) ) {
        
        cat(paste("CALCULATING CHROMOSOME", sprintf("%02d",i-1),
                  ":", chrS[i-1]+1, "-", chrS[i], "\n"))
        
        startt <- Sys.time()
        
        ## forward strand
        start <- chrS[i-1]+1
        end <- chrS[i]
        
        x <- start:end
        y <- tot[x]
        if ( use.raw ) { y <- y
        } else { y <- ash(y)}
        
        Sj <- dpseg(x=x, y=y, minl=minl, maxl=maxl, P=P,
                    jumps=jumps, negative=negative, verb=1)
        segments <- rbind(segments, cbind(chr=1, strand="+", Sj$segments))

        ## reverse strand
        x <- (chrS[i-1]+1):chrS[i] + max(chrS)
        y <- tot[x]
        if ( use.raw ) { y <- y
        } else { y <- ash(y)}
        
        Sj <- dpseg(x=x, y=y, minl=minl, maxl=maxl, P=P,
                    jumps=jumps, negative=negative, verb=1)
        segments <- rbind(segments, cbind(chr=1, strand="-", Sj$segments))
        
        times[i-1] <- Sys.time() -startt
    }
}


cutzero <- FALSE
if ( cutzero ) {
    ## revert!!
    if ( FALSE ) {
        segments[!is.na(segments$cutstart),"x1"] <- segments[!is.na(segments$cutstart),"cutstart"]
        segments[!is.na(segments$cutend),"x2"] <- segments[!is.na(segments$cutend),"cutend"]
    }
    
    ## cut start/end at y=a+b*x=0 -> x=-a/b
    x0 <- -segments$intercept/segments$slope
    cutstart <- x0>segments$x1 & segments$slope>0
    cutend <- x0<segments$x2 & segments$slope<0
    segments <- cbind(segments, x0=x0)
    ## original
    x1 <- segments$x1[cutstart]
    x2 <- segments$x2[cutend]
    ## replace by new
    segments$x1[cutstart] <- round(x0[cutstart])
    segments$x2[cutend] <- round(x0[cutend])
    nacol <- rep(NA,nrow(segments))
    segments <- cbind(segments, cutstart=nacol, cutend=nacol)
    ## store original
    segments$cutstart[cutstart] <- x1
    segments$cutend[cutend] <- x2
    ## TODO: use add.Lm to correct slopes 
}

## chromosome coordinates
seg <- index2coor(segments, chrS, cols=c("x1","x2"),strands=c("+","-"))
seg <- seg[,-which(colnames(seg)%in%c("start","end","intercept"))]
colnames(seg)[colnames(seg)=="x1"] <- "start"
colnames(seg)[colnames(seg)=="x2"] <- "end"

## swap columns for reverse strand
##tmp <- seg$start[seg$strand=="-"]
##seg$start[seg$strand=="-"] <- seg$end[seg$strand=="-"]
##seg$end[seg$strand=="-"] <- tmp

## reverse slope for reverse strand 
seg$slope[seg$strand=="-"] <- -seg$slope[seg$strand=="-"]


## generate colors from slope
nbreaks <- 101
## colors
colfunc <- colorRampPalette(c("blue", rgb(.75,.75,.75),"red")) 
colors <- colfunc(nbreaks-1)

## colors from slopes
slopes <- seg$slope 
slopes[slopes >  maxsl] <-  maxsl
slopes[slopes < -maxsl] <- -maxsl
rng <- max(abs(slopes),na.rm=TRUE)
breaks <- seq(-rng, +rng, length.out=nbreaks)
slp <- 1 + (slopes+rng)/(rng+rng)*100
#seg$color <- colors[slp]

## colors from total increase = length*slope for colors
lens <- abs(seg$end - seg$start) +1
lnsl <- lens*seg$slope
rng <- max(abs(lnsl),na.rm=TRUE)
breaks <- seq(-rng, +rng, length.out=nbreaks)
slp <- 1 + (lnsl+rng)/(rng+rng)*100
seg$color <- colors[slp]

## NOTE: nice normal distribution
## BUT BIASED AFTER STRAND CORRECTION??
plotdev(file.path(work.path,paste0(did,"_slope_distribution")), type="png", width=6,height=2)
par(mfcol=c(1,3), mai=c(.3,.3,.03,.03), mgp=c(1.2,.3,0),tcl=-.25)
hist(seg$slope, breaks=100, xlab="slope", main="")
hist(slopes, breaks=100, xlab="slope, cutoff", main="")
hist(lnsl, breaks=100, xlab="length*slope", main="")
dev.off()

## add ID
seg <- cbind(ID=paste0(did, "_",sprintf("%05d",1:nrow(seg))),type=did, seg)


## genomeBrowser track
## generate data object
DESC <- paste0("piecewise linear segments by dpseg",
               ifelse(use.primseg,"; based on pre-segmentation",""))
columns <- c(name="ID", type="type", chr="chr", strand="strand",
             start="start", end="end", color="color")
odat <- list(ID=datid,
             description=DESC,
             data=seg, 
             settings=list(type="plotFeatures",
                           names=FALSE, typord=TRUE,cuttypes=FALSE,
                           columns=columns, ylab="",hgt=.2))
## save genomeBrowser data track
file.name <- file.path(out.path,paste(odat$ID,".RData",sep=""))
save(odat,file=file.name)

## save to tab-delimited file for segment analysis
write.table(seg, file=file.path(work.path,paste0(did,".tsv")),sep="\t",quote=F,row.names=F)
## FULL WORKING COPY
save.image(file.path(work.path,paste0(datid,".RData")))

if ( !interactive() ) 
    quit(save="no")



### TEST P-VALUE SEGMENTATION? -> doesnt work well
## -> splits possible only with smoothed data
## -> KRE5, smoothed and with P=.05: opposite pattern, 3' is fragmented

test.pvalue.dpseg <- FALSE
if ( test.pvalue.dpseg ) {
    
    load(file.path(data.path,"genomeData",
                   "Sacchromyces.cerevisiae.clean.S288C_oscillation_3.RData"))
    i=2
    ## forward strand
    start <- chrS[i-1]+1
    end <- chrS[i]
    
    ## test FLO1
    flo1 <- 203403:208016
    x <- 200000:211000

    kre5 <- coor2index(annot[which(annot$name=="KRE5")[1],],chrS)[,c("start","end")]+ c(-3e3,3e3)
    x <- kre5[1,1]:kre5[1,2]
    
    y <- osc[x,"pval"]
    
    sm <- smooth.spline(x,y)
    x <- sm$x#[1:10000]
    y <- sm$y#[1:10000]
    
                                        #y <- ash(tot[x] # P=.1
    
    Sj <- dpseg::dpseg(x=x, y=y, minl=minl, maxl=maxl, P=0.05, jumps=jumps, verb=2)
    plot(Sj) 
    
}


