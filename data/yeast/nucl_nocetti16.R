library(segmenTools) # for alignData

## NUCLEOSOME TIME SERIES DATA FROM Nocetti and Whitehouse 2016

## data and output paths
yeast.path <- sub("YEASTDAT=","",system("env|grep YEASTDAT",intern=TRUE))
out.path <-  sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))
out.path <- file.path(out.path,"yeast")

## get functions (only for getChrIdx)
src.path <-  sub("GENBRO=","",system("env|grep GENBRO",intern=TRUE))
source(file.path(src.path,"src","genomeBrowser.R"))

## expand to full chromosome
## get chromosome index from annotation.RData
chrIdx <- getChrIdx(data.path=out.path)
chrS <-  c(0,cumsum(chrIdx[,"length"]))

## construct full matrix
data <- matrix(NA,nrow=sum(chrIdx[,3]),ncol=14)
colnames(data) <- c("chr","coor",1:12)
## generate coordinates
data[,"chr"] <- rep(chrIdx[,1],chrIdx[,3])
data[,"coor"] <- 1:nrow(data) - rep(chrS[1:nrow(chrIdx)],chrIdx[,3])
## NOTE: csv files generated in $TATADIR/yeast/preprocess.sh and
## by $TATADIR/perls/wig2coors.pl from
## files downloaded from GEO GSE77631; numbered by timepoints 1-12
## NOTE: using first columns as in wig-file, ignoring "end column"
files <- Sys.glob(file.path(yeast.path,"processedData/GSM20554*_gaussian.csv"))
expand <- TRUE ## pad values between given data, as indicated by WIG file 
for ( i in 1:length(files) ) {
  id <- sub(".*nucs_","",sub("_gaussian.csv","",files[i]))
  dat <- read.table(files[i],sep="\t",header=FALSE)
  ## cut values beyond chromosome length
  ## TODO: find out why these exist - produced by iNPS?
  rm <- NULL
  for ( chr in 1:nrow(chrIdx) ) 
    rm <- c(rm,which(dat[,1]==chr & dat[,2]>chrIdx[chr,3]))
  dat <- dat[-rm,]
  coors <- chrS[dat[,1]] + dat[,2]
  data[coors,id] <-  dat[,4]
  #if ( expand ) { ## TODO: instead interpolate!?
  #  for ( j in 1:9 ) {
  #    data[coors+j,id] <- dat[,4]
  #  }
  #}
}
## reset mito
data[data[,1]==17,3:ncol(data)] <- NA

## TODO: separately for each chromosome to get better values for ends
## TODO: cubic spline fit instead?
## TODO: don't interpolate but bin for cluster averages?
if ( expand ) {
  for ( i in 3:ncol(data) ) {
      x <- (1:nrow(data))[!is.na(data[,i])]
      y <- data[!is.na(data[,i]),i]
      xout <- 1:nrow(data)
      data[,i] <- approx(x=x,y=y,xout=xout)$y
      #data[,i] <- spline(x=x,y=y,xout=xout,method="fmm")$y
  }
}

## generate data object
odat <- list(ID="nucl_nocetti16",
             description="nucleosomes over oscillation",
             data=data,
             settings=list(type="plotHeat",
               ylab="occ., raw",colnorm=FALSE,axis2=FALSE,
               useChrS=TRUE,hgt=.4))
## store data
if ( !interactive() ) {
    file.name <- file.path(out.path,paste(odat$ID,".RData",sep=""))
    save(odat,file=file.name)
}


### ANALYZE DATA
dataID <- "nocetti16" #odat$ID

library(segmenTools) ## only for plotdev
fig.type <- "pdf" # "png" #
library("colorRamps")
colors <- matlab.like(100)  ## read count
grays <- gray(seq(1,0,length.out=100))
grays[1] <- "#FFFFFF" ## replace minimal by white

chemo.path <- sub("CHEMOSTAT=","",system("env|grep CHEMOSTAT",intern=TRUE))
                                        

fig.path <- file.path(yeast.path,"processedData",dataID)
dir.create(file.path(fig.path,"movie"),recursive=T)


## map to genes (similar to chromatinAverages.pl and genome2gene.R)
## but faster, using chrS
## read gene data, incl. TSS
#feats <- read.csv(file.path(yeast.path,"feature_R64-1-1_20110208_allData.csv"),sep="\t",header=TRUE)
load(file.path(out.path,"annotation.RData"))

feats <- odat$data

feats <- feats[feats[,'type']=='gene',]
## sort features by chromosome and start (captures similarities within
## domains, see plos one 2012 paper)
feats <- feats[order(feats[,"chr"], feats[,"start"]),]

## get start position and strand
chrs <- feats[,"chr"]

stends <- feats[,c("start","end")]
strnds <- as.character(feats[,"strand"])

## get start for forward and reverse strands
starts <- rep(NA, nrow(stends))
starts[strnds=="-"] <- apply(stends[strnds=="-",],1,max)
starts[strnds=="+"] <- apply(stends[strnds=="+",],1,min)
   
## get offset
offset <- feats[,"TSS.all.c.5p"]
starts[strnds=="-"] <- starts[strnds=="-"] - offset[strnds=="-"]
starts[strnds=="+"] <- starts[strnds=="+"] + offset[strnds=="+"]

## which are still present?
idx <- which(!is.na(starts))
feats <- feats[idx,]
starts <- starts[idx]
strnds <- strnds[idx]
chrs <- chrs[idx]

## ALIGN TO TSS
#mn <- apply(data[,3:ncol(data)],1,mean)
#mndata <- data
#mndata[,3:ncol(data)] <- log2(mndata[,3:ncol(data)]/mn)

coors <- data.frame(chr=chrs, coor=starts, strand=strnds)
dst <- 1000
geneData <- alignData(coors=coors, data=data, chrS=chrS, dst=dst)

## name geneData rows : TODO add in alignData function
geneData <- lapply(geneData, function(x) {rownames(x) <- feats[,"name"]; x})


## CALCULATE AVERAGES FOR CLUSTERS

### LOAD TRANSCRIPTOME TIME SERIES (from corecarbon.R)
noc16 <- read.csv(file.path(yeast.path,"processedData/nocetti16_transcriptome.csv"), header=T,row.names=1)
## LOG2 MEAN RATIO
noc16.nrm <- log2(noc16/apply(noc16,1,mean))
## 0-MEAN NORM.
noc16.nrm <- t(apply(noc16,1,scale))

## map to clustering
## cluster sorting
coarse.cls <- as.character(feats[,"CL_rdx"])
names(coarse.cls) <- rownames(feats) # feats[,"name"]

## TODO: require original sorting here?
coarse.srt <- unique(coarse.cls)
coarse.srt <- coarse.srt[coarse.srt!=""]
coarse.col <- as.character(feats[!duplicated(feats[,"CL_rdx"]),"CL_rdx_col"])
names(coarse.col) <- feats[!duplicated(feats[,"CL_rdx"]),"CL_rdx"]
noc162cls <- match(names(coarse.cls), rownames(noc16))

## cluster averages
clavg <- matrix(NA, ncol=ncol(noc16), nrow=length(coarse.srt))
rownames(clavg) <- coarse.srt
clstd <- clmed <- cllow <- clhig <- clavg
for ( cl in coarse.srt ) {
    clavg[cl,] <- apply(noc16.nrm[noc162cls[coarse.cls==cl],],2, function(x) mean(x,na.rm=T))
    clstd[cl,] <- apply(noc16.nrm[noc162cls[coarse.cls==cl],],2, function(x) sd(x,na.rm=T))
    clmed[cl,]<- apply(noc16.nrm[noc162cls[coarse.cls==cl],],2, function(x) median(x,na.rm=T))
    cllow[cl,]<- apply(noc16.nrm[noc162cls[coarse.cls==cl],],2,
                       function(x) quantile(x,.1,na.rm=T))
    clhig[cl,]<- apply(noc16.nrm[noc162cls[coarse.cls==cl],],2,
                       function(x) quantile(x,.9,na.rm=T))
}

## load oxygen data and sampling times (digitized by engauge from fig 1b)
## get time series
noc16ref <- read.csv(file.path(chemo.path,"originalData",
                               "nocetti16_fig1b.csv"))
noc16ref[,1] <- noc16ref[,1]/60 # time in hours

noc16.times <- noc16ref[which(!is.na(noc16ref[,"samples"])),1]
noc16ox <- noc16ref[!is.na(noc16ref[,2]),1:2]

## axis with TSS
xax <- as.numeric(colnames(geneData[[1]]))
pxax <- pretty(xax)
txax <- pxax
txax[txax==0] <- "TSS"


## zoom
zxax <- seq(-100,100,50)
tzxax <- as.character(zxax)
tzxax[tzxax=="0"] <- "TSS"

## total and cluster TSS-aligned avaerages
## all genes
tot <- lapply(geneData, function(x) apply(x,2, median, na.rm=TRUE))
tot <- matrix(unlist(tot), ncol=length(tot[[1]]), byrow=TRUE)
if ( dataID=="nocetti16" ) # NOTE: rownorm for nocetti16 only!!
    tot <- tot/rowMeans(tot) 
totnrm <- t(t(tot) - colMeans(tot))

colnames(tot) <- (1:ncol(tot))-dst
rownames(tot) <- 1:nrow(tot)

## x-axis in phases
nsamples <- nrow(tot)

## by clusters
## cluster sorting - TODO: use this below!
cls.srt <- c("A","AB","B","B.C","B.D","C","D")
cls <- as.character(feats[,"CL_rdx"])

clstot <- rep(list(NULL), length(cls.srt))
names(clstot) <- cls.srt
clstotnrm <- clstot
for ( cl in rev(cls.srt) ) {
    totc <- lapply(geneData, function(x) apply(x[cls==cl,],2,median,na.rm=TRUE))
    totc <- matrix(unlist(totc), ncol=length(totc[[1]]), byrow=TRUE)
    if ( dataID=="nocetti16" ) # NOTE: rownorm for nocetti16 only!!
        totc <- totc/rowMeans(totc) 
    totcnrm <- t(t(totc) - colMeans(totc))
    clstot[[cl]] <- totc
    clstotnrm[[cl]] <- totcnrm
}

### COMMON BREAKS & COLORS
n <- 100

totrng <- range(unlist(lapply(clstot, range)))
totbr <- seq(totrng[1],totrng[2],length.out=n+1)

nrmrng <- range(unlist(lapply(clstotnrm, range)))
nrmbr <- seq(nrmrng[1],nrmrng[2],length.out=n+1)

colors <- matlab.like(n)

## get total averages for each timepoint
#tot <- lapply(geneData, function(x) colMeans(x,na.rm=TRUE))
#tot <- matrix(unlist(tot), ncol=length(tot[[1]]), byrow=TRUE)
#tot <- tot/rowMeans(tot)
##tot <- t(t(tot)/colMeans(tot))
#colnames(tot) <- (1:ncol(tot))-dst
#rownames(tot) <- 1:nrow(tot)

## TODO: further align with nucl_amariei16! diff/common color breaks!

file.name <- file.path(fig.path,paste(dataID,"_TSS",sep=""))
plotdev(file.name,width=2.8,height=6, type=fig.type)
par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") ## NOTE: from corecarbon timeseries plot!
image(y=xax,x=noc16.times,tot,axes=FALSE,col=colors,breaks=totbr,main=NA,
      xlab="time, h",ylab=NA)
##image(y=xax,x=1:length(tsord),tot[tsord,],axes=FALSE,col=colors,main=NA,xlab=NA,ylab=NA)
axis(3, at=noc16.times, labels=NA, tcl=.3)
axis(1) #, at=1:length(tsord),labels=tsord,las=2)
axis(2, at=pxax,labels=txax,las=2)
abline(h=0,col="black",lty=3,lwd=2)
dev.off()

file.name <- file.path(fig.path,paste(dataID,"_TSS_diff",sep=""))
plotdev(file.name,width=2.8,height=6, type=fig.type)
par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") ## NOTE: from corecarbon timeseries plot!
image(y=xax,x=noc16.times,totnrm,axes=FALSE,col=colors,breaks=nrmbr,main=NA,
      xlab="time, h",ylab=NA)
axis(3, at=noc16.times, labels=NA, tcl=.3)
axis(1) #, at=1:length(tsord),labels=tsord,las=2)
axis(2, at=pxax,labels=txax,las=2)
abline(h=0,col="black",lty=3,lwd=2)
dev.off()

file.name <- file.path(fig.path,paste(dataID,"_TSS_zoom",sep=""))
plotdev(file.name,width=2.8,height=2, type=fig.type)
par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") ## NOTE: from corecarbon timeseries plot!
image(y=xax,x=noc16.times,tot,axes=FALSE,col=colors,breaks=totbr,main=NA,
      xlab="time, h",ylab=NA,ylim=c(-170,120))
##image(y=xax,x=1:length(tsord),tot[tsord,],axes=FALSE,col=colors,main=NA,xlab=NA,ylab=NA)
axis(3, at=noc16.times, labels=NA, tcl=.3)
axis(1) #, at=1:length(tsord),labels=tsord,las=2)
axis(2, at=zxax,labels=tzxax,las=2)
abline(h=0,col="black",lty=3,lwd=2)
dev.off()

ylim <-  c(min(cllow[coarse.srt,],na.rm=TRUE),
           max(clhig[coarse.srt,],na.rm=TRUE))    

## DO trace with cluster averages
file.name <- file.path(fig.path,paste(dataID, "_ranges_2",sep=""))
plotdev(file.name, width=2.8,height=2.2, type=fig.type, res=300)
par(mfcol=c(1,1),mai=c(0.45,.5,.5,.25), mgp=c(1.4,.3,0),tcl=-.3)#,xaxs="i",yaxs="i")
plot(noc16ox, type="l",lty=1,lwd=3,col="#55555555",axes=FALSE,xlab=NA,ylab=NA,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times) )
polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
        y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
        col="#C0C0C080",border=NA)
axis(4,at=round(range(noc16ox[,2])));mtext("DO, %", 4, .35)
par(new=T)
plot(1,col=NA,ylab="log2 mean ratio",xlab=NA,ylim=ylim,xlim=range(noc16.times),axes=FALSE);axis(1);axis(2)
axis(3, at=noc16.times, labels=FALSE, tcl=.3)
abline(h=0)
mtext("ALL",3,.5, font=2,cex=1.7)
for ( cl in coarse.srt )
    polygon(c(noc16.times,rev(noc16.times)),c(cllow[cl,],rev(clhig[cl,])),
            col=paste(coarse.col[cl],"33",sep=""),border=NA)
for ( cl in coarse.srt )
    lines(noc16.times, clmed[cl,],
          col=paste(coarse.col[cl],"DD",sep=""),pch=19,cex=.75,lwd=3)
mtext("time, h", 1, 1.2)
dev.off()

## only DO%
file.name <- file.path(fig.path, paste(dataID, "_TSS_DO",sep=""))
plotdev(file.name, width=2.8,height=2.2, type=fig.type, res=300)
par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i")#,yaxs="i")
plot(noc16ox, type="l",lty=1,lwd=3,col="#55555555",axes=FALSE,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times),xlab="time, h",ylab="DO, %" )
polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
        y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
        col="#C0C0C080",border=NA)
#axis(4,at=round(range(noc16ox[,2])));mtext("DO, %", 4, .35)
axis(1)
axis(2)
dev.off()


file.name <- file.path(fig.path, paste(dataID,"_TSS_DO_tight",sep=""))
plotdev(file.name,width=2.8,height=1,type=fig.type,res=300)
par(mai=c(0.05,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i")
plot(noc16ox, type="l",lty=1,lwd=0,col="#55555555",axes=FALSE,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times),xlab="time, h",ylab="DO, %" )
polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
        y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
        col="#C0C0C080",border=NA)
axis(1, at=seq(0,2*pi,pi/2),labels=expression(0,  pi/2, pi, 3*pi/2, 2*pi))
axis(2)
dev.off()



### 20170725: for phd-thesis: time-course plots by cluster!
### 201806: modified for MiBi and Conference Presentations
###          and aligned with nucl_amariei14_reanalysis.R

## PLOT CLUSTER AVERAGE TIMECOURSES
ylim <-  c(min(cllow[coarse.srt,],na.rm=TRUE),
           max(clhig[coarse.srt,],na.rm=TRUE))    
for ( cl in cls.srt ) {

    ## PLOT RNAseq TIMECOURSE; as in ranges_major/minor plots!
    ## from corecarbon.R
    ## plot cluster ranges and medians (boxplots)
    ylim <-  c(min(cllow[cl,],na.rm=TRUE),
               max(clhig[cl,],na.rm=TRUE))    
    file.name <- file.path(fig.path,
                           paste("nocetti16_ranges_",cl,sep=""))
    plotdev(file.name, width=2.8,height=2.2, type=fig.type, res=300)
    par(mfcol=c(1,1),mai=c(0.45,.5,.5,.05), mgp=c(1.4,.3,0),tcl=-.3)#,xaxs="i",yaxs="i")
    plot(noc16ox, type="l",lty=1,lwd=3,col="#55555555",axes=FALSE,xlab=NA,ylab=NA,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times) )
    polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
            y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
            col="#C0C0C080",border=NA)
    axis(4,at=round(range(noc16ox[,2])));mtext("DO, %", 4, .35)
    par(new=T)
    plot(1,col=NA,ylab="log2 mean ratio",xlab=NA,ylim=ylim,xlim=range(noc16.times),axes=FALSE);axis(1);axis(2)
    axis(3, at=noc16.times, labels=FALSE, tcl=.3)
    abline(h=0)
    mtext(cl,3,.5,col=coarse.col[cl],font=2,cex=1.7)
    polygon(c(noc16.times,rev(noc16.times)),c(cllow[cl,],rev(clhig[cl,])),
            col=paste(coarse.col[cl],"33",sep=""),border=NA)
    lines(noc16.times, clmed[cl,],
          col=paste(coarse.col[cl],"DD",sep=""),pch=19,cex=.75,lwd=3)
    mtext("time, h", 1, 1.2)
    dev.off()
    


    ### PLOT NUCL TIMECOURSE
    ## normalize be mean??
    ctot <- clstot[[cl]]
    ctotnrm <- clstotnrm[[cl]]
    
    #tot <- lapply(geneData, function(x)
    #    colMeans(x[coarse.cls==cl,],na.rm=TRUE))
    #tot <- matrix(unlist(tot), ncol=length(tot[[1]]), byrow=TRUE)
    #tot <- tot/rowMeans(tot)
    #colnames(tot) <- (1:ncol(tot))-dst
    #rownames(tot) <- 1:nrow(tot)
    
    file.name <- file.path(fig.path,paste(dataID,"_TSS_",cl,sep=""))
    plotdev(file.name,width=2.8,height=6, type=fig.type, res=300)
    par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") ## NOTE: from corecarbon timeseries plot!
    image(y=xax,x=noc16.times,ctot,axes=FALSE,col=colors,breaks=totbr,main=NA,
          xlab="time, h",ylab=NA)
    #image(y=xax,x=1:length(tsord),tot[tsord,],axes=FALSE,col=colors,main=NA,xlab=NA,ylab=NA)
    axis(3, at=noc16.times, labels=NA, tcl=.3)
    axis(1) #, at=1:length(tsord),labels=tsord,las=2)
    axis(2, at=pxax,labels=txax,las=2)
    abline(h=0,col="black",lty=3,lwd=2)
    dev.off()
    ## Delta DNA_occ
    file.name <- file.path(fig.path,paste(dataID,"_TSS_diff_",cl,sep=""))
    plotdev(file.name,width=2.8,height=6, type=fig.type, res=300)
    par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") 
    image(y=xax,x=noc16.times,ctotnrm,axes=FALSE,col=colors,breaks=nrmbr,main=NA,
          xlab="time, h",ylab=NA)
    axis(3, at=noc16.times, labels=NA, tcl=.3)
    axis(1) #, at=1:length(tsord),labels=tsord,las=2)
    axis(2, at=pxax,labels=txax,las=2)
    abline(h=0,col="black",lty=3,lwd=2)
    dev.off()
    ## zoom
    file.name <- file.path(fig.path,paste(dataID,"_TSS_",cl,"_zoom",sep=""))
    plotdev(file.name,width=2.8,height=2, type=fig.type, res=300)
    par(mfcol=c(1,1),mai=c(0.45,.5,.05,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i") ## NOTE: from corecarbon timeseries plot!
    image(y=xax,x=noc16.times,ctot,axes=FALSE,col=colors,breaks=totbr,main=NA,
          xlab="time, h",ylab=NA,ylim=c(-170,120))
    #image(y=xax,x=1:length(tsord),tot[tsord,],axes=FALSE,col=colors,main=NA,xlab=NA,ylab=NA)
    axis(3, at=noc16.times, labels=NA, tcl=.3)
    axis(1) #, at=1:length(tsord),labels=tsord,las=2)
    axis(2, at=zxax,labels=tzxax,las=2)
    abline(h=0,col="black",lty=3,lwd=2)
    dev.off()
}



### MOVIES

## by clusters
## cluster sorting
cls.srt <- c("A","AB","B","B.C","B.D","C","D")
cls <- as.character(feats[,"CL_rdx"])

## TODO: cluster stats, convert to function
avg <- rep(list(NULL),length(geneData))
avgnrm <- avg
for ( i in 1:length(geneData) )
    for ( cl in rev(cls.srt) )  
        avg[[i]] <- rbind(avg[[i]],
                          apply(geneData[[i]][cls==cl,],2,median,na.rm=T))

## TODO: row normalisation as above!!
    

## clusters time average
tavg <- avg[[1]]
tavg[] <- NA
for ( i in 1:nrow(tavg) ) {
  tmp <- lapply(avg, function(x) x[i,])
  tavg[i,] <- colMeans(matrix(unlist(tmp), ncol=length(tmp[[1]]), byrow=TRUE))
}

row.norm <- TRUE#FALSE#
col.norm <-  FALSE#TRUE#  
ts.norm <- FALSE#TRUE#
nname <- paste(dataID,"_movie_",sep="")
if ( row.norm ) nname <- paste(nname,"rn_",sep="")
if ( col.norm ) nname <- paste(nname,"cn_",sep="")
if ( ts.norm ) nname <- paste(nname,"ts_",sep="")
for ( i in 1:length(geneData) ) {
  file.name <- file.path(fig.path,"movie",
                         paste(nname,i,".png",sep=""))
  mavg <- avg[[i]]
  if ( row.norm ) mavg <- mavg/rowMeans(mavg)
  if ( col.norm ) mavg <- t(t(mavg)/colMeans(mavg))
  if ( ts.norm ) mavg <- mavg/tavg
  
  png(file.name,width=4,height=4,units="in",res=200)
  layout(matrix(1:2),widths=c(1,1),heights=rep(.5,1))
  par(mai=c(.5,.5,.01,.1),mgp=c(1.2,.3,0),tcl=-.2)
#  plot(fset[tidx,"phase"],fset[tidx,"DO"],xlab="phase",ylab="DO, %",axes=FALSE,pch=20,cex=.3)
#  axis(1, at=seq(0,2*pi,pi/2),labels=expression(0,  pi/2, pi, 3*pi/2, 2*pi))
  plot(noc16ox, type="l",lty=2,lwd=4,col="#000000FF",axes=F,xlab=NA,ylab=NA,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times))
  polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
          y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
          col="#C0C0C080",border=NA)
  axis(1)
  mtext("time, h", 1, 1.2)
  axis(2)
  abline(v=noc16.times[i])  
  #par(mai=c(.5,.5,.5,.1),mgp=c(1.2,.3,0),tcl=-.2)
  image(x=xax,y=1:nrow(avg[[i]]),t(mavg),axes=FALSE,col=colors,#main=i,
        xlab=NA,ylab=NA,xlim=c(-200,100))
  axis(2, at=1:nrow(avg[[i]]),labels=rev(cls.srt),las=2)
  axis(1, at=pxax,labels=txax,las=2)
  abline(v=0,col=4)
  dev.off()
  #scan()
}
## use image magick's convert to construct animated gif
system(paste("convert -delay 20 -loop 0", paste(file.path(fig.path,"movie",nname),c(1:length(geneData)),".png",collapse=" ",sep=""), file.path(fig.path,paste(nname,"allclusters.mp4",sep=""))))

#### OLD: pre 201806, not aliged with amariei

### PLOT BY TIME-POINTS

coarse.srt.major <- c("A","AB","B","B.C","C","B.D","D")

## legend heatmap
coarseLegend <- function() {
    ncls <- length(coarse.srt.major)
    image(x=1,y=1:ncls,t(as.matrix(ncls:1)),
          col=coarse.col[coarse.srt.major],axes=FALSE,xlab=NA,ylab=NA)
    text(x=1,y=ncls:1,coarse.srt.major,col="white",cex=3, font=2)
}
ncls <- length(coarse.srt.major)
#file.name <- file.path(fig.path,paste(dataID,"_legend",sep=""))
#plotdev(file.name,width=1,height=ncls, type=fig.type, res=300)
#par(mai=c(0,0,0,0))
#coarseLegend()
#dev.off()

## cluster averages
avg <- rep(list(NULL),12)
for ( i in 1:12 )
  for ( cl in rev(coarse.srt.major) ) 
    avg[[i]] <- rbind(avg[[i]],
                      colMeans(geneData[[i]][feats[,"CL_rdx"]==cl,],na.rm=T))

## clusters time average
tavg <- avg[[1]]
tavg[] <- NA
for ( i in 1:nrow(tavg) ) {
  tmp <- lapply(avg, function(x) x[i,])
  tavg[i,] <- colMeans(matrix(unlist(tmp), ncol=length(tmp[[1]]), byrow=TRUE))
}

## RNA-seq time-series plot template; for adding timepoint indicator
plotRNAseq <- function() {
    ylim <-  c(min(clmed[coarse.srt.major,],na.rm=TRUE),
               max(clmed[coarse.srt.major,],na.rm=TRUE)*1.1)    
    #par(mfcol=c(1,1),mai=c(0.45,.5,.1,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i")
    plot(noc16ox, type="l",lty=2,lwd=4,col="#000000FF",axes=F,xlab=NA,ylab=NA,ylim=round(range(noc16ox[,2])),xlim=range(noc16.times))
    polygon(x=c(noc16ox[1,1],noc16ox[,1],noc16ox[nrow(noc16ox),1]),
            y=c(min(noc16ox[,2]),noc16ox[,2],min(noc16ox[,2])),
            col="#C0C0C080",border=NA)
    par(new=TRUE)
    plot(1,col=NA,ylab="log2 mean ratio",xlab=NA,ylim=ylim,xlim=range(noc16.times),axes=F)#;axis(1);axis(2)
    axis(3, at=noc16.times, labels=NA, tcl=1,lwd.ticks=3,lwd=3)
    abline(h=0)
    for ( cl in coarse.srt.major )
        polygon(c(noc16.times,rev(noc16.times)),c(cllow[cl,],rev(clhig[cl,])),
                col=paste(coarse.col[cl],"33",sep=""),border=NA)
    for ( cl in coarse.srt.major )
        lines(noc16.times, clmed[cl,],
              col=paste(coarse.col[cl],"DD",sep=""),pch=19,cex=.75,lwd=3)
    mtext("time, h", 1, 1.2)
    #axis(4,at=round(range(noc16ox[,2])));mtext("DO, %", 4, .35)
}

## PLOT ALL TIME-POINTS
fig.type <- "pdf"
nname <- paste(dataID,"tp",sep="_")

emptyPlot <- function(...)
    plot(1,1,col=NA,axes=FALSE,xlab=NA,ylab=NA,...)
## X-AXIS PLOT
axisPlot <- function(side=3,label) {

    if ( missing(label) ) {
        yticklabel <- 0.5
        ylim <- c(0,1)
    } else {
        ylim <- c(0,1)
        yticklabel <- 0.25
        ylabel <- .66
    }
    
    par(mai=rep(0,4)) #c(.25,.5,.01,.01))
    #emptyPlot(ylim=ylim,xlim=range(xax))
    plot(xax,rep(1,length(xax)),col=NA,axes=FALSE,ylim=ylim)
    pxax <- pretty(xax)
    txax <- as.character(pxax)
    txax[txax=="0"] <- "TSS"
    txax[abs(pxax)==600] <- ""
    ##axis(2,tcl=.5)
    axis(side, at=pxax,tcl=.5,lwd.ticks=3,lwd=3)
    text(pxax,rep(yticklabel,length(pxax)),txax,cex=3)
    if ( !missing(label) )
        text(mean(range(xax)),ylabel,label,cex=3)
        
}
#file.name <- file.path(fig.path,paste(dataID,"_TSS_axis",sep=""))
#plotdev(file.name,width=6,height=1, type=fig.type, res=300)
#axisPlot()
#dev.off()

file.name <- file.path(fig.path,paste(nname,"_legend",sep=""))
plotdev(file.name,width=3*6+6,height=.5, type=fig.type, res=300)
layout(t(matrix(1:7)),widths=c(1,.15,1,.15,1,.15,.17),heights=rep(1,7))
par(mai=rep(0,4),xaxs="i")
emptyPlot(xlim=range(noc16.times),ylim=c(0,1));
axis(3,at=0:4,,tcl=.5,lwd.ticks=3,lwd=3);
text(1:4,rep(.5,4),1:4,cex=3)
text(2.5, .25, "time, h", cex=3)
par(mai=c(0,0.1,0,0.1))
emptyPlot()
par(mai=rep(0,4),xaxs="i") #c(.25,.5,.01,.01))
axisPlot()
par(mai=c(0,0.1,0,0.1))
emptyPlot()
par(mai=rep(0,4),xaxs="i") #c(.25,.5,.01,.01))
axisPlot()
par(mai=c(0,0.1,0,0.1))
emptyPlot()
emptyPlot
dev.off()

file.name <- file.path(fig.path,paste(nname,"_legend_top",sep=""))
plotdev(file.name,width=3*6+6,height=1, type=fig.type, res=300)
layout(t(matrix(1:7)),widths=c(1,.15,1,.15,1,.15,.17),heights=rep(1,7))
par(mai=rep(0,4),xaxs="i")
emptyPlot()
text(1,1,"RNA-seq Time-Series",cex=3)
par(mai=c(0,0.1,0,0.1))
emptyPlot()
par(mai=rep(0,4),xaxs="i") #c(.25,.5,.01,.01))
axisPlot(1,"MNase-seq Absolute Level")
par(mai=c(0,0.1,0,0.1))
emptyPlot()
par(mai=rep(0,4),xaxs="i") #c(.25,.5,.01,.01))
axisPlot(1,"MNase-seq Temporal Mean Ratio")
par(mai=c(0,0.1,0,0.1))
emptyPlot()
emptyPlot
dev.off()


timepointsOfInterest <- 1:12 # c(2,5,9) # min A, reset?, max A
for ( i in timepointsOfInterest ) {
    fname <- paste(nname,i,sep="") 

    ## average signal at this time-point
    ## and temporal mean ratio!
    mavg <- avg[[i]] ## cluster averages for this time-point
    tsmavg <- mavg/tavg ## normalized by temporal mean 

    ##png(file.name,width=4,height=3,
    ##    units="in",res=200)
    file.name <- file.path(fig.path,fname)
    plotdev(file.name,width=3*6+6,height=3, type=fig.type, res=100)
    layout(t(matrix(1:7)),widths=c(1,.15,1,.15,1,.15,.17),heights=rep(1,7))
    
    #file.name <- file.path(fig.path,paste(fname,"_ranges",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    #par(mfcol=c(1,1),mai=rep(0,4), mgp=c(1.4,.3,0),tcl=-.7,yaxs="i")
    par(mai=rep(0,4), mgp=c(1.4,.3,0),tcl=-.7,yaxs="i")
    plotRNAseq()
    abline(v=noc16.times[i],lwd=5)
    #dev.off()

    par(mai=c(0,0.1,0,0.1))
    coarseLegend()

    ## level
    #file.name <- file.path(fig.path,paste(fname,"_level",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    par(mai=rep(0,4)) #c(.25,.5,.01,.01))
    image(x=xax,y=1:nrow(avg[[i]]),t(mavg),axes=FALSE,col=grays,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE,xlim=c(-500,500))
    #axis(2, at=1:nrow(avg[[i]]),labels=rev(coarse.srt.major),las=2)
    axis(1, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="white",col.ticks="white")
    axis(3, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="white",col.ticks="white")
    abline(v=0,col="white",lwd=3,lty=2)
    #dev.off()

    par(mai=c(0,0.1,0,0.1))
    coarseLegend()
    
    ## temporal mean ratio
    #file.name <- file.path(fig.path,paste(fname,"_ratio",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    par(mai=rep(0,4)) #c(.25,.5,.01,.01))
    image(x=xax,y=1:nrow(avg[[i]]),t(tsmavg),axes=FALSE,col=colors,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE,xlim=c(-500,500))
    #axis(2, at=1:nrow(avg[[i]]),labels=rev(coarse.srt.major),las=2)
    axis(1, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="black",col.ticks="black")
    axis(3, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="black",col.ticks="black")
    abline(v=0,col="black",lwd=3,lty=2)
    #dev.off()

    par(mai=c(0,0.1,0,0.1))
    coarseLegend()

    ## COLOR BY MAXIMAL CLUSTER
    max.col <- coarse.col[names(which.max(clmed[coarse.srt.major,i]))]
    plot(1,1,col=NA,axes=FALSE,xlab=NA,ylab=NA)
    text(1,1,i,cex=8,font=2,col=max.col)

    dev.off()
}

files <- paste(file.path(fig.path,paste0(nname,2:12,".png")),collapse=" ",sep="")
movie <- file.path(fig.path,paste(nname,"_movie.mp4",sep=""))
cmd <- paste("convert -delay 50 -loop 0", files, movie)
system(cmd)

## TODO: log2 mean ratio for each nucleotide
quit(save="no")


### ANIMATION V2
library(segmenTools)

timepointsOfInterest <- 1:12 # c(2,5,9) # min A, reset?, max A
for ( i in timepointsOfInterest ) {
    fname <- paste(nname,i,sep="") 

    ## average signal at this time-point
    ## and temporal mean ratio!
    mavg <- avg[[i]] ## cluster averages for this time-point
    tsmavg <- mavg/tavg ## normalized by temporal mean 

    ##png(file.name,width=4,height=3,
    ##    units="in",res=200)
    file.name <- file.path(fig.path,fname)
    plotdev(file.name,width=(3*6),height=4, type=fig.type, res=100)
    layout(t(matrix(1:5)),widths=c(1,.15,1,.15,1),heights=rep(1,5))
    
    #file.name <- file.path(fig.path,paste(fname,"_ranges",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    #par(mfcol=c(1,1),mai=rep(0,4), mgp=c(1.4,.3,0),tcl=-.7,yaxs="i")
    par(mai=rep(0,4), mgp=c(1.4,.3,0),tcl=-.7,yaxs="i")
    plotClusters(noc16[noc162cls,], cls=coarse.cls, cls.col=coarse.col, cls.srt=coarse.srt.major,each=F,q=.5,norm="meanzero",time=noc16.times) #    plotRNAseq()
    abline(v=noc16.times[i],lwd=5)
    #dev.off()

    par(mai=c(0,0.1,0,0.1))
    coarseLegend()

    ## level
    #file.name <- file.path(fig.path,paste(fname,"_level",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    par(mai=rep(0,4)) #c(.25,.5,.01,.01))
    image(x=xax,y=1:nrow(avg[[i]]),t(mavg),axes=FALSE,col=grays,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE,xlim=c(-500,500))
    #axis(2, at=1:nrow(avg[[i]]),labels=rev(coarse.srt.major),las=2)
    axis(1, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="white",col.ticks="white")
    axis(3, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="white",col.ticks="white")
    abline(v=0,col="white",lwd=3,lty=2)
    #dev.off()

    par(mai=c(0,0.1,0,0.1))
    coarseLegend()
    
    ## temporal mean ratio
    #file.name <- file.path(fig.path,paste(fname,"_ratio",sep=""))
    #plotdev(file.name,width=6,height=2.5, type=fig.type, res=300)
    par(mai=rep(0,4)) #c(.25,.5,.01,.01))
    image(x=xax,y=1:nrow(avg[[i]]),t(tsmavg),axes=FALSE,col=colors,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE,xlim=c(-500,500))
    #axis(2, at=1:nrow(avg[[i]]),labels=rev(coarse.srt.major),las=2)
    axis(1, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="black",col.ticks="black")
    axis(3, at=pxax,labels=txax,tcl=1,lwd.ticks=2,col="black",col.ticks="black")
    abline(v=0,col="black",lwd=3,lty=2)
    #dev.off()

 #   par(mai=c(0,0.1,0,0.1))
 #   coarseLegend()

    ## COLOR BY MAXIMAL CLUSTER
#    max.col <- coarse.col[names(which.max(clmed[coarse.srt.major,i]))]
#    plot(1,1,col=NA,axes=FALSE,xlab=NA,ylab=NA)
#    text(1,1,i,cex=8,font=2,col=max.col)

    dev.off()
}

files <- paste(file.path(fig.path,paste0(nname,1:12,".png")),collapse=" ",sep="")
movie <- file.path(fig.path,paste(nname,"_movie.mp4",sep=""))
cmd <- paste("convert -delay 50 -loop 0", files, movie)
system(cmd)



### ANIMATION - MAY NOT WORK AFTER 20170725 REWORK!
cls.srt <- c("A","AB","B","B.C","C","B.D","D")

row.norm <- FALSE#TRUE#
col.norm <- FALSE#TRUE#
ts.norm <- TRUE#FALSE#
nname <- paste(dataID,"_movie_",sep="")
if ( row.norm ) nname <- paste(nname,"rn_",sep="")
if ( col.norm ) nname <- paste(nname,"cn_",sep="")
if ( ts.norm ) nname <- paste(nname,"ts_",sep="")
for ( i in c(2:11,1,12)) {
    file.name <- file.path(fig.path,
                           paste(nname,i,".png",sep=""))
    mavg <- avg[[i]]
    ##if ( row.norm ) mavg <- mavg/rowMeans(mavg)
    ##if ( col.norm ) mavg <- t(t(mavg)/colMeans(mavg))
    
    ##if ( ts.norm )
    ## normalized by temporal mean!
    tsmavg <- mavg/tavg
    
    ##png(file.name,width=4,height=3,
    ##    units="in",res=200)
    layout(matrix(1:3),heights=c(.3,.7,.7))
    
    ylim <- range(clmed[coarse.srt.major,])*1.2
    ##file.name <- file.path(fig.path,
    ##                       paste("nocetti16_ranges_major",sep=""))
    ##plotdev(file.name, type=fig.type,res=300,width=6,height=2.25)
    par(mai=c(0.25,.5,.01,.05), mgp=c(1.4,.3,0),tcl=-.3,xaxs="i",yaxs="i")
    plot(1,col=NA,ylab="log2 ratios",xlab=NA,ylim=ylim,xlim=c(0.9*min(noc16.times),1.1*max(noc16.times)),axes=F);axis(1);axis(2)
    axis(3, at=noc16.times, tcl=.3)
    for ( cl in coarse.srt.major ) 
        polygon(c(noc16.times,rev(noc16.times)),
                c(cllow[cl,],rev(clhig[cl,])),
                col=paste(coarse.col[cl],"33",sep=""),border=NA)
    for ( cl in coarse.srt.major ) 
        lines(noc16.times, clmed[cl,],
              col=paste(coarse.col[cl],"DD",sep=""),pch=19,cex=.75,lwd=3)
    mtext("time, h", 1, 1.2)
    abline(h=0)
    abline(v=i)
    
    ## change
    par(mai=c(.25,.5,.01,.01))
    image(x=xax,y=1:nrow(avg[[i]]),t(tsmavg),axes=FALSE,col=colors,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE)
    axis(2, at=1:nrow(avg[[i]]),labels=rev(cls.srt),las=2)
    axis(1, at=pxax,labels=txax)
    abline(v=0,col="black",lty=3,lwd=2)
    ## level
    image(x=xax,y=1:nrow(avg[[i]]),t(mavg),axes=FALSE,col=colors,main=i,
          xlab=NA,ylab=NA,useRaster=TRUE)
    axis(2, at=1:nrow(avg[[i]]),labels=rev(cls.srt),las=2)
    axis(1, at=pxax,labels=txax)
    abline(v=0,col="black",lty=3,lwd=2)
    ##  dev.off()
    ##scan()
}
##library(animation)
## see http://www.let.rug.nl/~wieling/statscourse/lecture5/index.Rmd
## how to incorporate an animation in slidify/knitr
##setwd(file.path(fig.path,"animation"))
##saveHTML({
##    ani.options(interval = 1, nmax = 12)
### ABOVE IMAGE CODE HERE w/o plotting to files
##}, img.name = "nocetti16_animate", title = "Demonstration of Brownian Motion",
##description = c("Nocetti & Whitehouse 2016"))

## use image magick's convert to construct animated gif
## NOTE: switching 1 and 12, since 1 is at lower DO then 12
## or skip 1
#system(paste("convert -delay 100 -loop 0", paste(file.path(fig.path,nname),c(2:11,12),".png",collapse=" ",sep=""), file.path(fig.path,paste(nname,".gif",sep=""))))

## AGAIN, but only for A and D 
avg <- rep(list(NULL),12)
for ( i in 1:12 )
  for ( cl in c("D","C","A") ) 
    avg[[i]] <- rbind(avg[[i]],
                      colMeans(geneData[[i]][feats[,"CL_rdx"]==cl,],na.rm=T))

for ( i in 1:12) {
  file.name <- file.path(fig.path,
                         paste(dataID,"_movie_AD",i,".png",sep=""))
  png(file.name,width=3,height=1.5,
      units="in",res=200)
  par(mai=c(.5,.5,.5,.01))
  image(x=xax,y=1:nrow(avg[[i]]),t(avg[[i]]),axes=FALSE,col=colors,main=i,
        xlab=NA,ylab=NA)
  axis(2, at=1:nrow(avg[[i]]),labels=c("D","C","A"),las=2)
  axis(1, at=pxax,labels=txax)
  abline(v=0,col="black",lty=3,lwd=2)
  dev.off()
  #scan()
}
## use image magick's convert to construct animated gif
## NOTE: switching 1 and 12, since 1 is at lower DO then 12
## or skip 1
system(paste("convert -delay 50 -loop 0", paste(file.path(fig.path,paste(dataID,"_movie_AD",sep="")),c(2:11,12),".png",collapse=" ",sep=""), file.path(fig.path,paste(dataID,"_movie_AD.gif",sep=""))))


##convert -delay 50 -loop 0 nucl_nocetti16_movie_1.png nucl_nocetti16_movie_2.png nucl_nocetti16_movie_3.png nucl_nocetti16_movie_4.png nucl_nocetti16_movie_5.png nucl_nocetti16_movie_6.png nucl_nocetti16_movie_7.png nucl_nocetti16_movie_8.png nucl_nocetti16_movie_9.png nucl_nocetti16_movie_10.png nucl_nocetti16_movie_11.png nucl_nocetti16_movie_12.png nucl_nocetti16_movie.gif

