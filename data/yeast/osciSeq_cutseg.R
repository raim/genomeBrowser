
## RNA-seq time-series by DBM
## generates osciSeq_cutseg: cutoff-segmentation of data

## for 0-padded segment names
library("stringr")

## load from sequencing data directory and save in genomeData
data.path <- sub("YEASTSEQ=","",system("env|grep -v YEASTSEQ2|grep YEASTSEQ",intern=TRUE))
out.path <-  sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))
out.path <- file.path(out.path,"yeast")

## load data
cseg <- read.table(file.path(data.path,"cutoff_segments.csv"),sep="\t",header=TRUE)

## cutoff segment colors: assign phase-color, if variance is small
phasecolors <- colorRampPalette(c("red","green","cyan","blue","blue"))(360) #colorRampPalette(coarse.col[coarse.srt.major])(360)
csegcol <- c("gray","black","gray")[cseg[,"type"]]
phvar <- cseg[,"r"] ## TODO: use r for alpha!
phvar[is.na(phvar)] <- 0
csegcol <- phasecolors[ceiling(cseg[,"mean"])] # phvar>0.5
csegcol <- paste(csegcol, format(as.hexmode(floor(255*phvar)),width=2),sep="")
csegcol[phvar==0] <- '#AAAAAA'
cseg <- cbind(cseg, color=csegcol)

## convert strands
cseg[cseg[,"strand"]==1,"strand"] <- "+"
cseg[cseg[,"strand"]==-1,"strand"] <- "-"

## add names
cseg <- lapply(split(cseg, cseg[,"type"]),
               function(x) cbind(x,name=paste("seg",x[,"type"],
                                     str_pad(1:nrow(x),4,pad="0"),sep="_")))
cseg <- do.call(rbind,cseg)
## generate data object
odat <- list(ID="osciSeq_cutseg",
             description="read-count segmentation (by cutoff)",
             data=cseg,
             settings=list(type="plotFeatures",
               types=1:3,typord=TRUE,cuttypes=FALSE,names=TRUE,
               ylab="segment",hgt=.5))
## store data
file.name <- file.path(out.path,paste(odat$ID,".RData",sep=""))
save(odat,file=file.name)
