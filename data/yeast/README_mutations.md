# Q&D Visual Scan:

Mutation patterns from Zhu et al. 2014; for the 3200 mutations of 
the ancestral strain ("zhu14anc/ANC") and the 872 mutations in
the mutation accumulation experiment ("zhu14ma/MA").

The notes below are all for the brogaard12 set of dyads from
a quick visual scan of mutation patterns around dyads by
mutation type, some of the patterns seem to be present in nocetti16 
as well. For both dyad sets only the 40000 highest scoring 
nucleosomes were used.

## Outlook

This would all require (a) more data, and (b) more systematic 
analyses/statistics of specific patterns; eg. definition of dyad-relative 
regions, local definition of linkers, etc.

The observed patterns may change with nucleosome pre-selection; one may
eg. think of a bootstrapping approach, randomly selecting subsets of
nucleosomes and scanning for consistency of patterns.

Accounting for transcription direction or filtering for clear lagging 
strand classifications (close to strong origins) may show interesting stuff 
as well. 

Of course a comparative analysis of dyad positions would also help, eg. 
the lowpass filtering of nocetti16 wrt the rotational bias
of brogaard12 properties should be better understood.

So many possibilities, focus recommendations are welcome :)


## Rough annotation key 

"left/right", - or + of dyad x-axis; "linker",
roughly at +/- 100; "left/right of dyad", peaks around +/- 50;

## MA: enrichment of G/C mutations, asymmetric around dyad

A->G, left linker, absence right of dyad
*C->G, right of dyad; left linker; absence left of dyad
C->T, left of dyad; absence left linker
*G->A, left linker, decreasing gradient across dyad to right linker
*G->T, left of dyad
T->C / T->A, perhaps a higher frequency component
T->C, at dyad
T->G, at dyad

## ANC: strongest patterns away from A, asymmetric for target type

A->C, absence directly left and right of dyad
*A->G, slightly left of dyad
*A->T, right of dyad
C->A, slight increase left and right of dyad
C->T, perhaps a higher frequency component
T->C / *T->A, perhaps a higher frequency component
G->A, increase around dyad
*G->C, left of dyad, absence right of dyad
G->T, right linker

