#!/bin/tcsh
# preprocess.sh
# SEPARATE ANALYSIS AND FILTERING OUT FROM MERE PREPROCESSING!

#REQUIRES: perl, Spreadsheet::ParseExcel,
#          ssconvert (gnumeric),
#	   pdftotext, wvText, 
#	   unzip, tar, 
#          cut, cat, sed
# 	   maf2fasta (via multiz-tba.111208.tar.gz from
#                     http://www.bx.psu.edu/miller_lab/dist/)

echo 'WARNING: This script converts M$ Excel sheets and can have high memory usage!'
echo 'You might want to close other programs or run on a machine with higher RAM ;)'


# REPLACE SUFFIXES (will lead to one duplication)
cat $YEASTDAT/originalData/isw2nhp10_exp_processed.oo.csv | sed 's/YJR151C.2/YJR151C/g' | sed 's/YGR032W.1/YGR032W/g' | sed 's/YGR032W.2/YGR032W/g' > $YEASTDAT/processedData/isw2nhp10_exp_processed.oo.csv

# UNPACK FILES

cd $YEASTDAT/processedData

tar -zxvf $YEASTDAT/originalData/amariei14.tar.gz ## added 20160404
tar -zxvf $YEASTDAT/originalData/nuc_HMM_calls.tar.gz
unzip  $YEASTDAT/originalData/Isw2_ChIP.sgr.zip
unzip  $YEASTDAT/originalData/Chr_remodelling.sgr.zip
unzip  $YEASTDAT/originalData/isw2_nucs.sgr.zip
unzip  $YEASTDAT/originalData/WT_nucs.sgr.zip
unzip  $YEASTDAT/originalData/total_RNA_Intensities_normalized.zip
unzip  $YEASTDAT/originalData/polyA_RNA_Intensities_normalized.zip 
unzip  $YEASTDAT/originalData/reads.zip
unzip  $YEASTDAT/originalData/predictions.zip
unzip  $YEASTDAT/originalData/features.zip
unzip  $YEASTDAT/originalData/chromFa.zip
# file:  E-MTAB-21-processed-data-1615256022.txt
unzip  $YEASTDAT/originalData/E-MTAB-21.processed.zip 

unzip $YEASTDAT/originalData/Yeast_2.na27.annot.csv.zip
unzip $YEASTDAT/originalData/yeast2_best_match.zip
unzip $YEASTDAT/originalData/yeast2_complex.zip
unzip $YEASTDAT/originalData/yeast2_good_match.zip

unzip $YEASTDAT/originalData//Yeast_2.probe_tab.zip 
unzip $YEASTDAT/originalData//YG_S98.probe_tab.zip 

## D'Ambrosio et al. 2008 - condensin and TFIIIC tiling array
## sent from Frank Uhlmann
unzip $YEASTDAT/originalData/dambrosio08figS1.zip

unzip $YEASTDAT/originalData/1158441_tables_s2_to_s6.zip
mv TableS1.xls nagalakshmi08.s1.xls
mv TableS2.xls nagalakshmi08.s2.xls
mv TableS3.xls nagalakshmi08.s3.xls
mv TableS4.xls nagalakshmi08.s4.xls
mv TableS5.xls nagalakshmi08.s5.xls
mv TableS6.xls nagalakshmi08.s6.xls

gunzip -c $YEASTDAT/originalData/journal.pone.0006700.s005 > $YEASTDAT/processedData/teytelman09.dat

unzip $YEASTDAT/originalData/yeast_pwm_all.zip


# badis chip data
unzip $YEASTDAT/originalData/Rsc8_ChIP-chip_data.zip
unzip $YEASTDAT/originalData/DIP_chip_GFF_files.zip
unzip $YEASTDAT/originalData/normalized-txt-files.zip RSC3-nucl-vs-WT_feb2006_signal.txt REB1-nucl-vs-WT_feb2006_signal.txt MCM1-nucl-vs-WT_feb2006_signal.txt CEP3-nucl-vs-WT_feb2006_signal.txt ABF1-nucl-vs-WT_feb2006_signal.txt RAP1-nucl-vs-WT_feb2006_signal.txt TBF1-nucl-vs-WT_feb2006_signal.txt 
unzip $YEASTDAT/originalData/normalized-txt-files.zip RSC3_cDNA_rest_R-vs-WT_feb2006_signal.txt REB1_cDNA_rest_R-vs-WT_feb2006_signal.txt MCM1_cDNA_rest_R-vs-WT_feb2006_signal.txt CEP3_cDNA_rest_R-vs-WT_feb2006_signal.txt ABF1_cDNA_rest_R-vs-WT_feb2006_signal.txt RAP1_cDNA_rest_R-vs-WT_feb2006_signal.txt TBF1_cDNA_rest_R-vs-WT_feb2006_signal.txt 
unzip $YEASTDAT/originalData/normalized-txt-files.zip RSC3_cDNA_rest_F-vs-WT_feb2006_signal.txt REB1_cDNA_rest_F-vs-WT_feb2006_signal.txt MCM1_cDNA_rest_F-vs-WT_feb2006_signal.txt CEP3_cDNA_rest_F-vs-WT_feb2006_signal.txt ABF1_cDNA_rest_F-vs-WT_feb2006_signal.txt RAP1_cDNA_rest_F-vs-WT_feb2006_signal.txt TBF1_cDNA_rest_F-vs-WT_feb2006_signal.txt 

tar -xvf $YEASTDAT/originalData/bogumil12.tar

## Chin et al. 2012 - TODO: reproduce how to generate chin12_2h.csv and chin12_4h.csv from these files!
gunzip $YEASTDAT/originalData/GSE30054_series_matrix.txt.gz
gunzip $YEASTDAT/originalData/GSE30053_series_matrix.txt.gz
R --vanilla <  $GENBRO/data/yeast/processChin2012.R

## NIEB data, sent via Email by Benjamin Audit and Arneodo Alain, 20180514
unzip $YEASTDAT/originalData//scer_sacCer3_loc_nfr.zip

cd $YEASTDAT

dos2unix $YEASTDAT/processedData/isw2_nucs.sgr
dos2unix $YEASTDAT/processedData/WT_nucs.sgr
dos2unix $YEASTDAT/processedData/Chr_remodelling.sgr
dos2unix $YEASTDAT/processedData/Isw2_ChIP.sgr

dos2unix $YEASTDAT/processedData/yeast*reads.txt

mac2unix $YEASTDAT/originalData/yen12_genes.txt

### FIX SGD liftOver chain files
grep -v "##" $YEASTDAT/originalData/V43_2004_07_26_V64_2011_02_03.over.chain | sed 's/\(chr..\)_[^ ]*/\1/g' > $YEASTDAT/processedData/V43_2004_07_26_V64_2011_02_03_fixed.over.chain
grep -v "##" $YEASTDAT/originalData/V56_2007_04_06_V64_2011_02_03.over.chain | sed 's/\(chr..\)_[^ ]*/\1/g' > $YEASTDAT/processedData/V56_2007_04_06_V64_2011_02_03_fixed.over.chain
grep -v "##" $YEASTDAT/originalData/V62_2009_02_18_V64_2011_02_03.over.chain | sed 's/\(chr..\)_[^ ]*/\1/g' > $YEASTDAT/processedData/V62_2009_02_18_V64_2011_02_03_fixed.over.chain
grep -v "##" $YEASTDAT/originalData/V55_2006_11_10_V64_2011_02_03.over.chain | sed 's/\(chr..\)_[^ ]*/\1/g' > $YEASTDAT/processedData/V55_2006_11_10_V64_2011_02_03_fixed.over.chain

# GENOME DATA
# TODO : store command-line call in originalData or separate table


# LEE et al. 2007 nucleosome data
perl $TATADIR/yeast/scripts/convertLee07.pl $YEASTDAT/originalData/analyzed_data_complete_bw20.txt > $YEASTDAT/processedData/analyzed_data_complete_bw20.csv

#!!! TODO : FUSE THESE SCRIPTS !!
perl $TATADIR/yeast/scripts/collapseLee07.pl > $YEASTDAT/processedData/nuc_HMM_calls.csv

perl $TATADIR/yeast/scripts/extractFuzzyLee07.pl $YEASTDAT/processedData/nuc_HMM_calls.csv > $YEASTDAT/processedData/nuc_HMM_fuzzy.csv

# !!! TODO : decide for version of hmmToDyad, move to process as here
# already filter is applied - same for other preprocess scripts where filters
# are applied!!!
perl $TATADIR/yeast/scripts/hmmToDyad.pl $YEASTDAT/processedData/nuc_HMM_calls.csv -d > $YEASTDAT/processedData/nuc_HMM_dyads.csv
# DO THIS LATER AS IT IS AN ANALYSIS STEP!
# REQUIRED FOR CALCULATION OF NUCLEOSOME AVERAGES/STRUCTURES!!!!
perl $TATADIR/yeast/scripts/hmmToDyad.pl $YEASTDAT/processedData/nuc_HMM_calls.csv | perl $TATADIR/yeast/scripts/addDyadTypes.pl > $YEASTDAT/nucleosomes.csv



# Albert et al.
cat $YEASTDAT/originalData/high-resolution-h2az.txt | sed 's/,/\t/g' > $YEASTDAT/processedData/high-resolution-h2az.csv

# POKHOLOK et al.
perl $TATADIR/perls/convertExcel.pl $YEASTDAT/originalData/Pokholok_et_al_data.xls -o $YEASTDAT/processedData/Pokholok_et_al_data.csv

# WHITEHOUSE et al. dyads
perl $TATADIR/perls/convertExcel.pl $YEASTDAT/originalData/nature06391-s2.xls -s 4 -c 1,2,3 -o $YEASTDAT/processedData/WT_dyads.csv
perl $TATADIR/perls/convertExcel.pl $YEASTDAT/originalData/nature06391-s2.xls -s 5 -c 1,2,3 -o $YEASTDAT/processedData/isw2_dyads.csv
#dos2unix processedData/WT_dyads.csv
#dos2unix processedData/isw2_dyads.csv

# SHIVASWAMY ... Iyer 2008 data
perl $TATADIR/yeast/scripts/convertIyer.pl originalData/normal_nuc.bed > processedData/normal_nuc.csv
perl $TATADIR/yeast/scripts/convertIyer.pl originalData/heatshock_nuc.bed  > processedData/heatshock_nuc.csv

perl $TATADIR/perls/wig2coordinates.pl originalData/heatshock.wig | perl $TATADIR/perls/genomeDataAddMissing.pl - 0 > processedData/heatshock.csv
perl $TATADIR/perls/wig2coordinates.pl originalData/normal.wig| perl $TATADIR/perls/genomeDataAddMissing.pl - 0 > processedData/normal.csv


# PEROCCHI et al. segment table 
# !!! TODO : separate filters from preprocessing !!!
perl $TATADIR/perls/convertExcel.pl originalData/perocchisegments.xls -s 1 -c 1,2,3,4,5,17,14,10 -fr 13 -o processedData/perocchisegments.csv

# filter transcribed and de-log2 
perl $TATADIR/yeast/scripts/filterPerocchi.pl processedData/perocchisegments.csv > processedData/segments_delog2.csv

# MIURA cDNA
cat originalData/Miura2006_cDNAclones.gff | grep "G-cap_judgement=Perfect"  > processedData/miura.gff

# ZHANG TSS
perl $TATADIR/yeast/scripts/filterZhang.pl originalData/Zhang2005_TSS.gff >  processedData/zhang.csv

# STEINMETZ et al. 2006 # TODO REPAIR
#perl perls/tmp.pl originalData/Steinmetz2006_PolIIoccupancy.gff
grep "sen1-PolII	" originalData/Steinmetz2006_PolIIoccupancy.gff > processedData/steinmetz_sen1.gff
grep "WT-PolII	" originalData/Steinmetz2006_PolIIoccupancy.gff > processedData/steinmetz_wt.gff


# STEIGELE et al. 2007
perl $TATADIR/yeast/scripts/convertSteigele.pl originalData/1741-7007-5-25-s4.txt > processedData/steigele07.dat

# MAVRICH et al. 2008 - nucleosome data
# WARNING: hardcoded path for output files!
perl $TATADIR/yeast/scripts/convertMavrich08.pl processedData/yeast-nucleosome-predictions-ALL-sigma-20.txt

# MACISAAC et al. 2006 - transcription factor binding sites
# using set with p-val < 0.005, no conservation filter
# !!! TODO : write parameter file for calculateChromatinExperiments.pl
# WARNING: hardcoded path for output files!
perl $TATADIR/yeast/scripts/convertMacisaac06.pl originalData/p005_c1.gff > processedData/macisaac06IDs.p5.dat
chmod a+x extractTFs.sh
./extractTFs.sh
\rm extractTFs.sh
# using set with p-val < 0.001, no conservation filter
perl $TATADIR/yeast/scripts/convertMacisaac06.pl originalData/p001_c1.gff > processedData/macisaac06IDs.p1.dat
chmod a+x extractTFs.sh
./extractTFs.sh
\rm extractTFs.sh

# replace outdated name : DON'T - is added to macisaac ID list later
## sed -i 's/RCS1/AFT1/g'processedData/macisaac* 

# NIEDUSZYNSKI et al. 2006 ARS DATA
perl $TATADIR/perls/convertExcel.pl originalData/Supp_Table_1.xls -s 1 -c 3,4,6 -fr 4 -o processedData/nieduszysnki06_ARS.csv

# ORIDB data
perl $TATADIR/perls/convertBEDwiggle.pl originalData/Raghu_timing.txt > processedData/raghuraman01.csv
perl $TATADIR/perls/convertBEDwiggle.pl originalData/Xu_ORC.txt > processedData/xu06orc.csv
perl $TATADIR/perls/convertBEDwiggle.pl originalData/Xu_Mcm2.txt > processedData/xu06mcm2.csv
perl $TATADIR/perls/convertBEDwiggle.pl originalData/Yabuki_timing.txt > processedData/yabuki02.csv
perl $TATADIR/perls/convertBED.pl originalData/OriDB_confirmed_ARS.php > processedData/nieduszysnki06.csv


## EATON et al. 2010 - ACS DATA - see below SGD DATA!
perl $TATADIR/yeast/scripts/processEaton10.pl originalData/GSM424494_acs_locations.bed > processedData/eaton10.csv

## Hawkins et al. 2013 - origin efficiency/stochasticity
## TODO: genome version??
$TATADIR/r/convertExcel.R --i originalData/hawkins13_mmc2.xlsx --lc 7 > processedData/hawkins13_oris.csv

## Smith and Whitehouse 2010 - okazaki fragments - mapped to May 2008 genome!
## TODO: this must currently be done MANUALLY because downloaddata.pl doesn't
## work, due to masking of _ and . in URL!
#mv GSM83565* originalData
#gunzip originalData/GSM835650_wt_sample.bed.gz
#gunzip originalData/GSM835651_wt_replicate.bed.gz
#gunzip originalData/GSM835652_pol32_sample.bed.gz
#gunzip originalData/GSM835653_pol32_replicate.bed.gz
## TODO: merge countReads.R and expandBed.R
$GENBRO/src/countReads.R --idx $YEASTDAT/originalData/Yeast_S288C.chromsizes --dir $YEASTDAT/originalData/ --pat GSM83565 --out $YEASTDAT/processedData --val 5  --str 6 --r2a --base1 --add --verb

## Reijns et al. 2015 - polalpha
## pol1-L868M (run192) http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1573441
## pol1-L868M (run36) http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSM1573454
## TODO: repair downloaddata perl so that bed.gz are save in originalData
## and NOT unzipped
mv ../GSM1573454_run36.bed.gz ../GSM1573441_run192.bed.gz ../originalData/ 

## correct error in Reijns et al. data
zcat ../originalData/GSM1573454_run36.bed.gz | sed 's/chrXVII/chrXVI/g' | gzip > GSM1573454_run36_corrected.bed.gz
zcat ../originalData/GSM1573441_run192.bed.gz | sed 's/chrXVII/chrXVI/g' | gzip > GSM1573441_run192_corrected.bed.gz
## count reads (w/o r2a since chromosomes are given as "chrIX")
$GENBRO/src/countReads.R --idx $YEASTDAT/originalData/Yeast_S288C.chromsizes --dir $YEASTDAT/processedData/ --pat GSM15734 --out $YEASTDAT/processedData --val 5  --str 6 --base1 --add --verb

## NOCETTI and WHITEHOUSE 2016
## TODO: repair downloaddata perl
## nucleosome data
mv ../GSM20554* ../originalData
#zcat ../originalData/GSM2055464_YRO_nucs_1_gaussian.wig.gz | perl $GENBRO/src/wig2coors.pl > GSM2055464_YRO_nucs_1_gaussian.csv
# bash!
foreach file (`ls ../originalData/GSM20554* -1`)
    set new=`echo $file|sed 's/..\/originalData/\./g;s/.wig.gz/.csv/g'`
    echo $file $new
    zcat $file  | perl $GENBRO/src/wig2coors.pl > $new
end
## transcriptome data
gunzip $YEASTDAT/originalData/GSE77631_RPKM_Raw_andNormalized.xlsx.gz
in2csv $YEASTDAT/originalData/GSE77631_RPKM_Raw_andNormalized.xlsx | sed 's/ (RKPM)//g' > $YEASTDAT/processedData/nocetti16_transcriptome.csv

## dyad data
for i in {1..12}; do
    echo $i
    ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='TP ${i}'" $YEASTDAT/originalData/nocetti16_suppdata1.xlsx $YEASTDAT/processedData/nocetti16_suppdata1_dyads_${i}.csv
done

## JIANG & PUGH, BMC Genome Biol 2009 : reference nucleosomes
## TODO: replaced by better but unpublished set, sent by Rossi/Pugh in 201802
unzip $YEASTDAT/originalData/gb-2009-10-10-r109-S1.ZIP -d $YEASTDAT/originalData/
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Measured nucleosomes'" $YEASTDAT/originalData/Additional\ data\ file\ 1.xls $YEASTDAT/processedData/jiang09_measuredNucleosomes.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Hypothetical nucleosomes'" $YEASTDAT/originalData/Additional\ data\ file\ 1.xls $YEASTDAT/processedData/jiang09_hypotheticalNucleosomes.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Arrays'" $YEASTDAT/originalData/Additional\ data\ file\ 1.xls $YEASTDAT/processedData/jiang09_arrays.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Linkers and NFRs'" $YEASTDAT/originalData/Additional\ data\ file\ 1.xls $YEASTDAT/processedData/jiang09_linkers.csv

## KRIETENSTEIN etal. 2016
mkdir $YEASTDAT/processedData/krietenstein16
tar xvf $YEASTDAT/originalData/GSE72106_RAW.tar --directory $YEASTDAT/processedData/krietenstein16
## NOTE: data sent by email by Megha Wal on 20180301, and stored
## at original data repository at 
## https://www.tbi.univie.ac.at/~raim/tataProject/originalData/
sed -i 's/\r/\n/g' $YEASTDAT/originalData/krietenstein16_Fig1_genelist_bovera_ratio_RP_removed.txt

## GBshape DATA
$GENBRO/src/wig2coors.pl $YEASTDAT/originalData/sacCer3.HelT.wig > $YEASTDAT/processedData/sacCer3.HelT.csv
$GENBRO/src/wig2coors.pl $YEASTDAT/originalData/sacCer3.MGW.wig  > $YEASTDAT/processedData/sacCer3.MGW.csv 
$GENBRO/src/wig2coors.pl $YEASTDAT/originalData/sacCer3.OC2.wig  > $YEASTDAT/processedData/sacCer3.OC2.csv 
$GENBRO/src/wig2coors.pl $YEASTDAT/originalData/sacCer3.ProT.wig > $YEASTDAT/processedData/sacCer3.ProT.csv
$GENBRO/src/wig2coors.pl $YEASTDAT/originalData/sacCer3.Roll.wig > $YEASTDAT/processedData/sacCer3.Roll.csv

## KAPLAN et al. 2008 - nucleosome data
## TODO: use liftOver to map to sacCer3 = SGD R64-1-1
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/YPD_dMean.chv -o processedData/YPD_dMean.chr
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/YPGal_dMean.chv -o processedData/YPGal_dMean.chr
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/YPEtOH_dMean.chv -o processedData/YPEtOH_dMean.chr
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/InVitro_dMean.chv -o processedData/InVitro_dMean.chr
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/S0106_avg_occ.txt -o processedData/S0106_avg_occ.chr
perl $TATADIR/yeast/scripts/nucleo08_chv2chr_MOD.pl originalData/S0106_model_score.txt -o processedData/S0106_model_score.chr


# MULTIPLE SEQUENCE ALIGNMENTS - CONVERT TO COORDINATE FORMAT
# Blanchette M, Kent WJ, Riemer C, Elnitski L, Smit AF, Roskin KM,
# Baertsch R, Rosenbloom K, Clawson H, Green ED, et al. Aligning
# multiple genomic sequences with the threaded blockset
# aligner. Genome Res. 2004 Apr;14(4):708-15.

# 1) convert chromosome fasta files (multiple sequence alignments)
# to coordinate format 
cd processedData
set files=`ls chr*.fa`
foreach fasta ( $files )
set maf=`echo $fasta |sed 's/.fa/.maf/g'`
set zfasta=`echo $fasta |sed 's/.fa/z.fa/g'`
echo "CONVERTING $maf alignment to fasta style $zfasta"
maf2fasta $fasta ../originalData/$maf fasta > $zfasta
echo "CONVERTING $zfasta to coordinate files"
perl ../$TATADIR/perls/fastaz2coordinates.pl $zfasta
end

# 2) concatenate chromosome coordinate files for each species
cat sacCer1.chr1 sacCer1.chr2 sacCer1.chr3 sacCer1.chr4 sacCer1.chr5 sacCer1.chr6 sacCer1.chr7 sacCer1.chr8 sacCer1.chr9 sacCer1.chr10 sacCer1.chr11 sacCer1.chr12 sacCer1.chr13 sacCer1.chr14 sacCer1.chr15 sacCer1.chr16 sacCer1.chrM |sed 's/chrM/chrMito/g' > sacCer1.dat
cat sacMik.chr1 sacMik.chr2 sacMik.chr3 sacMik.chr4 sacMik.chr5 sacMik.chr6 sacMik.chr7 sacMik.chr8 sacMik.chr9 sacMik.chr10 sacMik.chr11 sacMik.chr12 sacMik.chr13 sacMik.chr14 sacMik.chr15 sacMik.chr16 sacMik.chrM |sed 's/chrM/chrMito/g' > sacMik.dat
cat sacKud.chr1 sacKud.chr2 sacKud.chr3 sacKud.chr4 sacKud.chr5 sacKud.chr6 sacKud.chr7 sacKud.chr8 sacKud.chr9 sacKud.chr10 sacKud.chr11 sacKud.chr12 sacKud.chr13 sacKud.chr14 sacKud.chr15 sacKud.chr16 sacKud.chrM |sed 's/chrM/chrMito/g' > sacKud.dat
cat sacPar.chr1 sacPar.chr2 sacPar.chr3 sacPar.chr4 sacPar.chr5 sacPar.chr6 sacPar.chr7 sacPar.chr8 sacPar.chr9 sacPar.chr10 sacPar.chr11 sacPar.chr12 sacPar.chr13 sacPar.chr14 sacPar.chr15 sacPar.chr16 sacPar.chrM |sed 's/chrM/chrMito/g' > sacPar.dat
cat sacKlu.chr1 sacKlu.chr2 sacKlu.chr3 sacKlu.chr4 sacKlu.chr5 sacKlu.chr6 sacKlu.chr7 sacKlu.chr8 sacKlu.chr9 sacKlu.chr10 sacKlu.chr11 sacKlu.chr12 sacKlu.chr13 sacKlu.chr14 sacKlu.chr15 sacKlu.chr16 sacKlu.chrM |sed 's/chrM/chrMito/g' > sacKlu.dat
cat sacCas.chr1 sacCas.chr2 sacCas.chr3 sacCas.chr4 sacCas.chr5 sacCas.chr6 sacCas.chr7 sacCas.chr8 sacCas.chr9 sacCas.chr10 sacCas.chr11 sacCas.chr12 sacCas.chr13 sacCas.chr14 sacCas.chr15 sacCas.chr16 sacCas.chrM |sed 's/chrM/chrMito/g' > sacCas.dat
cat sacBay.chr1 sacBay.chr2 sacBay.chr3 sacBay.chr4 sacBay.chr5 sacBay.chr6 sacBay.chr7 sacBay.chr8 sacBay.chr9 sacBay.chr10 sacBay.chr11 sacBay.chr12 sacBay.chr13 sacBay.chr14 sacBay.chr15 sacBay.chr16 sacBay.chrM |sed 's/chrM/chrMito/g' > sacBay.dat

# remove single chromosome files
\rm sac*.chr*
cd -

# PHASTCONS DATA FROM UCSC, derived from above multiple sequence alignments
# QUOTE FROM http://genome.ucsc.edu/cgi-bin/hgTrackUi?hgsid=118401003&c=chr13&g=multizYeast
#These scores reflect the phylogeny (including branch lengths) of the
# species in question, a continuous-time Markov model of the
# nucleotide substitution process, and a tendency for conservation
# levels to be autocorrelated along the genome (i.e., to be similar at
# adjacent sites).  
perl $TATADIR/yeast/scripts/collapsePhastcons.pl > processedData/phastcons.csv
## new sacCer3 phastcons track: Apr. 2011 (SacCer_Apr2011/sacCer3), GCA_000146055.2
cat originalData/sacCer3.chrI.wigFixed originalData/sacCer3.chrII.wigFixed originalData/sacCer3.chrIII.wigFixed originalData/sacCer3.chrIV.wigFixed originalData/sacCer3.chrV.wigFixed originalData/sacCer3.chrVI.wigFixed originalData/sacCer3.chrVII.wigFixed originalData/sacCer3.chrVIII.wigFixed originalData/sacCer3.chrIX.wigFixed originalData/sacCer3.chrX.wigFixed originalData/sacCer3.chrXI.wigFixed originalData/sacCer3.chrXII.wigFixed originalData/sacCer3.chrXIII.wigFixed originalData/sacCer3.chrXIV.wigFixed originalData/sacCer3.chrXV.wigFixed originalData/sacCer3.chrXVI.wigFixed originalData/sacCer3.chrM.wigFixed > originalData/sacCer3_phastcons.wig
perl $TATADIR/perls/wig2coordinates.pl originalData/sacCer3_phastcons.wig > processedData/sacCer3_phastcons.csv

## MUTATION RATES, Zhu et al. PNAS 2014
## 201802: TODO analyze wrt nucleosomes as in catalyticSites.R
## windows2unix & extract two distinct tables
sed 's/\r/\n/g' $YEASTDAT/originalData/pnas.1323011111.sd01.txt | sed '10,3210 !d' >  $YEASTDAT/processedData/zhu14_ancestor.txt
sed 's/\r/\n/g' $YEASTDAT/originalData/pnas.1323011111.sd01.txt | sed '3231,4104 !d' >  $YEASTDAT/processedData/zhu14_MA.txt


# MOTIFS: PWM/PSSM DATA

perl $TATADIR/perls/convertExcel.pl originalData/PWMs.xls -o processedData/badis08pwm.csv
perl $TATADIR/perls/convertExcel.pl originalData/PWM_cutoffs_yeastDBDs.xls -o processedData/badis08cutoffs.csv
perl $TATADIR/perls/convertExcel.pl originalData/PWM-defined_targets_shown_in_Figures_5_and_6.xls -c 2,1 -o processedData/badis08targets.dat

# pwm targets
perl $TATADIR/yeast/scripts/convertBadis08.pl processedData/badis08targets.dat > processedData/badis08targets.csv

# REPLACE OUTDATED GENE NAMES in Badis et al. data set
sed -i 's/ZMS1/RSF2/g' processedData/badis*
sed -i 's/SIG1/MOT2/g' processedData/badis*
sed -i 's/YDR520c/YDR520C/g' processedData/badis*

# generate a list of pwm matrix id for later mapping on gene IDs
pcregrep -v "^[AGTC]\t" processedData/badis08pwm.csv | pcregrep -v "^PO\t" | pcregrep -v "^\s" > processedData/badis08IDs.dat

## TODO: PWM HANDLING, e.g. convert to EMBOSS::profit input format, use perl module TFBS etc.

## TODO : integrate Badis et al. CHIP data


## ZHU et al. 2009: TF binding site library, download via Bulyk PBM Database
# just remove the "-primary" postfix
sed -i 's/-primary//' processedData/yeast_pwm_all.txt
# convert into format used by calculateChromatinStructure.pl
# replace their new names Pbf1 and Pbf2 to allow mapping of the motif
# to gene IDs
perl $TATADIR/yeast/scripts/convertZhu09.pl processedData/yeast_pwm_all.txt | sed 's/Pbf1/YBL054W/' | sed 's/Pbf2/DOT6/' > processedData/zhu09.motifs.csv
# get IDs to be added to modifiers.dat !
grep "^MOTIF" processedData/zhu09.motifs.csv | sed 's/MOTIF\t//g' > processedData/zhu09IDs.dat

# TEYTELMAN et al. 2009 - silencing data
# print out all data (columns 0-7),
# plus a new column: divide "div" input by genomic reads in columns 3 and 5
perl $TATADIR/perls/mergeColumns.pl processedData/teytelman09.dat 3 5 div 0,1,2,3,4,5,6,7 > processedData/teytelman09.csv


# BADIS et al. 2010 - fuse cDNA data for different strands

perl $TATADIR/yeast/scripts/fuseBadis08.pl "ABF1,CEP3,MCM1,RAP1,REB1,RSC3,TBF1"


# HESSELBERTH et al. 2009 - DNAseI footprinting
# "Using this approach,
# we identified 4,384 footprints within the intergenic regions of the
# yeast genome at a false discovery rate (FDR) threshold of 5% or 0.05
# (Supplementary Table 2 online). We identified at least one footprint
# in the proximal promoter region of 1,778 genes, and 630 genes
# contained two or more footprints. At an FDR threshold of 10%, we
# identified 6,056 footprints distributed across 2,929 gene promoters,
# with 1,048 of them evincing more than two footprints."
# DESCRIPTION:
# -> the 6056 footprints at threshold 10% are in file generated here,
# the column "q-value" is the FDR value. The R script called below
# just adds a column indicating the 4384 footprints at threshold 0.005
# with column name fp.005
## TODO: get or reproduce clustering!
pdftotext originalData//nmeth.1313-S1.pdf -layout -f 10 -l 133 processedData/hesselberth09.txt
echo "chr\tstart\tend\tq-val" > processedData/hesselberth09.dat
pcregrep "chr\d+" processedData/hesselberth09.txt | sed 's/\s\+/\t/g' | sed 's/^\t//' >> processedData/hesselberth09.dat
R --vanilla < $TATADIR/yeast/scripts/convertHesselberth09.R


## PAN et al. 2011 : DSB HOTSPOTS
## the script calculates the midposition of reported DSB hotspot,
## and reports mid-position, size and value
perl $TATADIR/yeast/scripts/processPan11.pl originalData/Pan_2011_DSB_hotspots_V64.gff3 > processedData/pan11_DSBhotspots.csv

## BROGAARD et al. 2012: high-res nucl.pos.
## replace space by tabs in original files
sed 's/\s\+/\t/g' $YEASTDAT/originalData/nature11142-s2.txt > $YEASTDAT/processedData/brogaard12_unique.dat
sed 's/\s\+/\t/g' $YEASTDAT/originalData/nature11142-s3.txt > $YEASTDAT/processedData/brogaard12_redundant.dat

## liftOver: sacCer2 -> sacCer3
### TODO: use liftOver files from YGD
## https://downloads.yeastgenome.org/sequence/S288C_reference/genome_releases/liftover/
## TODO: move this to a general install script?
## TODO: convert brogaard coordinates to BED format, run through liftOver
## and reconstruct
UCSCUTILS=~/programs/ucsc_utils/

infile=$YEASTDAT/processedData/brogaard12_unique.dat
unfile=$YEASTDAT/processedData/brogaard12_unique_sacCer2_unlifted.dat
outfile=$YEASTDAT/processedData/brogaard12_unique_sacCer3.dat
awk  -v OFS='' '{printf("%s\t%d\t%d\t%s\t%s\n",$1,int($2)-1,int($2),$3, $4);}'  $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/originalData/sacCer2ToSacCer3.over.chain.gz stdout $unfile  | sed 's/:.*-/\t/' | cut -f 1,3,4,5 > $outfile

infile=$YEASTDAT/processedData/brogaard12_redundant.dat
unfile=$YEASTDAT/processedData/brogaard12_redundant_sacCer2_unlifted.dat
outfile=$YEASTDAT/processedData/brogaard12_redundant_sacCer3.dat
awk  -v OFS='' '{printf("%s\t%d\t%d\t%s\t%s\n",$1,int($2)-1,int($2),$3, $4);}'  $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/originalData/sacCer2ToSacCer3.over.chain.gz stdout $unfile  | sed 's/:.*-/\t/' | cut -f 1,3,4,5 > $outfile

cd -

### SCHACHERER et al. 2007 mutations between lab strains
## TODO: FIX unmatched in $unfile
## TODO: first convert gff to bed format!
## TODO: check sed command - causes problems for kubik18 data!
outfile=$YEASTDAT/processedData/schacherer07.dat
unfile=$YEASTDAT/processedData/schacherer07_unlifted.dat
grep SNPScanner $YEASTDAT/originalData/schacherer_snp.gff |grep _CALL | sed 's/XVI/16/;s/XV/15/;s/XIV/14/;s/XIII/13/;s/XII/12/;s/XI/11/;s/X/10/;s/chrM/chr17/;s/VIII/08/;s/VII/07/;s/VI/06/;s/IV/04/;s/V/05/;s/IX/09/;s/III/03/;s/II/02/;s/I/01/' | awk  -v OFS='' '{printf("%s\t%d\t%d\n",$1,int($4)-1,int($5));}' | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData//V43_2004_07_26_V64_2011_02_03_fixed.over.chain  stdout $unfile  > $outfile

# FEATURE DATA

# SGD PROTEIN PROPERTIES
cat originalData/protein_properties.tab.20071229 | sed s'/-\./-0\./g' | sed s'/\t\./\t0\./g' > processedData/protein_properties.tab

# SGD XREF : uniprot
perl $TATADIR/yeast/scripts/extractUniprot.pl originalData/dbxref.tab > processedData/uniprot.dat

## McCord et al. 2007 - gene expression sets
# generate simple header index, generate types from xsl annotation file
perl $TATADIR/perls/convertExcel.pl originalData/CondAnnot.xls -s 1 -o processedData/mccord07.annot.csv

perl $TATADIR/perls/convertHeader.pl  originalData/1327cond.txt -o processedData/mccord07.data.csv -id mcc

# add pmids and pre-classifications from 
#perl $TATADIR/yeast/scripts/generateMccord07annotation.pl

# fuse header names of data file with additional annotation 


# replace "NaN" by empty
sed -i 's/NaN//g' processedData/mccord07.data.csv
# construct headline


# LEE et al. 2007 chromatin clusters
# check name<->ID mapping
## TODO: preprocess to find IDs and add to
## parameters/R64-1-1_20110208_features.txt instead of the
## missing lee_clusters.csv
cat originalData/polyA_segments_verified_K_G4.kgg | sed 's/;/\t/g' | cut -f 1,6 | sed 's/"//g' >  processedData/lee_clusters.names.csv


# Holstege et al. 1998: transcription & degradation rates
# some corrections in originalData/orf_transcriptome.txt
# repair wrong gene names, correction individually verified
# YHR214C is YHR214CB in online interface
cat originalData/orf_transcriptome.txt | sed 's/YMRO17W/YMR017W/g' | sed 's/YHR214C/YHR214C-B/g'> processedData/orf_transcriptome.csv


# O'Shea protein localization
## TODO: use these data!!
cut -f 2,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32 originalData/OSheaLocalization.WeissmanAbundance.tab |sed 's/\tF/\t/g' | sed 's/\tT/\t1/g' > processedData/huh03.csv

# RNA halflives, Wang et al.
cat originalData/halflifeTable.txt |sed 's/,/_/g' > processedData/halflifeTable.csv

# LIEB et al. 2001 - Rap1 and Sir2/3/4
## TODO : add to modifies!!
perl $TATADIR/yeast/scripts/convertLieb01.pl ./originalData/Superset_Rap_Sir_Selected.txt > processedData/lieb01_rap1_sir.csv

## TODO : ADD RECOMBINATION / SIR2 data from Miczkowski et al. 2007


# LINDSTROM et al. 2006 - diverse chromatin remodellers
#cat originalData/data_for_clustering.txt | sed 's/^M/\n/g' > processedData/lindstrom06.dat
perl $TATADIR/yeast/scripts/convertLindstrom06.pl originalData/data_for_clustering.txt > processedData/lindstrom06.csv

# VARY et al. 2003 - ioc2 ioc3 isw1
#cat originalData/isw1_ioc2_ioc3.txt > processedData/isw1_ioc2_ioc3.dat # win2unix
perl $TATADIR/yeast/scripts/convertVary03.pl originalData/isw1_ioc2_ioc3.txt > processedData/isw1_ioc2_ioc3.csv


# GHAEMMAGHAMI et al. 
# from supplemental methods:
# -) indicates no detected expression, (%) indicates a detected band that was unquantifiable because of extremely low signal (approximately <50 molecules/cell) and  (#) indicates a detected band that was unquantifiable due to experimental problems with the Western blot. 
# Column 6. Spurious ORFs, as identified by the CEC/expression analysis are marked with ?e1?f
perl $TATADIR/perls/convertExcel.pl originalData/nature02046-s2.xls -s 1 -c 1,3 -o processedData/ghaemmaghami03.tmp
cat processedData/ghaemmaghami03.tmp | pcregrep -v "\t-" | pcregrep -v "\t%" | pcregrep -v "\t#" > processedData/ghaemmaghami03.csv


# MAN et al. 
perl $TATADIR/perls/convertExcel.pl originalData/tableS2.xls -s 1 -c 1,2 -fr 6 -o processedData/man07tai.csv
perl $TATADIR/yeast/scripts/convertMan07.pl processedData/man07tai.csv > processedData/man07.csv

# HUISINGA and PUGH et al. #!!!  TODO : find new URL
perl $TATADIR/perls/convertExcel.pl originalData/huisinga04S2.xls -s 1 -c 1,2 -fr 3 -o processedData/huisinga04s2.tab
perl $TATADIR/yeast/scripts/convertHuisinga04.pl processedData/huisinga04s2.tab > processedData/huisinga04s2.csv

# GERBER et al. 2004 PUF 
perl $TATADIR/perls/convertExcel.pl originalData/10.1371_journal.pbio.0020079.st003.xls -s 1 -c 1,7,8 -o processedData/puf1.csv
perl $TATADIR/perls/convertExcel.pl originalData/10.1371_journal.pbio.0020079.st004.xls -s 1 -c 1,7,8 -o processedData/puf2.csv
perl $TATADIR/perls/convertExcel.pl originalData/10.1371_journal.pbio.0020079.st005.xls -s 2 -c 1,7,8 -o processedData/puf3.csv
perl $TATADIR/perls/convertExcel.pl originalData/10.1371_journal.pbio.0020079.st006.xls -s 1 -c 1,7,8 -o processedData/puf4.csv
perl $TATADIR/perls/convertExcel.pl originalData/10.1371_journal.pbio.0020079.st007.xls -s 1 -c 1,7,8 -o processedData/puf5.csv

# YARRAGUDI et al. 
perl $TATADIR/perls/convertExcel.pl originalData/nar-02262-a-2006-File008.xls -s 1 -c 8,2,3,4,5 -o processedData/yarragudi07_abf1.dat
perl $TATADIR/yeast/scripts/convertYarragudi07.pl processedData/yarragudi07_abf1.dat > processedData/yarragudi07_abf1.csv

perl $TATADIR/perls/convertExcel.pl originalData/nar-02262-a-2006-File009.xls -s 1 -c 8,2,3,4,5 -o processedData/yarragudi07_rap1.dat
perl $TATADIR/yeast/scripts/convertYarragudi07.pl processedData/yarragudi07_rap1.dat > processedData/yarragudi07_rap1.csv

# MIYAKE et al. 2004 PNAS, Abf1 targets
pdftotext originalData/abf1targetes.pdf -layout processedData/miyake04_abf1targetes.txt
perl $TATADIR/yeast/scripts/convertMiyake04.pl processedData/miyake04_abf1targetes.txt > processedData/miyake04_abf1targetes.csv

# CVIJOVIC et al. 2007 - predicted uORFs
wvText originalData/cvijovic_s2.doc processedData/cvijovic_s2.txt
perl $TATADIR/yeast/scripts/convertCvijovic07.pl processedData/cvijovic_s2.txt > processedData/cvijovic_uORFs.csv

# PEROCCHI et al. - grep features with ANTISENSE transcripts
# WARNING : requires perocchi scripts run above under GENOME DATA !
cat processedData/segments_delog2.csv | grep antisense | cut -f 6,8 > processedData/perocchi07_antisense.dat
perl $TATADIR/yeast/scripts/expandPerocchi07.pl processedData/perocchi07_antisense.dat > processedData/perocchi07_antisense.csv

# NG et al. 2002 - RSC TARGETS - p-values for binding
# The data for the location analysis experiments of 5 RSC subunits (Sth1 and Rsc8 were done twice) and the general transcription factor TFIIB are available in two different formats. The data for all factors (P-values and ratios) can be downloaded for all the features spoted on the array. Those features correspond to intergenic regions of the yeast genome and are assigned to none, one or many genes.The data for all subunits (P-values and binding ratios) can also be downloaded for all the yeast genes. The genes have been assigned to the most proximal upstream intergenic feature. Each files are text files with each column separated by tabs. Descriptions of the contents of each column are provided in the first two rows.
# TODO : check many updates for generating feature file
# filter first and third row ("^assigned..", and "filter ...")
# TODO: what exactly is ratio_ORF norm, provided for second Rsc8 and Sth1 experiments, and for TFIIB??
# WARNING 16 duplicates
#cut -f 1,7,9,11,13,15,17,20,23 originalData/RSC_genelist_new.txt | grep -v filter | grep -v assigned > processedData/ng02.csv

# extract columns:
#echo "Rsc1.pval\tRsc1.ratio\tRsc2.pval\tRsc2.ratio\tRsc3.pval\tRsc3.ratio\tRsc8.pval\tRsc8.ratio\tSth1.pval\tSth1.ratio\tRsc8.ratio_ORFnorm\tSth1.ratio_ORFnorm\tRSC.comb.pval\tTFIIB.ratio_ORFnorm" > processedData/ng02.csv

## ALSO REPLACE THE 100% WRONG VALUE: 300.5654629 for gene YGR124W
cut -f 1,7,8,9,10,11,12,13,14,15,16,19,22,23,24 originalData/RSC_genelist_new.txt | grep -v filter | grep -v assigned > processedData/ng02.csv



# GELBART et al. 2005 - Isw2 binding
#WARNING 34 duplicates
perl $TATADIR/perls/convertExcel.pl originalData/Isw2WTK215R_ORF.xls -c 1,3,4 -o processedData/gelbart05orf.csv

#perl $TATADIR/perls/convertExcel.pl originalData/Isw2WTK215Rintergenic.xls -c 1,3,4 -o processedData/gelbart05intergenic.csv

# WHITEHOUSE et al. 2007 - clustering Isw2/remodelling data
# binding of ISW2 (inactive mutant) and remodelling score for yeast genes
# MAP:
# 0: no Isw2 binding, no remodelling
# 1: Isw2 binding AND remodelling
# 2: Isw2 binding, no remodelling
# 3: no Isw2 binding, BUT remodelling
perl $TATADIR/perls/convertExcel.pl originalData/nature06391-s2.xls -s 3 -o processedData/whitehouse07isw2targets.csv
perl $TATADIR/yeast/scripts/clusterWhitehouse07.pl processedData/whitehouse07isw2targets.csv > processedData/whitehouse07cluster.csv

## R64 mapping
## genome used was downloaded "between July 2004 and Nov. 2005"; this
## was the note in $TATADIR/yeast/parameters/chromatin.data.csv , likely from
## an email to Whitehouse; assuming release: V43_2004_07_26
#https://downloads.yeastgenome.org/sequence/S288C_reference/genome_releases/liftover/V43_2004_07_26_V64_2011_02_03.over.chain
UCSCUTILS=~/programs/ucsc_utils/
infile=$YEASTDAT/processedData/Isw2_ChIP.sgr
outfile=`echo $infile| sed 's/.sgr/_R64.tsv/'`
unfile=`echo $infile| sed 's/.sgr/.unfile/'`
echo -e "chr\tcoor\tIsw2_ChIP" > $outfile
cat $infile | sed 's/\(^[1-9]\)\t/chr0\1\t/;s/\(^[1-9][0-9]\)\t/chr\1\t/' | awk  -v OFS='' '{printf("%s\t%d\t%d\t%s\n",$1,int($2),int($2)+1,$3);}' | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V43_2004_07_26_V64_2011_02_03_fixed.over.chain  stdout $unfile | cut -f 1,3,4 | sed 's/^chr//;s/^0//' >> $outfile


# ROBERT et al. 2004 - GCN5, ESA1, POLII
# ORF and intergenic binding (ChIP-chip) of several histone modifiers
# and remodellers
# TODO : many duplicates, some unknown
#WARNING 41 duplicates
cut -f 2,4,5,6,7,8,9 originalData/CMT_ORF_data_file.txt | sed 's/NaN//g' | grep -v Empty |grep -v ybaS|grep -v ycxA|grep -v "^	" > processedData/robert04orf.csv
#WARNING 16 duplicates
cut -f 1,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 originalData/Chromatin_IG_genelist.txt | sed 's/NaN//g' > processedData/robert04ig.csv

# NEWMAN et al. 2006 - gene expression noise at protein level
# SD: minimal medium, YEPD: rich medium
# CV: coefficient of variation (standard deviation/mean) times 100(%)
# DM: distance of each CV to a running median of CV values
# "DM values permit protein-specific noise levels to be compared independently of confounding influences due to protein abundance, noise from the instrument response, or intracellular differences in cells globally affecting protein production."
perl $TATADIR/perls/convertExcel.pl -fr 4 -c 1,24,25,26,27 originalData/nature04785-s03.xls -o processedData/newman06noise.csv 
# difference of mRNA and protein-GFP levels - log2 ratios SD/YEPD (minimal vs. full medium)
perl $TATADIR/perls/convertExcel.pl -fr 4 -c 1,2,3 originalData/nature04785-s04.xls -o processedData/newman06change.csv 


# NAGALAKSHMI et al. 2008 - RNA-Seq transcriptional landscape
# uORFs
perl $TATADIR/perls/convertExcel.pl -fr 3 -c 1,6 processedData/nagalakshmi08.s6.xls -o processedData/tmp.csv
echo "gene\tuORF.length" > processedData/nagalakshmi08.uORFs.csv 
cat processedData/tmp.csv >> processedData/nagalakshmi08.uORFs.csv 

# ROBERTS AND HUDSON 2006 - glucose-glycerol transition -streamline
# names of transposons and repeats, and suffixes (the later will cause duplicates!)
cat originalData/glucose_glycerol_transition.pcl | sed 's/DELTA/delta/g' | sed 's/SIGMA/sigma/g' | sed 's/TAU/tau/g' | sed 's/OMEGA/omega/g' | sed 's/TY/Ty/g' | sed 's/_0//g' | sed 's/_1//g' | sed 's/_2//g' | sed 's/_3//g' | sed 's/_4//g' | sed 's/_5//g' | sed 's/_6//g' |  sed 's/_alt//g' | sed 's/YER014c-a/YER014C-A/g' | sed 's/YDRCdelta6A/YDRCdelta6a/g' | sed 's/YCLWdelta2A/YCLWdelta2a/g' > processedData/roberts06.csv
# WARNING 245 duplicates

# KOERBER et al. 2009 - TF-nucleosome interactions
perl $TATADIR/perls/convertExcel.pl originalData/PIIS109727650900639X.mmc3.xls -o processedData/koerber09.csv

## GRANOVSKAIA et al. 2010 - mitotic cycle, tiling array segments
## TODO: mapped to Saccharomyces Genome Database of 7 August 2005
## use liftOver from V43_2004_07_26_V64_2011_02_03.over.chain
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=| eol=unix sheet='523 antisense transcripts'" $YEASTDAT/originalData/13059_2009_2317_MOESM3_ESM.xls $YEASTDAT/processedData/granovskaia10_file3_antisense.csv
## TODO: grep color!
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=| eol=unix sheet='135 unannotated intergenic'" $YEASTDAT/originalData/13059_2009_2317_MOESM3_ESM.xls $YEASTDAT/processedData/granovskaia10_file3_intergenic.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=| eol=unix sheet='598 ORFs cycling'" $YEASTDAT/originalData/13059_2009_2317_MOESM5_ESM.xls $YEASTDAT/processedData/granovskaia10_file5_ORFs_cycling.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=| eol=unix sheet=' 37 antisense cycling'" $YEASTDAT/originalData/13059_2009_2317_MOESM5_ESM.xls $YEASTDAT/processedData/granovskaia10_file5_antisense_cycling.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=| eol=unix sheet='11 novel cycling'" $YEASTDAT/originalData/13059_2009_2317_MOESM5_ESM.xls $YEASTDAT/processedData/granovskaia10_file5_novel_cycling.csv



# AFFYMETRIX PROBE MAPS
cut -f 1,3,8 -d "," processedData/Yeast_2.na27.annot.csv | sed 's/,/\t/g' | sed 's/"//g' | grep -v "^#" |grep "cerevisiae" | cut -f 1,3 > processedData/yeast2.annot.csv
cut -f 3,6 processedData/yeast2_best_match.txt > processedData/yeast2.s98.csv

perl $TATADIR/yeast/scripts/mergeAffyProbes.pl processedData/yeast2.annot.csv processedData/yeast2.s98.csv > processedData/affyprobes.csv

# GO - simple
# TODO : move this to main folder, as GO is general
perl $TATADIR/yeast/scripts/extractOBOterms.pl originalData/gene_ontology.1_2.obo > processedData/goterms.csv
perl $TATADIR/yeast/scripts/extractOBOterms.pl originalData/goslim_yeast.obo > processedData/goterms.slim.csv

# SBO
perl $TATADIR/yeast/scripts/extractOBOterms.pl originalData//SBO_OBO.obo > processedData/sboterms.csv

# HARTLEY AND MADHANI 2009 - RSC/Abf1/Reb1
perl $TATADIR/perls/convertExcel.pl originalData/gene_classification.xls -c 1,2,4,5,7,8 -s 1 -o processedData/hartley09.all.csv
## attach fake 1 value to make usable for mergeFeatures.pl
perl $TATADIR/yeast/scripts/convertHartley09.pl processedData/hartley09.all.csv > processedData/hartley09.csv


# TIROSH AND BARKAY 2008 - ODP/NDP promotor classes
pdftotext originalData/Supplementary_Figures_april15.pdf -layout -f 16 -l 26 processedData/tirosh08.txt
perl $TATADIR/yeast/scripts/convertTirosh08.pl > processedData/tirosh08.dat

# CHOI AND KIM 2008 - cis and trans regulation
perl $TATADIR/perls/convertExcel.pl originalData/ng.2007.58-S2.xls -fr 3 -o processedData/choi08.csv

# IOSHIKES ET AL. 2006 - NPS-based clusters
perl $TATADIR/perls/convertExcel.pl originalData/ng1878-S7.xls -fr 14 -c 1,6 -o processedData/ioshikes06.csv

# VAILLANT ET AL. 2010 - bistable/crystalline nucleosome configuration
perl $TATADIR/yeast/scripts/collapseVaillant10.pl > processedData/vaillant10.csv

# XU ET AL. 2009 - CUTs/SUTs
## "Sequence and feature files (.gff files) for
## S288c were obtained from the Saccharomyces Genome Database on 4th
## September 2007."
perl $TATADIR/perls/convertExcel.pl originalData/nature07728-s2.xls -s 1  -o processedData/xu09.csv

# TIROSH ET AL. 2010 - ISW1 mid-coding region remodelling
perl $TATADIR/perls/convertExcel.pl originalData/gb-2010-11-5-r49-s3.xls -s 1  -o processedData/tirosh10.dat
## collapse and map from gene names to IDs
perl $TATADIR/yeast/scripts/convertTirosh10.pl processedData/tirosh10.dat > processedData/tirosh10.csv

# BRAUER et al. 2008
perl $TATADIR/perls/convertExcel.pl originalData/TableS1.xls -o processedData/brauer08.csv -c 2,5,6,7,8,9,11,12,13,14,15,16

# ANGUS-HILL et al. 2001 - RSC3/RSC30 transcriptomes
perl $TATADIR/perls/convertExcel.pl originalData/PIIS1097276501002192.mmc2.xls -s 1 -fr 3 -o processedData/angushill01.dat
perl $TATADIR/yeast/scripts/averageAngusHill01.pl processedData/angushill01.dat > processedData/angushill01.csv

# VAN DIJK et al. 2011, Nature
## data obtained from Chunlong Chen via email
# gene classes based on overlap with XUT noncoding transcripts
cut -f 6,8 originalData/20111121.AllORF.WithClassType.txt > processedData/vandijk11_classes.dat

## SLAVOV and BOTSTEIN 2011, PNAS: oscillation in EtOH batch culture
cut -f 2,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71 originalData/ymc_eth_raw_centered_ben.cdt > processedData/slavov11_timeseries.dat

## SLAVOV et al. 2014 (batch, aerobic glycolysis)
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 2 -o processedData/slavov14_phase1_transcripts.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 3 -o processedData/slavov14_phase2_transcripts.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 4 -o processedData/slavov14_phase1_proteins.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 8 -o processedData/slavov14_phase2_proteins.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 5 -o processedData/slavov14_phase1_phosphorylated.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 9 -o processedData/slavov14_phase2_phosphorylated.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 6 -o processedData/slavov14_phase1_acetylated.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 10 -o processedData/slavov14_phase2_acetylated.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 7 -o processedData/slavov14_phase1_methylated.dat
perl $TATADIR/perls/convertExcel.pl originalData/slavov14_mmc2.xls -s 11 -o processedData/slavov14_phase2_methylated.dat

## WANG et al. 2015, Cell Reports: transcriptome data from two dilution rates
## NOTE: ssconvert is not installed at TBI; convertExcel.pl can't handle xlsx
#perl  $TATADIR/perls/convertExcel.pl originalData/wang15_sup.xlsx -s 7 -o processedData/wang15_lowD.dat
#perl  $TATADIR/perls/convertExcel.pl originalData/wang15_sup.xlsx -s 8 -o processedData/wang15_highD.dat
## NOTE: RPKM values with HOMER (Heinz et al., 2010).
## loess normalized by non-oscillating genes!
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='HighGlucoseTranscription'" originalData/wang15_sup.xlsx processedData/wang15_highD.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='LowGlucoseTranscription'" originalData/wang15_sup.xlsx processedData/wang15_lowD.csv


## DAMELIN et al. 2002, Mol Cell Biol - Rsc9 locations
perl $TATADIR/perls/convertExcel.pl originalData/damelin02_mmc2.xls -s 1 -fr 4 -c 2,3,4,5,6 -o processedData/damelin02.dat

## ESSER et al. 2004, eubacterial vs. archaeal origin data
## data provided by Bill Martin in Feb. 2012 
## blast fasta files to map gene IDs, and merge back with data
$TATADIR/yeast/scripts/processEsser04.sh

## COTTON AND MCINERNY 2010 PNAS
pdftotext originalData/completeSI.pdf -layout -f 34 -l 189 processedData/cotton10.dat
perl $TATADIR/yeast/scripts/processCotton10.pl processedData/cotton10.dat > processedData/cotton10.csv

## BOGUMIL et al. 2012 - CHAPERON and ARCH/BAC DATA
perl $TATADIR/perls/convertExcel.pl processedData/bogumil12/classification.xls -s 1 -c 2,1,0,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 -o processedData/bogumil12.csv

## MACHNE et al. 2012 - own data
sed 's/,/\t/g;s/"//g' originalData/tuliCoarse.results.csv > processedData/machne12_timecourses.csv

## CASOLARI et al. 2004: remove windows newlines ^M
sed 's/\r/\n/g' originalData/mmc1.txt > processedData/casolari04_glucose.csv
sed 's/\r/\n/g' originalData/mmc3.txt > processedData/casolari04_galactose.csv

## 2024: summarizing casolari data
R --vanilla < $GENBRO/data/yeast/processCasolari2004.R

## 20141222 - SGD versions of ncRNA Transcript Data from three papers
#$TATADIR/yeast/scripts/merge_ncRNAs.R > processedData/ncRNAs_r64.csv
## 20180302 - including granovskaia10 transcripts
$GENBRO/data/yeast/merge_transcripts.R > $YEASTDAT/processedData/transcripts_r64.csv

## Chan et al. 2014 DRIP-ChIP
## TODO - genome version?
unzip  ./originalData/E-MTAB-2388.processed.1.zip

## El Hage et al. 2014 DNA:RNA hybrid ChIP - genome version sacCer3
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM1402299_Batch1_ChIP-seq_Wild-type_raw.bw $YEASTDAT/processedData/GSM1402299_Batch1_ChIP-seq_Wild-type_raw.bed
## convert to bed
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM1402301_Batch1_ChIP-seq_rnh1drnh201d_raw.bw $YEASTDAT/processedData/GSM1402301_Batch1_ChIP-seq_rnh1drnh201d_raw.bed
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM1402302_Batch1_ChIP-seq_PGAL_TOP1rnh1drnh201d_raw.bw $YEASTDAT/processedData/GSM1402302_Batch1_ChIP-seq_PGAL_TOP1rnh1drnh201d_raw.bed
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM1402300_Batch1_input_chromatin_Wild-type_raw.bw $YEASTDAT/processedData/GSM1402300_Batch1_input_chromatin_Wild-type_raw.bed
## expand bed 
$GENBRO/src/expandBed.R --idx $YEASTDAT/originalData//Yeast_S288C.chromsizes --dir $YEASTDAT/processedData --pat GSM14023 --out $YEASTDAT/processedData --verb


## Rouskin et al. 2014 - in vivo RNA structures 
## NOTE: this is for tcsh, for bash, additionally redirect stderr of curl with 
##  2>/dev/null; command line modified from http://stackoverflow.com/a/10189130
## NOTE: paper mentions 189, but downloaded and processed table only yields 188
curl "http://weissmanlab.ucsf.edu/yeaststructures/index.html"  | grep -e '</\?table\|</\?td\|</\?tr\|</\?th' | sed 's/^[\ \t]*//g' | tr -d '\n' | sed 's/<\/tr[^>]*>/\n/g'  | sed 's/<\/\?\(table\|tr\)[^>]*>//g' | sed 's/^<t[dh][^>]*>\|<\/\?t[dh][^>]*>$//g' | sed 's/<\/t[dh][^>]*><t[dh][^>]*>/\t/g' | sed -e 's/<[^ >][^>]*>//g;s/<\/[^>]*>//g'| sed 's/^[\ \t]*//g'| grep -v "^\&" | grep -v "^\&"   | grep -v "^Filter" | sed 's/If region overlaps both UTR and CDS its annotated as UTR5\/3." class="tooltip">//g' > $YEASTDAT/processedData/rouskin14_structuredRNAs.csv
## TODO: this must currently be done MANUALLY because downloaddata.pl doesn't
## work, due to masking of _ and . in URL!
#mv GSM1297509* originalData
#mv GSE45803_* originalData
## TODO: update from sacCer2/R62 to R64!!
gunzip -c $YEASTDAT/originalData/GSM1297509_Yeast_noATP_minus.wig.gz | $GENBRO/src/wig2coors.pl  > $YEASTDAT/processedData/GSM1297509_Yeast_noATP_minus.csv
gunzip -c $YEASTDAT/originalData/GSM1297509_Yeast_noATP_plus.wig.gz  | $GENBRO/src/wig2coors.pl > $YEASTDAT/processedData/GSM1297509_Yeast_noATP_plus.csv
gunzip -c $YEASTDAT/originalData/GSE45803_Feb13_VivoAllextra_1_15_Minus.wig.gz | $GENBRO/src/wig2coors.pl  > $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_Minus.csv
gunzip -c $YEASTDAT/originalData/GSE45803_Feb13_VivoAllextra_1_15_PLUS.wig.gz | $GENBRO/src/wig2coors.pl  > $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_PLUS.csv
### liftOver from R62 to R64
## 1) convert to bed file
cat $YEASTDAT/processedData/GSM1297509_Yeast_noATP_minus.csv | sed 's/\(^[1-9]\)\t/chr0\1\t/;s/\(^[1-9][0-9]\)\t/chr\1\t/' |  awk  -v OFS='' '{printf("%s\t%d\t%d\t%d\n",$1,int($2)-1,int($2),$3);}' >  $YEASTDAT/processedData/GSM1297509_Yeast_noATP_minus_R62.bed 
cat $YEASTDAT/processedData/GSM1297509_Yeast_noATP_plus.csv | sed 's/\(^[1-9]\)\t/chr0\1\t/;s/\(^[1-9][0-9]\)\t/chr\1\t/' |  awk  -v OFS='' '{printf("%s\t%d\t%d\t%d\n",$1,int($2)-1,int($2),$3);}' >  $YEASTDAT/processedData/GSM1297509_Yeast_noATP_plus_R62.bed 
cat $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_Minus.csv | sed 's/\(^[1-9]\)\t/chr0\1\t/;s/\(^[1-9][0-9]\)\t/chr\1\t/' |  awk  -v OFS='' '{printf("%s\t%d\t%d\t%d\n",$1,int($2)-1,int($2),$3);}' >  $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_Minus_R62.bed 
cat $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_PLUS.csv | sed 's/\(^[1-9]\)\t/chr0\1\t/;s/\(^[1-9][0-9]\)\t/chr\1\t/' |  awk  -v OFS='' '{printf("%s\t%d\t%d\t%d\n",$1,int($2)-1,int($2),$3);}' >  $YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_PLUS_R62.bed
## 2) call liftOver
UCSCUTILS=~/programs/ucsc_utils/
infile=$YEASTDAT/processedData/GSM1297509_Yeast_noATP_minus_R62.bed 
outfile=`echo $infile| sed 's/R62/R64/'`
unfile=`echo $infile| sed 's/R62.bed/R62.unfile/'`
cat $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V62_2009_02_18_V64_2011_02_03_fixed.over.chain  stdout $unfile  > $outfile
infile=$YEASTDAT/processedData/GSM1297509_Yeast_noATP_plus_R62.bed 
outfile=`echo $infile| sed 's/R62/R64/'`
unfile=`echo $infile| sed 's/R62.bed/R62.unfile/'`
cat $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V62_2009_02_18_V64_2011_02_03_fixed.over.chain  stdout $unfile  > $outfile
infile=$YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_Minus_R62.bed
outfile=`echo $infile| sed 's/R62/R64/'`
unfile=`echo $infile| sed 's/R62.bed/R62.unfile/'`
cat $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V62_2009_02_18_V64_2011_02_03_fixed.over.chain  stdout $unfile  > $outfile
infile=$YEASTDAT/processedData/GSE45803_Feb13_VivoAllextra_1_15_PLUS_R62.bed
outfile=`echo $infile| sed 's/R62/R64/'`
unfile=`echo $infile| sed 's/R62.bed/R62.unfile/'`
cat $infile | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V62_2009_02_18_V64_2011_02_03_fixed.over.chain  stdout $unfile  > $outfile





## OTHER CIRCADIAN DATASETS
## further datasets:
## pituitary transcriptome: http://symposium.cshlp.org/content/72/381.abstract
## maize: http://www.biomedcentral.com/1471-2229/10/126
## rice and poplar: http://www.ncbi.nlm.nih.gov/pubmed/21694767
## heart and liver: http://www.ncbi.nlm.nih.gov/sites/entrez/11967526
## -"- : phase analysis, http://www.ncbi.nlm.nih.gov/pubmed/16504088
## aribidopsis vs. hormones: http://pcp.oxfordjournals.org/content/49/3/481.full
## 
## oscillation tests:
## Jonckheere-Terpstra (JT) http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3119870/

## Arabidopsis, Covington et al. 2008, Genome Biology
perl $TATADIR/perls/convertExcel.pl originalData/gb-2008-9-8-r130-s1.xls -fr 3 -o processedData/covington08.csv

## FROM AFFY
mkdir circadian
mkdir circadian/covington07 ## arabidopsis
cd circadian/covington07
tar xvf ../../originalData/GSE13814_RAW.tar
cd -
mkdir circadian/miller07 ## mouse skeletal muscle and liver
cd circadian/miller07
tar xvf ../../originalData/GSE3751_RAW.tar
cd -
mkdir circadian/hughes09 ## mouse liver, NIH3T3, and U2OS cells; harmonics
cd circadian/hughes09
tar xvf ../../originalData/GSE11923_RAW.tar
cd -
mkdir circadian/vollmers09 ## mouse liver, clock vs. feeding regime
cd circadian/vollmers09
tar xvf ../../originalData/GSE13093_RAW.tar
cd -


# METABOLITE DATA
# map KEGG 2 CHEBI via KEGG compound file
perl $TATADIR/perls/kegg2chebi.pl originalData/compound >! processedData/kegg2chebi.dat
# replace the non-conforming SBML ID 
cat originalData/YeastMetabolicNetwork-nocomp-1.0.xml | sed 's/extracellular region/extracellular/g' >! processedData/YeastMetabolicNetwork-nocomp-1.0.corrected.xml
cat originalData/yeast_2.01_no_compartments.xml | sed 's/extracellular region/extracellular/g' >! processedData/yeast_2.01_no_compartments.corrected.xml

# ADD speciesType names and SBO terms to species !
perl $TATADIR/perls/sbmlTypeToSpecies.pl processedData/yeast_2.01_no_compartments.corrected.xml processedData/yeast_2.01_no_compartments.named.xml
perl $TATADIR/perls/sbmlTypeToSpecies.pl processedData/YeastMetabolicNetwork-nocomp-1.0.corrected.xml processedData/YeastMetabolicNetwork-nocomp-1.0.named.xml
perl $TATADIR/perls/sbmlTypeToSpecies.pl originalData/yeast_4.02_noCompartments.xml processedData/yeast_4.02_noCompartments.named.xml

## ZID AND O'SHEA: CLUSTER D ribosomes vs stress granules
## doesnt work, table is a picture
#pdftotext originalData/zid14sup.pdf -layout -f 1 -l 1 processedData/zid14.dat
## using the txt file kindly provided by Brian Zid via Email (20150714)
cut -f  1,8,15 originalData//ZidSuppTable1.txt|grep -v Systematic > processedData/zid14.dat

## GEISBERG et al. 2014: mRNA isoform half-lives 
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=; eol=unix sheet='Sheet1'" $YEASTDAT/originalData/geisberg14_s1_halflives.xlsx $YEASTDAT/processedData/geisberg14_s1_halflives.tmp
cut -d ";" -f 2,5  $YEASTDAT/processedData/geisberg14_s1_halflives.tmp | sed 's/;/\t/' > $YEASTDAT/processedData/geisberg14_s1_halflives.csv
\rm $YEASTDAT/processedData/geisberg14_s1_halflives.tmp

ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=; eol=unix sheet='Table S2'" $YEASTDAT/originalData/geisberg14_s2_isoforms.xlsx $YEASTDAT/processedData/geisberg14_s2_isoforms.tmp
cut -d ";" -f 2,8,9,10  $YEASTDAT/processedData/geisberg14_s2_isoforms.tmp | sed 's/;/\t/g' > $YEASTDAT/processedData/geisberg14_s2_isoforms.csv
\rm $YEASTDAT/processedData/geisberg14_s2_isoforms.tmp

## CHRISTIANO et al. 2014: protein half-lives
## TODO: fix bad names from excell eg. En.d.3 for END3
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Sheet1'" $YEASTDAT/originalData/christiano14.xlsx $YEASTDAT/processedData/christiano14.tmp
cut -d "," -f 2,3,4,5,6,7,8 $YEASTDAT/processedData/christiano14.tmp | sed 's/,/\t/g;s/\"//g'> $YEASTDAT/processedData/christiano14.tmp2
R --vanilla < $GENBRO/data/yeast/processChristiano2014.R
\rm $YEASTDAT/processedData/christiano14.tmp $YEASTDAT/processedData/christiano14.tmp2

## ARAVA et al. 2003- ribosome densities
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=; eol=unix sheet='all genes'"  $YEASTDAT/originalData/arava03_ribodense.xls  $YEASTDAT/originalData/arava03_ribodense.csv
## cut header and extract # of ribosomes per mRNA!
R --vanilla <  $GENBRO/data/yeast/processArava2003.R


## RIBA et al. 2019 - ribosome densities
## density: ribosomes per codon! to get per mRNA multiple by ORF length/3
R --vanilla <  $GENBRO/data/yeast/processRiba2019.R


## PELECHANO et al. 2014 - transcription rates
R --vanilla < $GENBRO/data/yeast/processPelechano2010.R



### KUANG et al. 2014 - YMC CLASSES
## RNAseq Data 
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='RNAseq&microarray'" $YEASTDAT/originalData/nsmb.2881-S2.xlsx $YEASTDAT/processedData/kuang14_tmp.csv
## microarray
cut -d "," -f 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37  $YEASTDAT/processedData/kuang14_tmp.csv |  grep -v ",,,,,,,,," >  $YEASTDAT/processedData/kuang14_microarray.csv
## RNAseq
cut -d "," -f 1,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54  $YEASTDAT/processedData/kuang14_tmp.csv |  grep -v ",,,,,,,,," >  $YEASTDAT/processedData/kuang14_rnaseq.csv
## time points
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='16 time points'" $YEASTDAT/originalData/nsmb.2881-S2.xlsx $YEASTDAT/processedData/kuang14_times.csv
\rm processedData/kuang14_tmp.csv

## RNAseq-based re-clustering
## NOTE: this data set contains a lot of duplicates, some with inconsistent cluster assigments!
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Ox_cluster'" originalData/nsmb.2881-S2.xlsx processedData/kuang14_ox.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='RB_cluster'" originalData/nsmb.2881-S2.xlsx processedData/kuang14_rb.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='RC_cluster'" originalData/nsmb.2881-S2.xlsx processedData/kuang14_rc.csv
cat processedData/kuang14_ox.csv |grep "^Y"| sed -r 's/,([0-9]),.*/\tOX\1/g' > processedData/kuang14_cls.csv
cat processedData/kuang14_rb.csv |grep "^Y"| sed -r 's/,([0-9]),.*/\tRB\1/g' >> processedData/kuang14_cls.csv
cat processedData/kuang14_rc.csv |grep "^Y"| sed -r 's/,([0-9]),.*/\tRC\1/g' >> processedData/kuang14_cls.csv
\rm processedData/kuang14_ox.csv processedData/kuang14_rb.csv processedData/kuang14_rc.csv 
## Hclusters - based on co-clustering with histone mods
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='RNA_Hcluster'" originalData/nsmb.2881-S5.xlsx processedData/kuang14_rnahcluster_tmp.csv
cat processedData/kuang14_rnahcluster_tmp.csv |grep "^Y"| sed -r 's/,.*,([0-9])/\tH\1/g' > processedData/kuang14_rnahcluster.csv
\rm  processedData/kuang14_rnahcluster_tmp.csv
## OX Subclusters - based on co-clustering of OX genes with histone modifiers
## NOTE: some genes seem to be numbered duplicates and are not official
## yeast names; often inconsistent
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='OX growth genes'" originalData/nsmb.2881-S6.xlsx processedData/kuang14_oxclusters_tmp.csv
cut -d ',' -f 1,44 processedData/kuang14_oxclusters_tmp.csv | grep "^Y" | sed 's/,/\t/' > processedData/kuang14_oxclusters.csv
\rm processedData/kuang14_oxclusters_tmp.csv
## Histone Mods
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='histone'" originalData/nsmb.2881-S4.xlsx processedData/kuang14_histonemods.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='16 time point'" originalData/nsmb.2881-S4.xlsx processedData/kuang14_histone_times.csv

## Wahba et al. 2016: S1DRIP-seq
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='rnh1rnh201.atleast3.bed'" $YEASTDAT/originalData/wahba16_s1_rnh.xlsx $YEASTDAT/processedData/wahba16_s1_rnh.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='wildtype.atleast3.bed'" $YEASTDAT/originalData/wahba16_s2_wt.xlsx $YEASTDAT/processedData/wahba16_s2_wt.csv
## add head and replace , by tabs; use awk to increase 0-based start in bed format
header="chr\tstart\tend\tID"
echo -e $header > $YEASTDAT/processedData/wahba16_s2_wt.tsv
sed 's/,/\t/g;s/chr//;s/VIII/8/;s/VII/7/;s/VI/6/;s/IV/4/;s/V/5/;s/IX/9/;s/III/3/;s/II/2/;s/I/1/;s/XIII/13/;s/XII/12/;s/XI/11/;s/X/1/;s/M/17/' $YEASTDAT/processedData/wahba16_s2_wt.csv | awk '{$2=$2+1}1' |sed 's/ /\t/g' >> $YEASTDAT/processedData/wahba16_s2_wt.tsv
echo -e $header > $YEASTDAT/processedData/wahba16_s1_rnh.tsv
sed 's/,/\t/g;s/chr//;s/VIII/8/;s/VII/7/;s/VI/6/;s/IV/4/;s/V/5/;s/IX/9/;s/III/3/;s/II/2/;s/I/1/;s/XIII/13/;s/XII/12/;s/XI/11/;s/X/1/;s/M/17/' $YEASTDAT/processedData/wahba16_s1_rnh.csv  | awk '{$2=$2+1}1' |sed 's/ /\t/g' >> $YEASTDAT/processedData/wahba16_s1_rnh.tsv
## TODO: 0-based bed file!


## CARVUNIS etal. 2012 - YEAST PROTO-GENES
## TODO: genome version update?
## reference genome: SGD from October 2007 -> R56-1-1_20070406
# liftOver https://downloads.yeastgenome.org/sequence/S288C_reference/genome_releases/liftover/V56_2007_04_06_V64_2011_02_03.over.chain
pdftotext -f 38 -l 63 -layout $YEASTDAT/originalData/NIHMS376111-supplement-2.pdf $YEASTDAT/processedData/carvunis12_tmp.txt
## header
echo -e "ORF\tConservation level\tRetained by natural selection\tLength greater or equal to 300 nucleotides\tWith translation signatures\tchr\tstart\tend\tstrand" > $YEASTDAT/processedData/carvunis12_protogenes.txt
## sed/grep: rm ^L (\f), leading spaces, empty lines, grep smorf and Y genes
## and replace whitespaces by tabs
sed 's/^\f//g;s/^[ \t]*//g;/^$/d;s/ \+/\t/g' $YEASTDAT/processedData/carvunis12_tmp.txt | grep "^[s|Y]" |grep -v "^selection" >> $YEASTDAT/processedData/carvunis12_protogenes.txt

#;s/[:blank:]+/,/g

## WACHOLDER et al. 2023 - protgenes new data
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='translated_all'" $YEASTDAT/originalData/wacholder23_tableS3.xlsx $YEASTDAT/processedData/wacholder23_translated_all.tsv
sed -i 's/,/\t/g;s/chromosome/chr/g;s/first_coord/start/g;s/last_coord/end/g' $YEASTDAT/processedData/wacholder23_translated_all.tsv

## Yang et al. 2015: asymmetric division of proteins between mother/daughter
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='SupplementaryTable2.all-zscores'" $YEASTDAT/originalData/pnas.1506054112.sd01.xls $YEASTDAT/processedData/yang15.csv

## KUBIK  etal. 2018: Rsc8-MNase vs. free MNase
## To obtain a conservative estimate of specific Rsc8 binding,
## we calculated the ratio of read counts in peaks from an early time point
## of Rsc8-MNase cleavage (10 s) with those of a much longer (20 min) free
## MNase digest
## genome version: sacCer3 - R64 - no lift over required
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM2589930_free_MNase_20min_merged.bw $YEASTDAT/processedData/kubik18_freeMNase_20min.bed
$UCSCUTILS/bigWigToBedGraph $YEASTDAT/originalData/GSM2589931_Rsc8_10_merged.bw $YEASTDAT/processedData/kubik18_Rsc8MNase_10sec.bed

## YEN et al. 2012 : chromatin remodellers
## Top 1500 Isw2 peaks
## genome version S. cerevisiae 2007-Jan-19 -> should be R55
## TODO: clarify why only chromosomes 10-16 are present!
## NOTE: original table S3 file erroneous, only chrom 10-16 present, first
## author Kuangyu Yen sent a new version of the analysis on 2019-07-03 to
## machne@hhu.de
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Isw2 ChIP-exo'" $YEASTDAT/originalData/1-s2.0-S0092867412005922-mmc2.xls $YEASTDAT/processedData/yen12_isw2.csv
## fixed new file from 2019-07-03 sent by Kuangyu Yen
## grep only midpoint CW distance and Occupancz values
cut -f 6,11,12,13 $YEASTDAT/originalData/D_lane-1-I6_Isw2_MHS_s10e20_rmBlacklist.txt > $YEASTDAT/processedData/yen12_isw2_fixed.csv
## lift over to R64
UCSCUTILS=~/programs/ucsc_utils/
infile=$YEASTDAT/processedData/yen12_isw2_fixed.csv
outfile=`echo $infile| sed 's/.csv/_R64.tsv/'`
unfile=`echo $infile| sed 's/.csv/.unfile/'`
echo -e "chr\tcoor\tc-w reads sum\tc-w distance (bp)" > $outfile
cat $infile | awk  -v OFS='' '{printf("%s\t%d\t%d\t%s\t%s\n",$1,int($2)-1,int($2),$3,$4);}' | $UCSCUTILS/liftOver stdin $YEASTDAT/processedData/V55_2006_11_10_V64_2011_02_03_fixed.over.chain stdout $unfile | cut -f 1,3,4,5  | sed 's/^chr//;s/^0//' >> $outfile


### HSIEH et al. 2015: Micro-C domains
## TODO: release number?
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Sheet1'" $YEASTDAT/originalData/1-s2.0-S0092867415006388-mmc4.xlsx $YEASTDAT/processedData/hsieh15_CIDboundaries.csv

## TSOCHATZIDOU et al. 2017 : co-expressed domains in topoII-ts mutant
cd $YEASTDAT/processedData
unzip $YEASTDAT/originalData/gkx198_Supp.zip
cd -
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Supplementary File 1'" $YEASTDAT/processedData/nar-00166-v-2017-File007.xls $YEASTDAT/processedData/tsochatzidou17_gro.csv
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Supplementary File 2'" $YEASTDAT/processedData/nar-00166-v-2017-File007.xls $YEASTDAT/processedData/tsochatzidou17_domains.csv

## SWYGERT et al. 2019 - L-CID domain boundaries
## NOTE: sent by email from Sarah Swygert, 20190727
## NOTE: also GEO sets for Brn1 ChIP, but used directly from GEO zip files

cut -f 1,2,3,5 $YEASTDAT/originalData/Log.merged--minDist0--is4800--nt0.4--ids3200--ss800--immean.insulation.boundaries.bed > $YEASTDAT/processedData/swygert19_boundaries_Log.csv
cut -f 1,2,3,5 $YEASTDAT/originalData/Q.merged--minDist0--is4800--nt0.4--ids3200--ss800--immean.insulation.boundaries.bed > $YEASTDAT/processedData/swygert19_boundaries_Q.csv

## Zhang et al. 2019 : recombination by H2O2 stress
## note: download behind firewall - requires manual download
mkdir $YEASTDAT/processedData/zhang19
cd $YEASTDAT/processedData/zhang19
unzip $YEASTDAT/originalData/gkz027_supplemental_files.zip 
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Dataset S5'" "$YEASTDAT/processedData/zhang19/Dataset S5.xlsx" $YEASTDAT/processedData/zhang19_recombination.csv
cd -

## HULL et al. 2017 : genes with gammaH2A peaks (replication fork sites)
R --vanilla < $GENBRO/data/yeast/processHull2017.R

## MOLLER et al. 2015: eccDNA by Circ-seq
R --vanilla < $GENBRO/data/yeast/processMoller2015.R

## Shchepachev et al. 2019: RNA binding proteins found by TRAPP in Dataset EV1
## NOTE: the automated download by `downloadData.pl` does not work, do manually:
wget https://www.embopress.org/action/downloadSupplement?doi=10.15252%2Fmsb.20188689
mv downloadSupplement\?doi\=10.15252%2Fmsb.20188689 $YEASTDAT/originalData/shchepachev19.zip
unzip -p $YEASTDAT/originalData/shchepachev19.zip msb188689-sup-0003-datasetev1.xlsx > $YEASTDAT/processedData/shchepachev19_EV1.xlsx
## map to gene IDs
R --vanilla < $GENBRO/data/yeast/processShchepachev19_EV1.R

## SGD GENOME RELEASE R64-2-1 : get new names
cd $YEASTDAT/originalData
tar -zxvf $YEASTDAT/originalData/S288C_reference_genome_R64-2-1_20150113.tgz
#cut -f 2,3 S288C_reference_genome_R64-2-1_20150113/gene_association_R64-2-1_20150113.sgd |sort -u  -k1,1  |grep -v "^!" > $YEASTDAT/processedData/genenames_R64-2-1.csv


## 20220901 - unpack Xia et al. 2022 supplement
unzip -p $YEASTDAT/originalData/41467_2022_30513_MOESM8_ESM.zip "NCOMMS-21-15807B_supp-soft/Code_01-Proteome data processing/TotalProteinMeasured.csv" > $YEASTDAT/processedData/xia22_TotalProteinMeasured.csv

## klevecz04
unzip -p $YEASTDAT/originalData/Klevecz_2004_PMID_14734811.zip > $YEASTDAT/originalData/Klevecz_2004_PMID_14734811.tsv

## CYCLEBASE3 YEAST CDC DATA
tar xvf $YEASTDAT/originalData/cerevisiae_periodic.tar


## MACHNE et al. 2012 - own data
## NOTE: old script, perhaps consider aligning info with processMachne2022.pre.R
sed 's/,/\t/g;s/"//g' originalData/tuliCoarse.results.csv > processedData/machne12_timecourses.csv
R --vanilla < $GENBRO/data/yeast/processMachne2012.R

## 20241119 - ADDING Machne et al. 2022pre v4
## NOTE: file integrated into feature table
R --vanilla < $GENBRO/data/yeast/processMachne2022pre.R

## ADDING VARIOUS GENE MEASURES (size, groth rate correlations, stress, etc.)
## NOTE: some scripts involves actual calculations (indicated below)
## So these are strictly speaking not just preprocessing of data!!

R --vanilla < $GENBRO/data/yeast/processGasch2000.R   # PCA OF DATA
R --vanilla < $GENBRO/data/yeast/processWu2010.R
R --vanilla < $GENBRO/data/yeast/processBrauer2008.R # re-calc of UGRR slopes
R --vanilla < $GENBRO/data/yeast/processODuibhir2014.R
R --vanilla < $GENBRO/data/yeast/processJonas2018.R
R --vanilla < $GENBRO/data/yeast/processSwaffer2021.R

R --vanilla < $GENBRO/data/yeast/processVanTreeck18.R ## TODO!

R --vanilla < $GENBRO/data/yeast/processRossi2021.R # promoter classes

## Bogumil et al. 2014 (replacing previous file supplied in 2012 by Tal Dagan)
unzip -p $YEASTDAT/originalData/mst212_Supplementary_Data.zip BogumilEtal-SuppTables.xls > $YEASTDAT/originalData/bogumil14_sdat.xls
## extract relevant data: archaeal/bacterial classification
ssconvert --export-type=Gnumeric_stf:stf_assistant -O "locale=C format=automatic separator=, eol=unix sheet='Table S2'" $YEASTDAT/originalData/bogumil14_sdat.xls $YEASTDAT/processedData/bogumil14_stable2.csv
cut -d ',' -f 1,2 $YEASTDAT/processedData/bogumil14_stable2.csv | sed 's/,/\t/' | tail -n +5 > $YEASTDAT/processedData/bogumil14_origins.tsv

### OWN DATA :

# move Tu et al. .CEL files
mkdir tuliData
mkdir tuliData/tu05data
# TODO: symbolic link instead of copying - avoid moving as it will be re-downloaded in download script
cp originalData/BT*.CEL tuliData/tu05data

\mv tuliData/tu05data/BT1.CEL tuliData/tu05data/BT01.CEL
\mv tuliData/tu05data/BT2.CEL tuliData/tu05data/BT02.CEL
\mv tuliData/tu05data/BT3.CEL tuliData/tu05data/BT03.CEL
\mv tuliData/tu05data/BT4.CEL tuliData/tu05data/BT04.CEL
\mv tuliData/tu05data/BT5.CEL tuliData/tu05data/BT05.CEL
\mv tuliData/tu05data/BT6.CEL tuliData/tu05data/BT06.CEL
\mv tuliData/tu05data/BT7.CEL tuliData/tu05data/BT07.CEL
\mv tuliData/tu05data/BT8.CEL tuliData/tu05data/BT08.CEL
\mv tuliData/tu05data/BT9.CEL tuliData/tu05data/BT09.CEL


R --vanilla < $TATADIR/yeast/scripts/extractTuOxygenTrace.R 

## 'raw' data from NCBI GEO
perl $TATADIR/perls/extractGeo.pl originalData/GSE3431_series_matrix.txt > processedData/tu05.raw.csv
cp processedData/tu05.raw.csv tuliData/tu05data

# TODO : add tu05/li06_time.xls to TBI downloaddata 
perl $TATADIR/perls/convertExcel.pl tuliData/li06_time.xls -c 1,2,5 
echo "time.h\tDOT\tsamples" > tuliData/li06tmp.csv
cat tuliData/li06_time.csv >> tuliData/li06tmp.csv
\mv  tuliData/li06tmp.csv tuliData/li06_time.csv
perl $TATADIR/yeast/scripts/extractLiOxygenTrace.pl tuliData/li06_time.csv > tuliData/li06_oxygen.csv


cd tuliData/
tar -xvf ../originalData/li06data.tar
cd -
ls tuliData/li06data/ > tuliData/li06_samples.csv

# COVINGTON et al. 2007: ARABIDOPSIS CIRCADIAN TILING ARRAY

# SHOULD BE PRESENT IN ORIGINAL DATA:
# originalData/comparative_analyses_334.csv
#scp raim@172.31.0.217:/home/dougie/Documents/nma/Yeast/comparative_analyses_334.csv originalData

#cat originalData/comparative_analyses_334.csv | sed 's/","/\t/g' | sed 's/^"//g' | sed 's/"$//g' > processedData/tuli.all.csv


#scp raim@172.31.0.217:tuli.snr.csv originalData/

# open in ooffice, delete description column and save as processedData/tuli.snr.tab.csv

#cat  processedData/tuli.snr.tab.csv | sed 's/,/\t/g'  | sed 's/"//g' > processedData/tuli.snr.tab

#$TATADIR/perls/getColumn.pl -v ID,Tu_basic_angle,Tu_basic_amplitude,Tu_basic_expression,Tu_basic_snr,Tu_basic_snr_pvals,Li_basic_angle,Li_basic_amplitude,Li_basic_expression,Li_basic_snr,Li_basic_snr_pvals,cluster_numbers_5 processedData/tuli.snr.tab -l > processedData/tuli.snr.csv

cp originalData/tuli.snr.csv processedData/tuli.snr.csv


#cat tuliData/SNR.csv | sed 's/,/\t/g' | cut -f 2,11,12,14,15,21,22,24,25,32,33,35,36,42,43,45,46 > tuliData/SNR.tab
#cat tuliData/SNR.new.csv | sed 's/"//g' | cut -f 2,11,12,14,15,21,22,24,25,32,33,35,36,42,43,45,46 >  tuliData/SNR.new.tab
#
#cat tuliData/snr.csv | sed 's/"//g'  >  tuliData/snr.csv.tmp
#
#perl $TATADIR/perls/getColumn.pl -v ID,Kl_CC,Kl_A_angle,Kl_A_amplitude,Kl_A_expression,Kl_A_SNR,Kl_A_pvals,Kl_B_angle,Kl_B_amplitude,Kl_B_expression,Kl_B_SNR,Kl_B_pvals,Kl_M_angle,Kl_M_amplitude,Kl_M_expression,Kl_M_SNR,Kl_M_pvals,Kl_E_angle,Kl_E_amplitude,Kl_E_expression,Kl_E_SNR,Kl_E_pvals,Tu_CC,Tu_A_angle,Tu_A_amplitude,Tu_A_expression,Tu_A_SNR,Tu_A_pvals,Tu_B_angle,Tu_B_amplitude,Tu_B_expression,Tu_B_SNR,Tu_B_pvals,Tu_M_angle,Tu_M_amplitude,Tu_M_expression,Tu_M_SNR,Tu_M_pvals,Tu_E_angle,Tu_E_amplitude,Tu_E_expression,Tu_E_SNR,Tu_E_pvals,Li_CC,Li_A_angle,Li_A_amplitude,Li_A_expression,Li_A_SNR,Li_A_pvals,Li_B_angle,Li_B_amplitude,Li_B_expression,Li_B_SNR,Li_B_pvals,Li_M_angle,Li_M_amplitude,Li_M_expression,Li_M_SNR,Li_M_pvals,Li_E_angle,Li_E_amplitude,Li_E_expression,Li_E_SNR,Li_E_pvals tuliData/snr.csv.tmp -l -s , > tuliData/snr.new.csv

#perl $TATADIR/yeast/scripts/filterTuli.pl tuliData/SNR.tab  > tuliData/SNR.filtered.tab


# ADP/ATP DATA
perl $TATADIR/perls/convertExcel.pl "originalData/ADP ATP ratio DM_B_12.xls" -s 2 -o processedData/murray.adp.atp.csv

perl $TATADIR/perls/convertExcel.pl "originalData/ADP ATP ratio DM_B_12.xls" -s 1 -o processedData/murray.adp.atp.o2.csv

## manually process processedData/murray.adp.atp.o2.csv to remove R-incompatible letters!



# End of file

