

mkdir $YEASTDAT/figures/heatmaps/
mkdir $YEASTDAT/figures/heatmaps/tu05
mkdir $YEASTDAT/figures/heatmaps/li06
mkdir $YEASTDAT/figures/heatmaps/machne22pre

sed 's/^ORDER.*/ORDER="phase_tu05"/' $GENBRO/data/yeast/clusterTranscriptomes.R | R --vanilla -


find $YEASTDAT/figures/transcriptomes/ -name '*heatmap.png' 
find $YEASTDAT/figures/transcriptomes/ -name '*heatmap_*tight.png' 
cp -a $YEASTDAT/figures/transcriptomes/
