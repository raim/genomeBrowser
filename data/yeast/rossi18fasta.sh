#!/bin/bash


## UNPUBLISHED CONSENSUS NUCLEOSOME SET FROM Matthew Rossi/Frank Pugh
## sent by email on 20180219

## TODO: find this file again on HHU exon!

## extract fasta sequences for nucleosomes ...
## TODO: ... and calculate rotational positioning


OUTDIR=YEASTDAT/processedData/rossi18_nucleosomes
mkdir $OUTDIR
rm $OUTDIR/rossi18.fasta
touch $OUTDIR/rossi18.fasta


## extract nucleosome coordinates and convert to input format for samtool
for coor in `cut -f 1,2,3 $YEASTDAT/unpublished/rossi18/All_Nucleosome_sacCer3.bed | sed 's/chr16/chrXVI/;s/chr15/chrXV/;s/chr14/chrXIV/;s/chr13/chrXIII/;s/chr12/chrXII/;s/chr11/chrXI/;s/chr10/chrX/;s/chr9/chrIX/;s/chr8/chrVIII/;s/chr7/chrVII/;s/chr6/chrVI/;s/chr5/chrV/;s/chr4/chrIV/;s/chr3/chrIII/;s/chr2/chrII/;s/chr1/chrI/' |awk '{printf("%s:%s-%s\n",$1,$2,$3);}'`; do
    echo extracting $coor;
    ## use samtools to extract nucleotide sequences, rm newlines with
    ## awk and send directly into WS scheme script (Cui et al.)
    samtools faidx /data/yeast/chromosomes/sequenceIndex_R64-1-1_20110208.fasta $coor | awk '!/^>/ { printf "%s", $0; n = "\n" } /^>/ { print n $0; n = "" } END { printf "%s", n } ' >> $OUTDIR/rossi18.fasta #| 
done

## predict rotational positioning starting from dyad
## using Feng Cui's script
## TODO: understand how the output of this script is interpreted
## and mapped to all positions
for seq in `grep ">" $OUTDIR/rossi18.fasta | sed 's/>//'`; do
    echo $seq
    #samtools faidx  $OUTDIR/rossi18.fasta $seq | awk '!/^>/ { printf "%s", $0; n = "\n" } /^>/ { print n $0; n = "" } END { printf "%s", n } '| perl $GENBRO/src/WS_scheme_average.pl -  $OUTDIR/rossi18_rotation_${seq}.txt
done
