#!/bin/bash

## NOTE: this is a yet INCOMPLETE move of data download and preprocessing
## from the CVS repo tataProject at the TBI in Vienna;

## TODO: move here completely; perhaps clean unused; main setup
## in tataProject happens in generateGenomeBase.sh and doAll.sh!!
## more recent data processing happens from the github repo yeastSeq2016

## 20180205: moved feature generation from yeastSeq2016/scripts/processSeq2016
## to here; TODO: mv perl functions

## TODO 20190619 - FIX MISSING 2-micron in chromsizes, leads to empty
## chromosome column in feature files!!!

## ENVIRONMENT VARIABLES REQUIRED
## $YEASTDAT : path to yeast data, with folders originalData and processedData
##             and genome feature files; used as input for genomeBrowser data
##             generation
## $GENBRO : genomeBrowser location
## $GENDAT : genomeBrowser data location
## $TATADIR: tataProject csv repo; to be moved to genomeBrowser

cd $YEASTDAT
mkdir originalData
mkdir processedData
mkdir parameters  # new 20241120, copying files from genbro git to data dir
mkdir figures  # new 20241120, new output dir for processing and analysis

## feature file generation
fas=$YEASTDAT/chromosomes/sequenceIndex_R64-1-1_20110208.fasta
cif=$YEASTDAT/chromosomes/sequenceIndex_R64-1-1_20110208.csv
gff=$YEASTDAT/originalData/saccharomyces_cerevisiae_R64-1-1_20110208.gff
ffb=$YEASTDAT/feature_R64-1-1_20110208.csv
ffl=$YEASTDAT/feature_R64-1-1_20110208_allData.csv
nffl=$YEASTDAT/feature_R64-1-1_20110208_v20241119.csv
## release R64-2-1 to map more recent data sets
ngff=$YEASTDAT/originalData/S288C_reference_genome_R64-2-1_20150113/saccharomyces_cerevisiae_R64-2-1_20150113.gff
nffb=$YEASTDAT/feature_R64-2-1_20150113.csv

# COPY PARAMETERS TO DATA FOLDER (20241120)
## NOTE: w/o option -a to record time of copying!
cp $GENBRO/data/yeast/parameters/clusters.tsv $YEASTDAT/parameters/


# DOWNLOAD DATA

perl $GENBRO/src/downloadData.pl -o $YEASTDAT/originalData/ $GENBRO/data/yeast/parameters/downloaddata.csv -l $YEASTDAT/originalData/download.log 
grep -i ERROR $YEASTDAT/originalData/download.log
echo 'IF ERRORS OCCURED PLEASE CHECK LOG FILE originalData/download.log'
echo 'GET URL, AND DOWNLOAD FILES MANUALLY, PLEASE!'


# PREPROCESS DATA 
# all preprocessing steps, using external tools, or extra scripts
# should be called from preprocess.sh

$GENBRO/data/yeast/preprocess.sh >& $YEASTDAT/processedData/preprocess.log
grep -i ERROR $YEASTDAT/processedData/preprocess.log
echo 'IF ERRORS OCCURED PLEASE CHECK LOG FILE processedData/preprocess.log'
echo 'AND CORRECT ERRORS OR SCRIPTS MANUALLY, PLEASE'

## CHROMOSOME indexxs
$GENBRO/src/chromsize2index.R --inf $YEASTDAT/originalData/Yeast_S288C.chromsizes --out $cif --yeast

## TODO: R64-2-1 - has no more chromsizes file

## FASTA from gff file
start=`grep -n -m 1 "^>"  $gff  | sed 's/:.*//g'`
end=`wc -l $gff | sed 's/\s.*//g'`
sed -n "$start, $end p" $gff > $fas

## index with samtools
samtools faidx /data/yeast/chromosomes/sequenceIndex_R64-1-1_20110208.fasta


## TODO: regenerate featuremotifs.sh and other data from tataProject
## here!!

#e) GENES: convert gff file to tataProject format (feature.csv)

# generates file ffb: $YEASTDAT/feature_R64-1-1_20110208.csv
perl $GENBRO/data/yeast/extractYeastFeatures.pl -i $gff -c $cif -o $ffb

## release R64-2-1
perl $GENBRO/data/yeast/extractYeastFeatures.pl -i $ngff -c $cif -o $nffb

## add data collection FROM TATAPROJECT
## TODO: recreate or store somewhere available!
cd $YEASTDAT
perl $GENBRO/src/mergeFeatures.pl -i $GENBRO/data/yeast/parameters/R64-1-1_20110208_features.txt -p $YEASTDAT -o $ffl
## 20241119: NEW REDUCED FEATURE FILE
## including RNA-seq (Dataset_S3_v4.tsv 10.1101/2021.05.26.444658)
perl $GENBRO/src/mergeFeatures.pl -i $GENBRO/data/yeast/parameters/R64-1-1_20110208_features_20241119.txt -p $YEASTDAT -o $nffl

#
## adding cluster colors as in genomeBrowser!
## used in segment analysis; generates file
## $YEASTDAT/feature_R64-1-1_20110208_withclusters.csv
## TODO: add colors to features.txt?
## TODO: this is redundant with annotation.R ?
R --vanilla < $GENBRO/data/yeast/generateFeature_R64.R

## feature file - clusters only
## file $YEASTDAT/feature_R64-1-1_20110208_clusters.csv
## TODO: redundant with above file? is this used anywhere?
perl $GENBRO/src/getColumn.pl -v ID,name,name.R64.2.1,type,CL_rdx $ffl |grep gene > $YEASTDAT/feature_R64-1-1_20110208_clusters.csv

## generate LTR-retrotransposon, LTR and and solo-LTR files
R --vanilla < $GENBRO/data/yeast/extractLTR.R > $YEASTDAT/feature_R64-1-1_LTR.log
R --vanilla < $GENBRO/data/yeast/extractTE.R > $YEASTDAT/feature_R64-1-1_TE.log
## calculate overlap LTR/TE to classify free-standing LTR
~/programs/segmenTools/scripts/segmentAnnotate.R -t $YEASTDAT/feature_R64-1-1_LTR.tsv --tcol ID,chr,strand,start,end,LTR.type --prefix TE  -q $YEASTDAT/feature_R64-1-1_TE.tsv --qcol ID,TE.type --chrfile $YEASTDAT/chromosomes/sequenceIndex_R64-1-1_20110208.csv -o $YEASTDAT/feature_R64-1-1_LTR_annotated.tsv --each.target
## extract solo LTR:
grep -vP "Ty[1-5]" $YEASTDAT/feature_R64-1-1_LTR_annotated.tsv | cut -f 1,2,3,4,5,6 > $YEASTDAT/feature_R64-1-1_LTR_solo.tsv


## ANNOTATING TRANSCRIPT DATA SET WITH CLUSTERS & CLUSTERCOLORS
## 20180302: only.best required to reproduce old version based on ncRNA_r64.csv
##           also: once LESS hit YIL154C vs. ST5258
~/programs/segmenTools/scripts/segmentAnnotate.R -q $YEASTDAT/feature_R64-1-1_20110208_withclusters.csv --qtypes gene --qcol ID,CL_rdx,CL_rdx_col --prefix gene -t $YEASTDAT/processedData/transcripts_r64.csv --ttypes ORF --chrfile $YEASTDAT/chromosomes/sequenceIndex_R64-1-1_20110208.csv -o $YEASTDAT/processedData/cltranscripts.csv --only.best
cd -


### 202411, using new feature table

## mini meta-analysis of data in the feature table
R --vanilla < $GENBRO/data/yeast/clusterClusters.R
R --vanilla < $GENBRO/data/yeast/kubik2019.R

## reproducing the cell size ~ transcript correlations
## as by @Swaffer2021
R --vanilla < $GENBRO/data/yeast/reproduceSwaffer2021.R

## analyze ONeill et al. 2020 proteome data, sent by email
## on 20241117 by John O'Neill
R --vanilla < $GENBRO/data/yeast/processONeill2020.R

## analyze Rossi et al. 2021 promoter classes
R --vanilla < $GENBRO/data/yeast/processRossi2021.R

## TODO: integrate archaea, AA composition and promoter classes
## into one figure.

## @Jackson2020 data: get means to treat as bulk data
## TODO: redundant with call in ../mammary/scrnaseq_analyses.sh
## EITHER split bulk vs. single cell file writing, or fuse here!
R --vanilla < $YSRC/processJackson2020.R

## Run stand-alone analysis script for Jackson et al. 2020, scRNAseq data
## TODO: align with clusterTranscriptomes.R and the new PCA-based
## analysis in ../mammary/cohorts_yeast.R
R --vanilla <  $GENBRO/data/yeast/jackson20.R

## TODO: run clusterTranscriptome series from here!
R --vanilla <  $GENBRO/data/yeast/clusterTranscriptomes.R
