

See README.md files in main directory and in data/pcc6803, and src/example\_yeast.R for details.


``` R
browser.path <- sub("GENBRO=","",system("env|grep GENBRO",intern=TRUE))
data.path <- sub("GENDAT=","",system("env|grep GENDAT",intern=TRUE))
source(file.path(browser.path,"src/genomeBrowser_utils.R"))
source(file.path(browser.path,"src/genomeBrowser.R"))
data.path <- file.path(data.path,"yeast")
chrS <- getChrS(data.path=data.path)
dataSets <- loadData(c("transcripts","annotation","nucleosomes_lee07"))
browseFeatures(features=c("MEP1","GDH3","SIR2"),type="gene")
```
