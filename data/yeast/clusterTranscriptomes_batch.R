
## SCRIPT TO BE CALLED FROM clusterTranscriptomes.R

## TODO: continue to untangle  clusterTranscriptomes.R

## @Slavov2014: BATCH GROWTH TIME SERIES
## TODO: add invidual data files here?
slavov14_figS2b <- file.path(chemo.data,"slavov14_figS2B.csv")


### SLAVOV et al. 2014 - CONSTANT GROWTH IN BATCH WITH INCREASING GLYCOLYSIS

## FROM README sheet in the original supplement

## The transcript levels for each time point are reported as log2
## ratios of the level in that condition relative to the average level
## of all time points.
## The protein levels are reported as normalized intensities relative
## to the mean of all time points.
## For the unmodified proteins, the fold changes are estimated as the
## median of the fold changes of their unique peptides.
## Peptides with PTMs are reported in the corresponding data sheets
## with the protein ID and peptide sequences


## NOTE: stored data has time t=0 at diauxic shift, here original
## time is recovered

## NOTE 368 clustered genes missing from slavov14 transcript data 
## and 191 sl14 transcripts missing from clusters

##fig.path <- file.path(out.path,"figures","diauxie") 
##dir.create(fig.path,recursive=TRUE)

## load cells/mL to show growth curves
## CELL NUMBERS ETC. digitized from Figure S2b
cells <- read.csv(slavov14_figS2b)

## million cells
cells[,2] <- cells[,2]/1e6

sets <- c("transcripts","proteins")#,"phosphorylated","acetylated","methylated")
dats <- c("phase1","phase2")

## TODO: compare transcript vs. protein levels at each time-bound
## find a transition to translational regulation??
for ( set in sets ) {

    ## read and process data
    for ( dat in dats ) {
        tmp <- paste("slavov14_",dat,"_",set,".dat",sep="")
        tmp <- file.path(yeast.path,"processedData",tmp)
        tmp <- read.delim(tmp,header=TRUE)
        ## data columns
        datcols <- grep("h$",colnames(tmp)) 
        ## average of duplicated
        ## TODO: just take first of duplicated? 
        dupls <- duplicated(tmp[,1])
        rms <- NULL
        for ( dupl in which(dupls) ) {
            idx <- which(tmp[,1] == tmp[dupl,1])
            tmp[idx[1],datcols] <- colMeans(tmp[idx,datcols])
            rms <- c(rms,idx[-1])
        }
        if ( !is.null(rms) ) tmp <- tmp[-rms,]
        ## assign rownames and rm all know data columns
        rownames(tmp) <- tmp[,1]
        tmp <- tmp[,datcols]
        assign(dat, tmp)
    }
    
    ## ranges
    ph1 <- 1:ncol(phase1)
    ph2 <- 1:ncol(phase2) + ncol(phase1)
    
    ## map to clustering
    sl14 <- cbind(phase1[names(coarse.cls),],phase2[names(coarse.cls),])
    sl14 <- sl14[,colnames(sl14)!="X"]
    rownames(sl14) <- names(coarse.cls)
    ## sample times in h
    sl14.times <- as.numeric(gsub("X","",gsub("\\.h","",colnames(sl14))))

    tme <- sl14.times

    for ( dat in dats ) {

        if ( dat=="phase1" ) {
            xlm <- range(tme[ph1])
            phr <- 1:ncol(phase1)
        } else {
            xlm <- range(tme[ph2])
            phr <- 1:ncol(phase2) + ncol(phase1)
        }
        xlm <- xlm + c(-.5,.5)
        
        ## phase-sorted heatmap
        file.base <- file.path(batch.path, paste0("slavov14_",set,"_",dat))
        plotHeatmap(data=sl14[,phr], cset=cset, times=tme[phr],
                    file.name=file.base, cut.time=FALSE,
                    id=paste0("slavov14_",set))

        ## CUSTOM PLOTS for paper
        file.name <- file.path(batch.path, paste0("slavov14_",set,"_",dat))
        plotdev(file.name, type=fig.type,
                width=W, height=H, res=200)
        par(mai=c(.5,.5,.1,.25),mgp=c(1.2,.2,0), tcl=-.25)
        plotClusters(sl14, xlim=xlm,
                     avg="median", q=.5, avg.pch=19, avg.cex=.75,
                     time=tme, cls=coarse.cls,
                     cls.col=coarse.col, cls.srt=coarse.srt.major,
                     each=FALSE,
                     xlab=NA,
                     ylab=paste0(set,", normalized"), axes=FALSE)
        mtext("batch culture time/h",1, 1.2, adj=0.1)
        axis(2)
        axis(1, at=1:100, tcl=-.125, labels=FALSE)
        axis(1, at=seq(0,100,2))
        axis(3, at=1:100, tcl=-.125, labels=FALSE)
        axis(3, at=seq(0,100,2), labels=FALSE)
        text(xlm[2]*1.02, -.975, expression(cells/mL), srt=45, xpd=TRUE, cex=.7)
        par(new=TRUE)
        plot(1,col=NA,xlab=NA,ylab=NA,axes=FALSE,ylim=c(0,1),xlim=c(0,1))
        if ( dat=="phase1" )
            text(-.05,c(.125,0.025),
                 label=c("YNB+Glu",expression(mu == 0.4~h^-1)),pos=4)
        par(new=TRUE)
        par(mgp=c(1.3,.2,0), tcl=-.25)
        plot(cells[,1], cells[,2]*1e6, log="y",
         xlim=xlm, axes=FALSE, ylab=NA, xlab=NA, type="l")
        axis(4, at=10^c(1:10), cex.axis=.7)
        xat <- rep(0:10,each=10) * 10^c(1:10)
        axis(4, at=xat, tcl=-.125, labels=FALSE)
        box()
        dev.off()
    }
    
    xlm <- range(tme)
    file.name <- file.path(batch.path, paste0("slavov14_",set, "_full"))
    plotdev(file.name, type=fig.type, width=2*W, height=H, res=200)
    par(mai=c(.5,.5,.1,.25),mgp=c(1.3,.3,0), tcl=-.25)
    plotClusters(sl14, xlim=xlm,
                 avg="median", q=.5, avg.pch=19, avg.cex=.75,
                 time=tme, cls=coarse.cls,
                 cls.col=coarse.col, cls.srt=coarse.srt.major, each=FALSE,
                 xlab="batch culture time/h",
                 ylab=paste0(set,", normalized"), axes=FALSE)
    axis(2)
    axis(1, at=1:100, tcl=-.125, labels=FALSE)
    axis(1, at=seq(0,100,2))
    axis(3, at=1:100, tcl=-.125, labels=FALSE)
    axis(3, at=seq(0,100,2), labels=FALSE)
    text(xlm[2]*1.03, par("usr")[3]-diff(par("usr")[3:4])/10,
         expression(cells/mL), srt=45, xpd=TRUE, cex=.7)
    par(new=TRUE)
    par(mgp=c(1.3,.2,0), tcl=-.25)
    plot(cells[,1], cells[,2]*1e6, log="y",
         xlim=xlm, axes=FALSE, ylab=NA, xlab=NA, type="l")
    axis(4, at=10^c(1:10), cex.axis=.7)
    xat <- rep(0:10,each=10) * 10^c(1:10)
    axis(4, at=xat, tcl=-.125, labels=FALSE)
    box()
    dev.off()

}
