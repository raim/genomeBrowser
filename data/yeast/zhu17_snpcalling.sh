#!/bin/bash


## CLINICAL YEAST ISOLATES, Zhu et al. PLoS Genetics 2017 
## get all BAM files from SRA, project PRJNA315044

SRATOOLKIT=~/programs/sratoolkit.2.8.2-1-centos_linux64/bin
EDIRECT=~/programs/edirect

fasta=$YEASTDAT/chromosomes/sequenceIndex_R64-1-1_20110208.fasta
srrs=$YEASTDAT/processedData/zhu17_sraids.txt 

## get all SRR (NCBI sequence read archive) files associated the bioproject
## see https://www.biostars.org/p/93494/ , https://www.biostars.org/p/111040/
$EDIRECT/esearch -db sra -query PRJNA315044  | $EDIRECT/efetch --format runinfo | cut -d ',' -f 1 | grep SRR > $YEASTDAT/processedData/zhu17_sraids.txt 

## SNP CALLING FROM SAM FILES
## after https://www.ebi.ac.uk/sites/ebi.ac.uk/files/content.ebi.ac.uk/materials/2014/140217_AgriOmics/dan_bolser_snp_calling.pdf

## directly get SAM files (alignments), convert to and sort BAM:
srr=SRR3265467
$SRATOOLKIT/sam-dump $srr | samtools view -S -b - | samtools sort - ${srr}_sorted

### CONVERT SRR FILES
## 1) Convert SAM to BAM for sorting
samtools view -S -b my.sam > my.bam
## 2) Sort BAM for SNP calling
samtools sort my.bam my-sorted

### CALL SNPs (using SAMtools)
## 1) Index the genome assembly (again!)
samtools faidx $fasta
## 2) Run 'mpileup' to generate VCF format
samtools mpileup -g -f $fasta my-sorted-1.bam my-sorted-2.bam my-sorted-n.bam >  my-raw.bcf

### CALL SNPs (using bcftools)
## 3) Call SNPs...
bcftools view -bvcg my-raw.bcf > my-var.bcf
## 4) ? again - 
samtools mpileup
## 5) ? again
bcftools view

## FILTER SNPs
bcftools view my.var.bcf | vcfutils.pl varFilter - >  my.var-final.vcf