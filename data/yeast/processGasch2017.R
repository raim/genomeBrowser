
options(stringsAsFactors=FALSE)

## local yeast data
data.path <- Sys.getenv("MYDATA")
yeast.path <- file.path(data.path,"yeast")

data.file <- file.path(yeast.path,"originalData","gasch17.txt")

out.path <- file.path(yeast.path,"processedData","scRNAseq")
dir.create(out.path, showWarnings=FALSE)

## load GASCH et al. 2017 data
gsh <- read.delim(data.file, row.names=1)

## simplify cell names
colnames(gsh) <- sub("BY4741_","",
                     sub("tressed_.*_BY","tressed_", colnames(gsh)))
condition <- sub("_.*","",colnames(gsh))

## write out each condition separately
for ( cond in c("Stressed","Unstressed") ) {
    gz1 <- gzfile(file.path(out.path,paste0("gasch17_",cond,".csv.gz")), "w")
    write.csv(cbind.data.frame(ID=rownames(gsh),
                               gsh[, condition==cond]),
              file=gz1, quote=FALSE, row.names=FALSE)
    close(gz1)
}
