#!/usr/bin/bash

R --vanilla < $GENBRO/data/yeast/osciSeq_dpseg.R &> $GENDAT/yeast/osciSeq_dpseg.log
R --vanilla < $GENBRO/data/yeast/osciSeq_dpseg_primseg.R &> $GENDAT/yeast/osciSeq_dpseg_primseg.log
