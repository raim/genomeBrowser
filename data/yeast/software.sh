
### REQUIRED SOFTWARE: DL & INSTALL
## TODO: make this official and add required command line tools

## UCSC liftOver
mkdir ~/programs/ucsc_utils
cd ~/programs/ucsc_utils/
wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/liftOver
chmod a+x liftOver
wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/bigWigToBedGraph
chmod a+x bigWigToBedGraph
cd -
UCSCUTILS=~/programs/ucsc_utils/

## SAMTOOLS
cd ~/programs
git clone git@github.com:samtools/htslib.git
git clone git@github.com:samtools/samtools.git
cd ~/programs/htslib
autoheader  
autoconf
./configure           
make
sudo make install
cd ~/programs/samtools
autoheader  
autoconf -Wno-syntax  
./configure           
make
make install

## NCBI SRA TOOLKIT
cd programs
wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-centos_linux64.tar.gz
tar zxvf sratoolkit.current-centos_linux64.tar.gz
SRATOOLKIT=~/programs/sratoolkit.2.8.2-1-centos_linux64/bin


## NCBI E-UTILS
cd ~/programs/
wget ftp.ncbi.nlm.nih.gov/entrez/entrezdirect/edirect.tar.gz
tar zxvf edirect.tar.gz 
~/programs/edirect/setup.sh
EDIRECT=~/programs/edirect


## R/BIONCONDUCTOR
## for liftOver
echo "install.packages('viridisLite',repos='http://cran.us.r-project.org');install.packages('viridis',repos='http://cran.us.r-project.org');source('http://bioconductor.org/workflows.R');workflowInstall('liftOver')" | sudo R --vanilla
