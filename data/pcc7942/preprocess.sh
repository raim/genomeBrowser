#!/bin/bash

## pre-process pcc7942 data 

### FEATURE DATA
## convert GENBANK FILES TO GFF TO genomeBrowser TAB
# 1 generate gff files
declare -a RefSeqID=("NC_007604.1" "NC_007595.1" "NC_004990.1")
\rm $PCC7942DAT/processedData/pcc7942.gff
touch $PCC7942DAT/processedData/pcc7942.gff
for  id in "${RefSeqID[@]}"; do
    echo converting $id;
    $GENBRO/src/genbank2gff.pl -semicol $PCC7942DAT/originalData/${id}.gbk >> $PCC7942DAT/processedData/pcc7942.gff
done

# 2 - TODO: generate feature.csv
# genomeBrowser format; or do that in annotation.R ?

### SEQUENCE DATA
## extract FASTA from GENBANK FILES
## renames sequences from RefSeq to human readable
\rm $PCC7942DAT/chromosomes/pcc7942.fasta
touch $PCC7942DAT/chromosomes/pcc7942.fasta
for  id in "${RefSeqID[@]}"; do
    echo extracting fasta from $id;
    $GENBRO/src/genbank2fasta.pl $PCC7942DAT/originalData/${id}.gbk | sed 's/NC_007604/genome/;s/NC_007595/plasmid_1/;s/NC_004990/plasmid_2/' >> $PCC7942DAT/chromosomes/pcc7942.fasta
done

# extract chromosome index file
$GENBRO/src/fastaindex.R $PCC7942DAT/chromosomes/pcc7942.fasta > $PCC7942DAT/chromosomes/pcc7942.csv

## Vijayan, Jain, O'Shea 2011
## extract transcripts from excel - sheet 1 ("Table S1")
ssconvert $PCC7942DAT/originalData/13059_2011_2547_MOESM1_ESM.XLSX $PCC7942DAT/processedData/vijayan11_transcripts.csv
## extract corrected ORFs from sheet "Table S4"
## TODO: how to extract specific sheet?
ssconvert $PCC7942DAT/originalData/13059_2011_2547_MOESM1_ESM.XLSX  -S $PCC7942DAT/processedData/vijayan11_correctedORF.csv
\rm vijayan11_correctedORF.csv.0 vijayan11_correctedORF.csv.1 vijayan11_correctedORF.csv.2 vijayan11_correctedORF.csv.4
mv vijayan11_correctedORF.csv.3 vijayan11_correctedORF.csv

## circadian time-series clustering
## produces an RData file with segmenTier clustering and timeseries
## objects to be loaded into annotation.R for coloring!
mkdir $PCC7942DAT/vijayan09
cd $PCC7942DAT/vijayan09
R --vanilla < $GENBRO/data/publications/vijayan09/vijayan09_clustering.R &>  $PCC7942DAT/vijayan09/clustering.log