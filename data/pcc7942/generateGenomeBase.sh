#!/bin/bash

# GENERATES THE BASE FILES FOR SYNECHOCYSTIS sp. PCC 7942 GENOME ANALYSIS
# from downloaded and preprocessed files

## REQUIRED ENVIRONMENT VARIABLES
## TO use command-line tools and R interface, these
## variables should be permanently set in your ~/.bashrc file
export GENBRO="/home/raim/programs/genomeBrowser/" # source files 
export PCC7942DAT="/data/pcc7942/" # working directory
export GENDAT="/data/genomeData/" # target directory for genomeBrowser RData

## REQUIRED SOFTWARE
# * wget, gunzip,  cat, sed
# * perl 5, including some CPAN perl modules (TODO: list)
# * NCBI Eutils in $PATH; see https://www.ncbi.nlm.nih.gov/books/NBK179288/ 

## Working directories for data downloads and pre-processing
## Note that this will contain useful tab-delimited files
## of raw data
mkdir $PCC7942DAT/originalData
mkdir $PCC7942DAT/processedData
mkdir $PCC7942DAT/chromosomes
mkdir $PCC7942DAT/genomeData
mkdir $PCC7942DAT/figures

### DOWNLOAD DATA 
## various data in data/pcc7942/parameters/downloaddata.csv
## and NCBI data via NCBI Eutils
$GENBRO/data/pcc7942/downloaddata.sh

### PREPROCESS DATA: unzips and converts data formats;
## fasta and gff from genbank
## INCLUDES ERROR FIXES
$GENBRO/data/pcc7942/preprocess.sh

## generate BLAST db
makeblastdb -in $PCC7942DAT/chromosomes/pcc7942.fasta -parse_seqids -dbtype nucl


## generate genome Browser RData files:

## 1) CYANOBASE: main manual annotation base
##    parse and collect cyanobase data, as main annotation
##    base; map data referring to it (e.g. lehmann14 transcriptome)
## 2) NCBI: main sequence and predicted annotation base
##    parse NCBI data; map cyanobase data by overlap analysis
## 3) Vijayan & O'Shea, Primary Transcriptome data

## TRANSCRIPTOME-BASED ANNOTATION
R --vanilla < $GENBRO/data/publications/vijayan09/vijayan09_clustering.R &> $PCC7942DAT/vijayan09/genbro_clustering.log

### CYANOBASE/NCBI annotations
mkdir $GENDAT/pcc7942
## cyanobase annotation
R --vanilla < $GENBRO/data/pcc7942/cyanobase.R
## ncbi annotation + mappings - requires cyanobase.RData !
R --vanilla < $GENBRO/data/pcc7942/annotation.R # takes a while

## NUCLEOTIDE DATA
R --vanilla < $GENBRO/data/pcc7942/nucleotides.R # takes long

## EXPERIMENTAL DATA
## TODO: run vijayan11_clustering.R
R --vanilla < $GENBRO/data/pcc7942/vijayan11.R # takes long

## TODO: generate windowed periodicity for two or more window sizes
## as heatmap
$GENBRO/src/calculateDiNuclProfile.R --id=pcc7942 --fdir=$PCC7942DAT/chromosomes --fend=.fasta --out=$PCC7942DAT/genomeData --win=396 --stp=10 --prm=1000 --nrm=snr --circular --verb  --din AT &> $PCC7942DAT/genomeData/at_period.log &

## and generate genomeData file
R --vanilla < $GENBRO/data/pcc6803/at_period.R

## SNAPGENE FILE
R --vanilla < $GENBRO/data/pcc7942/snapgene.R #
