#!/bin/bash


## DOWNLOAD OFFICIAL SEQUENCE AND ANNOTATION
# genome, NC_007604.1 - REFSEQ newer vesion, old_locus_tag refers to locus_tag in GENBANK
# genome, CP000100.1 - GENBANK
# plasmid_1, NC_007595.1 - REFSEQ
# plasmid_1, CP000101 - GENBANK - different locus_tag
# pANS (= pUH24), NC_004990.1 - REFSEQ original version, with locus_tag
# pANS (= pUH24), S89470.1 -  GENBANK staff version, no locus_tag

## OTHERS:
# pANL (= plasmid_1), NC_004073.2 - BUT DIFFERENT ORI!


declare -a RefSeqID=("NC_007604.1" "NC_007595.1" "NC_004990.1")
for  id in "${RefSeqID[@]}"; do 
    echo downloading $id;
    efetch -db sequences -format gbwithparts -id $id > $PCC7942DAT/originalData/${id}.gbk;
done

# DOWNLOAD DATA SETS
## NOTE: if you want to add new data, add to file
## $GENBRO/data/pcc7942/parameters/downloaddata.csv
## and simply rerun the following command; already downloaded data
## will NOT be downloaded again
$GENBRO/src/downloadData.pl -o $PCC7942DAT/originalData/ $GENBRO/data/pcc7942/parameters/downloaddata.csv -l $PCC7942DAT/originalData/download.log 
grep -i ERROR $PCC7942DAT/originalData/download.log
echo 'IF ERRORS OCCURED PLEASE CHECK LOG FILE $PCC7942DAT/originalData/download.log'
echo 'GET URL, AND DOWNLOAD FILES MANUALLY, PLEASE!'
