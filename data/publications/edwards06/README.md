
##  Edward et al. 2006: circadian Arabidopsis microarray

Two R scripts examplify for the use of the R libraries
[segmenTier](https://github.com/raim/segmenTier) for time-series
clustering, and [segmenTools](https://github.com/raim/segmenTools)
for cluster analysis.

1. Time series data is down-loaded and clustered into co-expressed
gene groups in
[edwards06_clustering.R](example/edwards06/edwards06_clustering.R),
2. Cluster enrichment analysis for full GO or slim GO annotation is
performed in [edwards06_go.R](example/edwards06/edwards06_go.R).

The analyses can be performed interactively in an R session, but work
without supervision from command-line, e.g. in bash type:

```{bash}
## clustering
echo "CLUSTER, see clustering.log"
R --vanilla < edwards06_clustering.R &> clustering.log
## GO analysis
echo "GO ANALYSIS, see go.log"
R --vanilla < edwards06_go.R &> go.log 
```
