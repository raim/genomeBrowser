#!/bin/bash

RFAMD=${MYDATA}/rfam
mkdir $RFAMD

cd ~/programs
wget http://eddylab.org/infernal/infernal-1.1.5.tar.gz
tar zxvf infernal-1.1.5.tar.gz
cd infernal-1.1.5
./configure
make
make check # All 170 exercises at level <= 1 passed.
sudo make install 

## download RFAM covariace models
wget https://ftp.ebi.ac.uk/pub/databases/Rfam/14.10/Rfam.tar.gz -P ${RFAMD}
cd ${RFAMD}
tar zxvf Rfam.tar.gz

## for full genome/transcriptome annotation
## after https://docs.rfam.org/en/latest/genome-annotation.html
wget https://ftp.ebi.ac.uk/pub/databases/Rfam/14.10/Rfam.cm.gz -P ${RFAMD}
gunzip ${RFAMD}/Rfam.cm.gz
wget ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/Rfam.clanin -P ${RFAMD}

## prepare full RFAM
cmpress ${RFAMD}/Rfam.cm
