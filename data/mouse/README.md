
## Circadian Consenus Cohorts

* cluster circadian sets from all organs:

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE54651

from Zhang et al. 2014: A circadian gene
expression atlas in mammals: Implications for biology and medicine
https://www.pnas.org/content/111/45/16219 


## TODO

* collect cluster mean-times, do statistical osci analysis, p-values
and phases, and save those that do oscillate (organ_peaktime) 

* define consensus clustering

* use clusters to analyze scRNAseq data, eg. comparable organ-wise
set at https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE109774
from   
    Tabula Muris Consortium., Overall coordination., Logistical coordination., Organ collection and processing. et al. Single-cell transcriptomics of 20 mouse organs creates a Tabula Muris. Nature 2018 Oct;562(7727):367-372. PMID: 30283141

## Circadian Datasets from Mammals

+ human vs. baboon circadian transcriptomes:
https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-022-01258-7

## Shorter Periods in Mammals


