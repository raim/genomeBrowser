#!/bin/bash

cd $MYDATA/mouse/originalData

## UNIPROT: reviewed swiss-prot entries, API URL, 20231103
wget -O uniprotkb_taxonomy_id_10090_AND_reviewe_2023_11_03.tsv.gz "https://rest.uniprot.org/uniprotkb/stream?compressed=true&fields=accession%2Creviewed%2Cid%2Cprotein_name%2Cgene_names%2Corganism_name%2Clength&format=tsv&query=%28%28taxonomy_id%3A10090%29%29+AND+%28reviewed%3Atrue%29"

