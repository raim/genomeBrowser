#!/bin/sh

module load python/3.6.6

seq_per_call=${1}
ncpu=${2}

fasta=/home/${USER}/data/mammary/processedData/Homo_sapiens.GRCh38.pep.large.fa
spred=/home/${USER}/programs/s4pred/
out=/home/${USER}/data/mammary/processedData/s4pred/

## find sequences in fasta where no results exist yet,
## build chunks of seq_per_call and run prediction

cnt=0
idfile=missing_ids.txt
if [ -f $idfile ]; then rm $idfile -f; fi
grep ">" ${fasta} |sed 's/>//' | while read -r id
do
    outfile=${out}/${id}.ss2
    if [ ! -f $outfile ]; then
	echo $id >> $idfile
	cnt=$((cnt+1))
    fi
    if [[ $cnt -ge $seq_per_call ]]; then
	echo collected max number of sequences: $cnt
	exit 1
    fi
done

if [ -f $idfile ]; then
    echo running `wc -l $idfile` missing sequences
    
    ## collect sequences via awk
    srun awk 'NR==FNR{n[">"$0];next} f{print f ORS $0;f=""} $0 in n{f=$0}' $idfile $fasta > chunk.fasta
    
    ## call python
    srun python ${spred}/run_model.py chunk.fasta --threads $ncpu -z -s -o $out
fi
echo done.



