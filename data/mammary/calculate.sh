#!/bin/bash

### RUN VARIOUS PREDICTION ALGORITHM BASED ON HUMAN GENOME
## PROTEIN, RNA and DNA SEQUENCES

## required tools and paths
bioawk=/home/raim/scripts/bioawk
infernal=/home/${USER}/programs/infernal-1.1.5/
RFAMD=${MYDATA}/rfam

## S4PRED : PREDICT SECONDARY STRUCTURE FOR ALL ENSEMBL PROTEINS 
## NOTE: run at NEU discovery HPC platform, and data copied to main
## data/mammary/processedData from there
if false; then
    ## set up directories at the `discovery` HPC of NEU
    DSRC=/home/${USER}/programs/genomeBrowser//data/mammary
    ##$SRC/setup_hpc.sh
    mkdir ~/s4pred
    cd ~/s4pred/
    ## run s4pred secondary structure prediction on all human proteins
    $DSRC/s4pred_batch.sh
    ## check results: <102691 of 121449 after first run
    ls ~/data/mammary/processedData/s4pred/ -1 |wc -l
    grep ">" ~/data/mammary/processedData/Homo_sapiens.GRCh38.pep.large.fa|wc -l
    ## repeat CANCELLED jobs - NOTE: run twice with ncpu=40 to catch last 4000
    $DSRC/s4pred_repeat.sh
    ## collect missing in chunks of 1000
    ncpu=20
    seq_per_call=1000
    sbatch --job-name=s4pred_missing --output=s4pred_missing.out -p short \
	   --nodes=1 --ntasks 1 --cpus-per-task $ncpu --mem=2GB \
	   $DSRC/s4pred_missing.sh $seq_per_call $ncpu
    sbatch --job-name=s4pred_missing2 --output=s4pred_missing2.out -p short \
	   --nodes=1 --ntasks 1 --cpus-per-task $ncpu --mem=2GB \
	   $DSRC/s4pred_missing.sh $seq_per_call $ncpu
    sbatch --job-name=s4pred_missing3 --output=s4pred_missing3.out -p short \
	   --nodes=1 --ntasks 1 --cpus-per-task $ncpu --mem=2GB \
	   $DSRC/s4pred_missing.sh $seq_per_call $ncpu

    ## clean up
    rm *.fas chunk.fasta missing_ids.txt -f
fi

## copy s4pred data to exon
rsync -avz r.machne@login.discovery.neu.edu:data/mammary/processedData/s4pred \
      $PROCDATA/
## write output to fasta
$SRC/s4pred_collect.sh
## check completeness
grep ">" $PROCDATA/Homo_sapiens.GRCh38.pep.large_s4pred.fas |wc -l
grep ">" $PROCDATA/Homo_sapiens.GRCh38.pep.large.fa|wc -l
## compress!
gzip $PROCDATA/Homo_sapiens.GRCh38.pep.large_s4pred.fas

### IUPRED3

## iupred3 requires to be run on each sequence separately
## * loop through ID file, check if it's already done,
##   then extract single fasta and run iupred3
## * FAST: 24 h in simple loop on exon. NOTE:
##   can be run twice as fast when the two loops in script
##   are run manually in two parallel terminas
$SRC/iupred3_run.sh
## NOTE: columns are
# POS   RES     IUPRED2 ANCHOR2

## TODO: redo iupred3 for ENSP00000352639 ENSP00000364986,
## since this seems to have a different sequence; potentially
## via the tmp.fa

## PairedCoil2 - TODO: find problem
if [ false ]; then
    mkdir $PROCDATA/paircoil2
    cd $PROCDATA/paircoil2
    ln -s $PROCDATA/Homo_sapiens.GRCh38.pep.large.fa
    ln -s ~/programs//paircoil2/*.tb .
    ln -s ~/programs//paircoil2/paircoil2 .
fi

### TODO: RFAM ANNOTATION OF TRANSCRIPTS
## follow instructions at
## https://docs.rfam.org/en/latest/genome-annotation.html
## where preparation of RFAM is done in ../rfam/setup.sh and
## here we start from "4. Determine the total database size"

## first, get a cleaned fasta file: remove all transcripts <50 nt
gunzip -c $ORIGDATA/Homo_sapiens.GRCh38.cdna.all.fa.gz \
    | $bioawk -c fastx '{if(length($seq) > 49) {print ">"$name; print $seq}}' \
	      - > $PROCDATA/Homo_sapiens.GRCh38.cdna.large.fa
gzip $PROCDATA/Homo_sapiens.GRCh38.cdna.large.fa

## FULL TRANSCRIPTOME SCAN FOR ALL RFAM
$infernal/easel/miniapps/esl-seqstat \
    $PROCDATA/Homo_sapiens.GRCh38.cdna.large.fa.gz
## Total # residues: 390031071
Z=78 # 780.0621 # 390031071*2/1e6 / 10 - database size, divide by 10 to account for sequence redundancy in ensembl transcript fasta
## NOTE: options via https://docs.rfam.org/en/latest/genome-annotation.html
## NOTE: rm options "--cut_ga --rfam --nohmmonly" and decreased Z to increase hit number
cmscan --cpu 7 -Z $Z \
       --tblout $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam.out \
       --fmt 2 --clanin $RFAMD/Rfam.clanin $RFAMD/Rfam.cm  \
       $PROCDATA/Homo_sapiens.GRCh38.cdna.large.fa.gz \
       > $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam.cmscan \
    &> $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam.log
cmscan --cpu 7 -Z $Z --cut_ga --rfam --nohmmonly \
       --tblout $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam_cut_ga.out \
       --fmt 2 --clanin $RFAMD/Rfam.clanin $RFAMD/Rfam.cm  \
       $PROCDATA/Homo_sapiens.GRCh38.cdna.large.fa.gz \
       > $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam_cut_ga.cmscan \
    &> $PROCDATA/Homo_sapiens.GRCh38.cdna.large.rfam_cut_ga.log

### SCAN FOR PFAM DOMAINS
### TODO: only do for the AAS proteins!
hmmpress $ORIGDATA/pfam/Pfam-A.hmm
hmmscan --noali --notextw \
	-o $PROCDATA/Homo_sapiens.GRCh38.pep.large_annotations.txt \
	--domtblout $PROCDATA/Homo_sapiens.GRCh38.pep.large_annotations.tbl \
	-E 0.01 --cpu 4 \
	$ORIGDATA/pfam/Pfam-A.hmm \
	$PROCDATA/Homo_sapiens.GRCh38.pep.large.fa \
    &> $PROCDATA/Homo_sapiens.GRCh38.pep.large_annotations.log

## replace spaces with ; and omit descript to make parsing easier
sed 's/ \+/;/g'  \
    $PROCDATA/Homo_sapiens.GRCh38.pep.large_annotations.tbl \
    | cut -d';' -f 1-22 \
	  > $PROCDATA/Homo_sapiens.GRCh38.pep.large_annotations.csv


### RNAplfold: unpaired probabilities
mkdir $PROCDATA/RNAplfold
cd $PROCDATA/RNAplfold
##gunzip -c $ORIGDATA/Homo_sapiens.GRCh38.cdna.all.fa.gz | RNAplfold -o -u 31 > $PROCDATA//Homo_sapiens.GRCh38.cdna.all_plfold-o-u.txt &
## script with custom L=W=length(seq) and -u 31
$SRC/runplfold.sh \
    $ORIGDATA/Homo_sapiens.GRCh38.cdna.all.fa.gz 7 \
    &> $PROCDATA/RNAplfold/runplfold_4.log &
   


#### MAP DATA FROM GENOME COORDINATES TO TRANSCRIPTS

### testing different options
if false; then
    cut -f 1  $MAMDATA/transcript_coordinates.tsv | sort|uniq

    myBWTBG=~/programs/ucsc_utils/bigWigToBedGraph

    ## convert full track to bed graph, ok for riboseq data
    $myBWTBG ~/Downloads/HEK293_high.RiboCov.bw hek_hig.bed
    $myBWTBG ~/Downloads/Haas20_All.RiboProElong.bw  haas.bed

    ## BEST for phastcons - full genome data
    ## extract regions for phastcons
    $myBWTBG $ORIGDATA/hg38.phastCons7way.bw \
	     -chrom=chrY -start=20756778 -end=20756855  out.bed

    ## manual download from https://gwips.ucc.ie/downloads/index.html
    ## mammalian > human
    ## Elongating Ribosomes (A Site) > Global Aggregate >
    ## Iwasaki19_RiboProElong_global_(child) -> DOWNLOAD
    $myBWTBG ~/Downloads/Iwasaki19_All.RiboProElong.bw Iwasaki19_All.RiboProElong.bed
    
    ## or
    ## 1) use bedops gff2bed to convert CDS to bed
    ## 2) for each unique transcript Parent, extract a specific bed
    ##gunzip -c  $ORIGDATA/Homo_sapiens.GRCh38.110_proteins.gff3.gz | grep ENST00000369817| gff2bed > tmp.bed
    ##multiBigwigSummary BED-file -b ~/Downloads/HEK293_high.RiboCov.bw --BED tmp.bed --outRawCounts
    
    ## BEST PERHAPS for riboSeq - data sparse
    ## A) convert to bed, doesn't seem too big;
    ## load transcript-CDS to R (readGFF), and
    ## use coor2index to get per transcript data, OR
    ## B) use specific tools eg. RiboDiPA, with .bam
    ## files and gff3 -> gtf

    ## RiboDiPA: http://127.0.0.1:18819/library/RiboDiPA/doc/RiboDiPA.html
    ## also steps until the following can be done with multiple bam files,
    ## and then used independent of difference calculations.
    ## data.psite <- psiteMapping(bam_file_list = bam_path[1:4], 
    ## gtf_file = bam_path[5], psite.mapping = "auto", cores = 2) 
fi


