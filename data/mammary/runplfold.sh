#!/bin/bash

## NOTE: this script was written with the help of chatGPT (GPT-4)

# Ensure that a gzipped FASTA file and the number of parallel jobs are provided as arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <input.fasta.gz> <max_parallel_jobs>"
    exit 1
fi

# Input gzipped FASTA file
input_fasta="$1"
max_parallel_jobs="$2"

# Ensure RNAplfold and GNU parallel are installed
if ! command -v /usr/local/bin/RNAplfold &> /dev/null; then
    echo "RNAplfold could not be found at /usr/local/bin/RNAplfold. Please ensure that it is installed and in your PATH."
    exit 1
fi

if ! command -v parallel &> /dev/null; then
    echo "GNU parallel could not be found. Please install it and try again."
    exit 1
fi

# Function to process a sequence
process_sequence() {
    local header="$1"
    local sequence="$2"
    local seq_length=${#sequence}

    local id=`echo $header|sed 's/\\..*//'`
    echo "Processing sequence $id with length $seq_length"

    ## check if output file already exists
    if compgen -G "${id}*_lunp" > /dev/null; then
	echo "result exists!"
    else
	# Use printf to format the sequence and pipe it into RNAplfold
	printf ">$header\n$sequence" | /usr/local/bin/RNAplfold -u 31 -L "$seq_length" -W "$seq_length"
    fi
}

export -f process_sequence

# Initialize variables
header=""
sequence=""
jobs_file=$(mktemp)

# Decompress and read the gzipped FASTA file, and prepare commands for parallel execution
gunzip -c "$input_fasta" | while read -r line; do
    if [[ $line == ">"* ]]; then
        # If we find a new header, prepare the previous sequence for processing
        if [[ -n $sequence ]]; then
            echo "process_sequence \"$header\" \"$sequence\"" >> "$jobs_file"
        fi
        # Start a new sequence
        header=${line#>}
        sequence=""
    else
        # Concatenate the sequence lines
        sequence+="$line"
    fi
done

# Prepare the last sequence in the file
if [[ -n $sequence ]]; then
    echo "process_sequence \"$header\" \"$sequence\"" >> "$jobs_file"
fi

# Run all commands in parallel
parallel -j "$max_parallel_jobs" < "$jobs_file" &

# Clean up
##rm "$jobs_file"

echo "Processing complete."
