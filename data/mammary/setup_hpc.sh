
## just a record of data and scripts for discovery

mkdir ~/programs
mkdir data
mkdir data/mammary
mkdir work

## from exon
## rsync -avz /home/raim/data/mammary/processedData r.machne@login.discovery.neu.edu:data/mammary/
## rsync -avz /home/raim/data/mammary/originalData r.machne@login.discovery.neu.edu:data/mammary/
## rsync -avz /home/raim/programs/iupred3.tar.gz r.machne@login.discovery.neu.edu:programs/


## 
cd ~/work
git clone git@github.com:raim/mistrans.git

## 
cd ~/programs
git clone https://github.com/lh3/bioawk 
cd bioawk
make

cd ~/programs
git clone https://github.com/psipred/s4pred
cd s4pred
wget http://bioinfadmin.cs.ucl.ac.uk/downloads/s4pred/weights.tar.gz
tar -xvzf weights.tar.gz

cd ~/programs
git clone git@github.com:raim/genomeBrowser.git


cd ~/programs
wget http://eddylab.org/software/hmmer/hmmer.tar.gz
tar zxvf hmmer.tar.gz
cd hmmer-3.4

## compile hmmer
srun -N 1 -n 28 --constraint=ib --pty /bin/bash
module load gcc/9.2.0
./configure --prefix /home/r.machne/
make
make check
make install

## get Pfam
srun -N 1 -n 28 --constraint=ib --pty /bin/bash
mkdir ~/data/mammary/originalData/pfam
cd ~/data/mammaryoriginalData/pfam
wget https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz
wget https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/relnotes.txt
wget https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/userman.txt
echo "Pfam-A.hmm.gz downloaded on 2024-04-11 as the current release (36.0) from EBI" > README.md
gunzip Pfam-A.hmm.gz
hmmpress ~/data/mammary/originalData/pfam/Pfam-A.hmm
