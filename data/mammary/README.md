# Human Genome Data

Project to (i) set up human genome data for efficient parsing and
coordinate mapping, (ii) run prediction algorithms (partially HPC),
and (iii) integrate various published omic data sets.

Note, that the main bash scripts are intended to be executed, but are
generated more as a **log book or lab journal** of all steps executed
during download and processing of individual data sets. Actual
execution of these scripts will likely fail, due to required manual
steps, as indicated in comments.

Steps:

``` bash
./download.sh # download Ensembl genome and data sets, REQUIRES MANUAL STEPS
./setup.sh # extract clean data tables from downloads
./calculate.sh # run various prediction algorithms, incl. MANUAL HPC STEPS
```

## Analyses

A comparative analysis of human and yeast single cell transcrptome and
protein half-live data is currently performed here and run from
`scrnaseq_analyses.sh`. This will likely be transferred to a separate
project in the future.
