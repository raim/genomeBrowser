
### COMPARATIVE YEAST-HUMAN ANALYSIS OF
## SINGLE CELL TRANSCRIPTOME DATA AND PROTEIN HALF-LIVES

export MAMDATA=${MYDATA}/mammary
SRC=$GENBRO/data/mammary
YSRC=$GENBRO/data/yeast
MCYCLE=~/work/mammacycle/ # NOTE: currently only log file output


## required tools
orthof=/home/${USER}/programs/OrthoFinder_source/orthofinder.py

### RUN OrthoFinder - yeast - human

## orthofinder expects input data in a specified directory
orthodir=$MAMDATA/processedData/orthofinder
mkdir ${orthodir}

## copy files to orthofinder run dir
gunzip -c $MAMDATA/originalData/Homo_sapiens.GRCh38.pep.all.fa.gz \
       > ${orthodir}/human_h38_20231204.faa

## get yeast proteins
## TODO: move this to yeast project?
yurl=http://sgd-archive.yeastgenome.org/sequence/S288C_reference/orf_protein/orf_trans_all.fasta.gz
wget -O $YEASTDAT/originalData/orf_trans_all_20230830.fasta.gz $yurl
     
gunzip -c $YEASTDAT/originalData/orf_trans_all_20230830.fasta.gz \
       > ${orthodir}/yeast_20230830.faa
## run OrthoFinder
$orthof -f ${orthodir}/ &
## copy results
cp -a ${orthodir}/OrthoFinder/Results_Dec04/Orthologues/human_h38_20231204.tsv \
   $MAMDATA/processedData/


### COHORT EXPRESSION ANALYSIS


## attempt to define human cell line cohorts from time series clustering
## cluster MCF10A and MCF7 time series
R --vanilla < $SRC/gutierrezmonreal16.R
## TODO: implement janes10 re-analysis
R --vanilla < $SRC/janes10.R


### RUN scRNAseq ANALYSIS SCRIPTS

## PRE-PROCESS DATA

## currently some required data were obtained
## via Andrew Leduc and the project yeast
## mv <files by andrew> $MAMDATA/processedData/slavovlab/

## yeast
R --vanilla < $YSRC/processJackson2020.R
R --vanilla < $YSRC/processGasch2017.R
R --vanilla < $YSRC/processHeineike24pre.R

## human
R --vanilla < $SRC/processBattich2020.R #RPE1
R --vanilla < $SRC/processHsiao2020.R #iPSC
R --vanilla < $SRC/processMahdessian2021.R #U2OS
R --vanilla < $SRC/processKrenning2022.R #RPE1k

## re-analysis of yeast CDC data vs. CGC cohorts
R --vanilla < $SRC/cluster_cdc_vs_cgc.R

## CALCULATE CDC and CGC COHORT STATES,
## AND CDC PSEUDOTIMES

## 20240913: moved old scripts with umaps etc.
## to cohorts_analysis_long.R and cohorts_analysis_long.R

## define cohorts and run pca analysis  (via cohorts_pca.R)
R --vanilla < $SRC/cohorts_human.R &> $MCYCLE/log/cohorts_human.txt &
R --vanilla < $SRC/cohorts_yeast.R &> $MCYCLE/log/cohorts_yeast.txt

## old scripts, generating nice educational movies
R --vanilla < $SRC/cohorts_human_old.R &> $MCYCLE/log/cohorts_human_old.txt &
R --vanilla < $SRC/cohorts_yeast_old.R &> $MCYCLE/log/cohorts_yeast_old.txt


R --vanilla < $SRC/cohorts_simulate.R


## PROTEIN HALFLIVES MAMMALS AND YEAST
R --vanilla < ${SRC}/halflives.R
R --vanilla < ${SRC}/processMuenzner24.R
R --vanilla < ${SRC}/folding.R
