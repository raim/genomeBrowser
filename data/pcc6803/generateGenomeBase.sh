#!/bin/bash

# GENERATES THE BASE FILES FOR SYNECHOCYSTIS sp. PCC 6803 GENOME ANALYSIS
# from downloaded and preprocessed files

## TODO: resolve timeline, kopf14.R <-> cyanobase.R/annotation.R

## REQUIRED ENVIRONMENT VARIABLES
## TO use command-line tools and R interface, these
## variables should be permanently set in your ~/.bashrc file
export GENBRO="/home/raim/programs/genomeBrowser/" # source files 
export PCC6803DAT="/data/pcc6803/" # working directory
export GENDAT="/data/genomeData/" # target directory for genomeBrowser RData

## REQUIRED SOFTWARE
# * wget, gunzip,  cat, sed
# * perl 5, including some CPAN perl modules (TODO: list)
# * NCBI Eutils in $PATH; see https://www.ncbi.nlm.nih.gov/books/NBK179288/ 

## Working directories for data downloads and pre-processing
## Note that this will contain useful tab-delimited files
## of raw data
mkdir $PCC6803DAT/originalData
mkdir $PCC6803DAT/processedData
mkdir $PCC6803DAT/chromosomes
mkdir $PCC6803DAT/genomeData
mkdir $PCC6803DAT/figures

### DOWNLOAD DATA 
## various data in data/pcc6803/parameters/downloaddata.csv
## and NCBI data via NCBI Eutils
$GENBRO/data/pcc6803/downloaddata.sh

### PREPROCESS DATA: unzips and converts data formats;
## fasta and gff from genbank
## INCLUDES ERROR FIXES
$GENBRO/data/pcc6803/preprocess.sh

## generate BLAST db
makeblastdb -in $PCC6803DAT/chromosomes/pcc6803.fasta -parse_seqids -dbtype nucl


# calculate structure file, INCL. nucleotides 
#perl $GENBRO/src/calculateChromatinStructure.pl $PCC6803DAT/chromosomes/pcc6803.csv -fasta $PCC6803DAT/chromosomes/pcc6803.fasta -o $PCC6803DAT/genomeData/ -din $GENBRO/parameters/dinucleotides.csv -tri $GENBRO/parameters/trinucleotides.csv -out pcc6803 -mot $GENBRO/data/pcc6803/parameters/motifs_reduced.csv -pwm $GENBRO/data/pcc6803/parameters/pwm_generated.tsv 


## generate genome Browser RData files:

## 1) CYANOBASE: main manual annotation base
##    parse and collect cyanobase data, as main annotation
##    base; map data referring to it (e.g. lehmann14 transcriptome)
## 2) NCBI: main sequence and predicted annotation base
##    parse NCBI data; map cyanobase data by overlap analysis
## 3) Mitscke et al. 2011: add TSS data -> NCBI
## 4) Kopf et al. 201xx: add sequencing data -> NCBI
## 5) Zavrel et al. 2019: proteome and growth rate

## TRANSCRIPTOME-BASED ANNOTATION - FEATURE COLORING
cd $PCC6803DAT/saha16/
R --vanilla < $GENBRO/data/publications/saha16/saha16_clustering.R &> $PCC6803DAT/saha16/genbro_clustering.log
cd -

## Zavrel et al. 2019: proteome and growth rate
R --vanilla < $GENBRO/data/pcc6803/zavrel19.R

## TODO: ADD UNIPROT IDs to cyanobase and ncbi
##       and make use of uniprot annotations, GO, etc.

### CYANOBASE/NCBI annotations
mkdir $GENDAT/pcc6803

## TODO: RESOLVE TIME-LINE:
## cyanobase.R and annotation.R parse data produced later,
## eg. in kopf14.R and scripts for Behle et al. 2021

## cyanobase annotation
R --vanilla < $GENBRO/data/pcc6803/cyanobase.R
## ncbi annotation + mappings - requires cyanobase.RData !
R --vanilla < $GENBRO/data/pcc6803/annotation.R # takes a while

### NUCLEOTIDE DATA
R --vanilla < $GENBRO/data/pcc6803/nucleotides.R # takes long

### EXPERIMENTAL DATA
R --vanilla < $GENBRO/data/pcc6803/kopf14.R
R --vanilla < $GENBRO/data/pcc6803/mitschke11.R

## RE-RUN to include generated data (kopf14.R)
## cyanobase annotation
R --vanilla < $GENBRO/data/pcc6803/cyanobase.R
## ncbi annotation + mappings - requires cyanobase.RData !
R --vanilla < $GENBRO/data/pcc6803/annotation.R # takes a while


## COLLECT ALL DATA INTO GENBANK FILE/SNAPGENE
## TODO: 
R --vanilla < $GENBRO/data/pcc6803/snapgene.R

## AT PERIODICITY
## TODO: generate windowed AT periodicity for two or more window sizes
## as heatmap
## p-values - takes very long
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --fdir=$PCC6803DAT/chromosomes --fend=.fasta --out=$PCC6803DAT/genomeData --win=396 --stp=10 --prm=1000 --nrm=snr --circular --verb  --din AT >& $PCC6803DAT/genomeData/at_period.log &

## just amplitudes
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --fdir=$PCC6803DAT/chromosomes --fend=.fasta --out=$PCC6803DAT/genomeData --win=396 --stp=10 --round=1 --nrm=snr --circular --verb  --din AT >& $PCC6803DAT/genomeData/at_period_amplitudes.log &

## and generate genomeData file
R --vanilla < $GENBRO/data/pcc6803/at_period.R

## SNAPGENE FILE
R --vanilla < $GENBRO/data/pcc6803/snapgene.R #
