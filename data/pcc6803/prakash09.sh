
## NOTE: from tataProject, currently genomeBrowser
## uses the outcome of the previus extraction from Lehmann et al. 2014
## supplement

## PRAKASH et al. 2009: stress-induced supercoiling
pdftotext $PCC6803DAT/originalData/prakash09_table_s1.pdf -layout  $PCC6803DAT/processedData/prakash09_s1.tmp

#grep '^ ' $PCC6803DAT/processedData/prakash09_s1.csv | sed 's/^[ \t]*//g' | grep '^[GCs]' | cut -f 1 -d ' ' >  $PCC6803DAT/processedData/prakash09_s1.dat

num=0
cls=0
for gene in `grep '^ ' $PCC6803DAT/processedData/prakash09_s1.tmp | sed 's/^[ \t]*//g;s/Cluster7/Cluster 7/' | grep '^[GCs]' | cut -f 1 -d ' '` ; do
    if [ "$gene" = "Group" ]; then
        num=$(($num + 1));
    elif [ "$gene" = "Cluster" ]; then
        cls=$(($cls + 1));
    else
        echo -e "$gene\t$num\t$cls";
    fi
done > $PCC6803DAT/processedData/prakash09_s1.tsv


