
## load ExpressionSet.R from CyanoExpress, Hernandez-Prieto & Futschik 2012


cyano.path <- "~/data/pcc6803/"

dat <- read.delim(file.path(cyano.path,"originalData","ExpressionData.txt"),
                  row.names=1, stringsAsFactors=FALSE)

## NOTE: only 101 rows??
genes.desc <- dat[,1]
names(genes.desc)
