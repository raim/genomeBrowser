#!/bin/bash

## pre-process pcc6803 data 

## Mitschke et al. unzip and fix errors
unzip $PCC6803DAT/originalData/Supplementary\ data\ file\ 1\ Syn6803TSS.gbk.zip -d $PCC6803DAT/originalData/
cat $PCC6803DAT/originalData/Supplementary_data_file_1_Syn6803TSS.gbk | sed 's/ssl1091/sll1091/g;s/sll2064/ssl2064/g;s/sll2507/ssl2507/g' | sed -e '54851s/slr0228/slr0335/g' > $PCC6803DAT/processedData/mitschke11_corrected.gbk
$GENBRO/src/genbank2gff.pl -comment -semicol $PCC6803DAT/processedData/mitschke11_corrected.gbk > $PCC6803DAT/processedData/mitschke11_corrected.gff
## extract relative TSS/TES data!
perl $GENBRO/data/pcc6803/processMitschke11.pl |grep "5'UTR" > $CYANODAT/processedData/mitschke11_tss.dat
perl $GENBRO/data/pcc6803/processMitschke11.pl |grep "3'UTR" > $CYANODAT/processedData/mitschke11_tes.dat

## Kopf et al. 2014
## extract from excel
ssconvert $PCC6803DAT/originalData/dsu018supp.xlsx $PCC6803DAT/processedData/kopf14_tu.csv

### FEATURE DATA
## convert GENBANK FILES TO GFF TO genomeBrowser TAB
# 1 generate gff files
declare -a RefSeqID=("NC_000911.1" "NC_005230.1" "NC_005231.1" "NC_005229.1" "NC_005232.1" "CP003272.1" "CP003270.1" "CP003271.1")
\rm $PCC6803DAT/processedData/pcc6803.gff
touch $PCC6803DAT/processedData/pcc6803.gff
for  id in "${RefSeqID[@]}"; do
    echo converting $id;
    $GENBRO/src/genbank2gff.pl -semicol $PCC6803DAT/originalData/${id}.gbk >> $PCC6803DAT/processedData/pcc6803.gff
done
# 2 - TODO: generate feature.csv
# genomeBrowser format; or do that in annotation.R ?

### SEQUENCE DATA
## extract FASTA from GENBANK FILES
## renames sequences from RefSeq to human readable
\rm $PCC6803DAT/chromosomes/pcc6803.fasta
touch $PCC6803DAT/chromosomes/pcc6803.fasta
for  id in "${RefSeqID[@]}"; do
    echo extracting fasta from $id;
    $GENBRO/src/genbank2fasta.pl $PCC6803DAT/originalData/${id}.gbk | sed 's/NC_000911/genome/;s/NC_005230/pSYSA/;s/NC_005231/pSYSG/;s/NC_005229/pSYSM/;s/NC_005232/pSYSX/;s/CP003272/pCC5.2_M/;s/CP003270/pCA2.4_M/;s/CP003271/pCB2.4_M/' >> $PCC6803DAT/chromosomes/pcc6803.fasta
done

## Sutormin et al. 2019 gyrase motif
## grep from file exported manually from supplemental excel file
echo -e "MOTIF\tGCS" > $GENBRO/data/pcc6803/parameters/pwm_generated.tsv
grep "^A" $GENBRO/data/pcc6803/parameters/sutormin19_TabSD10_gyrasemotif.csv | sed 's/,/./g;s/\s+/\t/g' >> $GENBRO/data/pcc6803/parameters/pwm_generated.tsv
grep "^C" $GENBRO/data/pcc6803/parameters/sutormin19_TabSD10_gyrasemotif.csv | sed 's/,/./g;s/\s+/\t/g' >> $GENBRO/data/pcc6803/parameters/pwm_generated.tsv
grep "^G" $GENBRO/data/pcc6803/parameters/sutormin19_TabSD10_gyrasemotif.csv | sed 's/,/./g;s/\s+/\t/g' >> $GENBRO/data/pcc6803/parameters/pwm_generated.tsv
grep "^T" $GENBRO/data/pcc6803/parameters/sutormin19_TabSD10_gyrasemotif.csv | sed 's/,/./g;s/\s+/\t/g' >> $GENBRO/data/pcc6803/parameters/pwm_generated.tsv
## add Box-10 motif from tataProject/cyano
cat $GENBRO/data/pcc6803/parameters/pwm.csv >> $GENBRO/data/pcc6803/parameters/pwm_generated.tsv

# extract chromosome index file
$GENBRO/src/fastaindex.R chromosomes/pcc6803.fasta > $PCC6803DAT/chromosomes/pcc6803.csv

## Koskinen et al. 2016: Delta sigBCDE transcriptome
unzip   $PCC6803DAT/originalData/mmi13214-sup-0001-si.zip MMI_13214_supp-0001-Dataset_S1.xlsx -d $PCC6803DAT/originalData/

## Kariyazono and Osanai 2022: SigA and SigE Targets
unzip $PCC6803DAT/originalData/kariyazono22_supp.zip tpj15687-sup-0002-TableS1-S6.xlsx -d $PCC6803DAT/originalData/
