#!/bin/bash

## DOWNLOAD OFFICIAL SEQUENCE AND ANNOTATION
# genome,   NC_000911.1
# pSYSA,    NC_005230.1
# pSYSG,    NC_005231.1
# pSYSM,    NC_005229.1
# pSYSX,    NC_005232.1
# pCA2.4_M, CP003270.1 
# pCB2.4_M, CP003271.1 
# pCC5.2_M, CP003272.1 
declare -a RefSeqID=("NC_000911.1" "NC_005230.1" "NC_005231.1" "NC_005229.1" "NC_005232.1" "CP003270.1" "CP003271.1" "CP003272.1")
for  id in "${RefSeqID[@]}"; do 
    echo downloading $id;
    efetch -db sequences -format gbwithparts -id $id > $PCC6803DAT/originalData/${id}.gbk;
done

# DOWNLOAD DATA SETS
## NOTE: if you want to add new data, add to file
## $GENBRO/data/pcc6803/parameters/downloaddata.csv
## and simply rerun the following command; already downloaded data
## will NOT be downloaded again

## NOTE: ExpressionData.txt from CyanoExpress needs to be downloaded manually

## TODO: save and upload downloaded files that are not available anymore
$GENBRO/src/downloadData.pl -o $PCC6803DAT/originalData/ $GENBRO/data/pcc6803/parameters/downloaddata.csv -l $PCC6803DAT/originalData/download.log 
grep -i ERROR $PCC6803DAT/originalData/download.log
echo 'IF ERRORS OCCURED PLEASE CHECK LOG FILE $PCC6803DAT/originalData/download.log'
echo 'GET URL, AND DOWNLOAD FILES MANUALLY, PLEASE!'

## NOTE: manual downloads for those not working with the script:
## Koskinen et. al 2016: https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fmmi.13214&file=mmi13214-sup-0001-si.zip

## UNIPROT IDs <-> KEGG
wget "https://www.uniprot.org/uniprot/?query=organism:1111708&format=tab&columns=id,database(KEGG)" --output-document $PCC6803DAT/originalData/pcc6803_uniprot.txt -o $PCC6803DAT/originalData/pcc6803_uniprot.log

## UNIPROT GO
wget "https://www.uniprot.org/uniprot/?query=organism:1111708&format=tab&columns=id,database(KEGG),go" --output-document $PCC6803DAT/originalData/pcc6803_uniprot_go.txt -o $PCC6803DAT/originalData/pcc6803_uniprot_go.log

## 


#### MANUAL DOWNLOAD DOC
## cyanoexpress
## from: http://193.136.227.175/cyanoX/cgi-bin/gx2?n=all
## date: 20210325
## how: click "Download expression data" and save ExpressionData.txt
## mv ~/Downloads/ExpressionData.txt /data/pcc6803/originalData/CyanoExpress_ExpressionData_20210325.txt
## -> NOTE: only downloads 100 genes selected in the heatmap on the left

## @Baers2019: protein localization data, supplemental file
## plphys_v181_4_1721_s1.zip from
## https://academic.oup.com/plphys/article/181/4/1721/6000548#supplementary-data
mv ~/Downloads/plphys_v181_4_1721_s1.zip  $PCC6803DAT/originalData/
unzip  $PCC6803DAT/originalData/plphys_v181_4_1721_s1.zip  00897Supplemental_Tables.xlsx -d $PCC6803DAT/originalData/
