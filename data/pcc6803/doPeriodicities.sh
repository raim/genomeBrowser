#!/bin/sh
##
## doPeriodicities.sh
## 
## Made by Rainer Machne
## Login   <raim@intron.tbi.univie.ac.at>
## 
## Started on  Tue Apr  2 14:29:27 2013 Rainer Machne
## Last update Tue Apr  2 14:29:27 2013 Rainer Machne

### NOTE: this used the previous genome for Scystis in /data/cyano
### 

#$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --fig.path=$CYANODAT/genomeData --window=396 --stepL=10 --perm=10000  --norm="" --maxk=15 --prng=10.7-12.5 --crng=9.2-10.7 --circular ---verb  > genomeData/dinuc.log

$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=396 --stp=10 --prm=10000 --phs --phf --fth=10 --inv=10-12 --circular --verb  > dinucPeriods/dinuc_perm.log

## genome profiles
## TODO: parallelize!
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=396 --stp=10 --prm=100 --nrm="snr" --circular --verb  > dinucPeriods/dinuc.log
## gene profiles
outdir=dinucPeriods/geneData/
logdir=$outdir/log
mkdir -p $logdir
\rm $logdir/*
for k in `grep "^chr" genomeData/pcc6803_AT_p100_w396_s10_SNR_amplitudes.csv`; do
    if [[ "$k" != "chr" && $k != "coor" ]]; then
        nam=X${k}_chromatinAverages
        qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR $TATADIR/perls/chromatinAverages.sh -chr chromosomes/pcc6803.csv -g genomeData/pcc6803_AT_p100_w396_s10_SNR_amplitudes.csv -f pcc6803.feature.base.csv -up -1500 -do 1500 -w 2 -o $outdir -name "features.-1500..1500.ATG" -v $k
    fi
done 
## gene data index file: sorted by period!
for dat in `ls dinucPeriods/geneData/features.-1500..1500.ATG.*dat`; do k=`echo $dat | sed 's|dinucPeriods/geneData/features.-1500..1500.ATG.||g;s/\.dat//g'`; echo -e "$k\t$dat"; done | grep -v coor | sort -n > dinucPeriods/geneData_files.dat

## cluster profiles: use parallel wrapper
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log
\rm $logdir/*
mkdir -p $logdir
for i in {1..191}; do 
    nam=X${i}_geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc CL --gdf dinucPeriods/geneData_files.dat --out $outdir  --typ gene --bns 10 --tst W --sde greater --dat $i
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files.dat --pfd $outdir --out $figdir --cls 1,2,3,4,5,6,7,8,9,10,11,12 --ylb period --xlb "relative position" --val pvalue --cut 3.3:13 --trf lg2 --dat 1:170 --verb
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files.dat --pfd $outdir --out $figdir --cls 1,2,3,4,5,6,7,8,9,10,11,12 --ylb period --xlb "relative position" --val mean --dat 1:170 --verb 

## Prakash et al. 2009 - supercoiling
outdir=dinucPeriods/prakash09Profiles
logdir=$outdir/log
\rm $logdir/*
mkdir -p $logdir
for i in {1..191}; do 
    nam=X${i}_geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff pcc6803.features.csv --clc CL.sc --gdf dinucPeriods/geneData_files.dat --out $outdir  --typ gene --bns 10 --tst W --sde greater --dat $i
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val pvalue --cut 3.3:13 --trf lg2 --dat 1:170 --verb
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files.dat --pfd $outdir --out $figdir  --ylb period --xlb "relative position" --val mean --dat 1:170 --verb 

## same as above but for lower windowsize 198

## genome profile
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=198 --stp=10 --prm=0 --nrm="snr" --circular  --verb  > genomeData/dinuc_snr_s10_w198.log

$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=198 --stp=10 --prm=0 --nrm="snr" --circular  --verb  --ipf > genomeData/dinuc_ipdft_snr_198.log

## gene profiles
## add -tss -tsnm TSS to use TSS (but too few available)
## TODO: circularize!!!
outdir=dinucPeriods/geneData/
logdir=$outdir/log_s10_snr_198
mkdir -p $logdir
for k in `grep "^chr" genomeData/pcc6803_AT_p0_w198_s10_SNR_amplitudes.csv`; do
    if [[ "$k" != "chr" && $k != "coor" ]]; then
        nam=X${k}_chromatinAverages
        qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR $TATADIR/perls/chromatinAverages.sh -chr chromosomes/pcc6803.csv -g genomeData/pcc6803_AT_p0_w198_s10_SNR_amplitudes.csv -f pcc6803.features.csv -up -1500 -do 1500 -w 2 -o $outdir -name "features_s10_snr_198.-1500..1500.ATG" -v $k
    fi
done 
## gene data index file: sorted by period!
for dat in `ls dinucPeriods/geneData/features_s10_snr_198.-1500..1500.ATG.*dat`; do k=`echo $dat | sed 's|dinucPeriods/geneData/features_s10_snr_198.-1500..1500.ATG.||g;s/\.dat//g'`; echo -e "$k\t$dat"; done | grep -v coor | sort -n > dinucPeriods/geneData_files_s10_snr_198.dat

## cluster profiles: use parallel wrapper
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_s10_snr_198
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_s10_snr_198.dat`
bns=10
## TODO: use L here!?
for i in {1..96}; do 
    nam=X${i}_s10_snr_w198geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc CL --gdf dinucPeriods/geneData_files_s10_snr_198.dat --out $outdir  --typ gene --bns $bns --tst W --sde greater --dat $i --pre s10_snr_w198_
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val pvalue --trf lg2 --dat 20:96 --verb --pre s10_esr_w198_  --cut 3.3:13 
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val mean --dat 20:96 --verb  --pre s10_esr_w198_ --col --cut 0.3:0.22 #--cut 0:2

## BY CHROMOSOME
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_s10_snr_198_chr
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_s10_snr_198.dat`
bns=10
for i in {1..96}; do 
    nam=X${i}_s10_snr_w198geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc chr --gdf dinucPeriods/geneData_files_s10_snr_198.dat --out $outdir  --typ gene --bns $bns --tst W --sde greater --dat $i --pre chr_s10_snr_w198_
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val pvalue --trf lg2 --dat 20:96 --verb --pre  chr_s10_snr_w198_ 
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val mean --dat 20:96 --verb  --pre chr_s10_snr_w198_ --col --trm 

## BY FEATURES
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_s10_snr_198_ftr
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_s10_snr_198.dat`
bns=10
for i in {1..96}; do 
    nam=X${i}_s10_snr_w198geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc type --gdf dinucPeriods/geneData_files_s10_snr_198.dat --out $outdir   --bns $bns --tst W --sde greater --dat $i --pre ftr_s10_snr_w198_
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val pvalue --trf lg2 --dat 20:96 --verb --pre  ftr_s10_snr_w198_ --trm
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_s10_snr_198.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --val mean --dat 20:96 --verb  --pre ftr_s10_snr_w198_ --col --trm 

## NON-OVERLAPPING WINDOWS
## genome profile
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=400 --stp=400 --prm=0 --nrm="esr" --circular  --verb  > genomeData/dinuc_snr_s400_w400.log


## TRINUC. PARAMS
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData3 --win=198 --stp=10 --prm=0 --nrm=snr --circular --verb  --ntf=$GENBRO/parameters/trinucleotides.csv:Bnd
## 1000 permutations
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=198 --stp=10 --prm=1000 --nrm=snr --circular --verb  --ntf=$GENBRO/parameters/trinucleotides.csv:Bnd

## Bnd at ATG
outdir=dinucPeriods/geneData/
logdir=$outdir/log_bnd
mkdir -p $logdir
for k in `grep "^chr" genomeData/pcc6803_Bnd_p0_w198_s10_SNR_amplitudes.csv`; do
    if [[ "$k" != "chr" && $k != "coor" ]]; then
        nam=X${k}_chromatinAverages
        qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR $TATADIR/perls/chromatinAverages.sh -chr chromosomes/pcc6803.csv -g genomeData/pcc6803_Bnd_p0_w198_s10_SNR_amplitudes.csv -f pcc6803.features.csv -up -1500 -do 1500 -w 2 -o $outdir -name "features_bnd_snr.-1500..1500.ATG" -v $k # -tss -tsnm TSS 
    fi
done 
## gene data index file: sorted by period!
for dat in `ls dinucPeriods/geneData/features_bnd_snr.-1500..1500.ATG.*dat`; do k=`echo $dat | sed 's|dinucPeriods/geneData/features_bnd_snr.-1500..1500.ATG.||g;s/\.dat//g'`; echo -e "$k\t$dat"; done | grep -v coor | sort -n > dinucPeriods/geneData_files_bnd_snr.dat
## cluster profiles
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_bnd_snr
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_bnd_snr.dat`
bns=10
## TODO: use L here!?
for i in {1..96}; do 
    nam=X${i}_bnd_snr_geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc CL --typ gene --gdf dinucPeriods/geneData_files_bnd_snr.dat --out $outdir   --bns $bns --tst W --sde greater --dat $i --pre bnd_snr
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_bnd_snr.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="p-value" --val pvalue --trf lg2 --dat 20:96 --verb --pre bnd_snr_  --trm --xlm="-500:100"
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_bnd_snr.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="amplitude" --val mean --dat 20:96 --verb  --pre bnd_snr_ --col  --trm --xlm="-500:100"


## Bnd at TSS for features
outdir=dinucPeriods/geneData/
logdir=$outdir/log_bnd
mkdir -p $logdir
for k in `grep "^chr" genomeData/pcc6803_Bnd_p0_w198_s10_SNR_amplitudes.csv`; do
    if [[ "$k" != "chr" && $k != "coor" ]]; then
        nam=X${k}_chromatinAverages
        qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR $TATADIR/perls/chromatinAverages.sh -chr chromosomes/pcc6803.csv -g genomeData/pcc6803_Bnd_p0_w198_s10_SNR_amplitudes.csv -f pcc6803.features.csv -up -1500 -do 1500 -w 2 -o $outdir -name "features_bnd_snr.-1500..1500.TSS" -v $k  -tss -tsnm TSS 
    fi
done 
## gene data index file: sorted by period!
for dat in `ls dinucPeriods/geneData/features_bnd_snr.-1500..1500.TSS.*dat`; do k=`echo $dat | sed 's|dinucPeriods/geneData/features_bnd_snr.-1500..1500.TSS.||g;s/\.dat//g'`; echo -e "$k\t$dat"; done | grep -v coor | sort -n > dinucPeriods/geneData_files_bnd_snr_tss.dat
## cluster profiles
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_bnd_snr_tss
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_bnd_snr_tss.dat`
bns=10
## TODO: use L here!?
for i in {1..96}; do 
    nam=X${i}_bnd_snr_geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc type --gdf dinucPeriods/geneData_files_bnd_snr_tss.dat --out $outdir   --bns $bns --tst W --mst median --sde greater --dat $i --pre bnd_snr_tss_
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_bnd_snr_tss.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="p-value" --val pvalue --trf lg2 --dat 20:96 --verb --pre bnd_snr_tss_  --xlm="-500:200"
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_bnd_snr_tss.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="amplitude" --val mean --dat 20:96 --verb  --pre bnd_snr_tss_ --col --xlm="-500:200"
# SNR medians
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_bnd_snr_tss.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="amplitude" --val median --dat 20:96 --verb  --pre bnd_snr_tss_ --col --xlm="-500:200"


## HIGH-RES FOR PROMOTOR ZOOM
## genome profile
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=110 --stp=1 --prm=0 --nrm="snr" --circular  --verb  > genomeData/dinuc_snr_s1_w110.log

outdir=dinucPeriods/geneData/
logdir=$outdir/log_w110_s1_SNR
mkdir -p $logdir
for k in `grep "^chr" genomeData/pcc6803_AT_p0_w110_s1_SNR_amplitudes.csv`; do
    if [[ "$k" != "chr" && $k != "coor" ]]; then
        nam=X${k}_chromatinAverages
        qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR $TATADIR/perls/chromatinAverages.sh -chr chromosomes/pcc6803.csv -g genomeData/pcc6803_AT_p0_w110_s1_SNR_amplitudes.csv -f pcc6803.features.csv -up -1500 -do 1500 -w 2 -o $outdir -name "features_w110_s1_SNR.-1500..1500.ATG" -v $k
    fi
done 
## gene data index file: sorted by period!
for dat in `ls dinucPeriods/geneData/features_w110_s1_SNR.-1500..1500.ATG.*dat`; do k=`echo $dat | sed 's|dinucPeriods/geneData/features_w110_s1_SNR.-1500..1500.ATG.||g;s/\.dat//g'`; echo -e "$k\t$dat"; done | grep -v coor | sort -n > dinucPeriods/geneData_files_w110_s1_SNR.dat
## cluster profiles: use parallel wrapper
outdir=dinucPeriods/clusterProfiles
logdir=$outdir/log_w110_s1_SNR
#\rm $logdir/*
mkdir -p $logdir
L=`wc -l dinucPeriods/geneData_files_w110_s1_SNR.dat`
bns=1
## TODO: use L here!?
for i in {1..54}; do 
    nam=X${i}_geneProfile
    qsub -r y -N $nam -e $logdir -o $logdir -v TATADIR=$TATADIR -v R_LIBS=$R_LIBS $TATADIR/r/geneProfiler.sh --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc CL --typ gene --gdf dinucPeriods/geneData_files_w110_s1_SNR.dat --out $outdir   --bns $bns --tst W --sde greater --dat $i --pre w110_s1_SNR_
done
##cluster profile plots
figdir=$outdir/figures
mkdir -p $figdir
## SNR p-values
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_w110_s1_SNR.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="p-value" --val pvalue --trf lg2 --verb --pre w110_s1_SNR_ --xlm="-500:100"
# SNR means
$TATADIR/r/geneProfilePlotter.R --gdf dinucPeriods/geneData_files_w110_s1_SNR.dat --pfd $outdir --out $figdir --ylb period --xlb "relative position" --zlb="amplitude" --val mean --verb  --pre w110_s1_SNR_ --col --xlm="-500:100"



## TODO:
## store as complex numbers to allow phase retrieval?

# domains: WW, large windows - extend, consistent phase?
# promoters: Bnd, small windows - use phase to extract aligned sequences

## coding region
geneAverages.R --gff ~/work/pcc6803/clustering/20130326/10_10/clustering.csv --clc CL --typ gene --gdf genomeData/pcc6803_AT_p0_w110_s1_SNR_amplitudes.csv --mst median --tst W --sde greater --dtc 11 --out dinucPeriods/clusterAverages  --pre test

## FINAL HIGH-RES AND WITH P-VALIUES

## genome profiles
## TODO: parallelize!
$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=396 --stp=10 --prm=1000 --circular --verb  > dinucPeriods/dinuc.log


$GENBRO/src/calculateDiNuclProfile.R --id=pcc6803 --din=AT --fdir=$CYANODAT/chromosomes --fend=.fasta --out=$CYANODAT/genomeData --win=198 --phs --phf

# End of file
