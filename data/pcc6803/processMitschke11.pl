#!/usr/bin/perl
# -*-Perl-*-
# Last changed Time-stamp: <02-Apr-2013 22:20:03 raim>
# $Id: processMitschke11.pl,v 1.2 2017/09/20 15:19:58 raim Exp $

## read gene and 5'UTR annotation from Mitschke et al. 2011
## gbk file and caculate relative TSS position

use strict;
use Bio::SeqIO;

my $seqio = Bio::SeqIO->new(-file => "processedData/mitschke11_corrected.gbk");
my $seq = $seqio->next_seq;

my %gene;
my %ends;
my %types;
my %utr;
my %utr3;
my %strand;
for my $feat ($seq->get_SeqFeatures) {
    my $type = $feat->primary_tag;
    if ( $type =~ /ncRNA|tRNA|rRNA|gene|5'UTR|3'UTR/ ) {
	#print "primary tag: ", $feat->primary_tag, "\n";
	#print "location: ", $feat->location->start()."-".$feat->location->end()." on ".$feat->location->strand()."\n";
	my $start = $feat->location->start();
	my $end = $feat->location->end();
	my $str = $feat->location->strand();
	die("ERROR") if $start > $end; ## make sure that coors are ordered

	## swap for rev strand
	($start,$end) = ($end,$start) if $str == -1;

	for my $tag ($feat->get_all_tags) {
	    if ( $tag eq "locus_tag" ) {
		my @id = $feat->get_tag_values($tag);
		#print "$type\t$id[0]\t$start\n";
		if ( $type =~ /ncRNA|tRNA|rRNA|gene/ ) {
		    $types{$id[0]} = $type;
		    $gene{$id[0]} = $start;
		    $ends{$id[0]} = $end;
		    $strand{$id[0]} = $str;
		}
		if ( $type eq "5'UTR" ) 
		{
		    ## concatenate multiple
		    if ( exists($utr{$id[0]}) ) {
			$utr{$id[0]} .= ";".$start;
		    } else {
			$utr{$id[0]} = $start;
		    }
		}
		if ( $type eq "3'UTR" ) 
		{
		    ## concatenate multiple
		    if ( exists($utr3{$id[0]}) ) {
			printf STDERR "3'UTR for $id[0] already defined\t";
			$utr3{$id[0]} .= ";".$end;
			printf STDERR "$utr3{$id[0]}\n";
		    } else {
			$utr3{$id[0]} = $end;
		    }
		}
	    }       
	}
    }
}
## calculate relative positions and print
foreach my $name ( keys %utr ) {
    if ( !defined($gene{$name}) ) {
	next;
    }
    my @alltss = split(/;/, $utr{$name}); # handles multiple TSS
    for my $i (0 .. $#alltss)
    {
	$alltss[$i] = -$gene{$name}+$alltss[$i] if ( $strand{$name} == 1 );
	$alltss[$i] = +$gene{$name}-$alltss[$i] if ( $strand{$name} == -1 );
    }
    @alltss = sort { $b <=> $a } @alltss;
    printf STDOUT "$name\t".join(";",@alltss)."\t5'UTR\n"; #$types{$name}
}
foreach my $name ( keys %utr3 ) {
    if ( !defined($gene{$name}) ) {
	next;
    }
    my @alltes = split(/;/, $utr3{$name}); # would handle multiple TES
    for my $i (0 .. $#alltes)
    {
	$alltes[$i] = -$ends{$name}+$alltes[$i] if ( $strand{$name} == 1 );
	$alltes[$i] = +$ends{$name}-$alltes[$i] if ( $strand{$name} == -1 );
    }
    @alltes = sort { $b <=> $a } @alltes;
    printf STDOUT "$name\t".join(";",@alltes)."\t3'UTR\n";
}

__END__
