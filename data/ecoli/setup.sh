
COLIPATH=/data/ecoli
#COLIPATH=/scr/doofsan/raim/data/ecoli
mkdir -p $COLIPATH/originalData
cd $COLIPATH/originalData

## download genbank annotation for W3110: NC_007779
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/000/010/245/GCA_000010245.1_ASM1024v1/GCA_000010245.1_ASM1024v1_genomic.gff.gz
gunzip $COLIPATH/originalData/GCA_000010245.1_ASM1024v1_genomic.gff.gz

# download gene expression collection
wget http://m3d.mssm.edu/norm/E_coli_v4_Build_6.tar.gz
tar zxvf E_coli_v4_Build_6.tar.gz
wget http://www.colombos.net/cws_data/compendium_data/ecoli_compendium_data.zip
unzip ecoli_compendium_data.zip

## TODO: Caglar et al. 2017
## RNAseq, proteome, C13 flux from 155 different growth
## conditions: https://www.ncbi.nlm.nih.gov/pubmed/28417974

## E.coli Gene Ontology
## release: 2019-07-01
wget http://current.geneontology.org/annotations/ecocyc.gaf.gz

## Download Uniprot Gene Synonym Mapping
wget https://www.uniprot.org/docs/ecoli.txt

## Download EcoCyc gene list
## TODO: 


### SET-UP and analyzse

## genome features - ID mapping and genomeBrowser track
## generates features_ncbi.csv
R --vanilla < $GENBRO/data/ecoli/annotation.R &> $COLIPATH/annotation.log

## meta-transcriptome analysis
R --vanilla < $GENBRO/data/ecoli/expression.R &> $COLIPATH/expression.log

## GO analysis of meta-transcriptome clusters
R --vanilla < $GENBRO/data/ecoli/goanalysis.R &> $COLIPATH/goanalysis.log


