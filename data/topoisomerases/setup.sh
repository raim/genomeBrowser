
## reciprocal blasts of cyanobacterial topoisomerases
## on a collection of complete genomes from NCBI

TOPOPATH=/data/topoisomerases
TAXPATH=/data/taxonomy/

## 1) download genomes
## 2) set-up blast DB

## 1-2 done by Halime Cerit as bachelor thesis,
## using a Iupyter Notebook by Nicolas Schmelling

## 3) map taxonomy information

## -> grep all Taxids from all genomes as file
## $TOPOPATH/processedData/all_taxonomy_IDs.dat
## -> The following R script using R package ncbitax
## to collect taxonomic info for all genomes as file
## $TOPOPATH/processedData/all_taxonomies.csv
R --vanilla < $GENBRO/data/topoisomerases/getReferenceTaxonomies.R  &> $TOPOPATH/processedData/all_taxonomies.log

## map all taxonomies 16S rRNA tree
## map all taxonomies to Shih et al. 2013 cyanobacteria tree

## download supp. info from Lehmann et al. 2014
## requires manual step
## NOTE: SupportingDataFile1.csv contains structural code and other info
#  wget https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/nar/42/14/10.1093_nar_gku641/2/gku641_Supplementary_Data.zip
## mv ~/Downloads/gku641_Supplementary_Data.zip $TOPOPATH/originalData/
## cd  $TOPOPATH/originalData/
## unzip gku641_Supplementary_Data.zip
## cd -

## 4) reciprocal blast

## done by Halime Cerit as bachelor thesis,
## using a Iupyter Notebook by Nicolas Schmelling

## -> all hits in files $TOPOPATH/processedData/*_annotated.csv

## 5) global analysis of hit distribution
##    & filter hits for cyanobacteria

## read in all hits and taxonomy mapping above,
## consider only one genome for each species
## write out fasta files for all where phylum==Cyanobacteria
## and write out taxonomy IDs for ALL cyanos in set,
## and plot global hit statistics
R --vanilla < $GENBRO/data/topoisomerases/filterSpecies.R &> $TOPOPATH/processedData/filterSpecies.log

## 6) calculate consensus trees for gyrA,gyrB,HU,topA
##    & plot occurence of multiple copies and gyrA2 on this tree

## 7) gyrA vs. gyrA2 trees

cd $TOPOPATH/processedData/

## gyrA + gyrA2 - cyanos plus 3 examples from all bacteria
cat gyrA_Bacteria.fasta gyrA2_Bacteria.fasta > gyrAA2_Bacteria.fasta
ginsi --thread 8 gyrAA2_Bacteria.fasta > gyrAA2_Bacteria_ginsi.fasta
~/scripts/trimal -in gyrAA2_Bacteria_ginsi.fasta  -gappyout -out gyrAA2_Bacteria_ginsi_gappyout.fasta

## bootstrap analysis and plot
R --vanilla --args ALISET tight < $GENBRO/data/topoisomerases/bootstrap.R &> bootstrap_tight.log
R --vanilla --args ALISET tight < $GENBRO/data/topoisomerases/bootstrap_plot.R


## gyrA + gyrA2 + 30 species from main phyla
cat gyrA_MainBacteria.fasta gyrA2_MainBacteria.fasta > gyrAA2_MainBacteria.fasta
ginsi --thread 8 gyrAA2_MainBacteria.fasta > gyrAA2_MainBacteria_ginsi.fasta
~/scripts/trimal -in gyrAA2_MainBacteria_ginsi.fasta  -gappyout -out gyrAA2_MainBacteria_ginsi_gappyout.fasta

## bootstrap analysis and plot
R --vanilla --args ALISET full < $GENBRO/data/topoisomerases/bootstrap.R
R --vanilla --args ALISET full < $GENBRO/data/topoisomerases/bootstrap_plot.R


## gyrB
mafft --auto gyrB_MainBacteria.fasta > gyrB_MainBacteria_mafft.fasta

# gyrB
linsi gyrB_Cyanobacteria.fasta > gyrB_Cyanoacteria_mafft.fasta


### .... obtained clean alignment by Halime, running bootstrapping
## with R package phangorn
R --vanilla < $GENBRO/data/topoisomerases/bootstrap.R &> bootstrap.log

## 7) MAP TO Shih et al. tree
## use ncbitax/scripts/cmpTaxa.R to find closest relatives in Shih et al.
## tree in genomeBrowser/data/taxonomy
datpath="/data/taxonomy/ncbi"
outpath="/data/topoisomerases/processedData"
query="/data/topoisomerases/processedData/cyanoids.dat"

## map to Shih et al. 2013 tree species
target="/data/taxonomy/cyanobacteria/shih13_metadata_annotated.tsv"
~/programs/ncbitax/scripts/cmpTaxa.R -sf $query -txd $datpath  -tf $target -tn tree.name -ti Organism -out $outpath/species2shih13 -xf ${outpath}/species2shih13_tree.RData 

## plot Shih et al. 2013 tree with our species names
R --vanilla < $GENBRO/data/topoisomerases/plotShih13.R &> $TOPOPATH/processedData/plotShih13.log

## map to Lehmann et al. species
target="/data/topoisomerases/originalData/SupportingDataFile1.csv"
~/programs/ncbitax/scripts/cmpTaxa.R -sf $query -txd $datpath  -tf $target  -tn organism -out $outpath/species2lehmann14 -xf ${outpath}/species2lehmann14_tree.RData 

## plot Lehmann et al. 2014 info
R --vanilla < $GENBRO/data/topoisomerases/plotLehmann14.R &> $TOPOPATH/processedData/plotLehmann14.log

## TODO: map to 16S rRNA tree species
target="/data/taxonomy/silva/LTPs132_SSU.dat"
~/programs/ncbitax/scripts/cmpTaxa.R -sf $query -txd $datpath  -tf $target -tn acc -tidcol id -out $outpath/species2silva -xf ${outpath}/species2silva_tree.RData 
