
## PLOT THE RESULTS OF BOOTSTRAPPING ANALYSIS in bootstrap.R

##setwd("/home/nic/BLAST/halime/top_gyr/all.csv/")
setwd("/data/topoisomerases/processedData/")

library(ape)
library(phangorn)
library(seqinr)
library(segmenTools)

## parse TEST or real data from bootstrap.R
TEST <- FALSE #  TRUE  #
fig.type <- "pdf" #"png" # 


##  SELECT ALIGNMENT:
## full: pre-filter by length, all cyano + all gyrA2 + 30 species
##       from main phyla
## tight: all cyano + 3 species from all phyla
ALISET <- "full" # "tight" #

# parse alginment set as argument to R --vanilla --args ALI:tight
args <- commandArgs(TRUE)
if ( length(args)>0 ) {
    arg <- as.character(args[1])
    args <- strsplit(arg,"") ## set M with M0, M5, etc.
    if ( args[[1]][1]=="ALI:" ) {
        ALISET <- as.numeric(args[[1]][2])
    } 
}
cat(paste("DOING ALIGNMENT SET:", ALISET, "\n"))

## INPUT ALIGNMENT NAME
if ( ALISET=="full" ) {
    ALINAME <- "updated.gyrAA2_MainBacteria"
    ALITYPE <- "ginsi_gappyout"
} else if ( ALISET=="tight" ) {
    ALINAME <- "gyrAA2_Bacteria"
    ALITYPE <- "ginsi_gappyout"
}

out.file <- ifelse(TEST,"TEST",ALINAME)

## load bootstrapping data
rda.file <- paste0(out.file,"_bootstrap.RData")
load(rda.file)

out.file <- ifelse(TEST,"TEST",ALINAME)

## plot settings and result file names

## load global taxonomy
tax.file <- file.path("all_taxonomies.csv")
tax <- read.csv(tax.file, stringsAsFactors=FALSE)

## TREE COLORS
## TODO: select different colorings for two trees ALINAME
sp.cols <- c(Actinobacteria="cyan",
             Bacteroidetes="yellow4",
             Proteobacteria="red",
             Tenericutes="brown",
             Firmicutes="blue4",
             Spirochaetes="orange",
             Chlamydiae="violet",
             ##Chloroflexi="green", # not present in current set :(
             Verrucomicrobia="green",
             Kiritimatiellaeota="gold",
             Cyanobacteria="green4")

## COLOR TIPS
## TODO: add colors for additional phyla which are now present
## TODO: indicate KNOWN topo IV (see filterSpecies.R)
## TODO: indicate gyrA.2 hits in cyanos, which should now be in gyrA2

## base color black
tip.col <- rep("black",length(gaa.NJ$tip.label))

## map tree names to ID MAP
trnames <- gaa.NJ$tip.label
idx <- match(trnames, idmap[,"tree"])
trtree <- idmap[idx,"tree"]
trspecies <- idmap[idx,"species"]
trprotein <- idmap[idx,"protein"]


#trspecies[is.na(trspecies)] <- trnames[is.na(trspecies)] 

## count phyla
tidx <- match(idmap[idx,"species"], tax$species)
table(tax$phylum[tidx])

## color by phylum
for ( i in 1:length(trspecies) ) {
    ## get tree species in taxonomy
    ## NOTE: single ' was replaced in one cyano species already in filterSpecies.R
    idx <- which(sub("'","",tax$species)==trspecies[i])[1]
    if ( length(idx)==1 )
        if ( !is.na(idx) )
            tip.col[i] <- sp.cols[tax$phylum[idx]]
        else cat(paste("is.na idx", i, "\n")) ## SHOULDN'T HAPPEN
    else cat(paste("length", i, "\n"))
    
}

if ( ALISET=="tight" ) {

    
    ## color to indicate match to our set
    tip.col <- rep("black",length(gaa.NJ$tip.label))
    tip.col[gaa.NJ$tip.label=="Bacillus_subtilis_gyrA.2"] <- "red"
    tip.col[gaa.NJ$tip.label=="Escherichia_coli_gyrA.2"] <- "red"
    tip.col[gaa.NJ$tip.label=="Bacillus_subtilis_gyrA"] <- "blue4"
    tip.col[gaa.NJ$tip.label=="Escherichia_coli_gyrA"] <- "blue4"
    tip.col[gaa.NJ$tip.label=="Streptomyces_coelicolor_gyrA.2"] <- "red"
    tip.col[gaa.NJ$tip.label=="Chlamydia_trachomatis_gyrA.2"] <- "red"
    tip.col[gaa.NJ$tip.label=="Streptomyces_coelicolor_gyrA"] <- "blue4"
    tip.col[gaa.NJ$tip.label=="Chlamydia_trachomatis_gyrA"] <- "blue4"
    

    tip.col[grep("gyrA2",gaa.NJ$tip.label)] <- "green4"

}

## replace tip names
#gaa.NJ$tip.label <- gsub(" ","_",paste(trspecies,trprotein))

## PLOT TREES with colors
## NOTE: p sets the minimal plotted bootstrap value
## NOTE: plotBS returns tree with bootstrap support as node labels
plotdev(file.path("figures",paste0(out.file, "_bootstrap_annotated")),
        type=fig.type, res=300, width=10, height=25)
treeBS <- plotBS(midpoint(gaa.NJ), bs, p = 0, type="p", no.margin=TRUE, cex=.5,
                 tip.color=tip.col)
if ( ALISET=="full" )
    legend("topright", legend=names(sp.cols), col=sp.cols, lty=1, lwd=2, cex=1.2)
if ( ALISET=="tight" )
    legend("topright", legend=c("known gyrA", "known topoIV", "gyrA2 hits"),
           col=c("blue4","red","green4"), lty=1, lwd=2, cex=1.2)
dev.off()


write.tree(treeBS, file = paste0(out.file,"_bootstrapsupport.tre"))


fanW <- fanH <- ifelse(ALISET=="tight", 10, 15)
plotdev(file.path("figures",paste0(out.file, "_bootstrap_annotated_fan")),
        type=fig.type, width=fanW, height=fanH)
plot(treeBS, show.node.label=FALSE, type="fan", no.margin=TRUE, cex=.5,
     tip.color=tip.col)
nodelabels(pch=16,col="darkgray", cex=.9) # all nodes
nodelabels(pch=16,col=ifelse(treeBS$node.label>=80,"black",NA), cex=1.1) # supported
nodelabels(pch=16,col=ifelse(treeBS$node.label<20,"red",NA), cex=1.1) # not supported
if ( ALISET=="full" )
    legend("topright", legend=names(sp.cols), col=sp.cols, lty=1, lwd=2, cex=1.2)
if ( ALISET=="tight" )
    legend("topright", legend=c("known gyrA", "known topoIV", "gyrA2 hits"),
           col=c("blue4","red","green4"), lty=1, lwd=2, cex=1.2)
dev.off()

## NOTE: 
plotdev(file.path("figures",paste0(out.file, "_consensus_annotated")),
        type=fig.type, width=W, height=H)
plot(cnet, "2D", show.edge.label=TRUE)
dev.off()


