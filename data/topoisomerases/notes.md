
# parC vs. gyrA

* https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6243147/

quinolone resistance in P. aeruginosa is mutations occurring in gyrA
and parC genes encoding the A subunits of type II and IV
topoisomerases, respectively, in quinolone resistance determining
regions (QRDR) of the bacterial chromosome.

Nine isolates with MIC≥8 μg/ml had a mutation in gyrA
(Thr83→Ile). Amongst these, seven isolates also had a mutation in parC
(Ser87→ Leu or Trp) indicating that the prevalent mutation in gyrA is
Thr83Ile and Ser87Leu/Trp in parC. No single parC mutation was
observed.

* https://www.sciencedirect.com/science/article/pii/S2210909911000890

fluoroquinolone-resistant mechanism of 56 clinical cases of A baumannii

mutations in gyrA (S83L), parC (S80L, S80W and S84K) and gyrB (containing the novel mutations E679D, D644Y and A677V).


* https://academic.oup.com/jac/article/66/3/476/730877

Molecular characterization and phylogenetic analysis of quinolone
resistance-determining regions (QRDRs) of gyrA, gyrB, parC and parE
gene loci in viridans group streptococci isolated from adult patients
with cystic fibrosis

* https://aac.asm.org/content/44/4/840

Horizontal Transfer of parC and gyrA in Fluoroquinolone-Resistant Clinical Isolates ofStreptococcus pneumoniae

# ParC Structure

* https://www.sciencedirect.com/science/article/pii/S0022283613004208

Journal home page for Journal of Molecular Biology Distinct Regions of
the Escherichia coli ParC C-Terminal Domain Are Required for Substrate
Discrimination by Topoisomerase IV

* https://www.sciencedirect.com/science/article/pii/S0022283613004208

The functional differences between gyrase and topo IV have been
ascribed (at least in part) to variations in the C-terminal domains
(CTDs) of their respective GyrA and ParC subunits. The GyrA and ParC
CTD share a common β-pinwheel fold that is formed by a series of
repeating Greek key motifs or “blades” (Fig. 1a and b) [18], [19],
[20]. The GyrA CTD is a DNA binding and wrapping domain composed of
six blades [20], [21], [22], along with a conserved motif known as the
“GyrA-box”, which latches the first blade of the domain to the last
blade [23], [24]. Loss of either the GyrA CTD or the GyrA box
abolishes the ability of gyrase to supercoil DNA but does not abrogate
strand passage [25], [26], [27]. ParC CTDs can bind but not wrap DNA
substrates and have a variable number of blades (from three to eight,
proteobacterial versions possess five) [18], [20], [28]. Although the
ParC CTD does not contain a canonical GyrA box, degenerate remnants of
the motif are found in each of its blades (Fig. 1a and Supplementary
Fig. 1) [28].

Loss of the Escherichia coli ParC CTD does not completely abolish
relaxation and decatenation function by topo IV but does generally
disrupt the ability of the enzyme to discriminate between
topologically distinct substrates [20].


...

 To more finely dissect the role of this region, we set out to
 identify prospective DNA binding residues using multiple-sequence
 alignments of ParC CTD homologs that contain only five blades. We
 focused first on residues that are both highly conserved and
 positively charged (Supplementary Fig. 1) and mapped these residues
 onto the E. coli ParC CTD crystal structure to find surface-exposed
 amino acids that did not appear to be required for structural
 stability. This analysis revealed that many of the most highly
 conserved positions reside in degenerate GyrA-box motifs that reside
 on the extended loops that latch adjoining blades together (Fig. 1d
 and Supplementary Fig. 1). We also identified a positively charged
 patch on the fifth blade that contains two arginines previously
 suggested to interact with the E. coli condensin homolog, MukB
 (Arg705 and Arg729) [30], [31].

Given the criteria described above, we next mutated representative
basic residues from each of the five blades to aspartate, using charge
substitutions to more actively disfavor potential electrostatic
interactions that might occur with DNA. Full-length ParC mutants were
purified to homogeneity and then subjected to size-exclusion
chromatography to confirm that the introduced mutations did not cause
the proteins to aggregate (Supplementary Fig. 2).

CONSERVED, BASIC AMINO ACIDS IN THE PARC CTD ARE IMPORTANT FOR
POSITIVE-SUPERCOIL RELAXATION

We initially tested the ability of our ParC mutants to relax topo IV's
favored substrate, positively supercoiled DNA.

We discovered that mutations within the CTD of ParC exhibited a broad
range of activity profiles, ranging from essentially WT behavior to
severe defects in the efficiency and/or processivity of supercoil
relaxation [a detailed explanation of topoisomerase processivity is
provided in Supplementary Fig. 3; relative defects in processivity are
roughly quantified by comparing the amount of mutant topo IV required
to produce a final topoisomer distribution similar to that seen for
the WT enzyme (boxed in blue, Fig. 2)].

For example, mutation of positions within blade 5
(Arg705Asp/Arg729Asp) resulted in no change in activity or
processivity, whereas mutation of blade 1 (Arg564Asp) significantly
impaired both (~ 10-fold decrease). Other mutations primarily affected
enzyme processivity; substitutions to blades 2–4 (Arg616Asp,
Lys665Asp, Arg723Asp) led to particularly distributive behavior.

To further characterize the effects of ParC CTD mutations on
positively supercoiled DNA, we looked at relaxation using a fixed
molar excess of DNA compared to topo IV (~ 50-fold) as a function of
time. This regime permitted us to observe differences in rate of
relaxation in addition to changes in processivity. Interestingly, all
mutations tested actually modestly enhanced the overall rate of
positive-supercoil relaxation by topo IV compared to the WT protein,
with the exception of Arg564Asp in blade 1, which was severely
compromised (Fig. 3).

As with the enzyme titration assays, however, defects in processivity
were again observed for mutations in blades 2 and 3, and, to a lesser
extent, blade 4. Together, these data demonstrate that positively
charged amino acids on the CTD of ParC not only influence the activity
of topo IV on positively supercoiled DNA but also unexpectedly show
that different blades make different contributions to activity.

BASIC RESIDUES ON THE PARC CTD DIFFERENTIALLY AFFECT DECATENATION AND
NEGATIVE-SUPERCOIL RELAXATION

Unexpectedly, the blade 5 mutation appeared to increase the activity
of topo IV on kDNA nearly 10-fold (as indicated by a need for a lower
amount of enzyme to fully unlink the catenated network).

...

BLADE 1 OF PARC CTD IS REQUIRED FOR G-SEGMENT BENDING


